<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\OnlineUserRegistration[]|\Cake\Collection\CollectionInterface $onlineUserRegistration
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Online User Registration'), ['action' => 'add']) ?></li>
    </ul>
</nav>
<div class="onlineUserRegistration index large-9 medium-8 columns content">
    <h3><?= __('Online User Registration') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('first__last_name') ?></th>
                <th scope="col"><?= $this->Paginator->sort('email') ?></th>
                <th scope="col"><?= $this->Paginator->sort('user_type') ?></th>
                <th scope="col"><?= $this->Paginator->sort('can_see_account_details') ?></th>
                <th scope="col"><?= $this->Paginator->sort('can_see_placed_order') ?></th>
                <th scope="col"><?= $this->Paginator->sort('can_see_invoiced_order') ?></th>
                <th scope="col"><?= $this->Paginator->sort('can_see_credit_memos') ?></th>
                <th scope="col"><?= $this->Paginator->sort('can_see_prices') ?></th>
                <th scope="col"><?= $this->Paginator->sort('can_see_price_list') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($onlineUserRegistration as $onlineUserRegistration): ?>
            <tr>
                <td><?= $this->Number->format($onlineUserRegistration->id) ?></td>
                <td><?= h($onlineUserRegistration->first__last_name) ?></td>
                <td><?= h($onlineUserRegistration->email) ?></td>
                <td><?= $this->Number->format($onlineUserRegistration->user_type) ?></td>
                <td><?= $this->Number->format($onlineUserRegistration->can_see_account_details) ?></td>
                <td><?= $this->Number->format($onlineUserRegistration->can_see_placed_order) ?></td>
                <td><?= $this->Number->format($onlineUserRegistration->can_see_invoiced_order) ?></td>
                <td><?= $this->Number->format($onlineUserRegistration->can_see_credit_memos) ?></td>
                <td><?= $this->Number->format($onlineUserRegistration->can_see_prices) ?></td>
                <td><?= $this->Number->format($onlineUserRegistration->can_see_price_list) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $onlineUserRegistration->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $onlineUserRegistration->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $onlineUserRegistration->id], ['confirm' => __('Are you sure you want to delete # {0}?', $onlineUserRegistration->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div>
