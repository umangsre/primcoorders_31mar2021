<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Family $family
 */

?>
<div class="orders index large-9 medium-8 columns content card card card-default scrollspy">
    <div class="card-content">
        <div class="families view large-9 medium-8 columns content">
            <h3><?= h($family->name) ?></h3>
            <table class="vertical-table">
                <tr>
                    <th scope="row"><?= __('Family') ?></th>
                    <td><?= h($family->family) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Name') ?></th>
                    <td><?= h($family->name) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Product Code') ?></th>
                    <td><?= h($family->product_code) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Role Cut') ?></th>
                    <td><?= h($family->role_cut) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Usd Cad') ?></th>
                    <td><?= h($family->usd_cad) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Units') ?></th>
                    <td><?= h($family->units) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Mill') ?></th>
                    <td><?= h($family->mill) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('VendorID') ?></th>
                    <td><?= h($family->vendorID) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Id') ?></th>
                    <td><?= $this->Number->format($family->id) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Mill Cost') ?></th>
                    <td><?= $this->Number->format($family->mill_cost) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Conversion Rate') ?></th>
                    <td><?= $this->Number->format($family->conversion_rate) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Selling Price') ?></th>
                    <td><?= $this->Number->format($family->selling_price) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Brokerage Multiplier(Brokerage)') ?></th>
                    <td><?= $this->Number->format($family->brokerage_multiplier) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Brokerage(Brokerage Multiplier)') ?></th>
                    <td><?= $this->Number->format($family->brokerage) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Freight') ?></th>
                    <td><?= $this->Number->format($family->freight) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Ad Fund Percentage') ?></th>
                    <td><?= $this->Number->format($family->ad_fund_percentage) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Ad Fund') ?></th>
                    <td><?= $this->Number->format($family->ad_fund) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Landed Cost Per Unit') ?></th>
                    <td><?= $this->Number->format($family->landed_cost_per_unit) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Load Percentage') ?></th>
                    <td><?= $this->Number->format($family->load_percentage) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Margin Lic') ?></th>
                    <td><?= $this->Number->format($family->margin_lic) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Sell Price Level1') ?></th>
                    <td><?= $this->Number->format($family->sell_price_level1) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Rebate Level1') ?></th>
                    <td><?= $this->Number->format($family->rebate_level1) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Gross Margin Level1') ?></th>
                    <td><?= $this->Number->format($family->gross_margin_level1) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Sell Price Level2') ?></th>
                    <td><?= $this->Number->format($family->sell_price_level2) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Rebate Level2') ?></th>
                    <td><?= $this->Number->format($family->rebate_level2) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Gross Margin Level2') ?></th>
                    <td><?= $this->Number->format($family->gross_margin_level2) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Sell Price Level3') ?></th>
                    <td><?= $this->Number->format($family->sell_price_level3) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Rebate Level3') ?></th>
                    <td><?= $this->Number->format($family->rebate_level3) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Gross Margin Level3') ?></th>
                    <td><?= $this->Number->format($family->gross_margin_level3) ?></td>
                </tr>
                 <tr>
                        <th scope="row"><?= __('Sqft Ctn') ?></th>
                        <td><?= $this->Number->format($family->sqft_ctn) ?></td>
                    </tr>
                    <tr>
                        <th scope="row"><?= __('Wgt Lbs Ctn') ?></th>
                        <td><?= $this->Number->format($family->wgt_lbs_ctn) ?></td>
                    </tr>
                    <tr>
                        <th scope="row"><?= __('Ctn Pallet') ?></th>
                        <td><?= $this->Number->format($family->ctn_pallet) ?></td>
                    </tr>
                    <tr>
                        <th scope="row"><?= __('Sqft Pallet') ?></th>
                        <td><?= $this->Number->format($family->sqft_pallet) ?></td>
                    </tr>
                    <tr>
                        <th scope="row"><?= __('Pallet Container 20') ?></th>
                        <td><?= $this->Number->format($family->pallet_container_20) ?></td>
                    </tr>
                    <tr>
                        <th scope="row"><?= __('Sqft Container 20') ?></th>
                        <td><?= $this->Number->format($family->sqft_container_20) ?></td>
                    </tr>
                    <tr>
                        <th scope="row"><?= __('Sqft Roll') ?></th>
                        <td><?= $this->Number->format($family->sqft_roll) ?></td>
                    </tr>
                    <tr>
                        <th scope="row"><?= __('Sqyd Roll') ?></th>
                        <td><?= $this->Number->format($family->sqyd_roll) ?></td>
                    </tr>
                <tr>
                    <th scope="row"><?= __('Created') ?></th>
                    <td><?= h($family->created) ?></td>
                </tr>
            </table>
        </div>
    </div>
</div>
