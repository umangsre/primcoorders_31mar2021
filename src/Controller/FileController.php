<?php

namespace App\Controller;

use App\Controller\AppController;
use RecursiveDirectoryIterator;
use RecursiveIteratorIterator;
use ZipArchive;


class FileController extends AppController
{
    public function initialize()
    {
        parent::initialize();
    }

    public function addFile()
    {
        !$this->request->is('post') && exit;
        $file = $this->request->getData()['file'];
        count($file) <= 0 && exit;
        $fileName = $file['name'];
        empty($fileName) && exit;
        $tempName = $file['tmp_name'];
        $exploded = explode('.', $fileName);
        $uniqueName = preg_replace('/[^A-Za-z0-9\-]/', '', uniqid());
        $newFileName = $uniqueName . '.' . end($exploded);
        $folderName = WWW_ROOT . 'uploads';
        if (!file_exists($folderName)) {
            mkdir($folderName);
        }
        $folderName .= '/files/';
        if (!file_exists($folderName)) {
            mkdir($folderName);
        }
        $filePath = $folderName . $newFileName;
        $uploadFile = move_uploaded_file($tempName, $filePath);
        if ($uploadFile) {
            $this->response->body(json_encode(['status' => 'success', 'filePath' => '/uploads/files/' . $newFileName]));
        } else {
            $this->response->body(json_encode(['status' => 'error']));
        }
        return $this->response;
    }

    public function deleteFile()
    {
        !$this->request->is('post') && exit;
        $file = WWW_ROOT . $this->request->getData()['filePath'];
        !file_exists($file) && exit;
        $deleteFile = unlink($file);
        if ($deleteFile) {
            $this->response->body(json_encode(['status' => 'success']));
        } else {
            $this->response->body(json_encode(['status' => 'error']));
        }
        return $this->response;
    }

    public function createZip($folderName)
    {
        $rootPath = WWW_ROOT . $folderName;
        !file_exists($rootPath) && exit;
        $zipFolderName = WWW_ROOT . 'uploads/zips/';
        if (!file_exists($zipFolderName)) {
            mkdir($zipFolderName);
        }
        $uniqueName = preg_replace('/[^A-Za-z0-9\-]/', '', uniqid());
        $fileName = $zipFolderName . $uniqueName . '.zip';
        $zip = new ZipArchive();
        $zip->open($fileName, ZipArchive::CREATE | ZipArchive::OVERWRITE);
        $files = new RecursiveIteratorIterator(
            new RecursiveDirectoryIterator($rootPath),
            RecursiveIteratorIterator::LEAVES_ONLY
        );
        foreach ($files as $name => $file) {
            if (!$file->isDir()) {
                $filePath = $file->getRealPath();
                $relativePath = substr($filePath, strlen($rootPath));
                if (file_exists($filePath))
                    $zip->addFile($filePath, $relativePath);
            }
        }
        $zip->close();
        header("Pragma: public");
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
        header("Cache-Control: public");
        header("Content-Description: File Transfer");
        header("Content-type: application/octet-stream");
        header('Content-Disposition: attachment; filename="' . time() . '.zip"');
        header("Content-Transfer-Encoding: binary");
        header("Content-Length: " . filesize($fileName));
        ob_end_flush();
        @readfile($fileName);
        unlink($fileName);
//        return explode('webroot', $fileName)[1];
        /*if (file_exists($fileName))
            $this->response->body(json_encode(['status' => 'success', 'filePath' => explode('webroot', $fileName)[1]]));
        else
            $this->response->body(json_encode(['status' => 'failure']));
        return $this->response;*/
    }

    public function deleteDirectory($dir)
    {
        if ($handle = opendir($dir)) {
            while (false !== ($file = readdir($handle))) {
                if ($file != "." && $file != "..") {
                    if (is_dir($dir . $file)) {
                        if (!@rmdir($dir . $file)) // Empty directory? Remove it
                        {
                            $this->deleteDirectory($dir . $file . '/'); // Not empty? Delete the files inside it
                        }
                    } else {
                        @unlink($dir . $file);
                    }
                }
            }
            closedir($handle);
            @rmdir($dir);
        }

    }
}
