<?php

namespace App\Controller\Component;

use Cake\ORM\TableRegistry;
use Cake\Controller\Component;


class ToolKitHandlerComponent extends Component{
    
    public $components = ['Common'];
    
    public function processPostRequest($request, $user_name) {
        
        $result = false;
        try{
			$this->ToolKits = TableRegistry::get('ToolKits');
            $toolkitsCategories = $this->getToolKitCategories();
            $catArr = [];
            foreach($toolkitsCategories as $key => $category){
                $catArr[$category->id]['name'] = $category->name;
            }
            
			$requestData = $request->getData();
			$statusPayload = $requestData['status'];
			$qtyPayload = $requestData['qty'];
        
			$trueData = [];
			$falseData = [];
			$qtyData = [];
			foreach($statusPayload as $key => $data){
				$keyInt = filter_var($key, FILTER_SANITIZE_NUMBER_INT);
				$dataInt = filter_var($data, FILTER_SANITIZE_NUMBER_INT);
				if($dataInt){
			    	$trueData[$keyInt] = $keyInt;
			        $qtyData[$keyInt] = filter_var($qtyPayload[$key], FILTER_SANITIZE_NUMBER_INT);
			    } else {
			        $falseData[$keyInt] = $keyInt;
			    }
			}

            $this->ToolKits->updateAll(
                ['quantity' => 0],
                ['id >' => 0]
            );
            if($trueData){
                foreach($qtyData as $key => $qty){
                    $this->ToolKits->updateAll(
                        ['status' => 1,'quantity' => $qty],
                        ['id' => $key]
                    );
                }
                
                $data = $this->ToolKits->find('all')->where(['id IN' => $trueData])->all();
                
                foreach($data as $key => $tool){
                    $catArr[$tool->tool_kit_category_id]['data'][]=$tool;
                }
                
                $html = '<b>Tools Required:</b> <br>';
                foreach($catArr as $key => $categoryTool){
                    if(isset($categoryTool['data'])){
                        $html .= '<br>Category : '.$categoryTool['name'].'<br>';
                        foreach($categoryTool['data'] as $key => $tool){
                            $html .= ($key+1).'. '.$tool->tool.' (#'.$tool->item_code.') Quantity : '.$tool->quantity.'<br>';
                        }
                    }
                }
                $html .= '<br>';
                $html .= 'Sample Acct : '.$requestData['sample_acct'].'<br>';
                $html .= 'Salesman Name : '.$requestData['salesmanName'].'<br>';
				
				$currentUserEmail = "";
				
				try{
					if($user_name==null || $user_name==''){
						$currentUserEmail = $request->getSession()->read('Auth.User.username');
					}
					$currentUserEmail = $user_name;
					if(strpos($user_name,'@') == false){
			     	   $currentUserEmail = $currentUserEmail.'@primco.ca';
				    }					
				}catch(Exception $err){
					
				}

                $toEmail = [
                    $currentUserEmail,
                    'orderdesk@primco.ca'
                ];
                $subject = 'Vehicle Kit';
                
                $this->Common->sendMail($toEmail, $subject, $html);
                
            }
            if($falseData){
                $this->ToolKits->updateAll(
                    ['status' => 0],
                    ['id IN' => $falseData]
                );
            }
            $result = true;
        }catch(Exception $e) {
            echo 'Message: ' .$e->getMessage();
        }
        return $result;
        
    }
	
    public function handleGetRequest($user_id){
        $salesRep = $this->getSalesRep($user_id);
        $toolkitsCategories = $this->getToolKitCategories();
        $response = array("toolkitsCategories"=>$toolkitsCategories, "salesRep"=>$salesRep);
        return $response;
    }
    
    public function getSalesRep($user_id){
        
        $salesRep = array();
        
        $salesmen_model = TableRegistry::get('Salesmen');
        $salesmen = $salesmen_model->find()->where(['user_id' => $user_id])->first();
        
        if($salesmen['sales_rep'] != null && $salesmen['sales_rep']!=""){
            $salesRep[] = $salesmen['sales_rep'];
        }else{
            $salesRep[] = "NA";
        }
        
        if($salesmen['sd_rep'] != null && $salesmen['sd_rep']!=""){
            $salesRep[] = $salesmen['sd_rep'];
        }else{
            $salesRep[] = "NA";
        }
        return $salesRep;
        
    }
    
    public function getToolKitCategories(){
        
        //$toolkits_model = TableRegistry::get('ToolKits');
        //$toolkits = $toolkits_model->find('all')->all();
        $toolkits_cat_model = TableRegistry::get('ToolKitCategory');
        $toolkitsCategories = $toolkits_cat_model->find('all')->contain(['ToolKits'])->all();
        
        return $toolkitsCategories;
    }
}