<?php
$logo = BASE_URL.'webroot/img/primco-logo.png';
$customerPin = $claim->customer->city.", ".$claim->customer->postal_code;

$primcoInvoiceTr="";
foreach (json_decode($claim->primco_invoice_number) as $invoiceInfo) {
    $primcoInvoiceTr .= '<tr style="height: 10px; padding: 2px;">
<td>'. $invoiceInfo->invoiceNumber .'</td>
<td>'. $invoiceInfo->invoiceDate .'</td>
</tr>';
}

$millerInvoiceTr="";
foreach (json_decode($claim->miller_invoice_number) as $invoiceInfo) {
    $millerInvoiceTr .= '<tr style="height: 10px; padding: 2px;">
<td>'. $invoiceInfo->invoiceNumber .'</td>
<td>'. $invoiceInfo->invoiceDate .'</td>
</tr>';
}

$claimJson = json_decode($claim->claim_json)->{"0"};
$claimJsonMisc = json_decode($claim->claim_json)->{"misc"};

$categoryName = "";
$subCategoryName = "";
foreach($categoryData as $categoryItem) {
    if ( !empty($claimJson->Category) && ($categoryItem->id == $claimJson->Category) ) {
        $categoryName = $categoryItem->category;
    }
    if ( !empty($claimJson->Sub_Category) && ($categoryItem->id == $claimJson->Sub_Category) ) {
        $subCategoryName = $categoryItem->category;
    }
}

$claimSettlementMethod = $claim->settlement_method;
$claimSettlementAmountDescription = "";
$additionalInformation = "";
if ($claimSettlementMethod = "cash") {
    $claimSettlementAmountDescription = "Total Settlement Amount (in CAD)";
    $additionalInformation = $claimJsonMisc;
} else if ($claimSettlementMethod = "cash")
    $claimSettlementAmountDescription = "Total Settlement Amount (in CAD)";

$claimMiscTr="";
if($additionalInformation!="") {
    $claimMiscTr .= '<tr style="height: 10px; padding: 2px;">
                        <td>
                            <table cellspacing="0" border="0">
                                <tr style="height: 10px; padding: 2px;">
                                    <td>
                                        <table cellspacing="0" border="0">
                                            <tr style="height: 10px; padding: 2px;">
                                                <td><b>Additional Information</b></td>
                                            </tr>
                                            <tr style="height: 10px; padding: 2px;">
                                                <td>'. $additionalInformation .'</td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>';
}

$claimPictures1 = explode(",",$claim->claim_pictures_1);
$claimPictures1Tr = "";
foreach($claimPictures1 as $picture) {
    $claimPictures1Tr .= '<tr style="height: 10px; padding: 2px;">
        <td><img src="'. $picture .'" /></td>
    </tr>';
}

$claimPictures2 = explode(",",$claim->claim_pictures_2);
$claimPictures2Tr = "";
foreach($claimPictures2 as $picture) {
    $claimPictures1Tr .= '<tr style="height: 10px; padding: 2px;">
        <td><img src="'. $picture .'" /></td>
    </tr>';
}

$claimPictures3 = explode(",",$claim->claim_pictures_3);
$claimPictures3Tr = "";
foreach($claimPictures3 as $picture) {
    $claimPictures3Tr .= '<tr style="height: 10px; padding: 2px;">
        <td><img src="'. $picture .'" /></td>
    </tr>';
}

$claimPictures4 = explode(",",$claim->claim_pictures_4);
$claimPictures4Tr = "";
foreach($claimPictures4 as $picture) {
    $claimPictures4Tr .= '<tr style="height: 10px; padding: 2px;">
        <td><img src="'. $picture .'" /></td>
    </tr>';
}

// Set some content to print
echo $html = <<<EOD
<!DOCTYPE html>
<html>
<head>
	<title>Print &amp; Preview</title>
	<link href="https://fonts.googleapis.com/css?family=Montserrat:400,600,800&display=swap" rel="stylesheet">
</head>
<body>
<style type="text/css">
body {
    background: #555;
    font-family: 'Montserrat', sans-serif;
    font-size: 12px;
}
table {
    border: 1px solid black;
    border-collapse: separate;
    border-spacing: 0;
}
table td, table th {
    border: 1px solid red;

}

table tr td {
    border-right: 0;
}
table tr:last-child td {
    border-bottom: 0;
}
table tr td:first-child,
table tr th:first-child {
    border-left: 0;
}
table tr td{
    border-top: 0;
}

table {
    width: 100%;
}
table th {
    font-weight: bold;

}
table th, table td {
    padding: 5px;
    vertical-align: top;
}
.main-table{
	width: 665px;
	margin: 0 auto;
	background: #fff;
	padding: 5px;
	height:auto;
}

table tr,table tr th,table tr td {
    border: none !important;
}

th {

	font-weight: bold;
	font-size: 12px;
}
.border {

	border-collapse: collapse;
}
table.border tr td, table.border tr th {
    border:1px solid #cecece;
	border-collapse: collapse;
}
table.border tr td:last-child, table.border tr th:last-child {
    border-right: none;
}
table.border tr {
    margin: 0;
    border: none;
    border-spacing: 0px;
}
table.border tr:last-child td, table.border tr:last-child th {
    border-bottom: none !important;
}
.print-button-container{
	width: 920px;
	margin: 0 auto;
	text-align: center;
	padding: 10px 0px;
	background: #fff;
	display: block;
}
.print-button{
	padding: 10px 20px;
	font-size: 14px;
	background-color: #1f555f;
	color: #fff;
	border: 1px solid #1f555f;
}
@media print{
	.print-button-container{
		display: none;
	}
}
</style>

<table class="main-table" cellspacing="0" id="claim-table">
    <tr>
        <td><h5 style="text-align: center"></h5></td>
    </tr>
    <tr>
		<td>
			<table cellspacing="0" border="0">
				<tr style="height: 10px; padding: 2px;">
					<td></td>
					<td></td>
					<td style="padding: 0px;vertical-align: bottom; padding-left: 10px;"><strong>Phone</strong></td>
					<td style="padding: 0px;vertical-align: bottom; padding-left: 10px;"><strong>Fax</strong></td>
				</tr>
				<tr>
					<td>
						<img style="width: 250px;" src="{$logo}">
					</td>
					<td>

						<p style="font-size:10px">12300 - 44th Street SE, Calgary, AB T2Z 4A2<br>
						14320 - 112th Avenue, Edmonton, AB T5M 2T9<br>
						704 - 47th Street East, Saskatoon, SK S7K 0X3<br>
						645 Mcleod Street, Regina, SK S4N 4Y2<br>
						Winnipeg, MB<br>
						Vancouver, BC</p>

					</td>
					<td>

						<p style="font-size:10px">(403) 730-0222<br>
						(780) 454-0316<br>
						(306) 934-6121<br>
						(306) 721-8484<br>
						(204) 6337256<br>
						(604) 278-7771</p>

					</td>
					<td style="font-size:10px">(403) 295-8313<br>
						(780) 452-3377<br>
						(306) 934-1833<br>
						<h2>Claim Number<br>{$claim->id}</h2>

					</td>

				</tr>
			</table>
		</td>

	</tr>
    <tr>
        <td>
            <table cellspacing="0" border="0">
                <tr style="height: 10px; padding: 2px;">
                    <td style="text-align: center"><b><u>Retailer/Dealer Information</u></b></td>
                </tr>
                <tr style="height: 10px; padding: 2px;">
                    <td>
                        <table cellspacing="0" border="0">
                            <tr style="height: 10px; padding: 2px;">
                                <td>
                                    <table cellspacing="0" border="0">
                                        <tr style="height: 10px; padding: 2px;">
                                            <td><b>Dealer Name</b></td>
                                            <td>{$claim->customer->name}</td>
                                        </tr>
                                        <tr style="height: 10px; padding: 2px;">
                                            <td><b>Contact Name</b></td>
                                            <td>{$claim->customer->contact_name}</td>
                                        </tr>
                                        <tr style="height: 10px; padding: 2px;">
                                            <td><b>Phone</b></td>
                                            <td>{$claim->customer->phone}</td>
                                        </tr>
                                        <tr style="height: 10px; padding: 2px;">
                                            <td><b>Address</b></td>
                                            <td>{$claim->customer->address}</td>
                                        </tr>
                                        <tr style="height: 10px; padding: 2px;">
                                            <td><b>City and Postal Code</b></td>
                                            <td>{$customerPin}</td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr style="height: 10px; padding: 2px;">
                    <td style="text-align: center"><b><u>Customer Information</u></b></td>
                </tr>
                <tr style="height: 10px; padding: 2px;">
                    <td>
                        <table cellspacing="0" border="0">
                            <tr style="height: 10px; padding: 2px;">
                                <td>
                                <table cellspacing="0" border="0">
                                    <tr style="height: 10px; padding: 2px;">
                                        <td><b>Customer Name</b></td>
                                        <td>{$claim->consumer->consumer_name}</td>
                                    </tr>
                                    <tr style="height: 10px; padding: 2px;">
                                        <td><b>Phone</b></td>
                                        <td>{$claim->consumer->phone}</td>
                                    </tr>
                                    <tr style="height: 10px; padding: 2px;">
                                        <td><b>Address</b></td>
                                        <td>{$claim->consumer->address}</td>
                                    </tr>
                                    <tr style="height: 10px; padding: 2px;">
                                        <td><b>City</b></td>
                                        <td>{$claim->consumer->city}</td>
                                    </tr>
                                    <tr style="height: 10px; padding: 2px;">
                                        <td><b>Postal Code</b></td>
                                        <td>{$claim->consumer->postal_code}</td>
                                    </tr>
                                </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr style="height: 10px; padding: 2px;">
                    <td style="text-align: center"><b><u>Claim Information</u></b></td>
                </tr>
                <tr style="height: 10px; padding: 2px;">
                    <td>
                        <table cellspacing="0" border="0">
                            <tr style="height: 10px; padding: 2px;">
                                <td>
                                    <table cellspacing="0" border="0">
                                        <tr style="height: 10px; padding: 2px;">
                                            <td><b>Primco Invoice Number</b></td>
                                            <td><b>Invoice Date</b></td>
                                        </tr>
                                        {$primcoInvoiceTr}
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr style="height: 10px; padding: 2px;">
                    <td>
                        <table cellspacing="0" border="0">
                            <tr style="height: 10px; padding: 2px;">
                                <td>
                                    <table cellspacing="0" border="0">
                                        <tr style="height: 10px; padding: 2px;">
                                            <td><b>Mill Invoice Number</b></td>
                                            <td><b>Mill Invoice Date</b></td>
                                        </tr>
                                        {$millerInvoiceTr}
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr style="height: 10px; padding: 2px;">
                    <td>
                        <table cellspacing="0" border="0">
                            <tr style="height: 10px; padding: 2px;">
                                <td>
                                    <table cellspacing="0" border="0">
                                        <tr style="height: 10px; padding: 2px;">
                                            <td><b>Category</b></td>
                                            <td><b>Sub-Category</b></td>
                                        </tr>
                                        <tr style="height: 10px; padding: 2px;">
                                            <td>{$categoryName}</td>
                                            <td>{$subCategoryName}</td>
                                        </tr>
                                        <tr style="height: 10px; padding: 2px;">
                                            <td><b>Product Number</b></td>
                                            <td><b>Product Description</b></td>
                                        </tr>
                                        <tr style="height: 10px; padding: 2px;">
                                            <td>{$claimJson->Product_Number}</td>
                                            <td>{$claimJson->Product_Description}</td>
                                        </tr>
                                        <tr style="height: 10px; padding: 2px;">
                                           <td><b>Quantity Purchased</b></td>
                                            <td> <b>UOM</b></td>
                                            <td><b>Quantity Affected</b></td>
                                            <td><b>UOM</b></td>
                                        </tr>
                                        <tr style="height: 10px; padding: 2px;">
                                            <td>{$claimJson->Quantity_Purchased}</td>
                                            <td>{$claimJson->Quantity_Purchased_UOM}</td>
                                            <td>{$claimJson->Quantity_Affected}</td>
                                            <td>{$claimJson->Quantity_Affected_UOM}</td>
                                        </tr>
                                        <tr style="height: 10px; padding: 2px;">
                                            <td><b>Installed</b></td>
                                            <td><b>Quantity Installed</b></td>
                                            <td><b>UOM</b></td>
                                        </tr>
                                        <tr style="height: 10px; padding: 2px;">
                                            <td>{$claimJson->Installed}</td>
                                            <td>{$claimJson->Quantity_Installed}</td>
                                            <td>{$claimJson->Quantity_Installed_UOM}</td>
                                        </tr>
                                        <tr style="height: 10px; padding: 2px;">
                                            <td><b>Where the product was installed?</b></td>
                                            <td><b>Date Installed</b></td>
                                        </tr>
                                        <tr style="height: 10px; padding: 2px;">
                                            <td>{$claimJson->Rooms_Installed}</td>
                                            <td>{$claimJson->Date_Installed}</td>
                                        </tr>
                                        <tr style="height: 10px; padding: 2px;">
                                            <td><b>Detailed Reason of Claim</b></td>
                                        </tr>
                                        <tr style="height: 10px; padding: 2px;">
                                            <td>{$claimJson->Detailed_Claim_Reason}</td>
                                        </tr>
                                        <tr style="height: 10px; padding: 2px;">
                                            <td><b>Request for Resolution</b></td>
                                        </tr>
                                        <tr style="height: 10px; padding: 2px;">
                                            <td>{$claimJson->Request_For_Resolution}</td>
                                        </tr>
                                        <tr style="height: 10px; padding: 2px;">
                                            <td><b>Primary Contact Name (in case of inspection)</b></td>
                                            <td><b>Primary Contact Number (in case of inspection)</b></td>
                                        </tr>
                                        <tr style="height: 10px; padding: 2px;">
                                            <td>{$claimJson->Primary_Contact_Name}</td>
                                            <td>{$claimJson->Primary_Contact_Number}</td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr style="height: 10px; padding: 2px;">
                    <td style="text-align: center"><b><u>Settlement Information</u></b></td>
                </tr>
                <tr style="height: 10px; padding: 2px;">
                    <td>
                        <table cellspacing="0" border="0">
                            <tr style="height: 10px; padding: 2px;">
                                <td>
                                    <table cellspacing="0" border="0">
                                        <tr style="height: 10px; padding: 2px;">
                                            <td><b>Settlement Method</b></td>
                                            <td><b>{$claimSettlementAmountDescription}</b></td>
                                        </tr>
                                        <tr style="height: 10px; padding: 2px;">
                                            <td>{$claimSettlementMethod}</td>
                                            <td>{$claim->total_settlement_amount}</td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                {$claimMiscTr}
                <tr style="height: 10px; padding: 2px;">
                    <td style="text-align: center">Claim Pictures</td>
                </tr>
                <tr style="height: 10px; padding: 2px;">
                    <td>
                        <table cellspacing="0" border="0">
                            <tr style="height: 10px; padding: 2px;">
                                <td>
                                    <table cellspacing="0" border="0">
                                        <tr style="height: 10px; padding: 2px;">
                                            <td><b>Pictures - Full Room Pictures / Area(s) where the issue took place
                                                </b></td>
                                        </tr>
                                        {$claimPictures1Tr}
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr style="height: 10px; padding: 2px;">
                    <td>
                        <table cellspacing="0" border="0">
                            <tr style="height: 10px; padding: 2px;">
                                <td>
                                    <table cellspacing="0" border="0">
                                        <tr style="height: 10px; padding: 2px;">
                                            <td><b>Pictures of the issue - Closeup</b></td>
                                        </tr>
                                        {$claimPictures2Tr}
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr style="height: 10px; padding: 2px;">
                    <td>
                        <table cellspacing="0" border="0">
                            <tr style="height: 10px; padding: 2px;">
                                <td>
                                    <table cellspacing="0" border="0">
                                        <tr style="height: 10px; padding: 2px;">
                                            <td><b>Labour Bill / Quote, if applicable</b></td>
                                        </tr>
                                        {$claimPictures3Tr}
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr style="height: 10px; padding: 2px;">
                    <td>
                        <table cellspacing="0" border="0">
                            <tr style="height: 10px; padding: 2px;">
                                <td>
                                    <table cellspacing="0" border="0">
                                        <tr style="height: 10px; padding: 2px;">
                                            <td><b>Consumer Proof of Purchase</b></td>
                                        </tr>
                                        {$claimPictures4Tr}
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
</body>
</html>
EOD;
?>
