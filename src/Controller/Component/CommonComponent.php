<?php

namespace App\Controller\Component;

use Cake\Controller\Component;
use Cake\Mailer\MailerAwareTrait;
use Cake\Mailer\Email;


class CommonComponent extends Component{

	//public $components = ['Constants'];
	
	public static function getCurrentUserFromHeader($request){
		$authHeader = $request->getHeaderLine('Authorization');
	    $auth_array = explode(" ", $authHeader);
	    $username_password = explode(":", base64_decode($auth_array[1]));
	    $currentUser = $username_password[0];
	    return $currentUser;
	}
    
	public static function sendMail($toEmail, $subject, $html){
        
		try{
	    	$email = new Email();
	        $email->setTransport('default');
	        $email->setEmailFormat('html');
	        $email->setFrom(['support@primco.ca' => 'Primco'])
	        ->setTo($toEmail)
	        ->setSubject($subject)
	        ->send($html);  
	    } catch(Exception $e){
	     	return false;
	    }
	    return true;
	}
    
}