<?php

use Cake\Routing\Router;

?>
<aside class="sidenav-main nav-expanded nav-lock nav-collapsible sidenav-dark sidenav-active-rounded">
    <div class="brand-sidebar">
        <h1 class="logo-wrapper"><a class="brand-logo darken-1" href="#"><span class="logo-text hide-on-med-and-down">PRIMCO.CA</span></a><a
                class="navbar-toggler" href="#"><i class="material-icons">radio_button_checked</i></a></h1>
    </div>
    <ul class="sidenav sidenav-collapsible leftside-navigation collapsible sidenav-fixed menu-shadow" id="slide-out"
        data-menu="menu-navigation" data-collapsible="accordion">


        <?php
        $active = strtolower($this->request->params['controller']) == 'toolkits' ? 'active' : '';
        ?>
        <?php if ($loggedInUserInfo['role'] == 'sdm') { ?>
        <li class="bold <?= $active ?>">
            <a class="collapsible-header waves-effect waves-cyan " href="javascript:void(0)"><i class="material-icons">face</i><span
                    class="menu-title" data-i18n="">Vehicle Kit</span></a>
            <div class="collapsible-body">
                <ul class="collapsible collapsible-sub" data-collapsible="accordion">
                    <li>
                        <?= $this->Html->link(__('Vehicle Kit'), ['controller' => 'ToolKits', 'action' => 'index']) ?>
                    </li>
                </ul>
            </div>
        </li>
        <?php } ?> 

        <?php
        $active = strtolower($this->request->params['controller']) == 'bill' ? 'active' : '';
        ?>
        <li class="bold <?= $active ?>">
            <a class="collapsible-header waves-effect waves-cyan "
               href="javascript:void(0)">
                <i class="material-icons">face</i>
                <span class="menu-title" data-i18n="">Expense Allowance</span>
            </a>
            <div class="collapsible-body">
                <ul class="collapsible collapsible-sub" data-collapsible="accordion">
                    <li>
                        <?= $this->Html->link(__('All Expense Allowance'), ['controller' => 'Bill', 'action' => 'index']) ?>
                    </li>
                    <li>
                        <?= $this->Html->link(__('New Expense Allowance'), ['controller' => 'Bill', 'action' => 'add']) ?>
                    </li>
                </ul>
            </div>
        </li>

        <?php
        $active = strtolower($this->request->params['controller']) == 'users' ? 'active' : '';
        ?>
        <li class="bold <?= $active ?>">
            <a class="waves-effect waves-cyan"
               href="<?php echo $this->Url->build(['controller' => 'Users', 'action' => 'change', 'prefix' => false]) ?>">
                <i class="material-icons">face</i>
                <span class="menu-title" data-i18n="">Change Password</span>
            </a>
        </li>

        <li class="bold">
            <a class="waves-effect waves-cyan "
               href="<?php echo $this->Url->build(['controller' => 'Users', 'action' => 'logout', 'prefix' => false]) ?>">
                <i class="material-icons">help_outline</i>
                <span class="menu-title" data-i18n="">Logout</span>
            </a>
        </li>
    </ul>
    <div class="navigation-background"></div>
    <a class="sidenav-trigger btn-sidenav-toggle btn-floating btn-medium waves-effect waves-light hide-on-large-only"
       href="#" data-target="slide-out">
        <i class="material-icons">menu</i>
    </a>
</aside>
