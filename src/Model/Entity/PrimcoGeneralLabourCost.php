<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Retailer Entity
 *
 * @property int $id
 * @property string|null $category
 * @property string|null $uom
 * @property string|null $labour_cost
 *
// * @property \App\Model\Entity\Claim[] $claims
 */
class PrimcoGeneralLabourCost extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'category' => true,
        'uom' => true,
        'labour_cost' => true,
    ];
}
