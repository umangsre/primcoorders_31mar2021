<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * CreditApplications Model
 *
 * @property \App\Model\Table\ContactsListTable&\Cake\ORM\Association\HasMany $ContactsList
 * @property \App\Model\Table\OnlineUserRegistrationTable&\Cake\ORM\Association\HasMany $OnlineUserRegistration
 *
 * @method \App\Model\Entity\CreditApplication get($primaryKey, $options = [])
 * @method \App\Model\Entity\CreditApplication newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\CreditApplication[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\CreditApplication|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\CreditApplication saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\CreditApplication patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\CreditApplication[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\CreditApplication findOrCreate($search, callable $callback = null, $options = [])
 */
class CreditApplicationsTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('credit_applications');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->hasMany('ContactsList', [
            'foreignKey' => 'credit_application_id'
        ]);
        $this->hasMany('OnlineUserRegistration', [
            'foreignKey' => 'credit_application_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('company_legal_name')
            ->maxLength('company_legal_name', 255)
            ->requirePresence('company_legal_name', 'create')
            ->notEmptyString('company_legal_name');

        $validator
            ->scalar('company_trade_name')
            ->maxLength('company_trade_name', 255)
            ->requirePresence('company_trade_name', 'create')
            ->notEmptyString('company_trade_name');

        $validator
            ->scalar('street_address')
            ->maxLength('street_address', 255)
            ->requirePresence('street_address', 'create')
            ->notEmptyString('street_address');

        $validator
            ->scalar('phone')
            ->maxLength('phone', 255)
            ->requirePresence('phone', 'create')
            ->notEmptyString('phone');

        $validator
            ->scalar('fax')
            ->maxLength('fax', 150)
            ->allowEmptyString('fax');

        $validator
            ->scalar('city')
            ->maxLength('city', 150)
            ->requirePresence('city', 'create')
            ->notEmptyString('city');

        $validator
            ->scalar('province')
            ->maxLength('province', 150)
            ->requirePresence('province', 'create')
            ->notEmptyString('province');

        $validator
            ->scalar('postal_code')
            ->maxLength('postal_code', 150)
            ->requirePresence('postal_code', 'create')
            ->notEmptyString('postal_code');

        $validator
            ->scalar('email_address')
            ->maxLength('email_address', 150)
            ->requirePresence('email_address', 'create')
            ->notEmptyString('email_address');

        $validator
            ->scalar('owner_name')
            ->maxLength('owner_name', 150)
            ->requirePresence('owner_name', 'create')
            ->notEmptyString('owner_name');

        $validator
            ->scalar('owner_home_address')
            ->maxLength('owner_home_address', 255)
            ->requirePresence('owner_home_address', 'create')
            ->notEmptyString('owner_home_address');

        $validator
            ->scalar('owner_home_phone')
            ->maxLength('owner_home_phone', 150)
            ->requirePresence('owner_home_phone', 'create')
            ->notEmptyString('owner_home_phone');

        $validator
            ->scalar('owner_cell_phone')
            ->maxLength('owner_cell_phone', 150)
            ->requirePresence('owner_cell_phone', 'create')
            ->notEmptyString('owner_cell_phone');

        $validator
            ->scalar('manager_name')
            ->maxLength('manager_name', 150)
            ->requirePresence('manager_name', 'create')
            ->notEmptyString('manager_name');

        $validator
            ->scalar('manager_phone')
            ->maxLength('manager_phone', 150)
            ->requirePresence('manager_phone', 'create')
            ->notEmptyString('manager_phone');

        $validator
            ->scalar('manager_finacial_address')
            ->maxLength('manager_finacial_address', 255)
            ->allowEmptyString('manager_finacial_address');

        $validator
            ->scalar('manager_bank_account')
            ->maxLength('manager_bank_account', 255)
            ->allowEmptyString('manager_bank_account');

        $validator
            ->scalar('manager_city')
            ->maxLength('manager_city', 150)
            ->allowEmptyString('manager_city');

        $validator
            ->scalar('manager_province')
            ->maxLength('manager_province', 150)
            ->allowEmptyString('manager_province');

        $validator
            ->scalar('manager_postal_code')
            ->maxLength('manager_postal_code', 150)
            ->allowEmptyString('manager_postal_code');

        $validator
            ->scalar('manager_phone_2')
            ->maxLength('manager_phone_2', 150)
            ->allowEmptyString('manager_phone_2');

        $validator
            ->scalar('manager_gst')
            ->maxLength('manager_gst', 150)
            ->requirePresence('manager_gst', 'create')
            ->notEmptyString('manager_gst');

        $validator
            ->scalar('province_sales_tax')
            ->maxLength('province_sales_tax', 150)
            ->requirePresence('province_sales_tax', 'create')
            ->notEmptyString('province_sales_tax');

        $validator
            ->scalar('amount_credit_required')
            ->maxLength('amount_credit_required', 150)
            ->requirePresence('amount_credit_required', 'create')
            ->notEmptyString('amount_credit_required');

        $validator
            ->scalar('yearly_sales_volume')
            ->maxLength('yearly_sales_volume', 150)
            ->allowEmptyString('yearly_sales_volume');

        $validator
            ->scalar('fiscal_year_end')
            ->maxLength('fiscal_year_end', 150)
            ->allowEmptyString('fiscal_year_end');

        $validator
            ->allowEmptyString('is_group');

        $validator
            ->scalar('group_name')
            ->maxLength('group_name', 150)
            ->allowEmptyString('group_name');

        $validator
            ->scalar('_date')
            ->maxLength('_date', 150)
            ->allowEmptyString('_date');

        $validator
            ->scalar('authorized_signature')
            ->maxLength('authorized_signature', 150)
            ->allowEmptyString('authorized_signature');

        $validator
            ->scalar('print_name')
            ->maxLength('print_name', 150)
            ->allowEmptyString('print_name');

        $validator
            ->scalar('_title')
            ->maxLength('_title', 150)
            ->allowEmptyString('_title');

        return $validator;
    }
}
