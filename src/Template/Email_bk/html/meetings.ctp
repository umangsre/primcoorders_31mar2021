<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>

<body>
	<table>
		<tr>
			<td>Customer Email</td>
			<td><?= $meeting['email'] ?></td>
		</tr>
		<tr>
			<td>Agenda</td>
			<td><?= $meeting['agenda'] ?></td>
		</tr>
		<tr>
			<td>Description</td>
			<td><?= $meeting['description'] ?></td>
		</tr>
		<tr>
			<td>Date and Time</td>
			<td><?= date('d-m-Y h:i a', strtotime($meeting['date_time']) ) ?></td>
		</tr>

	</table>

</body>
</html>