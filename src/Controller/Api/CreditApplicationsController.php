<?php
namespace App\Controller\Api;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;
use Cake\Event\Event;
use Cake\Mailer\Email;
use Cake\Routing\Router;
use \PHPQRCode\QRcode;

/**
 * CreditApplications Controller
 *
 * @property \App\Model\Table\CreditApplicationsTable $CreditApplications
 *
 * @method \App\Model\Entity\CreditApplication[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class CreditApplicationsController extends AppController
{
    public function initialize()
    {
        parent::initialize();
        $this->loadComponent('RequestHandler');
        $this->Auth->allow([ 'viewCustomerDetails','viewCustomer']);
    }
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $creditApplications =$this->CreditApplications->find('all');
        $this->set([
                    'status' => 1,
                    'creditApplications' => $creditApplications,
                    '_serialize' => ['status', 'creditApplications']
                ]);

        //$this->set(compact('creditApplications'));
    }

    /**
     * View method
     *
     * @param string|null $id Credit Application id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function viewCustomerDetails()
    {
        
        $this->request->allowMethod(['post', 'put']);
        $id = $this->request->getData('id');
        $creditApplication = $this->CreditApplications->get($id, [
            'contain' => ['ContactsList', 'OnlineUserRegistration']
        ]);

        $this->set([
                    'status' => 1,
                    'creditApplication' => $creditApplication,
                    '_serialize' => ['status', 'creditApplication']
                ]);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function addcustomer()
    {
        $this->request->allowMethod(['post', 'put']);
        $creditApplication = $this->CreditApplications->newEntity();
        if ($this->request->is('post')) {
            $creditApplication = $this->CreditApplications->patchEntity($creditApplication, $this->request->getData());
            if ($this->CreditApplications->save($creditApplication)) {
                $this->set([
                    'status' => 1,
                    'creditApplication' => $creditApplication,
                    '_serialize' => ['status', 'creditApplication']
                ]);
            }else{
             //print_r($creditApplication->errors());  
             $error_str ='';
             foreach($creditApplication->errors() as $k =>$v){
                $error_str .= $k. ': '.$v['_required'].", ";
             }
                $this->set([
                    'status' => 0,
                     'errors' =>$error_str,
                    '_serialize' => ['status','errors']
                ]);
            }
            
        }
        
    }

    public function thankyou( $id = null )
    {
        // $this->autoRender = false;

        if ( $id == null ) {
            return $this->redirect( $this->referer() );
        }
        $id = base64_decode($id);

        $creditApplication = $this->CreditApplications->get($id, [
            'contain' => []
        ]);
        
        $image_name ='';
        $link ='';
        if($creditApplication){
            // $downLoad_link=$actual_link."/product-form/general_pdf.php?id=".$encoded_id;
             $link =  Router::url([ 'controller' => 'CreditApplications', 'action' => 'viewCustomer',base64_encode($creditApplication->id)
                                  ],TRUE);                
            //$downLoad_link = $site_url.'order_pdfs/'.$file_name;
            $path_qr = WWW_ROOT . "/img/QRcode/"; 
            $image_name = "qrcodes_creditApplication_".$creditApplication->id.".png"; 
            $qrcode_img = $path_qr.$image_name;
            // $ecc stores error correction capability('L') 
            $ecc = 'L'; 
            $pixel_Size = 10; 
            $frame_Size = 10;     
            // Generates QR Code and Stores it in directory given 
           QRcode::png($link, $qrcode_img, $ecc, $pixel_Size, $frame_Size); 
           $this->sendMail($image_name,$link, $creditApplication->id);
         }
        
        
        $this->set(compact('image_name','link','creditApplication'));
    }


 public function viewCustomer($id = null)
    {
        if ( $id == null ) {
            return $this->redirect( $this->referer() );
        }
        $id = base64_decode($id);
        $creditApplication = $this->CreditApplications->get($id, [
            'contain' => ['ContactsList','OnlineUserRegistration']
        ]);

        $contacts = TableRegistry::get('ContactsList');
        $onlineusers = TableRegistry::get('OnlineUserRegistration');
        $contactlist = $contacts->find( 'all' )
            ->where( [ 'credit_application_id' => $id  ] )->toArray();
        $onlineusersList = $onlineusers->find( 'all' )
            ->where( [ 'credit_application_id' => $id  ] )->toArray();    
        
        $this->set('creditApplication', $creditApplication);
        $this->set('contactlist', $contactlist);
        $this->set('onlineusersList', $onlineusersList);
        $this->set(compact('creditApplication'));
        $this->viewBuilder()->layout('ajax');
        $this->response->type('pdf');
    }
    

    /**
     * Edit method
     *
     * @param string|null $id Credit Application id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $creditApplication = $this->CreditApplications->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $creditApplication = $this->CreditApplications->patchEntity($creditApplication, $this->request->getData());
            if ($this->CreditApplications->save($creditApplication)) {
                $this->Flash->success(__('The credit application has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The credit application could not be saved. Please, try again.'));
        }
        $this->set(compact('creditApplication'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Credit Application id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function _delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $creditApplication = $this->CreditApplications->get($id);
        if ($this->CreditApplications->delete($creditApplication)) {
            $this->Flash->success(__('The credit application has been deleted.'));
        } else {
            $this->Flash->error(__('The credit application could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
