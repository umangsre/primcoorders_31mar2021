<?php
namespace App\Controller\Api;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;
use Cake\Event\Event;
use Cake\Mailer\Email;
use Cake\Routing\Router;
use \PHPQRCode\QRcode;

/**
 * Author - Abhishek Sharma
 * 
 * ToolKits Controller
 *
 * @property \App\Model\Table\ToolKits $ToolKits
 *
 * @method \App\Model\Entity\took_kits[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class ToolKitsController extends AppController
{
    public function initialize()
    {
        parent::initialize();
        $this->loadComponent('RequestHandler');
		$this->loadComponent('ToolKitHandler');
		$this->loadComponent('Common');
        $this->Auth->allow([ 'getalltoolkitscategories', 'updateToolkits', 'getAllToolKitCategories', 'updateVehicleToolkits']);
    }


    /*** rest api ***/

    public function getAllToolKitCategories()
    {
        $this->autoRender = false;
        
		$user_name = $this->Common->getCurrentUserFromHeader($this->request);
        
		$users_model = TableRegistry::get('Users');
		$user = $users_model->find()->where(['username' => $user_name])->first();
         
		$result = $this->ToolKitHandler->handleGetRequest($user['id']);
		$toolkitsCategories = $result['toolkitsCategories'];
		$salesRep = $result['salesRep'];
        
		$json_records = array();
		$cat_records = array();
		foreach ($toolkitsCategories as $toolkitsCategory):
			$toolkits_json_records = array();
			$toolkitsValues = $toolkitsCategory->ToolKits;
            
			foreach ($toolkitsValues as $toolkitsValue):
				$toolkits_json_records[] = [
					'id' => $toolkitsValue->id,
					'tool' => $toolkitsValue->tool,
					'item_code' => $toolkitsValue->item_code,
				];
			endforeach;
			$cat_records[] = [
				'name' => $toolkitsCategory->name,
				'toolkits' => $toolkits_json_records
			 ];
		endforeach;
        
		$json_records['categories'] = $cat_records;
		$json_records['salesRep'] = $salesRep;
		$json_records['user_name'] = $user['name'];
		
		$content = json_encode($json_records);
		$this->response = $this->response->withStringBody($content);
		$this->response = $this->response->withType('json');
		return $this->response;
    }
	
		/**
	     * This method update the toolkits
	     * @return json response
	     */
	    public function updateVehicleToolkits()
	    {
	        $this->request->allowMethod(['post', 'put']);
	        $this->autoRender = false;
			$json_records = array();
			$isAuthorized = parent::isAPIAuthorized($this->request);
			if(!$isAuthorized){
		       $json_records[] = [
		       		'status' => 'Failed',
		            'message' => 'Authentication failed'
		       ];
			
   	        	$content = json_encode($json_records);
   	        	$this->response = $this->response->withStringBody($content);
   	        	$this->response = $this->response->withType('json');
   	        	return $this->response;
			}
	        
			$user_name = $this->Common->getCurrentUserFromHeader($this->request);
			$result = $this->ToolKitHandler->processPostRequest($this->getRequest(), $user_name);
			
			if($result){
			    
				$json_records[] = [
					'status' => 'success',
					'message' => 'Toolkit updated successfully'
				];
			}else{
				$json_records[] = [
					'status' => 'Failed',
					'message' => 'Not a valid request'
				];
			}
			$content = json_encode($json_records);
			$this->response = $this->response->withStringBody($content);
			$this->response = $this->response->withType('json');
			return $this->response;
	    }
}
