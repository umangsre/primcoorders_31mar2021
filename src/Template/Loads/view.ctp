<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Load $load
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Load'), ['action' => 'edit', $load->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Load'), ['action' => 'delete', $load->id], ['confirm' => __('Are you sure you want to delete # {0}?', $load->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Loads'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Load'), ['action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="loads view large-9 medium-8 columns content">
    <h3><?= h($load->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('VendorID') ?></th>
            <td><?= h($load->vendorID) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Vendor') ?></th>
            <td><?= h($load->vendor) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __(' Load Multiplier') ?></th>
            <td><?= h($load-> load_multiplier) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($load->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Created') ?></th>
            <td><?= h($load->created) ?></td>
        </tr>
    </table>
</div>
