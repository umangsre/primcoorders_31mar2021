<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Claim $claim
 * @var \App\Model\Entity\PrimcoCategoryLabourCost $categoryData
 * @var \App\Model\Entity\PrimcoGeneralLabourCost $generalData
 */
$options = ['select' => 'Select', 'yes' => 'Yes', 'no' => 'No'];
$categories = ['select' => 'Select', 'carpet' => 'Carpet', 'wood' => 'Wood', 'vinyl' => 'Vinyl', 'laminates' => 'Laminates', 'luxury_vinyl' => 'Luxury Vinyl Plank', 'underlay' => 'Underlay', 'rubber' => 'Rubber'];
$appliances = ['select' => 'Select', 'dishwasher' => 'Dishwasher', 'mouldings' => 'Mouldings'];
$uom = ['select' => 'Select', 'sq.ft.' => 'sq.ft.', 'sq.yd.' => 'sq.yd.'];
$settlement_methods = ['select' => 'Select', 'cash' => 'Cash Settlement', 'replacement' => 'Replacement'];
?>
<script>
    const categories = <?php echo json_encode($categoryData) ?>;
    const general = <?php echo json_encode($generalData) ?>;
</script>
<style>
    #suggesstion-dealers {
        background: #fff;
        box-shadow: 3px 3px 3px rgba(0, 0, 0, 0.5);
        padding: 5px;
    }

    #suggesstion-dealers ul li {
        margin: 5px;
        border-bottom: 1px solid #000;
    }
</style>
<!--nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Claim'), ['action' => 'edit', $claim->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Claim'), ['action' => 'delete', $claim->id], ['confirm' => __('Are you sure you want to delete # {0}?', $claim->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Claims'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Claim'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Customers'), ['controller' => 'Customers', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Customer'), ['controller' => 'Customers', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Consumers'), ['controller' => 'Consumers', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Consumer'), ['controller' => 'Consumers', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Claim Products'), ['controller' => 'ClaimProducts', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Claim Product'), ['controller' => 'ClaimProducts', 'action' => 'add']) ?> </li>
    </ul>
</nav --->
<div class="orders index large-9 medium-8 columns content card card card-default scrollspy">
    <div class="card-content">
        <div class="claims view large-9 medium-8 columns content">
            <div class="row">
                <h3 style="float: right">Claim Number: <?= h($claim->id) ?></h3>
            </div>
            <table class="vertical-table">
                <?= $this->Form->create($claim) ?>
                <?php
                $disabled = true;
                if (count($claim->claim_status) > 0) {
                    $lastStatus = $claim->claim_status[count($claim->claim_status) - 1];
                    if ($lastStatus['status'] == 'send to dealer' || $lastStatus['status'] == 'saved') {
                        $disabled = false;
                    }
                }
                ?>
                <fieldset style="margin-bottom: 2rem;">
                    <legend><?= __('Retailer / Dealer Information') ?></legend>
                    <?php
                    echo $this->Form->control('name', ['disabled' => true, 'value' => $claim->customer->name, 'label' => 'Dealer Name']);
                    echo $this->Form->control('contact_name', ['disabled' => true, 'value' => $claim->customer->contact_name, 'label' => 'Contact Name']);
                    echo $this->Form->control('phone', ['disabled' => true, 'value' => $claim->customer->phone, 'label' => 'Phone']);
                    echo $this->Form->control('address', ['disabled' => true, 'value' => $claim->customer->address]);
                    echo $this->Form->control('province', ['disabled' => true, 'value' => $claim->customer->province]);
                    echo $this->Form->control('city', ['disabled' => true, 'value' => $claim->customer->city]);
                    echo $this->Form->control('postal_code', ['disabled' => true, 'value' => $claim->customer->postal_code]);
                    ?>
                </fieldset>
                <fieldset style="margin-bottom: 2rem;">
                    <legend><?= __('Customer Information') ?></legend>
                    <?php
                    echo $this->Form->control('retailer_name', ['disabled' => $disabled, 'value' => $claim->retailer->retailer_name, 'label' => 'Customer Name']);
                    echo $this->Form->control('retailer_contact_name', ['disabled' => $disabled, 'value' => $claim->retailer->contact_name, 'label' => 'Contact Name']);
                    echo $this->Form->control('retailer_phone', ['disabled' => $disabled, 'value' => $claim->retailer->phone, 'label' => 'Phone']);
                    echo $this->Form->control('retailer_address', ['disabled' => $disabled, 'value' => $claim->retailer->address, 'label' => 'Address']);
                    echo $this->Form->control('retailer_city', ['disabled' => $disabled, 'value' => $claim->retailer->city, 'label' => 'City']);
                    echo $this->Form->control('retailer_postal_code', ['disabled' => $disabled, 'value' => $claim->retailer->postal_code, 'label' => 'Postal Code']);
                    ?>
                </fieldset>
                <fieldset style="margin-bottom: 2rem;">
                    <legend><?= __('Claim Information') ?></legend>
                    <div id="primcoInvoiceNumbers"></div>
                    <?php if (!$disabled) { ?>
                        <button type="button" onclick="addInvoiceField()">Add More Invoice Numbers</button>
                    <?php } ?>
                    <input type="hidden" name="primco_invoice_number" id="primco-invoice-number"
                           value="<?php echo $claim->primco_invoice_number; ?>"/>
                    <?php
                    if ($claim->role === 'admin' || $claim->role === 'miller') {
                        $millerInvoiceDisabled = ($claim->miller_invoice_number === '' || $claim->miller_invoice_number === null || empty($claim->miller_invoice_number)) ? false : true;
                        echo $this->Form->control('miller_invoice_number', ['disabled' => $millerInvoiceDisabled, 'value' => $claim->miller_invoice_number]);
                    }
                    ?>
                    <div id="claimInformation"></div>
                </fieldset>
                <fieldset style="margin-bottom: 2rem;">
                    <?php
                    if ($disabled) {
                        echo $this->Form->control('settlement_method', ['disabled' => $disabled, 'value' => $claim->settlement_method]);
                        if ($claim->settlement_method === 'cash') {
                            echo $this->Form->control('total_settlement_amount', ['disabled' => $disabled, 'value' => $claim->total_settlement_amount]);
                        } else {
                            echo $this->Form->control('total_labour_cost_claimed', ['disabled' => $disabled, 'value' => $claim->total_settlement_amount]);
                        }
                    } else {
                        echo $this->Form->control('settlement_method', [
                            'options' => $settlement_methods,
                            'placeholder' => 'select',
                            'controller' => 'total_settlement_amount'
                        ]);
                        ?>
                        <div class="settlement_amount_div" style="display: none;">
                            <?php
                            echo $this->Form->control('total_settlement_amount');
                            ?>
                        </div>
                        <div class="replacement_amount_div" style="display: none;">
                            <?php
                            echo $this->Form->control('total_labour_cost_claimed');
                            ?>
                        </div>
                        <p>*In case of replacement, it is mandatory to fill customer information.</p>
                        <?php
                    }
                    echo '<div class="row"><h6 style="text-align: center;">Claim Pictures</h6></div>';
                    echo '<div class="row"><label for="billing-image">Pictures - Full Room Pictures / Area(s) where the issue took place</label></div>';
                    $explodeimage = explode(",", $claim->claim_pictures_1);
                    if (!empty($explodeimage)) {
                        foreach ($explodeimage as $key => $value) {
                            if (strpos($value, 'pdf')) {
                                echo "<a href='$value' target='_blank' ><img src='/img/pdf-icon.png' class='billing_image' style='width: 200px'></a>";
                            } else {
                                echo "<a href='$value' target='_blank' ><img src='$value' class='billing_image' style='width: 200px'></a>";
                            }
                        }
                        echo '<br /><br />';
                    }
                    echo '<div class="row"><label for="billing-image">Pictures of the issue - Closeup</label></div>';
                    $explodeimage = explode(",", $claim->claim_pictures_2);
                    if (!empty($explodeimage)) {
                        foreach ($explodeimage as $key => $value) {
                            if (strpos($value, 'pdf')) {
                                echo "<a href='$value' target='_blank' ><img src='/img/pdf-icon.png' class='billing_image' style='width: 200px'></a>";
                            } else {
                                echo "<a href='$value' target='_blank' ><img src='$value' class='billing_image' style='width: 200px'></a>";
                            }
                        }
                        echo '<br /><br />';
                    }
                    echo '<div class="row"><label for="billing-image">Labour Bill / Quote, if applicable</label></div>';
                    $explodeimage = explode(",", $claim->claim_pictures_3);
                    if (!empty($explodeimage)) {
                        foreach ($explodeimage as $key => $value) {
                            if (strpos($value, 'pdf')) {
                                echo "<a href='$value' target='_blank' ><img src='/img/pdf-icon.png' class='billing_image' style='width: 200px'></a>";
                            } else {
                                echo "<a href='$value' target='_blank' ><img src='$value' class='billing_image' style='width: 200px'></a>";
                            }
                        }
                        echo '<br /><br />';
                    }
                    echo '<div class="row"><label for="billing-image">Consumer Proof of Purchase</label></div>';
                    $explodeimage = explode(",", $claim->claim_pictures_4);
                    if (!empty($explodeimage)) {
                        foreach ($explodeimage as $key => $value) {
                            if (strpos($value, 'pdf')) {
                                echo "<a href='$value' target='_blank' ><img src='/img/pdf-icon.png' class='billing_image' style='width: 200px'></a>";
                            } else {
                                echo "<a href='$value' target='_blank' ><img src='$value' class='billing_image' style='width: 200px'></a>";
                            }
                        }
                        echo '<br /><br />';
                    }
                    ?>
                </fieldset>
                <?php
                if (!$disabled) {
                    ?>
                    <fieldset style="margin-bottom: 2rem; display: none;" id="miscClaimInfoArea">
                        <legend><?= __('Additional Information') ?></legend>
                        <div id="miscClaimInformation">
                        </div>
                    </fieldset>
                    <label>Important Notes:<br/>Unless a claim is approved / accepted by the responsible mill, you the
                        retailer cannot hold back funds or deduct funds owed to Primco for outstanding invoices.<br/>If
                        the
                        retailer chooses to remove and replace the claim material without an approved claim or a claim
                        accepted by the responsible mill, this will be at the cost of the retailer and the claim may not
                        be
                        honored by the mill.<br/><br/><span style="color:red;">What will you need to provide after claims form is complete:</span>
                        <br/>2-3 full uninstalled samples from the order.<br/> Upon return and inspection of
                        uninstalled
                        material, freight cost will be reimbursed to account via Credit Note if and when material is
                        deemed
                        defective.
                    </label>
                    <br/>
                    <input type="submit" name="clicked" class="save-data save-claim btn waves-effect waves-light border-round gradient-45deg-black col s6 mt-4" value="save">
                    <?= $this->Form->button(__('Submit'), ['class' => 'submit-claim btn waves-effect waves-light border-round gradient-45deg-purple-deep-orange col s6 mt-4']) ?>
                    <?php
                } else {
                    if ($claim->settlement_method === 'replacement') {
                        ?>
                        <fieldset style="margin-bottom: 2rem;">
                            <legend><?= __('Miscellaneous Claim Information') ?></legend>
                            <div id="miscClaimInformation"></div>
                        </fieldset>
                    <?php }
                }
                ?>
                <?php if ($claim->role === 'admin') { ?>
                    <fieldset style="margin-bottom: 2rem;">
                        <legend><?= __('Miller Information') ?></legend>
                        <?php
                        $millerDisabled = !empty($claim->miller);
                        echo $this->Form->control('miller_name', ['disabled' => $millerDisabled, 'value' => $millerDisabled ? $claim->miller->miller_name : '', 'label' => 'Miller Name']);
                        echo $this->Form->control('miller_contact_name', ['disabled' => $millerDisabled, 'value' => $millerDisabled ? $claim->miller->contact_name : '', 'label' => 'Contact Name']);
                        echo $this->Form->control('miller_email', ['disabled' => $millerDisabled, 'value' => $millerDisabled ? $claim->miller->email : '']);
                        echo $this->Form->control('miller_phone', ['disabled' => $millerDisabled, 'value' => $millerDisabled ? $claim->miller->phone : '']);
                        echo $this->Form->control('miller_address', ['disabled' => $millerDisabled, 'value' => $millerDisabled ? $claim->miller->address : '']);
                        echo $this->Form->control('miller_city', ['disabled' => $millerDisabled, 'value' => $millerDisabled ? $claim->miller->city : '']);
                        echo $this->Form->control('miller_postal_code', ['disabled' => $millerDisabled, 'value' => $millerDisabled ? $claim->miller->postal_code : '']);
                        ?>
                    </fieldset>
                <?php } ?>
                <?php
                echo $this->Form->control('claim_id', ['type' => 'hidden', 'value' => h($claim->id)]);
                $claimStatuses = $claim->claim_status;
                if (count($claimStatuses) > 0) {
                    $lastStatus = $claimStatuses[count($claimStatuses) - 1];
                    if ($lastStatus['status'] != 'saved') {
                        echo '<div class="row">&nbsp;</div>';
                        ?>
                        <?php
                        $checkClosure = $lastStatus['status'] === 'closed' || $lastStatus['status'] === 'approved by primco' || $lastStatus['status'] === 'declined by primco';
                        $noStatus = count($claimStatuses) === 0;
                        if ($claim->role === 'admin') {
                            if (!$checkClosure) {
//                            $disabled = $lastStatus['status'] === 'send to mill' || $lastStatus['status'] === 'send to dealer';
                                $disabled = false;
                                echo $this->Form->button(__('Approve'), ['disabled' => $disabled, 'class' => 'btn waves-effect waves-light border-round gradient-45deg-purple-deep-orange col s5 ml-4 mr-4', 'name' => 'status', 'value' => 'approved by primco']);
                                echo $this->Form->button(__('Reject'), ['disabled' => $disabled, 'class' => 'btn waves-effect waves-light border-round gradient-45deg-purple-deep-orange col s5', 'name' => 'status', 'value' => 'declined by primco']);
                                echo $this->Form->button(__('Send to Mill'), ['disabled' => $disabled, 'class' => 'btn waves-effect waves-light border-round gradient-45deg-purple-deep-orange col s5 ml-4 mr-4', 'name' => 'status', 'value' => 'send to mill']);
                                echo $this->Form->button(__('Send to Dealer'), ['disabled' => $disabled, 'class' => 'btn waves-effect waves-light border-round gradient-45deg-purple-deep-orange col s5', 'name' => 'status', 'value' => 'send to dealer']);
                            }
                        } else if ($claim->role === 'dealer') {
                            if (!$checkClosure && !$noStatus) {
                                if ($lastStatus['status'] == 'saved') {
                                    $disabled = false;
                                } else {
                                    $disabled = $lastStatus['status'] !== 'send to dealer' || $lastStatus['status'] === 'declined by primco';
                                    if (!$disabled) {
                                        echo $this->Form->control('status_comments', ['label' => 'Comments']);
                                    }
                                }
                                echo $this->Form->button(__('Send to Primco'), ['disabled' => $disabled, 'class' => 'btn waves-effect waves-light border-round gradient-45deg-purple-deep-orange col s5 ml-4 mr-4', 'name' => 'status', 'value' => 'send to admin']);
                            }
                        } else if ($claim->role === 'miller') {
                            if (!$checkClosure && !$noStatus) {
                                $disabled = $lastStatus['status'] === 'send to primco' || $lastStatus['status'] === 'approved by miller' || $lastStatus['status'] === 'declined by miller' || $lastStatus['status'] === 'send to dealer';
                                if (!$disabled) {
                                    echo $this->Form->control('status_comments', ['label' => 'Comments']);
                                }
                                echo $this->Form->button(__('Approve'), ['disabled' => $disabled, 'class' => 'btn waves-effect waves-light border-round gradient-45deg-purple-deep-orange col s5 ml-4 mr-4', 'name' => 'status', 'value' => 'approved by miller']);
                                echo $this->Form->button(__('Reject'), ['disabled' => $disabled, 'class' => 'btn waves-effect waves-light border-round gradient-45deg-purple-deep-orange col s5', 'name' => 'status', 'value' => 'declined by miller']);
                                echo $this->Form->button(__('Send to Primco'), ['disabled' => $disabled, 'class' => 'btn waves-effect waves-light border-round gradient-45deg-purple-deep-orange col s5 ml-4 mr-4', 'name' => 'status', 'value' => 'send to primco']);
                            }
                        }
                    }
                }
                ?>
                <?= $this->Form->end() ?>
                <?php
                echo '<div class="row">&nbsp;</div>';
                echo '<div class="row">&nbsp;</div>';
                foreach ($claimStatuses as $claimStatus) {
                    echo '<div style="margin-bottom: 2rem; padding: 5px; border: 1px solid #000; width: 45%; margin-right: 3%; display: inline-block;">';
                    echo $claimStatus['status'] . ' on ' . $claimStatus['created_at'] . ' by ' . $claimStatus['username'] . '<br />';
                    echo $claimStatus['status_comments'] . '<br />';
                    echo '</div>';
                }
                ?>
            </table>
        </div>
    </div>
</div>
<?php
$disableValues = true;
if (count($claim->claim_status) > 0) {
    $lastStatus = $claim->claim_status[count($claim->claim_status) - 1];
    if ($lastStatus['status'] == 'send to dealer' || $lastStatus['status'] == 'saved') {
        $disableValues = false;
    }
}
?>
<script>
    const disableValues = '<?php echo $disableValues; ?>';
</script>
<script>
    const claim_json_data = <?php echo $claim->claim_json; ?>;
</script>
<script>
    const userRole = '<?php echo $claim->role; ?>';
</script>
