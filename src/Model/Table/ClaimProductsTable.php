<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * ClaimProducts Model
 *
 * @property \App\Model\Table\ClaimsTable&\Cake\ORM\Association\BelongsTo $Claims
 * @property \App\Model\Table\ProductsTable&\Cake\ORM\Association\BelongsTo $Products
 *
 * @method \App\Model\Entity\ClaimProduct get($primaryKey, $options = [])
 * @method \App\Model\Entity\ClaimProduct newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\ClaimProduct[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\ClaimProduct|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\ClaimProduct saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\ClaimProduct patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\ClaimProduct[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\ClaimProduct findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class ClaimProductsTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('claim_products');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Claims', [
            'foreignKey' => 'claim_id'
        ]);
        $this->belongsTo('Products', [
            'foreignKey' => 'product_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('product_number')
            ->maxLength('product_number', 50)
            ->allowEmptyString('product_number');

        $validator
            ->integer('claim_quantity')
            ->allowEmptyString('claim_quantity');

        $validator
            ->scalar('is_installed')
            ->allowEmptyString('is_installed');

        $validator
            ->scalar('installed_area')
            ->maxLength('installed_area', 50)
            ->allowEmptyString('installed_area');

        $validator
            ->date('installed_date')
            ->allowEmptyDate('installed_date');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['claim_id'], 'Claims'));
        $rules->add($rules->existsIn(['product_id'], 'Products'));

        return $rules;
    }
}
