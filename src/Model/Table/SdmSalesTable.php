<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * SdmSalesTable Model
 *
 * @method \App\Model\Entity\SdmSalesTable get($primaryKey, $options = [])
 * @method \App\Model\Entity\SdmSalesTable newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\SdmSalesTable[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\SdmSalesTable|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\SdmSalesTable saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\SdmSalesTable patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\SdmSalesTable[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\SdmSalesTable findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class SdmSalesTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('sdm_sales');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Salesmen', [
            'className' => 'Salesmen',
            'foreignKey' => 'salesmen_id',
            'propertyName' => 'Salesmen'
        ]);

        $this->belongsTo('sdm', [
            'className' => 'Users',
            'foreignKey' => 'sdm_id',
            'propertyName' => 'sdm'
        ]);
    }
}
