<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\SubEventsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\SubEventsTable Test Case
 */
class SubEventsTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\SubEventsTable
     */
    public $SubEvents;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.SubEvents',
        'app.Events'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('SubEvents') ? [] : ['className' => SubEventsTable::class];
        $this->SubEvents = TableRegistry::getTableLocator()->get('SubEvents', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->SubEvents);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
