<?php
class xtcpdf extends TCPDF {

}

// create new PDF document
$pdf = new xtcpdf(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Primco');
$pdf->SetTitle('Primco Order Form');
$pdf->SetSubject('Order Form Download');
$pdf->SetKeywords('Primco, Order, Form, Download, PDF');

$pdf->SetMargins(10, 10, 10);

// set auto page breaks
$pdf->SetAutoPageBreak(true, PDF_MARGIN_BOTTOM);
// remove default header/footer
$pdf->setPrintHeader(false);
$pdf->setPrintFooter(false);
// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

// set some language-dependent strings (optional)
if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
    require_once(dirname(__FILE__).'/lang/eng.php');
    $pdf->setLanguageArray($l);
}


// ---------------------------------------------------------

// set default font subsetting mode
$pdf->setFontSubsetting(true);

// Set font
// dejavusans is a UTF-8 Unicode font, if you only need to
// print standard ASCII chars, you can use core fonts like
// helvetica or times to reduce file size.
$pdf->SetFont('dejavusans', '', 14, '', true);
// $pdf->addTTFfont('https://fonts.googleapis.com/css?family=Montserrat:400,600,800&display=swap', 'TrueTypeUnicode',, 32);
// Add a page
// This method has several options, check the source code documentation for more information.
$pdf->AddPage('P', 'A4');

// set text shadow effect
//$pdf->setTextShadow(array('enabled'=>true, 'depth_w'=>0.2, 'depth_h'=>0.2, 'color'=>array(196,196,196), 'opacity'=>1, 'blend_mode'=>'Normal'));

$html= $this->element('claim', [
    "claim" => $claim,
    "categoryData" => $categoryData,
    "generalData" => $generalData,
]);

$pdf->writeHTML($html, true, false, true, false, '');
$pdf->Output('primco-claim.pdf', 'I');

?>
