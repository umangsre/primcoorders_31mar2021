<?php

namespace App\Controller\Sales;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;
use Cake\Mailer\MailerAwareTrait;
use Cake\Mailer\Email;

/* use Cake\Filesystem\Folder;
use Cake\Filesystem\File; */

/**
 * Bill Controller
 *
 *
 * @method \App\Model\Entity\Bill[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class BillController extends AppController
{
    
	public function initialize() {
        parent::initialize();
    }
	
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $this->viewBuilder()->setLayout('sales_dashboard');
        $user_id = $this->Auth->user('id');
        $user_role = $this->Auth->user('role');
        $query = $this->Bill->find('all', ['order' => 'Bill.id DESC'])
            ->contain(['BillComment' => function ($q) {
                return $q->order(['id' => 'desc'])->limit(1);
            }])
            ->where(['Bill.billing_userid' => $user_id]);
			
         $this->loadModel('Salesmen');
         $salesmen = $this->Salesmen->find()->where(['user_id' => $user_id])->first();

         $this->loadModel('SdmSales');
         $sdm = $this->SdmSales->find()->where(['salesmen_id' => $user_id])
         	->contain(['sdm' => function ($q) {
            	return $q->select(['id', 'name', 'username']);
            }])
            ->first();
			
			
		  // If SDM is not available then skip the workflow and submit the bills to admin directly.
		  $isSDMAvailable = false;
		  if($sdm!=null){
			  $isSDMAvailable = true;	
		  }

        $bill = $this->paginate($query);

        $this->set(compact('bill', 'isSDMAvailable'));
    }

    /**
     * View method
     *
     * @param string|null $id bill id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $this->viewBuilder()->setLayout('sales_dashboard');
        $bill = $this->Bill->get($id, [
            'contain' => []
        ]);
        $billcommnets = TableRegistry::getTableLocator()->get('bill_comment')->find()->where(['and' => ['billid' => $id]]);
        //print_r($billcommnets);
        //exit;
        $this->set(compact('billcommnets', 'bill'));
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $this->viewBuilder()->setLayout('sales_dashboard');
        $user_id = $this->Auth->user('id');
        $user_role = $this->Auth->user('role');
        $bill = $this->Bill->newEntity();

        if ($this->request->is('post')) {
            $this->loadModel('Salesmen');
            $salesmen = $this->Salesmen->find()->where(['user_id' => $user_id])->first();

            $this->loadModel('SdmSales');
            $sdm = $this->SdmSales->find()->where(['salesmen_id' => $user_id])
                ->contain(['sdm' => function ($q) {
                    return $q->select(['id', 'name', 'username']);
                }])
                ->first();
			
			
			// If SDM is not available then skip the workflow and submit the bills to admin directly.
			$isSDMAvailable = false;
			if($sdm!=null){
				$isSDMAvailable = true;	
			}
			
            $data = $this->request->getData();
            $mailsend = 'Y';
            if (!empty($data['save'])) {
                if ($data['save'] == 'Save') {
                    $data['billing_status'] = 'S';
                    $mailsend = 'N';
                }
            } else {
				if($isSDMAvailable){
					$data['billing_status'] = 'P';
				}else{
					$data['billing_status'] = 'AWS';
				}
                
            }

            $data['billing_image'] = $data['files'];
            $data['billing_userid'] = $this->Auth->user('id');
            $data['billing_type_of_expense'] = implode(";", $data['billing_type_of_expense']);
            $placeOfExpense = '';
            switch ($data['billing_tax']) {
                case 'SASK: 6% PST and 5% GST':
                    $placeOfExpense = 'SASK';
                    break;
                case 'MANITOBA: 7% PST and 5% GST':
                    $placeOfExpense = 'MANITOBA';
                    break;
                case 'BC: 7% PST and 5% GST':
                    $placeOfExpense = 'BC';
                    break;
            }
            $data['billing_place_of_expense'] = $placeOfExpense;
            $bill = $this->Bill->patchEntity($bill, $data);
            if ($this->Bill->save($bill)) {
                $this->Flash->success(__('The bill has been saved.'));
                if ($mailsend == 'Y') {
					 $toEmail = array();
					if($isSDMAvailable){
				        $toEmail = [
				            $this->Auth->user('username'),
				            $sdm->sdm->username
				        ];
					}else{
				        $toEmail = [
				            $this->Auth->user('username'),
				            'admin@primco.ca'
				        ];
					}
                    $this->sendMail($toEmail, "New Bill", $bill);
                }
                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The bill could not be saved. Please, try again.'));
        }
        $this->set(compact('bill', 'user_id'));
    }

    public function sendMail($toEmail, $billsubject, $bill)
    {
		
       $email = new Email();
        $email->setTransport('default');
        $email->viewVars(['bill' => $bill]);
        $email->viewBuilder()->setTemplate('bill');
        $email->setEmailFormat('html');
        $email->setFrom(['support@primco.ca' => 'Primco'])
            ->setTo($toEmail)
            ->setSubject('Notification: ' . $billsubject)
            ->send();
        return true;
    }

    /**
     * Edit method
     *
     * @param string|null $id bill id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $this->viewBuilder()->setLayout('sales_dashboard');
        $bill = $this->Bill->get($id, [
            'contain' => []
        ]);
        $user_id = $this->Auth->user('id');

        if ($this->request->is(['patch', 'post', 'put'])) {
            $data = $this->request->getData();
            $data['billing_image'] = $data['files'];;
            $bill = $this->Bill->patchEntity($bill, $data);
            if ($this->Bill->save($bill)) {
                $bill = $bill->toArray();
                $this->loadModel('SdmSales');
                $sdm = $this->SdmSales->find()->where(['salesmen_id' => $bill['billing_userid']])
                    ->contain(['sdm' => function ($q) {
                        return $q->select(['id', 'name', 'username']);
                    }])
                    ->first();
				
				// If SDM is not available then skip the workflow and submit the bills to admin directly.
				$toEmail = array();
				if($sdm!=null){
			        $toEmail = [
			            $this->Auth->user('username'),
			            $sdm->sdm->username
			        ];
				}else{
			        $toEmail = [
			            $this->Auth->user('username'),
			            'admin@primco.ca'
			        ];
				}
				
                $this->sendMail($toEmail, "Update Bill", $bill);

                $this->Flash->success(__('The bill has been saved.'));
                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The bill could not be saved. Please, try again.'));
        }
        $this->set(compact('bill', 'user_id'));
    }

    /**
     * Delete method
     *
     * @param string|null $id bill id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $bill = $this->Bill->get($id);
        if ($this->Bill->delete($bill)) {
            $this->Flash->success(__('The bill has been deleted.'));
        } else {
            $this->Flash->error(__('The bill could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
