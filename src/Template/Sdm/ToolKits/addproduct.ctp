<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\took_kits $took_kits
 */
?>
<div class="ToolKits form large-9 medium-8 columns content">
    <?= $this->Form->create('took_kits') ?>
    <fieldset>
        <legend><?= __('Add Category') ?></legend>
        <?php
            echo $this->Form->control('tool', ['label' => 'Product Name']);
            echo $this->Form->control('item_code',['label' => 'Product Code']);

        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit'), ['class' => 'btn waves-effect waves-light border-round col s3 mt-4', 'style' => 'background: #000;','action'=>'edit/1']) ?>
    <?= $this->Html->link(__('Back'), ['action' => 'index'],['class' => 'btn waves-effect waves-light border-round col s3 mt-4', 'style' => 'background: #000;', 'name' => 'clicked', 'value' => 'save']) ?>
    <?= $this->Form->end() ?>
</div>
