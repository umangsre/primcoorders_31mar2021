<?php
namespace App\Controller\Api;

use App\Controller\AppController;
use Cake\Mailer\MailerAwareTrait;
use Cake\Mailer\Email;
/**
 * Meetings Controller
 *
 * @property \App\Model\Table\MeetingsTable $Meetings
 *
 * @method \App\Model\Entity\Meeting[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class MeetingsController extends AppController
{
    use MailerAwareTrait;
    public function initialize()
    {
        parent::initialize();
        $this->loadComponent('RequestHandler');
        $this->Auth->allow(['addmeeting','editmeeting','getmeeting']);
    }
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Salesmen']
        ];
        $meetings = $this->paginate($this->Meetings);

        $this->set(compact('meetings'));
    }

    /**
     * View method
     *
     * @param string|null $id Meeting id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function getmeeting()
    {
        $this->request->allowMethod(['post', 'put']);
        $id =$this->request->getData('id');
        $meeting = $this->Meetings->get($id, [
            'contain' => ['Salesmen']
        ]);
       $this->set([
                    'status' => 1,
                    'meeting' => $meeting,
                    '_serialize' => ['status', 'meeting']
                ]);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function addmeeting()
    {
        $this->request->allowMethod(['post', 'put']);
        $meeting = $this->Meetings->newEntity();

        if ($this->request->is('post')) {
           
            $salesman_id = $this->request->getData('salesman_id');
            $salesman_arr = explode(': ',$salesman_id);
            $salesman_id =$salesman_arr[0];
            $salesman =$this->request->getData();
            $salesman['salesman_id'] =$salesman_id;
           // $saleman_data = $this->Meetings->Salesmen->get($salesman_id);
           // $salesman['email'] = $saleman_data['rep_email'];
            $meeting = $this->Meetings->patchEntity($meeting,  $salesman);

            if ($this->Meetings->save($meeting)) {
                 $saleman = $this->Meetings->Salesmen->get($salesman_id, [
                    'contain' => []
                ]);

$from_name = "Primco";        
$from_address = "support@primco.ca";        
$to_name = "";        
$to_address = $meeting['email'].",".$saleman['rep_email'];        
$startTime = $meeting['date_time'];        
$endTime = $meeting['date_time'];        
$subject = "Notification: New Meeting";        
$description = "<!DOCTYPE html><html><head><title></title></head><body><table><tr><td>Customer Email</td><td>".$meeting['email']."</td></tr><tr><td>Agenda</td><td>".$meeting['agenda']."</td></tr><tr><td>Description</td><td>".$meeting['description']."</td></tr><tr><td>Date and Time</td><td>".date('d-m-Y h:i a', strtotime($meeting['date_time']) )."</td></tr></table></body></html>";        
$location = "";
$this->sendIcalEvent($from_name, $from_address, $to_name, $to_address, $startTime, $endTime, $subject, $description, $location,$meeting);

              //  $this->sendMail($meeting, $saleman);
                
                 $this->set([
                    'status' => 1,
                    'meeting' => $meeting,
                    '_serialize' => ['status', 'meeting']
                ]);
               // $this->getMailer('Meetings')->send('meetingBySalesRep', [$meeting]);
            }else{
               // print_r($meeting->errors());
                 $this->set([
                    'status' => 0,
                    'meeting' => $meeting,
                    '_serialize' => ['status', 'meeting']
                ]);
            }
            
        }
        
        // $salesmen = $this->Meetings->Salesmen->find('list', ['limit' => 200]);
        // $this->set(compact('meeting', 'salesmen'));
    }

    public function sendMail($meeting, $salesman){
        
        $toEmail = [
            'm.malhotra@primco.ca',
            $meeting['email'],
            $salesman['rep_email']
        ];


        $email = new Email();
        $email->setTransport('default');
        $email->viewVars(['meeting' => $meeting ]);
        $email->viewBuilder()->setTemplate('meetings');
        $email->setEmailFormat('html');

        $email->setFrom(['support@primco.ca' => 'Primco'])
        ->setTo($toEmail)
        ->setBcc('Juneja.ankit@gmail.com')
        ->setSubject('Notification: New Meeting')
        ->send();
        
        
      return true;
    }
	public function sendIcalEvent($from_name, $from_address, $to_name, $to_address, $startTime, $endTime, $subject, $description, $location,$meeting)
	{
		$domain = 'exchangecore.com';

		//Create Email Headers
		$mime_boundary = "----Meeting Booking----".MD5(TIME());

		//$headers = "From: ".$from_name." <".$from_address.">. '\r\n' .
	//'BCC: somebodyelse@example.com'";

	/* $headers = "From: ".$from_address. "\r\n" .
	"CC: Juneja.ankit@gmail.com"; */

	$headers = 'From: <'.$from_address.'>' . "\r\n";
	$headers .= 'Bcc: Juneja.ankit@gmail.com' . "\r\n";

		$headers .= "Reply-To: ".$from_name." <".$from_address.">\n";
		$headers .= "MIME-Version: 1.0\n";
		$headers .= "Content-Type: multipart/alternative; boundary=\"$mime_boundary\"\n";
		$headers .= "Content-class: urn:content-classes:calendarmessage\n";

		//Create Email Body (HTML)
		$message = "--$mime_boundary\r\n";
		$message .= "Content-Type: text/html; charset=UTF-8\n";
		$message .= "Content-Transfer-Encoding: 8bit\n\n";
		$message .= "<html>\n";
		$message .= "<body>\n";
		//$message .= '<p>Dear '.$to_name.',</p>';
		$message .= '<p>'.$description.'</p>';
		$message .= "</body>\n";
		$message .= "</html>\n";
		$message .= "--$mime_boundary\r\n";

		$ical = 'BEGIN:VCALENDAR' . "\r\n" .
		'PRODID:-//Microsoft Corporation//Outlook 10.0 MIMEDIR//EN' . "\r\n" .
		'VERSION:2.0' . "\r\n" .
		'METHOD:REQUEST' . "\r\n" .
		'BEGIN:VTIMEZONE' . "\r\n" .
		'TZID:Eastern Time' . "\r\n" .
		'BEGIN:STANDARD' . "\r\n" .
		'DTSTART:20091101T020000' . "\r\n" .
		'RRULE:FREQ=YEARLY;INTERVAL=1;BYDAY=1SU;BYMONTH=11' . "\r\n" .
		'TZOFFSETFROM:-0400' . "\r\n" .
		'TZOFFSETTO:-0500' . "\r\n" .
		'TZNAME:EST' . "\r\n" .
		'END:STANDARD' . "\r\n" .
		'BEGIN:DAYLIGHT' . "\r\n" .
		'DTSTART:20090301T020000' . "\r\n" .
		'RRULE:FREQ=YEARLY;INTERVAL=1;BYDAY=2SU;BYMONTH=3' . "\r\n" .
		'TZOFFSETFROM:-0500' . "\r\n" .
		'TZOFFSETTO:-0400' . "\r\n" .
		'TZNAME:EDST' . "\r\n" .
		'END:DAYLIGHT' . "\r\n" .
		'END:VTIMEZONE' . "\r\n" .  
		'BEGIN:VEVENT' . "\r\n" .
		'ORGANIZER;CN="'.$from_name.'":MAILTO:'.$from_address. "\r\n" .
		'ATTENDEE;CN="'.$to_name.'";ROLE=REQ-PARTICIPANT;RSVP=TRUE:MAILTO:'.$to_address. "\r\n" .
		'LAST-MODIFIED:' . date("Ymd\TGis") . "\r\n" .
		'UID:'.date("Ymd\TGis", strtotime($startTime)).rand()."@".$domain."\r\n" .
		'DTSTAMP:'.date("Ymd\TGis"). "\r\n" .
		'DTSTART;TZID="Eastern Time":'.date("Ymd\THis", strtotime($startTime)). "\r\n" .
		'DTEND;TZID="Eastern Time":'.date("Ymd\THis", strtotime($endTime)). "\r\n" .
		'TRANSP:OPAQUE'. "\r\n" .
		'SEQUENCE:1'. "\r\n" .
		'SUMMARY:' . $meeting['agenda'] . "\r\n" .
		'LOCATION:' . $location . "\r\n" .
		'CLASS:PUBLIC'. "\r\n" .
		'PRIORITY:5'. "\r\n" .
		'BEGIN:VALARM' . "\r\n" .
		'TRIGGER:-PT15M' . "\r\n" .
		'ACTION:DISPLAY' . "\r\n" .
		'DESCRIPTION:Reminder' . "\r\n" .
		'END:VALARM' . "\r\n" .
		'END:VEVENT'. "\r\n" .
		'END:VCALENDAR'. "\r\n";
		$message .= 'Content-Type: text/calendar;name="meeting.ics";method=REQUEST'."\n";
		$message .= "Content-Transfer-Encoding: 8bit\n\n";
		$message .= $ical;

		$mailsent = mail($to_address, $subject, $message, $headers);

		return ($mailsent)?(true):(false);
	}
    /**
     * Edit method
     *
     * @param string|null $id Meeting id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function editmeeting()
    {
        $this->request->allowMethod(['post', 'put']);
        $id =$this->request->getData('id');
        $meeting = $this->Meetings->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $meeting = $this->Meetings->patchEntity($meeting, $this->request->getData());
            if ($this->Meetings->save($meeting)) {
               $this->set([
                    'status' => 1,
                    'meeting' => $meeting,
                    '_serialize' => ['status', 'meeting']
                ]);
            }else{
                $this->set([
                    'status' =>0,
                    'meeting' => $meeting,
                    '_serialize' => ['status', 'meeting']
                ]);
            }
           
        }
        // $salesmen = $this->Meetings->Salesmen->find('list', ['limit' => 200]);
        // $this->set(compact('meeting', 'salesmen'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Meeting id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $meeting = $this->Meetings->get($id);
        if ($this->Meetings->delete($meeting)) {
            $this->Flash->success(__('The meeting has been deleted.'));
        } else {
            $this->Flash->error(__('The meeting could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
