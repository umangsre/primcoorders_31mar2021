<?php
use Cake\Routing\Router;
 use \PHPQRCode\QRcode;
$this->Form->setTemplates([
    'inputContainer' => '{{content}}'
]);
 $link =  Router::url([ 'controller' => 'Users', 'action' => 'login'],TRUE);
    //$downLoad_link = $site_url.'order_pdfs/'.$file_name;
    $path_qr = WWW_ROOT .'/img/';
  $image_name = 'Login_QRCode.png';
    $qrcode_img = $path_qr.$image_name;
    // $ecc stores error correction capability('L')
    $ecc = 'L';
    $pixel_Size = 5;
    $frame_Size = 5;
    // Generates QR Code and Stores it in directory given
   QRcode::png($link, $qrcode_img, $ecc, $pixel_Size, $frame_Size);
?>
<div class="col s12 m6 l4 z-depth-4 card-panel border-radius-6 login-card bg-opacity-8">
	<div class="text-center" style="text-align: center; padding-top: 20px;">
                      <?php if($image_name): ?>

                           <?= $this->Html->image('/img/'.$image_name); ?>
                        <?php endif; ?>
    </div>
    <?= $this->Form->create() ?>
      <div class="row">
        <div class="input-field col s12">
          <h5 class="ml-4">Sign in</h5>
        </div>
      </div>
      <div class="row margin">
        <p><?= $this->Flash->render() ?></p>
      </div>
      <div class="row margin">
        <div class="input-field col s12">
          <i class="material-icons prefix pt-2">person_outline</i>
          <!-- <input id="username" type="text"> -->
          <?= $this->Form->control('username', [ 'label' => false, 'div' => false, 'id' => 'username', 'autocomplete' =>'off' ] ) ?>
          <label for="username" class="center-align">Username</label>

        </div>
      </div>
      <div class="row margin">
        <div class="input-field col s12">
          <i class="material-icons prefix pt-2">lock_outline</i>
          <!-- <input id="password" type="password"> -->
          <?= $this->Form->control('password', [ 'label' => false, 'div' => false, 'id' => 'password', 'autocomplete' =>'off' ]) ?>
          <label for="password">Password</label>
        </div>
      </div>
      <!-- <div class="row">
        <div class="col s12 m12 l12 ml-2 mt-1">
          <p>
            <label>
              <input type="checkbox" />
              <span>Remember Me</span>
            </label>
          </p>
        </div>
      </div> -->
      <div class="row">
        <div class="input-field col s12">

          <?= $this->Form->button(__('Login'), [ 'class' => "btn waves-effect waves-light border-round gradient-45deg-purple-deep-orange col s12" ] ); ?>
        </div>
      </div>
      <div class="row">
	   <div class="input-field col s12">
			<?php echo $this->Html->link(
						'Place Order',
						['controller' => 'Orders', 'action' => 'add'],
						['class' => 'btn btn-primary border-round col s12']
					);
					?>
		</div>
		<div class="input-field col s12">
			<?php echo $this->Html->link(
						'Add Customer',
						['controller' => 'CreditApplications', 'action' => 'addCustomer'],
						['class' => 'btn btn-warning border-round col s12']
					);
					?>
		</div>
		<!--div class="input-field col s12">
			<?php echo $this->Html->link(
						'Add Claim',
						['controller' => 'Claims', 'action' => 'addclaim'],
						['class' => 'btn btn-success border-round col s12']
					);
					?>
		</div -->
        <!-- <div class="input-field col s6 m6 l6">
          <p class="margin medium-small"><a href="user-register.html">Register Now!</a></p>
        </div>
        <div class="input-field col s6 m6 l6">
          <p class="margin right-align medium-small"><a href="user-forgot-password.html">Forgot password ?</a></p>
        </div> -->
      </div>
    <?= $this->Form->end() ?>
  </div>
