<?php

namespace App\Controller\Api;

use App\Controller\AppController;


class FileController extends AppController
{
    public function add()
    {
        !$this->request->is('post') && exit;
        $file = $this->request->getData()['file'];
        $image_parts = explode(";base64,", $file);
        $image_type_aux = explode("image/", $image_parts[0]);
        $image_type = $image_type_aux[1];
        $image_base64 = base64_decode($image_parts[1]);
        $uniqueName = preg_replace('/[^A-Za-z0-9\-]/', '', uniqid());
        $newFileName = $uniqueName . '.' . $image_type;
        $folderName = WWW_ROOT.'uploads';
        if (!file_exists($folderName)) {
            mkdir($folderName);
        }
        $folderName .= '/files/';
        if (!file_exists($folderName)) {
            mkdir($folderName);
        }
        $filePath = $folderName . $newFileName;
        $uploadFile = file_put_contents($filePath, $image_base64);
        if ($uploadFile) {
            $this->response->body(json_encode(['status' => 'success', 'filePath' => '//' . $newFileName]));
        } else {
            $this->response->body(json_encode(['status' => 'error']));
        }
        return $this->response;
    }

    public function delete() {
        !$this->request->is('post') && exit;
        $file = WWW_ROOT.$this->request->getData()['filePath'];
        !file_exists($file) && exit;
        $deleteFile = unlink($file);
        if ($deleteFile) {
            $this->response->body(json_encode(['status' => 'success']));
        } else {
            $this->response->body(json_encode(['status' => 'error']));
        }
        return $this->response;
    }
}
