
    <div class="row">
                <div class="col-12 ">
                    <div class="text-center"> 
                      <div class="logo-container p-4">
					 
					   <?= $this->Html->image('logo.png'); ?> 
					  </div>	
					
					
						<h2 class="m-4">Thank you so much for placing your order with Primco</h2> 					
						  <p>
						  <?php echo $this->Html->link(
							'Back to Home',
							['controller' => 'Users', 'action' => 'login'],
							['class' => 'btn btn-primary']
						); ?>

					   <?php 
						echo $this->Html->link(
						'Edit Order',
						['controller' => 'Orders', 'action' => 'editorder',base64_encode($order_id )],
						['class' => 'btn btn-warning']
					  );

					  ?>


						<?php echo $this->Html->link(
							'Preview and Download',
							['controller' => 'Orders', 'action' => 'pdf_view',base64_encode($order_id )],
							['class' => 'btn btn-info']
						); ?>

						  </p>
						  <div class="qrcode_img">
							<?php if($image_name): ?>

							   <?= $this->Html->image('/img/QRcode/'.$image_name); ?> 
							<?php endif; ?>
						  </div>
					</div> 
                    
            </div>
    </div>