<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Bill Entity
 *
 * @property int $id
 * @property \Cake\I18n\FrozenDate|null $billing_date
 * @property string|null $billing_type_of_expense
 * @property string|null $billing_place_of_expense
 * @property float|null $billing_amount
 * @property string|null $billing_tax
 * @property float|null $billing_tax_amount
 * @property float|null $tip_amount
 * @property float|null $billing_total_amount
 * @property string|null $billing_image
 * @property string $billing_status
 * @property int $billing_userid
 * @property string|null $billing_comment
 * @property string|null $company_credit_card
 * @property \Cake\I18n\FrozenTime $created
 */
class Bill extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'billing_date' => true,
        'billing_type_of_expense' => true,
        'billing_place_of_expense' => true,
        'billing_amount' => true,
        'billing_tax' => true,
        'billing_tax_amount' => true,
        'tip_amount' => true,
        'billing_total_amount' => true,
        'billing_image' => true,
        'billing_status' => true,
        'billing_userid' => true,
        'billing_comment' => true,
        'company_credit_card' => true,
        'created' => true
    ];
}
