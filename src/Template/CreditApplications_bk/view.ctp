<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\CreditApplication $creditApplication
 */
?>
<!--nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Credit Application'), ['action' => 'edit', $creditApplication->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Credit Application'), ['action' => 'delete', $creditApplication->id], ['confirm' => __('Are you sure you want to delete # {0}?', $creditApplication->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Credit Applications'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Credit Application'), ['action' => 'add']) ?> </li>
    </ul>
</nav -->
<div class="orders index large-9 medium-8 columns content card card card-default scrollspy">
    <div class="card-content">
        <div class="creditApplications view large-9 medium-8 columns content">
            <h3><?= h($creditApplication->id) ?></h3>
            <table class="vertical-table">
                <tr>
                    <th scope="row"><?= __('Company Legal Name') ?></th>
                    <td><?= h($creditApplication->company_legal_name) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Company Trade Name') ?></th>
                    <td><?= h($creditApplication->company_trade_name) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Street Address') ?></th>
                    <td><?= h($creditApplication->street_address) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Phone') ?></th>
                    <td><?= h($creditApplication->phone) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Fax') ?></th>
                    <td><?= h($creditApplication->fax) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('City') ?></th>
                    <td><?= h($creditApplication->city) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Province') ?></th>
                    <td><?= h($creditApplication->province) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Postal Code') ?></th>
                    <td><?= h($creditApplication->postal_code) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Email Address') ?></th>
                    <td><?= h($creditApplication->email_address) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Owner Name') ?></th>
                    <td><?= h($creditApplication->owner_name) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Owner Home Address') ?></th>
                    <td><?= h($creditApplication->owner_home_address) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Owner Home Phone') ?></th>
                    <td><?= h($creditApplication->owner_home_phone) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Owner Cell Phone') ?></th>
                    <td><?= h($creditApplication->owner_cell_phone) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Manager Name') ?></th>
                    <td><?= h($creditApplication->manager_name) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Manager Phone') ?></th>
                    <td><?= h($creditApplication->manager_phone) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Manager Finacial Address') ?></th>
                    <td><?= h($creditApplication->manager_finacial_address) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Manager Bank Account') ?></th>
                    <td><?= h($creditApplication->manager_bank_account) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Manager City') ?></th>
                    <td><?= h($creditApplication->manager_city) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Manager Province') ?></th>
                    <td><?= h($creditApplication->manager_province) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Manager Postal Code') ?></th>
                    <td><?= h($creditApplication->manager_postal_code) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Manager Phone 2') ?></th>
                    <td><?= h($creditApplication->manager_phone_2) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Manager Gst') ?></th>
                    <td><?= h($creditApplication->manager_gst) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Province Sales Tax') ?></th>
                    <td><?= h($creditApplication->province_sales_tax) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Amount Credit Required') ?></th>
                    <td><?= h($creditApplication->amount_credit_required) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Yearly Sales Volume') ?></th>
                    <td><?= h($creditApplication->yearly_sales_volume) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Fiscal Year End') ?></th>
                    <td><?= h($creditApplication->fiscal_year_end) ?></td>
                </tr>
        		 <tr>
                    <th scope="row"><?= __('Is Group') ?></th>
                    <td><?= $creditApplication->is_group?'YES':'NO' ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Group Name') ?></th>
                    <td><?= h($creditApplication->group_name) ?></td>
                </tr>
                
        		
                <tr>
                    <th scope="row"><?= __('Id') ?></th>
                    <td><?= $this->Number->format($creditApplication->id) ?></td>
                </tr>
               
            </table>
        </div>
    </div>
</div>
