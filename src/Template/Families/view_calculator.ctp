<style>
.input.text input, .input.number input {
    width: 100%;
	border-bottom: 1px solid;
    border-top: 0;
    border-left: 0;
    border-right: 0;
}
.col-md-6 {
    position: inherit;
    float: left;
}
.col-md-3 {
    position: inherit;
    float: left;
}
.border-heading h3 {
    font-size: 20px;
}
.header_input {
    border-bottom: 1px solid;
    border-top: 0;
    border-left: 0;
    border-right: 0;
    width: 100%;
}
.table-form td.price-ls {
    padding: 11px;
}
.table-bordered thead th {
    border-bottom-width: 0px;
    padding: 15px 0 !important;
    background: #eee;
}
.border-heading h3 {
    padding: 14px 0;
    background: #eee;
    text-align: center;
    margin-top: 10px;
}
.list_wraper {
    top: 28px;
}
.list_wraper {
    top: 28px;
    height: 300px;
    overflow: auto;
    width: 300px;
}
li.item_pricing {
    border-bottom: 1px solid;
    padding: 2px 10px;
    font-size: 15px;
}
</style>
<div class="row">
    <div class="col-12 logo-container text-center p-4">
        <?= $this->Html->image('logo.png'); ?>           
    </div>
</div>
<input type="hidden" id="family_id" value="<?=$id?>" >
	 
     <div class="container-fluid mt-2">
    
            <div class="row">
			<h3 style="background: #eee;padding: 10px 20px;border-radius: 2px;">Margin Calculator</h3> 
                <!--   <h2><?= __('Margin Calculator') ?></h2> -->
                <div class="tab w-100">
				
				
				
				 <div class='col-md-12'>
				 <form>
                                <div class="col-md-3">
                                    <?php
                                 echo $this->Form->control('search_name', ['class' =>'search_name_margin header_input' , 'label'=>false,
                                 'placeholder' =>'Search Product Name or Product Code or Product Families','autocomplete' =>'off','data-link' =>'/families/findMarginbyname']);
                                  ?>
                                  <div id="suggesstion-pricing" class="list_wraper" style="display:none;">    
                                  </div>
                                </div>
								<div class="col-md-3">
								<?php
									echo $this->Form->control('defaut_dropdown', ['class' =>'defaut_dropdown header_input' ,'id' =>'defaut_dropdown' , 'label'=>false, 
                        'options' =>[
                                        'defaut' =>'Defaut',
                                        'manually' =>'Dollar'
                                    ]
                                ]);
								
								?>
								</div>
								<div class="col-md-3">
									 <?php
                                 echo $this->Form->control('defaut_value', ['class' =>'defaut_value float-number header_input' ,'id' =>'defaut_value' , 'label'=>false, 'readonly' =>true, 'onkeyup'=>'getdefaut(); gettmsdefaut();','keypress'=>'tmsgetdefaut();']);
                                  ?>
								</div>

                                 <div class="col-md-3">
                                    <?= $this->Form->button(__('Reset'), array( 'class' => 'btn btn-success pricing_reset', 'type' => 'reset' )  ) ?>
                                 </div>
								 </form>
                            </div> 
							
				<div class="col-md-6">
				<div class="border-heading">
				<h3>Current Dollar Rate Current Margin and Current Selling Price</h3>
				<table class="table table-bordered ">
                             <thead>
                            <tr>
                                <th>Slab Type</th>
                                <th>Sell Price</th>
                                <th>Rebate</th>
                                <th>Gross margin(In Percentage)</th>
                                
                                
                            </tr>
                        </thead> 
                        <tbody>   
                             <tr class="product_row">

                                <td class="price-ls" id="slab_sm_1"> Slab 1</td>
                                <td class="price-ls" id="sell_sm_1"> </td>
                                <td class="price-ls" id="rebate_sm_1"></td>
                                <td class="price-ls" id="gross_sm_1"></td>
                                
                             </tr> 

                             <tr class="product_row">

                                <td class="price-ls" id="slab_sm_2">Slab 2 </td>
                                <td class="price-ls" id="sell_sm_2">  </td>
                                <td class="price-ls" id="rebate_sm_2"></td>
                                <td class="price-ls" id="gross_sm_2"></td>
                               
                             </tr>  
                             <tr class="product_row">

                                <td class="price-ls" id="slab_sm_3"> Slab 3</td>
                                <td class="price-ls" id="sell_sm_3"></td>
                                <td class="price-ls" id="rebate_sm_3"></td>
                                <td class="price-ls" id="gross_sm_3"></td>
                            
                             </tr> 

                            
                            </tbody>
                        </table>
				</div>
				</div>
				
				<div class="col-md-6">
				<div class="border-heading">
				<h3>New Dollar Rate, Old Selling Price & New Margin</h3>
                    
                 
                            <table  class="table table-bordered table-form">
                             <thead>
                            <tr>
                                <!--th>Sr.No.</th-->
                                <th>Slab Type</th>
                                <th>Sell Price</th>
                                <th>Rebate</th>
                                <th>Gross margin(In Percentage)</th>
                                
                                
                            </tr>
                        </thead> 
                        <tbody id="pricing-matrix">   
                             <tr class="product_row">

                                <td class="price-ls"><?php  echo $this->Form->control('slevel',['value' =>'Slab 1','id' =>'slab_1','label' => false, 'readonly' => true]); ?></td>
                                <td class="price-ls"> <?php echo $this->Form->control('sell_price_level1',['label' => false,'readonly' => true,'class' =>'sell_price']); ?>
                                </td>
                                <td class="price-ls"><?php echo $this->Form->control('rebate_level1',['class' =>'rebate','label' => false,'readonly' => true]); ?></td>
                                <td class="price-ls"><?php  echo $this->Form->control('gross_margin_level1',['class' =>'gross_margin','label' => false,'readonly' => true]); ?></td>
                                
                             </tr> 

                             <tr class="product_row">

                                <td class="price-ls"><?php  echo $this->Form->control('slevel',['value' =>'Slab 2','id' =>'slab_2','label' => false,'readonly' => true]); ?></td>
                                <td class="price-ls"> <?php echo $this->Form->control('sell_price_level2',['label' => false,'readonly' => true,'class' =>'sell_price']); ?>
                                </td>
                                <td class="price-ls"><?php echo $this->Form->control('rebate_level2',['class' =>'rebate','label' => false,'readonly' => true]); ?></td>
                                <td class="price-ls"><?php  echo $this->Form->control('gross_margin_level2',['class' =>'gross_margin','label' => false,'readonly' => true]); ?></td>
                                 
                             </tr>  
                             <tr class="product_row">

                                <td class="price-ls"><?php  echo $this->Form->control('slevel',['value' =>'Slab 3','id' =>'slab_3','label' => false,'readonly' => true]); ?></td>
                                <td class="price-ls"> <?php echo $this->Form->control('sell_price_level3',['label' => false,'class' =>'sell_price']); ?>
                                </td>
                                <td class="price-ls"><?php echo $this->Form->control('rebate_level3',['class' =>'rebate','label' => false,'readonly' => true]); ?></td>
                                <td class="price-ls"><?php  echo $this->Form->control('gross_margin_level3',['class' =>'gross_margin','label' => false,'readonly' => true]); ?>
                                    
                                </td>
                                
                             </tr> 

                                
                            </tbody>
                        </table>
					
                           
                  
						
						</div>
						</div>
						<div class="col-md-6">
						<div class="border-heading">
						<h3>New Dollar Rate &  New Selling Price and Old Margin</h3>
                 
                            <table  class="table table-bordered table-form">
                             <thead>
                            <tr>
                                <!--th>Sr.No.</th-->
                                <th>Slab Type</th>
                                <th>Sell Price</th>
                                <th>Rebate</th>
                                <th>Gross margin(In Percentage)</th>
                           
                                
                            </tr>
                        </thead> 
                        <tbody id="pricing-matrix-new">   
                             <tr class="new_product_row">

                                <td class="price-ls"><?php  echo $this->Form->control('slevel',['value' =>'Slab 1','id' =>'slab_ms_1','label' => false, 'readonly' => true]); ?></td>
                                <td class="price-ls"> <?php echo $this->Form->control('sell_price_level1',['label' => false,'readonly' => true,'class' =>' new_sell_price','id' =>'sell_ms_1']); ?>
                                </td>
                                <td class="price-ls"><?php echo $this->Form->control('rebate_level1',['class' =>' new_rebate','label' => false,'readonly' => true,'id' =>'rebate_ms_1']); ?></td>
                                <td class="price-ls"><?php  echo $this->Form->control('gross_margin_level1',['class' =>' new_gross_margin','label' => false,'readonly' => true,'id' =>'gross_ms_1']); ?></td>
                                
                             </tr> 

                             <tr class="new_product_row">

                                <td class="price-ls"><?php  echo $this->Form->control('slevel',['value' =>'Slab 2','id' =>'slab_2','label' => false,'readonly' => true]); ?></td>
                                <td class="price-ls"> <?php echo $this->Form->control('sell_price_level2',['label' => false,'readonly' => true,'class' =>' new_sell_price','id' =>'sell_ms_2']); ?>
                                </td>
                                <td class="price-ls"><?php echo $this->Form->control('rebate_level2',['class' =>' new_rebate','label' => false,'readonly' => true,'id' =>'rebate_ms_2']); ?></td>
                                <td class="price-ls"><?php  echo $this->Form->control('gross_margin_level2',['class' =>' new_gross_margin','label' => false,'readonly' => true,'id' =>'gross_ms_2']); ?></td>
                                 
                             </tr>  
                             <tr class="new_product_row">

                                <td class="price-ls"><?php  echo $this->Form->control('slevel',['value' =>'Slab 3','id' =>'slab_3','label' => false,'readonly' => true]); ?></td>
                                <td class="price-ls"> <?php echo $this->Form->control('sell_price_level3',['label' => false,'class' =>' new_sell_price','readonly' => true,'id' =>'sell_ms_3']); ?>
                                </td>
                                <td class="price-ls"><?php echo $this->Form->control('rebate_level3',['class' =>' new_rebate','label' => false,'readonly' => true,'id' =>'rebate_ms_3']); ?></td>
                                <td class="price-ls"><?php  echo $this->Form->control('gross_margin_level3',['class' =>' new_gross_margin','label' => false,'readonly' => true,'id' =>'gross_ms_3']); ?>
                                    
                                </td>
                                
                             </tr> 

                                
                            </tbody>
                        </table>

                           
                    
						
						</div>
						</div>
						<div class="col-md-6">
						<div class="border-heading">
						<h3>New Dollar Rate, New Selling Price & New Margin </h3>
                 
                            <table  class="table table-bordered table-form">
                             <thead>
                            <tr>
                                <!--th>Sr.No.</th-->
                                <th>Slab Type</th>
                                <th>Sell Price</th>
                                <th>Rebate</th>
                                <th>Gross margin(In Percentage)</th>
                           
                                
                            </tr>
                        </thead> 
                        <tbody id="pricing-matrix-tms">   
                             <tr class="tms_product_row">

                                <td class="price-ls"><?php  echo $this->Form->control('slevel',['value' =>'Slab 1','id' =>'slab_tms_1','label' => false, 'readonly' => true]); ?></td>
                                <td class="price-ls"> <?php echo $this->Form->control('sell_price_level1',['label' => false,'onkeyup'=>'tmsgetdefaut()','class' =>'tms_sell_price','id' =>'sell_tms_1']); ?>
                                </td>
                                <td class="price-ls"><?php echo $this->Form->control('rebate_level1',['class' =>'tms_rebate','label' => false,'onkeyup'=>'tmsgetdefaut()','id' =>'rebate_tms_1']); ?></td>
                                <td class="price-ls"><?php  echo $this->Form->control('gross_margin_level1',['class' =>'tms_gross_margin','label' => false,'id' =>'gross_tms_1','onkeyup'=>'tms_get_defaut()','pattern'=>'^\d*(\.\d{0,2})?$']); ?></td>
                                
                             </tr> 

                             <tr class="tms_product_row">

                                <td class="price-ls"><?php  echo $this->Form->control('slevel',['value' =>'Slab 2','id' =>'slab_2','label' => false,'readonly' => true]); ?></td>
                                <td class="price-ls"> <?php echo $this->Form->control('sell_price_level2',['label' => false,'onkeyup'=>'tmsgetdefaut()','class' =>'tms_sell_price','id' =>'sell_tms_2']); ?>
                                </td>
                                <td class="price-ls"><?php echo $this->Form->control('rebate_level2',['class' =>'tms_rebate','label' => false,'onkeyup'=>'tmsgetdefaut()','id' =>'rebate_tms_2']); ?></td>
                                <td class="price-ls"><?php  echo $this->Form->control('gross_margin_level2',['class' =>'tms_gross_margin','label' => false,'id' =>'gross_tms_2','onkeyup'=>'tms_get_defaut()']); ?></td>
                                 
                             </tr>  
                             <tr class="tms_product_row">

                                <td class="price-ls"><?php  echo $this->Form->control('slevel',['value' =>'Slab 3','id' =>'slab_3','label' => false,'readonly' => true]); ?></td>
                                <td class="price-ls"> <?php echo $this->Form->control('sell_price_level3',['label' => false,'class' =>'tms_sell_price','onkeyup'=>'tmsgetdefaut()','id' =>'sell_tms_3']); ?>
                                </td>
                                <td class="price-ls"><?php echo $this->Form->control('rebate_level3',['class' =>'tms_rebate','label' => false,'onkeyup'=>'tmsgetdefaut()','id' =>'rebate_tms_3']); ?></td>
                                <td class="price-ls"><?php  echo $this->Form->control('gross_margin_level3',['class' =>'tms_gross_margin','label' => false,'id' =>'gross_tms_3','onkeyup'=>'tms_get_defaut()']); ?>
                                    
                                </td>
                                
                             </tr> 

                                
                            </tbody>
                        </table>

                           
                    
						
						</div>
						</div>
                    </div>
                </div>
          
    </div>
