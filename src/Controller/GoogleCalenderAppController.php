<?php
namespace App\Controller;

use App\Controller\AppController;


/**
 * GoogleCalenderApp Controller
 *
 *
 * @method \App\Model\Entity\GoogleCalenderApp[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class GoogleCalenderAppController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $googleCalenderApp = $this->paginate($this->GoogleCalenderApp);

        $this->set(compact('googleCalenderApp'));
    }

    /**
     * View method
     *
     * @param string|null $id Google Calender App id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $googleCalenderApp = $this->GoogleCalenderApp->get($id, [
            'contain' => []
        ]);

        $this->set('googleCalenderApp', $googleCalenderApp);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $googleCalenderApp = $this->GoogleCalenderApp->newEntity();
        if ($this->request->is('post')) {
            $googleCalenderApp = $this->GoogleCalenderApp->patchEntity($googleCalenderApp, $this->request->getData());
            if ($this->GoogleCalenderApp->save($googleCalenderApp)) {
                $this->Flash->success(__('The google calender app has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The google calender app could not be saved. Please, try again.'));
        }
        $this->set(compact('googleCalenderApp'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Google Calender App id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $googleCalenderApp = $this->GoogleCalenderApp->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $googleCalenderApp = $this->GoogleCalenderApp->patchEntity($googleCalenderApp, $this->request->getData());
            if ($this->GoogleCalenderApp->save($googleCalenderApp)) {
                $this->Flash->success(__('The google calender app has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The google calender app could not be saved. Please, try again.'));
        }
        $this->set(compact('googleCalenderApp'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Google Calender App id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $googleCalenderApp = $this->GoogleCalenderApp->get($id);
        if ($this->GoogleCalenderApp->delete($googleCalenderApp)) {
            $this->Flash->success(__('The google calender app has been deleted.'));
        } else {
            $this->Flash->error(__('The google calender app could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
