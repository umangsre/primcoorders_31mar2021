<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\ContactsList[]|\Cake\Collection\CollectionInterface $contactsList
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Contacts List'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Credit Applications'), ['controller' => 'CreditApplications', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Credit Application'), ['controller' => 'CreditApplications', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="contactsList index large-9 medium-8 columns content">
    <h3><?= __('Contacts List') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('credit_application_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('statement_name') ?></th>
                <th scope="col"><?= $this->Paginator->sort('statement_email') ?></th>
                <th scope="col"><?= $this->Paginator->sort('invoice_name') ?></th>
                <th scope="col"><?= $this->Paginator->sort('invoice_email') ?></th>
                <th scope="col"><?= $this->Paginator->sort('price_pages_name') ?></th>
                <th scope="col"><?= $this->Paginator->sort('price_pages_email') ?></th>
                <th scope="col"><?= $this->Paginator->sort('order_confirmation_name') ?></th>
                <th scope="col"><?= $this->Paginator->sort('order_confirmation_email') ?></th>
                <th scope="col"><?= $this->Paginator->sort('bank_order_name') ?></th>
                <th scope="col"><?= $this->Paginator->sort('bank_order_email') ?></th>
                <th scope="col"><?= $this->Paginator->sort('purchasing_agents_name') ?></th>
                <th scope="col"><?= $this->Paginator->sort('purchasing_agents_email') ?></th>
                <th scope="col"><?= $this->Paginator->sort('_date') ?></th>
                <th scope="col"><?= $this->Paginator->sort('authorized_signature') ?></th>
                <th scope="col"><?= $this->Paginator->sort('print_name') ?></th>
                <th scope="col"><?= $this->Paginator->sort('_title') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($contactsList as $contactsList): ?>
            <tr>
                <td><?= $this->Number->format($contactsList->id) ?></td>
                <td><?= $contactsList->has('credit_application') ? $this->Html->link($contactsList->credit_application->id, ['controller' => 'CreditApplications', 'action' => 'view', $contactsList->credit_application->id]) : '' ?></td>
                <td><?= h($contactsList->statement_name) ?></td>
                <td><?= h($contactsList->statement_email) ?></td>
                <td><?= h($contactsList->invoice_name) ?></td>
                <td><?= h($contactsList->invoice_email) ?></td>
                <td><?= h($contactsList->price_pages_name) ?></td>
                <td><?= h($contactsList->price_pages_email) ?></td>
                <td><?= h($contactsList->order_confirmation_name) ?></td>
                <td><?= h($contactsList->order_confirmation_email) ?></td>
                <td><?= h($contactsList->bank_order_name) ?></td>
                <td><?= h($contactsList->bank_order_email) ?></td>
                <td><?= h($contactsList->purchasing_agents_name) ?></td>
                <td><?= h($contactsList->purchasing_agents_email) ?></td>
                <td><?= h($contactsList->_date) ?></td>
                <td><?= h($contactsList->authorized_signature) ?></td>
                <td><?= h($contactsList->print_name) ?></td>
                <td><?= h($contactsList->_title) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $contactsList->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $contactsList->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $contactsList->id], ['confirm' => __('Are you sure you want to delete # {0}?', $contactsList->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div>
