<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\BillTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\BillTable Test Case
 */
class BillTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\BillTable
     */
    public $Bill;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.Bill'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('Bill') ? [] : ['className' => BillTable::class];
        $this->Bill = TableRegistry::getTableLocator()->get('Bill', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Bill);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
