<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\CreditApplication[]|\Cake\Collection\CollectionInterface $creditApplications
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Credit Application'), ['action' => 'add']) ?></li>
    </ul>
</nav>
<div class="creditApplications index large-9 medium-8 columns content">
    <h3><?= __('Credit Applications') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('company_legal_name') ?></th>
                <th scope="col"><?= $this->Paginator->sort('company_trade_name') ?></th>
                <th scope="col"><?= $this->Paginator->sort('street_address') ?></th>
                <th scope="col"><?= $this->Paginator->sort('phone') ?></th>
                <th scope="col"><?= $this->Paginator->sort('fax') ?></th>
                <th scope="col"><?= $this->Paginator->sort('city') ?></th>
                <th scope="col"><?= $this->Paginator->sort('province') ?></th>
                <th scope="col"><?= $this->Paginator->sort('postal_code') ?></th>
                <th scope="col"><?= $this->Paginator->sort('email_address') ?></th>
                <th scope="col"><?= $this->Paginator->sort('owner_name') ?></th>
                <th scope="col"><?= $this->Paginator->sort('owner_home_address') ?></th>
                <th scope="col"><?= $this->Paginator->sort('owner_home_phone') ?></th>
                <th scope="col"><?= $this->Paginator->sort('owner_cell_phone') ?></th>
                <th scope="col"><?= $this->Paginator->sort('manager_name') ?></th>
                <th scope="col"><?= $this->Paginator->sort('manager_phone') ?></th>
                <th scope="col"><?= $this->Paginator->sort('manager_finacial_address') ?></th>
                <th scope="col"><?= $this->Paginator->sort('manager_bank_account') ?></th>
                <th scope="col"><?= $this->Paginator->sort('manager_city') ?></th>
                <th scope="col"><?= $this->Paginator->sort('manager_province') ?></th>
                <th scope="col"><?= $this->Paginator->sort('manager_postal_code') ?></th>
                <th scope="col"><?= $this->Paginator->sort('manager_phone_2') ?></th>
                <th scope="col"><?= $this->Paginator->sort('manager_gst') ?></th>
                <th scope="col"><?= $this->Paginator->sort('province_sales_tax') ?></th>
                <th scope="col"><?= $this->Paginator->sort('amount_credit_required') ?></th>
                <th scope="col"><?= $this->Paginator->sort('yearly_sales_volume') ?></th>
                <th scope="col"><?= $this->Paginator->sort('fiscal_year_end') ?></th>
                <th scope="col"><?= $this->Paginator->sort('is_group') ?></th>
                <th scope="col"><?= $this->Paginator->sort('group_name') ?></th>
                <th scope="col"><?= $this->Paginator->sort('_date') ?></th>
                <th scope="col"><?= $this->Paginator->sort('authorized_signature') ?></th>
                <th scope="col"><?= $this->Paginator->sort('print_name') ?></th>
                <th scope="col"><?= $this->Paginator->sort('_title') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($creditApplications as $creditApplication): ?>
            <tr>
                <td><?= $this->Number->format($creditApplication->id) ?></td>
                <td><?= h($creditApplication->company_legal_name) ?></td>
                <td><?= h($creditApplication->company_trade_name) ?></td>
                <td><?= h($creditApplication->street_address) ?></td>
                <td><?= h($creditApplication->phone) ?></td>
                <td><?= h($creditApplication->fax) ?></td>
                <td><?= h($creditApplication->city) ?></td>
                <td><?= h($creditApplication->province) ?></td>
                <td><?= h($creditApplication->postal_code) ?></td>
                <td><?= h($creditApplication->email_address) ?></td>
                <td><?= h($creditApplication->owner_name) ?></td>
                <td><?= h($creditApplication->owner_home_address) ?></td>
                <td><?= h($creditApplication->owner_home_phone) ?></td>
                <td><?= h($creditApplication->owner_cell_phone) ?></td>
                <td><?= h($creditApplication->manager_name) ?></td>
                <td><?= h($creditApplication->manager_phone) ?></td>
                <td><?= h($creditApplication->manager_finacial_address) ?></td>
                <td><?= h($creditApplication->manager_bank_account) ?></td>
                <td><?= h($creditApplication->manager_city) ?></td>
                <td><?= h($creditApplication->manager_province) ?></td>
                <td><?= h($creditApplication->manager_postal_code) ?></td>
                <td><?= h($creditApplication->manager_phone_2) ?></td>
                <td><?= h($creditApplication->manager_gst) ?></td>
                <td><?= h($creditApplication->province_sales_tax) ?></td>
                <td><?= h($creditApplication->amount_credit_required) ?></td>
                <td><?= h($creditApplication->yearly_sales_volume) ?></td>
                <td><?= h($creditApplication->fiscal_year_end) ?></td>
                <td><?= $this->Number->format($creditApplication->is_group) ?></td>
                <td><?= h($creditApplication->group_name) ?></td>
                <td><?= h($creditApplication->_date) ?></td>
                <td><?= h($creditApplication->authorized_signature) ?></td>
                <td><?= h($creditApplication->print_name) ?></td>
                <td><?= h($creditApplication->_title) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $creditApplication->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $creditApplication->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $creditApplication->id], ['confirm' => __('Are you sure you want to delete # {0}?', $creditApplication->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div>
