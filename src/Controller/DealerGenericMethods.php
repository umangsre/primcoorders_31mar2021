<?php


namespace App\Controller;


use Cake\I18n\Time;

class DealerGenericMethods extends AppController
{
    public function initialize()
    {
        parent::initialize();
        $this->loadModel('Customers');
        $this->loadModel('Users');
    }

    public function getAllDealers($search = false, $searchData = '') {
        $query = $this->Customers->find('all');
        if ($search) {
            if (!empty($searchData['name'])) {
                $name = $searchData['name'];
                $query = $query->where(['Customers.customer_name LIKE' => '%' . $name . '%']);
            }
            if (!empty($searchData['email'])) {
                $email = $searchData['email'];
                $query = $query->where(['Customers.customer_email LIKE' => '%' . $email . '%']);
            }
            if (!empty($searchData['phone'])) {
                $phone = $searchData['phone'];
                $query = $query->where(['Customers.phone LIKE' => '%' . $phone . '%']);
            }
        }
        return $query;
    }

    public function getDealerDetails($dealerId) {
        return $this->Customers->find()->where(['Customers.user_id' => $dealerId])->first();
    }

    public function addDealerToUsers($details, $password) {
        $addDealerToUsers = $this->Users->newEntity();
        $userDetails = [
            'username' => $details['customer_email'],
            'password' => $password,
            'role' => 'dealer',
            'created' => new Time()
        ];
        $addDealerToUsers = $this->Users->patchEntity($addDealerToUsers, $userDetails);
        if ($this->Users->save($addDealerToUsers)) {
            return $addDealerToUsers;
        }
    }

    public function addDealer($details, $user_id) {
            $addDealer = $this->Customers->newEntity();
            $dealerDetails = [
                'customerid' => $user_id,
                'name' => $details['customer_name'],
                'contact_name' => $details['contact_name'],
                'phone' => $details['phone'],
                'address' => $details['address'],
                'city' => $details['city'],
                'postal_code' => $details['postal_code'],
                'created' => new Time()
            ];
            $addDealer = $this->Customers->patchEntity($addDealer, $dealerDetails);
            if ($this->Customers->save($dealerDetails)) {
                return $addDealer;
            }
    }

    public function updateDealer($dealerId, $dealerData) {
        $updateDealer = $this->Customers->get($dealerId, [
            'contain' => []
        ]);
        $updateDealer = $this->Customers->patchEntity($updateDealer, $dealerData);
        if ($this->Customers->save($updateDealer)) {
            return $updateDealer;
        }
    }

    public function createUniquePassword() {
        $passwordString = '1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZabcefghijklmnopqrstuvwxyz';
        return substr(str_shuffle($passwordString), 0, rand(8, 20));
    }
}
