<?php
 use Cake\Routing\Router;
 use \PHPQRCode\QRcode;
 //\PHPQRCode\QRcode::png("Test", "/tmp/qrcode.png", 'L', 4, 2);
 // if($order_id){
	// // $downLoad_link=$actual_link."/product-form/general_pdf.php?id=".$encoded_id;
	//  $link =  Router::url([ 'controller' => 'Orders', 'action' => 'pdf_view',base64_encode($order_id )
	// 					  ],TRUE);				  
 //    //$downLoad_link = $site_url.'order_pdfs/'.$file_name;
 //    $path_qr = WWW_ROOT .'/img/QRcode/'; 
	// $image_name = 'qr_codes_'.$order_id."_order.png"; 
 //    $qrcode_img = $path_qr.$image_name;
 //    // $ecc stores error correction capability('L') 
 //    $ecc = 'L'; 
 //    $pixel_Size = 10; 
 //    $frame_Size = 10;     
 //    // Generates QR Code and Stores it in directory given 
 //   QRcode::png($link, $qrcode_img, $ecc, $pixel_Size, $frame_size); 
 // }
?> 
    <div class="row">
                <div class="col-12 m-0 p-0">
                    <div class="text-center"> 
                      <div class="logo-container p-4">
					 
					   <?= $this->Html->image('logo.png'); ?> 
					</div>		
					<h2 class="m-4">Thank you so much for Credit Application with Primco</h2> 					
              <p>
      					  <?php echo $this->Html->link(
      						'Back to Home',
      						['controller' => 'Users', 'action' => 'login'],
      						['class' => 'btn btn-primary']
      					); ?>

                <?php  echo $this->Html->link(
                  'Edit Credit Application',
                  ['controller' => 'CreditApplications', 'action' => 'editCustomer',base64_encode($creditApplication->id )],
                  ['class' => 'btn btn-warning']
                );  
                 ?>
      					
      					
      					<?php 
                if($link){
                    echo $this->Html->link(
                      'Preview and Download',
                      $link,
                      ['class' => 'btn btn-info']
                    ); 
                }
                
                ?>
                        
              </p>
                      <div>
                        <?php if($image_name): ?>
                          
                           <?= $this->Html->image('/img/QRcode/'.$image_name ) ?> 
                        <?php endif; ?>
                      </div>
                      
                    </div>
            </div>
    </div>
