<footer class="page-footer footer footer-static footer-light navbar-border navbar-shadow">
  <div class="footer-copyright">
    <div class="container"><span>&copy; 2019 <a href="http://primco.ca" target="_blank">Primco.ca</a> All rights reserved.</span></div>
  </div>
</footer>