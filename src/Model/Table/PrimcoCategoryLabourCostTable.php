<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * ClaimStatus Model
 *
 * @property &\Cake\ORM\Association\BelongsTo $Claims
 *
 * @method \App\Model\Entity\PrimcoCategoryLabourCost get($primaryKey, $options = [])
 * @method \App\Model\Entity\PrimcoCategoryLabourCost newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\PrimcoCategoryLabourCost[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\PrimcoCategoryLabourCost|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\PrimcoCategoryLabourCost saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\PrimcoCategoryLabourCost patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\PrimcoCategoryLabourCost[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\PrimcoCategoryLabourCost findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class PrimcoCategoryLabourCostTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('primco_category_labour_cost');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        return $validator;
    }
}
