<?php


namespace App\Controller;


use Cake\I18n\Time;

class MillerGenericMethods extends AppController
{
    public function initialize()
    {
        parent::initialize();
        $this->loadModel('Millers');
        $this->loadModel('Users');
    }

    public function getAllMillers($search = false, $searchData = '') {
        $query = $this->Millers->find('all');
        if ($search) {
            if (!empty($searchData['name'])) {
                $name = $searchData['name'];
                $query = $query->where(['Millers.miller_name LIKE' => '%' . $name . '%']);
            }
            if (!empty($searchData['email'])) {
                $email = $searchData['email'];
                $query = $query->where(['Millers.miller_email LIKE' => '%' . $email . '%']);
            }
            if (!empty($searchData['phone'])) {
                $phone = $searchData['phone'];
                $query = $query->where(['Millers.phone LIKE' => '%' . $phone . '%']);
            }
        }
        return $query;
    }

    public function addMillerToUsers($details, $password) {
        $addMillerToUsers = $this->Users->newEntity();
        $userDetails = [
            'username' => $details['miller_email'],
            'password' => $password,
            'role' => 'miller',
            'created' => new Time()
        ];
        $addMillerToUsers = $this->Users->patchEntity($addMillerToUsers, $userDetails);
        if ($this->Users->save($addMillerToUsers)) {
            return $addMillerToUsers;
        }
    }

    public function addMiller($details, $user_id) {
            $addMiller = $this->Millers->newEntity();
            $millerDetails = [
                'user_id' => $user_id,
                'miller_name' => $details['miller_name'],
                'contact_name' => $details['miller_contact_name'],
                'email' => $details['miller_email'],
                'phone' => $details['miller_phone'],
                'address' => $details['miller_address'],
                'city' => $details['miller_city'],
                'postal_code' => $details['miller_postal_code'],
                'created' => new Time()
            ];
            $addMiller = $this->Millers->patchEntity($addMiller, $millerDetails);
            if ($this->Millers->save($addMiller)) {
                return $addMiller;
            }
    }

    public function updateMiller($details) {
        $updateMiller = $this->Millers->get($details['miller_id']);
        $millerDetails = [
            'user_id' => $details['miller_user_id'],
            'miller_name' => $details['miller_name'],
            'contact_name' => $details['miller_contact_name'],
            'email' => $details['miller_email'],
            'phone' => $details['miller_phone'],
            'address' => $details['miller_address'],
            'city' => $details['miller_city'],
            'postal_code' => $details['miller_postal_code'],
            'created' => new Time()
        ];
        $updateMiller = $this->Millers->patchEntity($updateMiller, $millerDetails);
        if ($this->Millers->save($updateMiller)) {
            return $updateMiller;
        }
    }

    public function doesMillerExists($email) {

    }

    public function createUniquePassword() {
        $passwordString = '1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZabcefghijklmnopqrstuvwxyz';
        return substr(str_shuffle($passwordString), 0, rand(8, 20));
    }
}
