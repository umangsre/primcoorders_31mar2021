<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link      https://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   https://opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller;

use Cake\Controller\Controller;
use Cake\Event\Event;
use Cake\ORM\TableRegistry;
use Cake\I18n\I18n;
use Cake\I18n\Time;
use Cake\I18n\Date;
use Cake\I18n\FrozenDate;
use Cake\I18n\FrozenTime;
Time::setDefaultLocale('us_US'); // For any mutable DateTime
FrozenTime::setDefaultLocale('us_US'); // For any immutable DateTime
Date::setDefaultLocale('us_US'); // For any mutable Date
FrozenDate::setDefaultLocale('us_US'); // For any immutable Date
Time::setJsonEncodeFormat('yyyy-MM-dd HH:mm:ss');  // For any mutable DateTime
FrozenTime::setJsonEncodeFormat('yyyy-MM-dd HH:mm:ss');  // For any immutable DateTime
Date::setJsonEncodeFormat('yyyy-MM-dd HH:mm:ss');  // For any mutable Date
FrozenDate::setJsonEncodeFormat('yyyy-MM-dd HH:mm:ss');  // For any immutable Date


/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @link https://book.cakephp.org/3.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller
{

    /**
     * Initialization hook method.
     *
     * Use this method to add common initialization code like loading components.
     *
     * e.g. `$this->loadComponent('Security');`
     *
     * @return void
     */
    public function initialize()
    {
        parent::initialize();

        $this->loadComponent('RequestHandler', [
            'enableBeforeRedirect' => false,
        ]);
        $this->loadComponent('Flash');
         //$this->loadComponent('Auth');
        if ($this->request->getParam('prefix') == 'api') {
              $this->loadComponent('Auth', [
                'authenticate' => [
                    'Basic' => [
                        'fields' => ['username' => 'username', 'password' => 'password'],
                        'userModel' => 'Users'
                    ],
                ],
                'storage' => 'Memory',
                'unauthorizedRedirect' => false
            ]);

            }else{

             $this->loadComponent('Auth', [
                'loginRedirect' => [
                    'controller' => 'Orders',
                    'action' => 'index'
                ],
                'logoutRedirect' => [
                    'controller' => 'Users',
                    'action' => 'login',
                    'prefix' => false
                ],
                'unauthorizedRedirect' => [
                    'controller' => 'Users',
                    'action' => 'login',
                    'prefix' => false
                ],
                'redirectUrl' => [
                    'controller' => 'Orders',
                    'action' => 'index',
                    'home'
                ]
            ]);

        }

      $this->Auth->allow([ 'appAdd','pdfView', 'file_upload','base64test']);
        $loggedInUserInfo = $this->Auth->user();
        $this->set(compact('loggedInUserInfo'));
    }




    public function file_upload($file,$file_name,$path)
    {
        if($file){
            print_r($file);
            $ext = substr(strtolower(strrchr($file['name'], '.')), 1);
            //get the extension
            $arr_ext = array('jpg', 'jpeg', 'gif','png','pdf','txt');
            //set allowed extensions
            //only process if the extension is valid
            if(in_array($ext, $arr_ext))
            {
                $file_name = $file['name'];
                if(move_uploaded_file($file['tmp_name'], $path.$file_name)){
                    return true;
                }
            }
       }
        return false;
    }

    public function base64test($base64string, $sign_file) {

        // open the output file for writing
        $ifp = fopen($sign_file, 'wb' );
        fwrite( $ifp, base64_decode($base64string) );
        // clean up the file resource
        fclose( $ifp );
        if($sign_file){
          return true;
       }

         return false;

    }

    public function base64toimg($base64_string, $output_file) {
        // open the output file for writing
        $ifp = fopen( $output_file, 'wb' );
        // split the string on commas
        // $data[ 0 ] == "data:image/png;base64"
        // $data[ 1 ] == <actual base64 string>
        //$data = explode( ',', $base64_string );
        // we could add validation here with ensuring count( $data ) > 1
        $out =fwrite( $ifp, base64_decode( $base64_string ) );
        // clean up the file resource
        fclose( $ifp );

        if($out){
            return true;
        }else{
            return false;
        }
    }
    
    public function isAPIAuthorized($request){
        
	        $authHeader = $request->getHeaderLine('Authorization');
	        $requestUrl = $request->url;
	        $result = false;
	        $auth_array = explode(" ", $authHeader);
	        $un_pw = explode(":", base64_decode($auth_array[1]));
	        $username = $un_pw[0];
	        $pw = $un_pw[1];
	        
	        $users = TableRegistry::get('Users');
	        $user_data = $users->find('all') ->where( [ "username" => $username ] );
        
	        foreach ($user_data->toList() as $key => $value) {
	            $role = $value->role;
				
	            if(strpos($requestUrl,'toolkits') !== false && (strcasecmp($role,'admin') 
	                || strcasecmp($role,'dealer'))){
	                $result = true;    
	            }
	            break;
	        }
	        return $result;
	    }
}
