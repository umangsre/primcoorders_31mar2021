<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Claim $claim
 * @var \App\Model\Entity\Customer $customer
 * @var \App\Model\Entity\PrimcoCategoryLabourCost $category
 * @var \App\Model\Entity\PrimcoGeneralLabourCost $general
 */
$options = ['select' => 'Select', 'yes' => 'Yes', 'no' => 'No'];
$categories = ['select' => 'Select', 'carpet' => 'Carpet', 'wood' => 'Wood', 'vinyl' => 'Vinyl', 'laminates' => 'Laminates', 'luxury_vinyl' => 'Luxury Vinyl Plank', 'underlay' => 'Underlay', 'rubber' => 'Rubber'];
$subCategories = ['select' => 'Select', 'floating_floor' => 'Floating Floor', 'gluedown_floor' => 'Gluedown Floor', 'stapled_floor' => 'Stapled Floor', 'wo_u_e' => 'Without Underlayment / Embossing', 'wu' => 'With Underlayment', 'w_e' => 'With Embossing'];
$appliances = ['select' => 'Select', 'dishwasher' => 'Dishwasher', 'mouldings' => 'Mouldings'];
$uom = ['select' => 'Select', 'sq.ft.' => 'sq.ft.', 'sq.yd.' => 'sq.yd.'];
$settlement_methods = ['select' => 'Select', 'cash' => 'Cash Settlement', 'replacement' => 'Replacement', 'return' => 'Return'];
?>
<script>
    const categories = <?php echo $category ?>;
    const general = <?php echo $general ?>;
    const disableValues = false;
    console.log(categories);
</script>
<style>
    #suggesstion-dealers {
        background: #fff;
        box-shadow: 3px 3px 3px rgba(0, 0, 0, 0.5);
        padding: 5px;
    }

    #suggesstion-dealers ul li {
        margin: 5px;
        border-bottom: 1px solid #000;
    }
</style>

<div class="orders index large-9 medium-8 columns content card card card-default scrollspy">
    <div class="card-content">
        <div class="claims form large-9 medium-8 columns content card card-default" style="padding: 2rem;">
            <?= $this->Form->create($claim, ['id' => 'claims_form']) ?>
            <div>
                <h5 style="margin-bottom: 2rem;"><?= __('Add Claim') ?></h5>
                <div id="errors" style="color:red;"></div>
                <?php
                if ($loggedInUserInfo['role'] == 'dealer') {
                    ?>
                <fieldset style="margin-bottom: 2rem;">
                    <legend><?= __('Retailer / Dealer Information') ?></legend>
                    <?php
                    echo $this->Form->control('customer_id', ['type' => 'hidden', 'id' => 'customer_id', 'value' => $dealerDetails['id']]);
                    echo $this->Form->control('name', ['value' => $dealerDetails['name'], 'disabled' => 'disabled']);
                    echo $this->Form->control   ('contact_name', ['value' => $dealerDetails['contact_name']]);
                    echo '<br /><br />';
                    echo $this->Form->control('phone', ['value' => $dealerDetails['phone']]);
                    echo $this->Form->control('address', ['value' => $dealerDetails['address']]);
                    echo '<br /><br />';
                    echo $this->Form->control('province', ['value' => $dealerDetails['province']]);
                    echo $this->Form->control('city', ['value' => $dealerDetails['city']]);
                    echo '<br /><br />';
                    echo $this->Form->control('postal_code', ['id' => 'postal_code', 'value' => $dealerDetails['postal_code']]);
                    echo $this->Form->control('sin', ['id' => 'postal_code', 'value' => $dealerDetails['sin']]);
                    ?>
                </fieldset>
                    <?php
                } else {
                    ?>
                    <fieldset style="margin-bottom: 2rem;">
                        <legend><?= __('Retailer / Dealer Information') ?></legend>
                        <?= $this->Form->control('name', ['type' => 'text', 'required' => 'required', 'autocomplete' => 'off', 'id' => 'customer_name', 'class' => 'form-control', 'label' => 'Select Dealer']) ?>
                        <div id="suggesstion-dealers" class="list_wraper shadow-sm"
                             style="display:none; height: 10rem; overflow-y: scroll;"></div>
                        <?php
                        echo $this->Form->control('customer_id', ['type' => 'hidden', 'id' => 'customer_id']);
                        echo $this->Form->control('contact_name');
                        echo '<br /><br />';
                        echo $this->Form->control('phone');
                        echo $this->Form->control('address');
                        echo '<br /><br />';
                        echo $this->Form->control('province');
                        echo $this->Form->control('city');
                        echo '<br /><br />';
                        echo $this->Form->control('postal_code', ['id' => 'postal_code']);
                        echo $this->Form->control('sin', ['id' => 'sin']);
                        ?>
                    </fieldset>
                <?php } ?>
                <br /><br/>
                <fieldset style="margin-bottom: 2rem;">
                    <legend><?= __('End User Information') ?></legend>
                    <?php
                    echo $this->Form->control('consumer_name', ['label' => 'Customer Name']);
                    echo $this->Form->control('consumer_phone', ['label' => 'Phone']);
                    echo '<br /><br />';
                    echo $this->Form->control('consumer_address', ['label' => 'Address']);
                    echo $this->Form->control('consumer_city', ['label' => 'City']);
                    echo '<br /><br />';
                    echo $this->Form->control('consumer_postal_code', ['label' => 'Postal Code']);
                    ?>
                </fieldset>
                <br />
                <fieldset style="margin-bottom: 2rem;">
                    <legend><?= __('Claim Information') ?></legend>
                    <div id="primcoInvoiceNumbers">
                    </div>
                    <button type="button" onclick="addPrimcoInvoiceField()">Add More Invoice Numbers</button>
                    <input type="hidden" name="primco_invoice_number" id="primco-invoice-number"/>
                    <div id="claimInformation">
                    </div>
                    <div>
                        <?php
                        echo $this->Form->button(__('Add Claim'), ['class' => 'add-claim-information', 'type' => 'button', 'style' => 'display: none;']);
                        ?>
                    </div>
                </fieldset>
                <br />
                <fieldset style="margin-bottom: 2rem;">
                    <?php
                    echo $this->Form->control('settlement_method', [
                        'options' => $settlement_methods,
                        'placeholder' => 'select',
                        'controller' => 'total_settlement_amount'
                    ]);
                    ?>
                    <div class="settlement_amount_div" style="display: none;">
                        <?php
                        echo $this->Form->control('total_settlement_amount', ['label' => 'Total Settlement Amount (in CAD)']);
                        ?>
                    </div>
                    <div class="replacement_amount_div" style="display: none;">
                        <?php
                        echo $this->Form->control('total_labour_cost_claimed', ['label' => 'Total Labour Cost Claimed (in CAD)']);
                        ?>
                    </div>
                    <div class="return_qty_div" style="display: none;">
                        <?php
                        echo $this->Form->control('return_quantity', ['label' => 'Return Quantity']);
                        ?>
                    </div>
                    <p>*In case of replacement, it is mandatory to fill customer information.</p>
                    <br />
                    <div class="input file" style="width: 100%; max-width: 100%;">
                        <label for="billing-image">Pictures - Full Room Pictures / Area(s) where the issue took
                            place</label>
                        <br/>
                        <input type="file" name="image" multiple="multiple" id="image" onchange="imagePreview(this, 1)"
                               accept="image/png,image/jpeg,image/jpg,application/pdf">
                        <input type="hidden" name="claim_pictures_1" id="files-1"/>
                        <br/><br/>
                        <label>Please upload only jpeg/jpg/png/pdf files of maximum 5MB</label>
                    </div>
                    <div id="preview-images-1"></div>
                    <br/><br/>
                    <div class="input file" style="width: 100%; max-width: 100%;">
                        <label for="billing-image">Pictures of the issue - Closeup</label>
                        <br/>
                        <input type="file" name="image" multiple="multiple" id="image" onchange="imagePreview(this, 2)"
                               accept="image/png,image/jpeg,image/jpg,application/pdf">
                        <input type="hidden" name="claim_pictures_2" id="files-2"/>
                        <br/><br/>
                        <label>Please upload only jpeg/jpg/png/pdf files of maximum 5MB</label>
                    </div>
                    <div id="preview-images-2"></div>
                    <br/><br/>
                    <div class="input file" style="width: 100%; max-width: 100%;">
                        <label for="billing-image">Labour Bill / Quote, if applicable</label>
                        <br/>
                        <input type="file" name="image" multiple="multiple" id="image" onchange="imagePreview(this, 3)"
                               accept="image/png,image/jpeg,image/jpg,application/pdf">
                        <input type="hidden" name="claim_pictures_3" id="files-3"/>
                        <br/><br/>
                        <label>Please upload only jpeg/jpg/png/pdf files of maximum 5MB</label>
                    </div>
                    <div id="preview-images-3"></div>
                    <br/><br/>
                    <div class="input file" style="width: 100%; max-width: 100%;">
                        <label for="billing-image">Consumer Proof of Purchase</label>
                        <br/>
                        <input type="file" name="image" multiple="multiple" id="image" onchange="imagePreview(this, 4)"
                               accept="image/png,image/jpeg,image/jpg,application/pdf">
                        <input type="hidden" name="claim_pictures_4" id="files-4"/>
                        <br/><br/>
                        <label>Please upload only jpeg/jpg/png/pdf files of maximum 5MB</label>
                    </div>
                    <div id="preview-images-4"></div>
                    <?php
                    echo $this->Form->control('created_date', ['type' => 'hidden', 'value' => date('Y-m-d')]);
                    ?>
                </fieldset>
                <br />
                <fieldset style="margin-bottom: 2rem; display: none;" id="miscClaimInfoArea">
                    <legend><?= __('Additional Information Pertaining to Labour (if applicable)') ?></legend>
                    <div id="miscClaimInformation">
                    </div>
                </fieldset>
                <label>Important Notes:<br/>Unless a claim is approved / accepted by the responsible mill, you the
                    retailer cannot hold back funds or deduct funds owed to Primco for outstanding invoices.<br/>If the
                    retailer chooses to remove and replace the claim material without an approved claim or a claim
                    accepted by the responsible mill, this will be at the cost of the retailer and the claim may not be
                    honored by the mill.<br/><br/><span style="color:red;">What will you need to provide after claims form is complete:</span>
                    <br/>2-3 full uninstalled samples from the order.<br/> Upon return and inspection of uninstalled
                    material, freight cost will be reimbursed to account via Credit Note if and when material is deemed
                    defective.
                </label>
            </div>
            <?= $this->Form->button(__('Save'), ['class' => 'save-claim btn waves-effect waves-light border-round col s6 mt-1', 'style' => '    background: #000;', 'name' => 'clicked', 'value' => 'save']) ?>
            <?= $this->Form->button(__('Submit'), ['class' => 'submit-claim btn waves-effect waves-light border-round col s6 mt-1', 'style' => '    background: #000;']) ?>
            <?= $this->Form->end() ?>
        </div>
    </div>
</div>
<script>
    const userRole = '<?php echo $currentUserInfo['role']; ?>';
</script>
