<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Product[]|\Cake\Collection\CollectionInterface $products
 */
?>
<!--nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Product'), ['action' => 'add']) ?></li>
    </ul>
</nav -->
<div class="orders index large-9 medium-8 columns content card card card-default scrollspy">
   
    <div class="products index large-9 medium-8 columns content">
           <div class="card-content">
            <?= $this->Form->create('search') ?>
            <fieldset>
                <?php
                    echo $this->Form->input('search',['label' =>false]);
                    echo $this->Form->button(__('Search'));
                ?>
            </fieldset>
           
            <?= $this->Form->end() ?> 
            <div class="table-responsive">
            <table cellpadding="0" cellspacing="0" class="table table-stripped">
                <thead>
                    <tr>
                        <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                        <th scope="col"><?= $this->Paginator->sort('product_name') ?></th>
                        <th scope="col"><?= $this->Paginator->sort('item_name') ?></th>
                        <th scope="col"><?= $this->Paginator->sort('item_number') ?></th>
                        <th scope="col"><?= $this->Paginator->sort('color_name') ?></th>
                        <th scope="col"><?= $this->Paginator->sort('single_item') ?></th>
                        <th scope="col"><?= $this->Paginator->sort('bulk_item') ?></th>
                        <th scope="col"><?= $this->Paginator->sort('single_franklin') ?></th>
                        <th scope="col"><?= $this->Paginator->sort('bulk_franklin') ?></th>
                        <th scope="col"><?= $this->Paginator->sort('per_item') ?></th>
                        <th scope="col"><?= $this->Paginator->sort('bulk_count') ?></th>
                        <!-- <th scope="col"><?= $this->Paginator->sort('item_type') ?></th>
                        <th scope="col"><?= $this->Paginator->sort('franklin_type') ?></th> -->
                        <!--th scope="col"><?= $this->Paginator->sort('created_at') ?></th>
                        <th scope="col"><?= $this->Paginator->sort('updated_on') ?></th -->
                        <th scope="col" class="actions"><?= __('Actions') ?></th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($products as $product): ?>
                    <tr>
                        <td><?= $this->Number->format($product->id) ?></td>
                        <td><?= h($product->product_name) ?></td>
                        <td><?= h($product->item_name) ?></td>
                        <td><?= h($product->item_number) ?></td>
                        <td><?= h($product->color_name) ?></td>
                        <td><?= $this->Number->format($product->single_item) ?></td>
                        <td><?= $this->Number->format($product->bulk_item) ?></td>
                        <td><?= $this->Number->format($product->single_franklin) ?></td>
                        <td><?= $this->Number->format($product->bulk_franklin) ?></td>
                        <td><?= $this->Number->format($product->per_item) ?></td>
                        <td><?= $this->Number->format($product->bulk_count) ?></td>
                       <!--  <td><?= h($product->item_type) ?></td>
                        <td><?= h($product->franklin_type) ?></td> -->
                        <!--td><?= h($product->created_at) ?></td>
                        <td><?= h($product->updated_on) ?></td -->
                        <td class="actions">
                            <?= $this->Html->link(__('View'), ['action' => 'view', $product->id]) ?>
                            <?= $this->Html->link(__('Edit'), ['action' => 'edit', $product->id]) ?>
                            <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $product->id], ['confirm' => __('Are you sure you want to delete # {0}?', $product->id)]) ?>
                        </td>
                    </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
        </div>
          <div class="paginator">
                <ul class="pagination">
                    <?= $this->Paginator->first('<< ' . __('first')) ?>
                    <?= $this->Paginator->prev('< ' . __('previous')) ?>
                    <?= $this->Paginator->numbers() ?>
                    <?= $this->Paginator->next(__('next') . ' >') ?>
                    <?= $this->Paginator->last(__('last') . ' >>') ?>
                </ul>
                <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
            </div>
        </div>
    </div>
</div>
