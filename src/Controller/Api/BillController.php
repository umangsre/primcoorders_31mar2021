<?php
namespace App\Controller\Api;
use Cake\I18n\Time;
use Cake\ORM\TableRegistry;
use App\Controller\AppController;
use Cake\Mailer\Email;

/**
 * Bill Controller
 *
 *
 * @method \App\Model\Entity\Bill[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class BillController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function billlist($id = null)
    {
        //$bill = $this->paginate($this->Bill);
        $bill = $this->Bill->find('all', ['order' => 'Bill.id DESC']);
        if(!empty($id)){
            $bill = $bill->where(['Bill.billing_userid' =>$id]);
        }else{
            $bill = $bill->where(['Bill.billing_status !=' =>'S']);
        }
        foreach($bill as $row) {
		    $comments = TableRegistry::getTableLocator()->get('bill_comment')->find()->where(['and' => ['billid' => $row['id']]]);
		    if (empty($comments)) {
                $row['comment'] = [];
            } else {
		        $row['comment'] = $comments;
            }
        }
        echo json_encode(array("response"=>$bill));
        exit;

    }

    /**
     * View method
     *
     * @param string|null $id Bill id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $bill = $this->Bill->get($id, [
            'contain' => []
        ]);

        $this->set('Bill', $bill);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $bill = $this->Bill->newEntity();
        $this->request->allowMethod(['post', 'put']);
        if ($this->request->is('post')) {

            $data = $this->request->getData();
            $mailsend = 'Y';
            if (array_key_exists('save', $data) && !empty($data['save'])) {
                if ($data['save'] == 'Save') {
                    $data['billing_status'] = 'S';
                    $mailsend = 'N';
                }
            } else {
                $data['billing_status'] = 'P';
            }
            $data['billing_tax_amount'] = number_format($data['billing_tax_amount'], 2, '.', '');
            $data['billing_total_amount'] = number_format($data['billing_total_amount'], 2, '.', '');
            $bill = $this->Bill->patchEntity($bill, $data);
            if ($this->Bill->save($bill)) {
                if ($mailsend == 'Y') {
                    $this->loadModel('Salesmen');
                    $userdetail = TableRegistry::getTableLocator()->get('salesmen')->find()->where(['user_id' => $data['billing_userid']]);
                    $newbillinfo = [
                        "bill_no" => $bill['id'],
                        "user_name" => $userdetail->name,
                        "user_email" => $userdetail->rep_email,
                        "bill_status" => 'New bill request',
                        "link" => 'http://orders.primco.ca/bill?bid=' . $bill['id']
                    ];

                    $this->sendMail("New Bill", $bill);
                }
                echo json_encode(array("response"=>"Done"));
                exit;
                $this->set('bill', $bill);
            }else{
                echo json_encode(array("response"=>"Error"));
                exit;
            }
        }
    }
    /* public function add()
    {
        $bill = $this->Bill->newEntity();
        if ($this->request->is('post')) {
            $bill = $this->Bill->patchEntity($Bill, $this->request->getData());
            if ($this->Bill->save($Bill)) {
                $this->Flash->success(__('The Bill has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The Bill could not be saved. Please, try again.'));
        }
        $this->set(compact('Bill'));
    } */

    /**
     * Edit method
     *
     * @param string|null $id Bill id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $bill = $this->Bill->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $data =  $this->request->getData();
            $data['comment'] = $data['billing_comment'];
            $bill_status = $bill->billing_status;
            $billing_userid = $bill->billing_userid;
            if(!empty($data['billing_status'])){
                if($data['billing_status'] == 'D'){
                    $data['billing_status'] = 'R';

                    $articlesTable = TableRegistry::getTableLocator()->get('BillComment');
                    $article = $articlesTable->newEntity();

                    $article->comment = $data['billing_comment'];
                    $article->billid = $id;

                    if ($articlesTable->save($article)) {
                        // The $article entity contains the id now
                        $id = $article->id;
                    }
                    unset($data['billing_comment']);
                }
            }
            $bill = $this->Bill->patchEntity($bill,$data);
            if ($this->Bill->save($bill)) {

                $billing_user = TableRegistry::getTableLocator()->get('salesmen')->find()->where(["user_id" => $bill['billing_userid']]);
                $userdetail = "";
                foreach ($billing_user as $userinfo) {
                    $userdetail = $userinfo;
                }
                $newbillinfo = [
                    "bill_no" => $bill['id'],
                    "user_name" => $userdetail->name,
                    "user_email" => $userdetail->rep_email,
                    //"bill_status"=>'New bill request',
                    "link" => 'http://orders.primco.ca/bill?bid=' . $bill['id']
                ];
                if ($bill_status == 'S') {
                    $newbillinfo['bill_status'] = "New bill request";
                } else {
                    $newbillinfo['bill_status'] = "Modify bill request";
                }
                $this->sendMail("Bill Update", $bill);
                echo json_encode(array("response"=>"Done"));
                exit;
            }
            //$this->Flash->error(__('The Bill could not be saved. Please, try again.'));
        }
        //$this->set(compact('Bill'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Bill id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $bill = $this->Bill->get($id);
        if ($this->Bill->delete($bill)) {

            echo json_encode(array("response"=>"Done"));
            exit;
            // $this->Flash->success(__('The Bill has been deleted.'));
        } else {
            // $this->Flash->error(__('The Bill could not be deleted. Please, try again.'));
        }

        //return $this->redirect(['action' => 'index']);
    }
    public function sendMail($billsubject, $bill)
    {

        $toEmail = [
            'admin@primco.ca',
            'c.vanputten@primco.ca',
            $this->Auth->user('username')
        ];
        $email = new Email();
        $email->setTransport('default');
        $email->viewVars(['bill' => $bill]);
        $email->viewBuilder()->setTemplate('bill');
        $email->setEmailFormat('html');
        $email->setFrom(['support@primco.ca' => 'Primco'])
            ->setTo($toEmail)
            ->setSubject('Notification: ' . $billsubject)
            ->send();
        return true;
    }
}
