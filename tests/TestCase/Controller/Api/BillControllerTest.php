<?php
namespace App\Test\TestCase\Controller;

use App\Controller\Api/BillController;
use Cake\TestSuite\IntegrationTestTrait;
use Cake\TestSuite\TestCase;

/**
 * App\Controller\Api/BillController Test Case
 *
 * @uses \App\Controller\Api/BillController
 */
class Api/BillControllerTest extends TestCase
{
    use IntegrationTestTrait;

    /**
     * Test initial setup
     *
     * @return void
     */
    public function testInitialization()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
