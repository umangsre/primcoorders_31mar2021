<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Load Entity
 *
 * @property int $id
 * @property string $vendorID
 * @property string|null $vendor
 * @property string|null $ load_multiplier
 * @property \Cake\I18n\FrozenTime|null $created
 */
class Load extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'vendorID' => true,
        'vendor' => true,
        ' load_multiplier' => true,
        'created' => true
    ];
}
