<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Family $family
 */

?>
<div class="orders index large-9 medium-8 columns content card card card-default scrollspy">
    <div class="card-content">
        <div class="families form large-9 medium-8 columns content">
            <?= $this->Form->create($family) ?>
            <fieldset>
                <legend><?= __('Edit Family') ?></legend>
                <?php
                    echo $this->Form->control('family');
                    echo $this->Form->control('name');
                    echo $this->Form->control('product_code');
                    echo $this->Form->control('role_cut');
                    echo $this->Form->control('mill_cost',['class' =>'mill_cost']);
                   echo $this->Form->control('usd_cad', ['class' =>'usd_cad', 
                        'options' =>[
                                        'cad' =>'CAD',
                                        'usd_mannington' =>'USD-Mannington',
                                        'usd_general' =>'USD-General',
                                        'usd_others' =>'USD-Others',
                                    ]
                                ]);
                    echo $this->Form->control('conversion_rate',['class' =>'conversion_rate', 'readonly' =>true]);
                    echo $this->Form->control('selling_price', ['class' =>'selling_price','readonly' =>true]);
                    echo $this->Form->control('brokerage_multiplier', [ 'class' =>'brokerage_multiplier', 'min' =>'0','step' =>'0.0001']);
                    echo $this->Form->control('brokerage',['class'=> 'brokerage', 'min' =>'0' ,'step' =>'0.0001','readonly' =>true ]);
                    echo $this->Form->control('units', ['options' =>['EACH' =>'EACH','SF' =>'SF','SY' =>'SY','ROLL' =>'ROLL','DSF' =>'DSF']]);
                    echo $this->Form->control('freight', ['class' =>'freight',  'min' =>'0' ,'step' =>'0.0001']);

                    echo $this->Form->control('ad_fund_percentage', ['class' =>'ad_fund_percentage', 'min' =>'0', 'step' =>'0.0001']);
                    echo $this->Form->control('ad_fund',['class' =>'ad_fund', 'min' =>'0', 'step' =>'0.0001','readonly' =>true ]);
                    echo $this->Form->control('landed_cost_per_unit', ['class' =>'landed_cost_per_unit', 'min' =>'0', 'step' =>'0.0001','readonly' =>true]);
					echo $this->Form->control('_load',['class' =>'load_pent','min' =>'0','step' =>'0.0001']);
                    echo $this->Form->control('load_percentage',['class' =>'load_percentage','min' =>'0','step' =>'0.01','readonly' =>true]);
                    echo $this->Form->control('margin_lic',['class' =>'margin_lic',  'min' =>'0', 'step' =>'0.0001','readonly' =>true]);
                    echo $this->Form->control('sell_price_level1', ['class' =>'sell_price_level1', 'min' =>'0','step' =>'0.0001']);
                    echo $this->Form->control('rebate_level1', ['class' =>'rebate_level1',  'min' =>'0','step' =>'0.0001']);
                    echo $this->Form->control('gross_margin_level1', ['class' =>'gross_margin_level1', 'min' =>'0','step' =>'0.0001', 'readonly' =>true]);
                    echo $this->Form->control('sell_price_level2', ['class' =>'sell_price_level2', 'min' =>'0','step' =>'0.0001']);
                    echo $this->Form->control('rebate_level2',['class' =>'rebate_level2', 'min' =>'0','step' =>'0.0001']);
                    echo $this->Form->control('gross_margin_level2', ['class' =>'gross_margin_level2', 'min' =>'0','step' =>'0.0001', 'readonly' =>true]);
                    echo $this->Form->control('sell_price_level3', ['class' =>'sell_price_level3',  'min' =>'0','step' =>'0.0001']);
                    echo $this->Form->control('rebate_level3', ['class' =>'rebate_level3',  'min' =>'0','step' =>'0.0001']);
                    echo $this->Form->control('gross_margin_level3', ['class' =>'gross_margin_level3', 'min' =>'0','step' =>'0.0001','readonly' =>true]);
                    echo $this->Form->control('mill');
                    echo $this->Form->control('vendorID');
                    echo $this->Form->control('sqft_ctn',['class' =>'sqft_ctn',  'min' =>'0','step' =>'0.0001']);
                    echo $this->Form->control('wgt_lbs_ctn',['class' =>'wgt_lbs_ctn',  'min' =>'0','step' =>'0.0001']);
                    echo $this->Form->control('ctn_pallet', ['class' =>'ctn_pallet',  'min' =>'0','step' =>'0.0001']);
                    echo $this->Form->control('sqft_pallet', ['class' =>'sqft_pallet',  'min' =>'0','step' =>'0.0001','readonly' =>true]);
                    echo $this->Form->control('pallet_container_20',['class' =>'pallet_container_20',  'min' =>'0','step' =>'0.0001']);
                    echo $this->Form->control('sqft_container_20', ['class' =>'sqft_container_20',  'min' =>'0','step' =>'0.0001','readonly' =>true]);
                    echo $this->Form->control('sqft_roll', ['class' =>'sqft_roll',  'min' =>'0','step' =>'0.0001']);
                    echo $this->Form->control('sqyd_roll', ['class' =>'sqyd_roll',  'min' =>'0','step' =>'0.0001']);
                ?>
                 
            </fieldset>
            <?= $this->Form->button(__('Submit')) ?>
            <?= $this->Form->end() ?>
        </div>
    </div>
</div>
