<?php
// Set some content to print
//'authorized_signature_'+ order_id +'.jpg'
$file_name = 'authorized_signature_' . $creditApplication->id . '.jpg';
$_signature = BASE_URL . 'webroot/img/Signs/' . $file_name;
$logo = BASE_URL . 'webroot/img/primco-logo.png';
?>


<!DOCTYPE html>
<html>
<head>
    <title>Print &amp; Preview</title>
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,600,800&display=swap" rel="stylesheet">
</head>
<body>
<style type="text/css">
    body {
        background: #555;
        font-family: 'Montserrat', sans-serif;
        font-size: 12px;
    }

    table {
        border: 1px solid black;
        border-collapse: separate;
        border-spacing: 0;
    }

    table td, table th {
        border: 1px solid red;

    }

    table tr td {
        border-right: 0;
    }

    table tr:last-child td {
        border-bottom: 0;
    }

    table tr td:first-child,
    table tr th:first-child {
        border-left: 0;
    }

    table tr td {
        border-top: 0;
    }

    table {
        width: 100%;
    }

    table th {
        font-weight: bold;

    }

    table th, table td {
        padding: 5px;
        vertical-align: top;
    }

    .main-table {
        width: 665px;
        margin: 0 auto;
        background: #fff;
        padding: 5px;
        height: auto;
    }

    table tr, table tr th, table tr td {
        border: none !important;
    }

    th {

        font-weight: bold;
        font-size: 12px;
    }

    .border {

        border-collapse: collapse;
    }

    table.border tr td, table.border tr th {
        border: 1px solid #cecece;
        border-collapse: collapse;
    }

    table.border tr td:last-child, table.border tr th:last-child {
        border-right: none;
    }

    table.border tr {
        margin: 0;
        border: none;
        border-spacing: 0px;
    }

    table.border tr:last-child td, table.border tr:last-child th {
        border-bottom: none !important;
    }

    .print-button-container {
        width: 920px;
        margin: 0 auto;
        text-align: center;
        padding: 10px 0px;
        background: #fff;
        display: block;
    }

    .print-button {
        padding: 10px 20px;
        font-size: 14px;
        background-color: #1f555f;
        color: #fff;
        border: 1px solid #1f555f;
    }

    @media print {
        .print-button-container {
            display: none;
        }
    }
</style>

<table style="page-break-after: always;">

    <!-- Page 1 Header -->
    <tr>
        <td>
            <table>
                <tr>
                    <td>
                        <img width="250" src="<?= $logo ?>">
                    </td>
                    <td>
                        <h3>CREDIT APPLICATION FOR NEW CUSTOMER</h3>
                        <p>Wholesale Distributor of Floor Coverings</p>
                        <p>
                            Head Office: 12300 44 Street SE, Calgary, AB, T2Z 4A2 <br>
                            Phone: (403) 255-4416 Fax: (403) 255-7879
                        </p>
                        <p>
                            Website: www.primco.ca Email: AR@primco.ca
                        </p>
                        <p>
                            <strong>Calgary * Edmonton * Saskatoon * Regina * Winnipeg * Vancouver</strong>
                        </p>


                    </td>
                </tr>

            </table>

        </td>

    </tr>
    <tr>
        <td>&nbsp;</td>
    </tr>

    <!-- Page 1 Section 1 -->
    <tr>
        <td>
            <table border="1" cellpadding="5">
                <tr>
                    <td colspan="2">Company Legal Name:
                        <strong><?= $creditApplication->company_legal_name ?></strong>

                    </td>

                    <td colspan="2">Company Trade Name:
                        <strong><?= $creditApplication->company_trade_name ?></strong>
                    </td>

                </tr>
                <tr>
                    <td colspan="2">Street Address:
                        <strong><?= $creditApplication->street_address ?></strong>
                    </td>
                    <td>Phone #:
                        <strong><?= $creditApplication->phone ?></strong>
                    </td>
                    <td>Fax #:
                        <strong><?= $creditApplication->fax ?></strong>
                    </td>
                </tr>
                <tr>
                    <td>City: <strong><?= $creditApplication->city ?></strong></td>
                    <td>Province:<strong><?= $creditApplication->province ?></strong></td>
                    <td>Postal Code: <strong><?= $creditApplication->postal_code ?></strong></td>
                    <td>Email Address: <strong><?= $creditApplication->email_address ?></strong></td>
                </tr>

            </table>

        </td>

    </tr>

    <!-- Page 1 Section 2 -->
    <tr>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td>
            <h3>OWNER INFORMATION</h3>
        </td>
    </tr>
    <tr>
        <td>
            <table border="1" cellpadding="5">
                <tr>
                    <td colspan="2">Owner's Name: <strong><?= $creditApplication->owner_name ?></strong></td>
                    <td colspan="2">Owner's Home Address: <strong><?= $creditApplication->owner_home_address ?></strong>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">Owner's Home Phone:<strong><?= $creditApplication->owner_home_phone ?></strong></td>
                    <td colspan="2">Owner's Cell Phone: <strong><?= $creditApplication->owner_cell_phone ?></strong>
                    </td>
                </tr>
            </table>

        </td>

    </tr>

    <!-- Page 1 Section 3 -->
    <tr>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td>
            <h3>MANAGEMENT INFORMATION</h3>
        </td>
    </tr>
    <tr>
        <td>
            <table border="1" cellpadding="5">
                <tr>
                    <td colspan="2">Manager's Name: <strong><?= $creditApplication->manager_name ?></strong></td>
                    <td colspan="2">Manager's Phone: <strong><?= $creditApplication->manager_phone ?></strong></td>
                </tr>
                <tr>
                    <td colspan="2">Financial Institution including Financial
                        Address:<strong><?= $creditApplication->manager_finacial_address ?></strong></td>
                    <td colspan="2">Bank Account Information (please provide void
                        cheque):<strong><?= $creditApplication->manager_bank_account ?></strong></td>
                </tr>
                <tr>
                    <td>City: <strong><?= $creditApplication->manager_city ?></strong></td>
                    <td>Province: <strong><?= $creditApplication->manager_province ?></strong></td>
                    <td>Postal Code: <strong><?= $creditApplication->manager_postal_code ?></strong></td>
                    <td>Phone #:<strong><?= $creditApplication->manager_phone_2 ?></strong></td>
                </tr>
                <tr>
                    <td colspan="2">GST/HST Registration #: <strong><?= $creditApplication->manager_gst ?></strong></td>
                    <td colspan="2">Provincial Sales Tax #:
                        <strong><?= $creditApplication->province_sales_tax ?></strong></td>
                </tr>
                <tr>
                    <td colspan="2">Amount of Credit Required:
                        <strong><?= $creditApplication->amount_credit_required ?></strong></td>
                    <td>Expected Sales Volume per year: <strong><?= $creditApplication->yearly_sales_volume ?></strong>
                    </td>
                    <td>Fiscal Year End: <strong><?= $creditApplication->fiscal_year_end ?></strong></td>
                </tr>
                <tr>
                    <td colspan="2">Are you part of a Buy
                        Group:<strong><?= $creditApplication->is_group ? 'YES' : 'NO' ?></strong></td>
                    <td colspan="2">Buy Group Name: <strong><?= $creditApplication->group_name ?></strong></td>
                </tr>


            </table>

        </td>

    </tr>

    <!-- Page 1 Section 4 -->
    <tr>
        <td>
            <table>
                <tr>
                    <td colspan="2">
                        <h3>THE FOLLOWING MUST BE COMPLETED:</h3>
                    </td>
                    <td colspan="2">
                        <h3>PLEASE SEE TERMS & CONDITIONS ON PAGE 2,</h3>
                    </td>
                </tr>
                <tr>
                    <td colspan="4">
                        <p>
                            The above information is for the purpose of obtaining credit and is warranted to be true.
                            All fields indicated with ** must be completed in order to accept your credit application. I
                            agree to pay all invoices upon receipt in accordance with the Terms & Conditions as outlined
                            on Page 2.
                        </p>
                        <p>
                            I am an authorized representative for the above company and hereby authorize the person or
                            firm to whom this application is submitted to obtain such credit reports or other
                            information as may be deemed necessary in conjunction with the establishment and maintenance
                            of a credit account or for any other direct business requirements.
                        </p>
                        <p>
                            Credit policies are subject to change from time to time at the discretion of the credit
                            department. Upon acceptance of the application and the issuance of an open line of credit,
                            THE CUSTOMER agrees to abide by the credit policies of Primco Limited.
                        </p>
                    </td>
                </tr>

                <tr>

                    <td>
                        <strong>Date: </strong> <?= $onlineusersList[0]->_date ? date('d-M-Y', strtotime($onlineusersList[0]->_date)) : '' ?>
                    </td>
                    <td>
                        <strong>Authorized Signature:</strong> <?= $onlineusersList[0]->company_name3 ?>
                    </td>
                    <td>
                        <strong>Print Name:</strong> <?= $onlineusersList[0]->print_name ?>
                    </td>
                    <td>
                        <strong>Title:</strong><?= $onlineusersList[0]->_title ?>
                    </td>
                </tr>
                <tr>
                    <td colspan="4">
                        <img width="100" src="<?= $_signature ?>">
                    </td>

                </tr>

            </table>
        </td>
    </tr>

</table>
<table style="page-break-after: always;">

    <!-- Page 2 Header -->
    <tr>
        <td>
            <table>
                <tr>
                    <td>
                        <img width="250" src="<?= $logo ?>">
                    </td>
                </tr>

            </table>

        </td>

    </tr>
    <tr>
        <td>&nbsp;</td>
    </tr>

    <!-- Page 2 Section 1 -->
    <tr>
        <td>
            <table cellpadding="5">
                <tr>
                    <td colspan="4"><h3>TERMS</h3></td>
                </tr>
                <tr>
                    <td colspan="4">
                        <p>
                            In completing this application, I understand and accept my account as COD terms. I agree and
                            accept all orders require immediate payment in full until your account with Primco is
                            finalized. Primco will review for credit limit once all documentation has been received
                            update you on the status of your account.
                        </p>
                        <p>
                            In the event credit terms are not established to you with Primco for special terms, you have
                            the option of legal documentation prepared for Personal Guarantee of funds to Primco. This
                            can be further discussed with Credit Department should this be of interest to you.
                        </p>
                        <p>
                            If you are opting for special terms with Primco, please complete credit card information
                            (form attached).
                        </p>
                    </td>
                </tr>
                <tr>
                    <td colspan="4">
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td colspan="4"><h3>CONDITIONS</h3></td>
                </tr>
                <tr>
                    <td colspan="4">
                        <ol>
                            <li>
                                Prices: All prices shown in catalogue are F.O.B. Warehouse unless otherwise specified
                                and are subject to change without notice. Price changes will be made or announced as
                                soon as possible.
                            </li>
                            <li>
                                All orders are subject to our acceptance and all materials are subject to prior sale.
                            </li>
                            <li>
                                All material returned must be in saleable condition and subject to our approval. Freight
                                must be prepaid on merchandise returned. Merchandise returned will be credited at
                                invoice cost, less 25% handling fee.
                            </li>
                            <li>
                                Returns of Resilient Floor Coverings and Carpet, cut to customer's order, are not
                                accepted.
                            </li>
                            <li>
                                Claims for goods damaged in transit must be made by the consignee to the carrier.
                            </li>
                            <li>
                                Goods and Services Tax is not included in Primco's selling price. Goods and Services Tax
                                will be charged. Provincial sales tax Is extra where applicable. Both taxes will appear
                                as a separate line item on each invoice.
                            </li>
                            <li>
                                Finance charges (24% per annum) will be charged if your account is overdue and not paid
                                within credit terms established with Primco.
                            </li>
                            <li>
                                SPECIAL ORDERS: Orders for special material, special finishes, size, or color must be
                                covered by a purchase order. Once accepted these "special orders" are binding and cannot
                                be cancelled.
                            </li>
                            <li>
                                Any portion of a customer's order omitted from shipments because of shortage of stock or
                                other circumstances beyond Primco's control will be back ordered and shipped as soon as
                                possible, unless customer has advised us that materials are not to be back ordered.
                            </li>
                            <li>
                                Orders not shipped or picked up on the requested date may be invoiced to the customer
                                and stored in Primco warehouses and a storage fee may apply. Invoices for stored goods
                                carry the same terms as shipped goods.
                            </li>
                            <li>
                                If any payments become overdue or the financial responsibility of the purchaser becomes
                                impaired or unsatisfactory to Primco, Primco may change the terms of payment on any
                                unshipped remainder to a Cash on Delivery (C.O.D.), prepaid or certified cheque basis.
                            </li>
                            <li>
                                If Credit is granted, I agree that if this account is placed in the hands of an attorney
                                for collection agency, bankruptcy or probate proceedings, the associated fees on both
                                the principal and service charges (such as collection fees, court fees, etc) are my
                                responsibility.
                            </li>
                            <li>
                                A copy of the Company's most recent Business License is required, attached, and will be
                                provided when requested.
                            </li>
                            <li>
                                Invoices related to a potential manufacturer claim remain the customers responsibility.
                                A credit will be issued once a claim is approved.
                            </li>
                        </ol>
                    </td>
                </tr>

                <tr>
                    <td colspan="4">
                        <p>
                            <strong>I have read, understand and agree to the Terms and Conditions of Sale and am an
                                authorized representative of the Company.
                            </strong>
                        </p>
                    </td>
                </tr>

                <tr>

                    <td>
                        <strong>Date: </strong><?= $onlineusersList[0]->_date ? date('d-M-Y', strtotime($onlineusersList[0]->_date)) : '' ?>
                    </td>
                    <td>
                        <strong>Authorized Signature:</strong> <?= $onlineusersList[0]->company_name3 ?>
                    </td>
                    <td>
                        <strong>Print Name:</strong> <?= $onlineusersList[0]->print_name ?>
                    </td>
                    <td>
                        <strong>Title:</strong><?= $onlineusersList[0]->_title ?>
                    </td>
                </tr>
                <tr>
                    <td colspan="4">
                        <img width="100" src="<?= $_signature ?>">
                    </td>

                </tr>
            </table>

        </td>

    </tr>


</table>

<table style="page-break-after: always;">

    <!-- Page 2 Header -->
    <tr>
        <td>
            <table>
                <tr>
                    <td>
                        <img width="250" src="<?= $logo ?>">
                    </td>
                </tr>

            </table>

        </td>

    </tr>
    <tr>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td>
            <h3>ANTI-SPAM CONSENT</h3>
        </td>
    </tr>
    <tr>
        <td>
            <p>
                To ensure compliance with CASL (Canadian Anti Spam Legislation), Primco requires permission to send
                sales and marketing related emails. We are committed to keeping your e-mail address confidential. We do
                not sell, rent, or lease our subscription lists to third parties, and we will not provide your personal
                information to any third party individual, government agency, or company at any time unless compelled to
                do so by law. Please check the box below indicating consent to receiving emails from Primco.
            </p>
        </td>
    </tr>
    <tr>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td>
            <p>
                <strong>YES</strong>, I want to receive future sales and marketing emails from Primco and as signing
                authority for the Company, I give permission for Primco to send ALL email addresses listed on this page
                sales and marketing related emails.
            </p>
        </td>
    </tr>
    <tr>
        <td>
            <table>
                <tr>

                    <td>
                        <strong>Date: </strong> <?= $onlineusersList[0]->_date ? date('d-M-Y', strtotime($onlineusersList[0]->_date)) : '' ?>
                    </td>
                    <td>
                        <strong>Authorized Signature:</strong> <?= $onlineusersList[0]->company_name3 ?>
                    </td>
                    <td>
                        <strong>Print Name:</strong> <?= $onlineusersList[0]->print_name ?>
                    </td>
                    <td>
                        <strong>Title:</strong><?= $onlineusersList[0]->_title ?>
                    </td>
                </tr>
                <tr>
                    <td colspan="4">
                        <img width="100" src="<?= $_signature ?>">
                    </td>

                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td>
            <p>
                Note: Individuals who would like to be removed from the list can send their request to
                contactus@primco.ca
            </p>
        </td>
    </tr>
    <tr>
        <td>
            &nbsp;
        </td>
    </tr>
    <tr>
        <td>
            <table>
                <tr>
                    <td><h3>CONTACT LIST</h3></td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td>
            &nbsp;
        </td>
    </tr>
    <tr>
        <td>
            <table>
                <tr>
                    <td colspan="4"><h3>STATEMENTS</h3></td>
                </tr>
                <!-- <tr>
                    <td colspan="2">Name</td>
                    <td colspan="2">Email</td>
                </tr> -->

                <?php
                $array_name = array();
                $array_email = array();
                $array_name = unserialize($contactlist[0]->statement_name);
                $array_email = unserialize($contactlist[0]->statement_email);
                for ($i = 0; $i < count($array_name); $i++) { ?>
                    <tr>
                        <td><strong>Name</strong></td>
                        <td><?= $array_name[$i] ?></td>
                        <td><strong>Email</strong></td>
                        <td><?= $array_email[$i] ?></td>
                    </tr>
                <?php } ?>

            </table>
        </td>
    </tr>
    <tr>
        <td>
            &nbsp;
        </td>
    </tr>
    <tr>
        <td>
            <table>
                <tr>
                    <td colspan="4"><h3>INVOICES</h3></td>
                </tr>


                <?php
                $array_name = array();
                $array_email = array();
                $array_name = unserialize($contactlist[0]->invoice_name);
                $array_email = unserialize($contactlist[0]->invoice_email);
                for ($i = 0; $i < count($array_name); $i++) { ?>
                    <tr>
                        <td><strong>Name</strong></td>
                        <td><?= $array_name[$i] ?></td>
                        <td><strong>Email</strong></td>
                        <td><?= $array_email[$i] ?></td>
                    </tr>
                <?php } ?>
            </table>
        </td>
    </tr>
    <tr>
        <td>
            &nbsp;
        </td>
    </tr>
    <tr>
        <td>
            <table>
                <tr>
                    <td colspan="4"><h3>PRICE PAGES</h3></td>
                </tr>


                <?php
                $array_name = array();
                $array_email = array();
                $array_name = unserialize($contactlist[0]->price_pages_name);
                $array_email = unserialize($contactlist[0]->price_pages_email);
                for ($i = 0; $i < count($array_name); $i++) { ?>
                    <tr>
                        <td><strong>Name</strong></td>
                        <td><?= $array_name[$i] ?></td>
                        <td><strong>Email</strong></td>
                        <td><?= $array_email[$i] ?></td>
                    </tr>
                <?php } ?>
            </table>
        </td>
    </tr>
    <tr>
        <td>
            &nbsp;
        </td>
    </tr>
    <tr>
        <td>
            <table>
                <tr>
                    <td colspan="4"><h3>ORDER CONFIRMATION</h3></td>
                </tr>

                <?php
                $array_name = array();
                $array_email = array();
                $array_name = unserialize($contactlist[0]->order_confirmation_name);
                $array_email = unserialize($contactlist[0]->order_confirmation_email);
                for ($i = 0; $i < count($array_name); $i++) { ?>
                    <tr>
                        <td><strong>Name</strong></td>
                        <td><?= $array_name[$i] ?></td>
                        <td><strong>Email</strong></td>
                        <td><?= $array_email[$i] ?></td>
                    </tr>
                <?php } ?>
            </table>
        </td>
    </tr>
    <tr>
        <td>
            &nbsp;
        </td>
    </tr>
    <tr>
        <td>
            <table>
                <tr>
                    <td colspan="4"><h3>NOTIFICATION OF BACK ORDER ARRIVAL</h3></td>
                </tr>

                <?php
                $array_name = array();
                $array_email = array();
                $array_name = unserialize($contactlist[0]->bank_order_name);
                $array_email = unserialize($contactlist[0]->bank_order_email);
                for ($i = 0; $i < count($array_name); $i++) { ?>
                    <tr>
                        <td><strong>Name</strong></td>
                        <td><?= $array_name[$i] ?></td>
                        <td><strong>Email</strong></td>
                        <td><?= $array_email[$i] ?></td>
                    </tr>
                <?php } ?>
            </table>
        </td>
    </tr>
    <tr>
        <td>
            &nbsp;
        </td>
    </tr>
    <tr>
        <td>
            <table>
                <tr>
                    <td colspan="4"><h3>PURCHASING AGENTS:</h3></td>
                </tr>

                <?php
                $array_name = array();
                $array_email = array();
                $array_name = unserialize($contactlist[0]->purchasing_agents_name);
                $array_email = unserialize($contactlist[0]->purchasing_agents_email);
                for ($i = 0; $i < count($array_name); $i++) { ?>
                    <tr>
                        <td><strong>Name</strong></td>
                        <td><?= $array_name[$i] ?></td>
                        <td><strong>Email</strong></td>
                        <td><?= $array_email[$i] ?></td>
                    </tr>
                <?php } ?>
            </table>
        </td>
    </tr>

    <tr>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td>
            <h3>
                Completed By:
            </h3>
        </td>
    </tr>
    <tr>
        <td>
            <table>
                <tr>

                    <td>
                        <strong>Date: </strong> <?= $onlineusersList[0]->_date ? date('d-M-Y', strtotime($onlineusersList[0]->_date)) : '' ?>
                    </td>
                    <td>
                        <strong>Authorized Signature:</strong> <?= $onlineusersList[0]->company_name3 ?>
                    </td>
                    <td>
                        <strong>Print Name:</strong> <?= $onlineusersList[0]->print_name ?>
                    </td>
                    <td>
                        <strong>Title:</strong><?= $onlineusersList[0]->_title ?>
                    </td>
                </tr>
                <tr>
                    <td colspan="4">
                        <img width="100" src="<?= $_signature ?>">
                    </td>

                </tr>
            </table>
        </td>
    </tr>

</table>

<table>
    <tr>
        <td>
            <table>
                <tr>
                    <td>
                        <img width="250" src="<?= $logo ?>">
                    </td>
                </tr>

            </table>

        </td>

    </tr>
    <tr>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td>
            <h3>Primco Online User Registration</h3>
        </td>
    </tr>
    <tr>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td>
            <table>
                <tr>
                    <td>
                        <p> Submission
                            Date:<strong><?= $onlineusersList[0]->submission_date ? date('d-M-Y', strtotime($onlineusersList[0]->submission_date)) : '' ?></strong>
                        </p>
                    </td>
                    <td></td>
                </tr>
                <tr>
                    <td><p> Customer Number: <strong><?= $onlineusersList[0]->customer_number ?></strong></p></td>
                    <td></td>
                </tr>
                <tr>
                    <td><p> Company Name: <strong><?= $onlineusersList[0]->company_name ?></strong></p></td>
                    <td></td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td>
            <p>There are two user settings, Administrator and Standard User . The Administrator can decide what
                information each Standard User sees.
            </p>
        </td>
    </tr>
    <tr>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td>
            <p>All Users must have a unique email address . Please include first and last name of all users – your
                unique username for logging on will be provided to you.
            </p>
        </td>
    </tr>
    <tr>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td>
            <?php
            $array_name = [];
            $array_name = unserialize($onlineusersList[0]->administartor_will_see);
            ?>
            <table>
                <tr>
                    <td>
                        <h3>Administrators will see:</h3>
                    </td>
                </tr>
                <tr>
                    <td>
                        <p>
                            Account Details, including Credit Details
                            <strong><?= $array_name[0] ? 'Yes' : 'NO' ?></strong>
                        </p>
                    </td>
                </tr>
                <tr>
                    <td>
                        <p>
                            Invoices (Open/Closed/Credit Memos)
                            <strong><?= $array_name[1] ? 'Yes' : 'NO' ?></strong>
                        </p>
                    </td>
                </tr>
                <tr>
                    <td>
                        <p>
                            Pricelists <strong><?= $array_name[2] ? 'Yes' : 'NO' ?></strong>
                        </p>
                    </td>
                </tr>
                <tr>
                    <td>
                        <p>
                            Purchases <strong><?= $array_name[3] ? 'Yes' : 'NO' ?></strong>
                        </p>
                    </td>
                </tr>
                <tr>
                    <td>
                        <p>
                            Prices <strong><?= $array_name[4] ? 'Yes' : 'NO' ?></strong>
                        </p>
                    </td>
                </tr>
            </table>
        </td>

    </tr>
    <tr>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td>
            <table>
                <tr>
                    <td colspan="2">
                        <h3>Administrator:</h3>
                    </td>
                </tr>
                <tr>
                    <td>Name (First & Last):<strong> <?= $onlineusersList[0]->first__last_name ?></strong></td>

                    <td> User Email: <strong><?= $onlineusersList[0]->user_email ?></strong>
                    </td>

                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td>
            <table>
                <tr>
                    <td colspan="4">
                        <h3>Standard Users:</h3>
                    </td>
                </tr>
                <tr>
                    <td>Name (First & Last):</td>
                    <td><strong><?= $onlineusersList[0]->standard_name ?> </strong></td>
                    <td>User Email:</td>
                    <td><strong><?= $onlineusersList[0]->standard_email ?></strong></td>
                </tr>
                <tr>
                    <td>Name (First & Last):</td>
                    <td><strong><?= $onlineusersList[0]->standard_name1 ?></strong></td>
                    <td>User Email:</td>
                    <td><strong><?= $onlineusersList[0]->standard_email1 ?></strong></td>
                </tr>
                <tr>
                    <td>Name (First & Last):</td>
                    <td><strong><?= $onlineusersList[0]->standard_name2 ?></strong></td>
                    <td>User Email:</td>
                    <td><strong><?= $onlineusersList[0]->standard_email2 ?></strong></td>
                </tr>
                <tr>
                    <td>Name (First & Last):</td>
                    <td><strong><?= $onlineusersList[0]->standard_name3 ?></strong></td>
                    <td>User Email:</td>
                    <td><strong><?= $onlineusersList[0]->standard_email3 ?></strong></td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td>
            <table>
                <tr>
                    <td>
                        <h3>Standard User Options:</h3>
                    </td>
                </tr>
                <tr>
                    <td>
                        <p>
                            Can see Account details (Y/N)?
                            <strong><?= $onlineusersList[0]->can_see_account_details ? 'YES' : 'NO' ?></strong>
                        </p>
                    </td>
                </tr>
                <tr>
                    <td>
                        <p>
                            Can users place Orders (Y/N)?
                            <strong><?= $onlineusersList[0]->can_see_placed_order ? 'YES' : 'NO' ?></strong>
                        </p>
                    </td>
                </tr>
                <tr>
                    <td>
                        <p>
                            Can users see Credit Memos (Y/N)?
                            <strong><?= $onlineusersList[0]->can_see_credit_memos ? 'YES' : 'NO' ?></strong>
                        </p>
                    </td>
                </tr>
                <tr>
                    <td>
                        <p>
                            Can users see Invoiced Orders (Y/N)?
                            <strong><?= $onlineusersList[0]->can_see_invoiced_order ? 'YES' : 'NO' ?></strong>
                        </p>
                    </td>
                </tr>
                <tr>
                    <td>
                        <p>
                            Can users see Price Lists (Y/N)?
                            <strong><?= $onlineusersList[0]->can_see_price_list ? 'YES' : 'NO' ?></strong>
                        </p>
                    </td>
                </tr>
                <tr>
                    <td>
                        <p>
                            Can users see Prices (Y/N)?
                            <strong><?= $onlineusersList[0]->can_see_prices ? 'YES' : 'NO' ?></strong>
                        </p>
                    </td>
                </tr>
            </table>
        </td>

    </tr>
    <tr>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td>
            <p>
                <strong>Please return to: AR@primco.ca or fax to: ATTN: Accounts Receivable (403) 255-7879</strong>
            </p>
        </td>
    </tr>
    <tr>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td>
            <table>
                <tr>

                    <td>
                        <strong>Date: </strong> <?= $onlineusersList[0]->_date ? date('d-M-Y', strtotime($onlineusersList[0]->_date)) : '' ?>
                    </td>
                    <td>
                        <strong>Authorized Signature:</strong> <?= $onlineusersList[0]->company_name3 ?>
                    </td>
                    <td>
                        <strong>Print Name:</strong> <?= $onlineusersList[0]->print_name ?>
                    </td>
                    <td>
                        <strong>Title:</strong><?= $onlineusersList[0]->_title ?>
                    </td>
                </tr>
                <tr>
                    <td colspan="4">
                        <img width="100" src="<?= $_signature ?>">
                    </td>

                </tr>
            </table>
        </td>
    </tr>
</table>
</body>
</html>
