<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Order $order
 */
?>
<!--nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $order->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $order->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Orders'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Customers'), ['controller' => 'Customers', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Customer'), ['controller' => 'Customers', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Salesmen'), ['controller' => 'Salesmen', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Salesman'), ['controller' => 'Salesmen', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Order Items'), ['controller' => 'OrderItems', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Order Item'), ['controller' => 'OrderItems', 'action' => 'add']) ?></li>
    </ul>
</nav -->
<div class="orders index large-9 medium-8 columns content card card card-default scrollspy">
    <div class="card-content">
        <div class="orders form large-18 medium-16 columns content">
       
            <?= $this->Form->create($order) ?>
            <h1><?= h($order->id) ?></h1>
            
            
            <fieldset>
                <legend><?= __('Edit Order') ?></legend>
                
             <table class="vertical-table">
                <tr>
                <th><?= ('Customer') ?></th>
                <td><?= $this->Form->control('customer_id',array('label'=>''))?></td>
                </tr>
                <tr>
                <th scope="row"><?= __('Salesman') ?></th>
                <td><?= $this->Form->control('salesman_id',array('label'=>''));?></td>
                </tr>
                <tr>
                <th scope="row"><?= __('PO Number') ?></th>
                <th><?= $this->Form->control('po_number',array('label'=>''));?></td>
                </tr>
                <tr>
                <th scope="row"><?= __('Ship To') ?></th>
                <td><?= $this->Form->control('ship_to',array('label'=>''));?></td>
                </tr>
                <tr>
                <th scope="row"><?= __('Ship Via') ?></th>
                <td><?= $this->Form->control('ship_via',array('label'=>''));?></td>
                </tr>
                <th scope="row"><?= __('Additional Comments') ?></th>
                <td><?= $this->Form->control('additional_comments',array('label'=>''));?></td>
                </tr>
                <th scope="row"><?= __('Events') ?></th>
                <td><?= $this->Form->control('events',array('label'=>''));?></td>
                </tr>
                <tr>
                <th scope="row"><?= __('Sale Order Number') ?></th>
                <td><?= $this->Form->control('sale_order_number',array('label'=>''));?></td>
                </tr>
                <tr>
                <th scope="row"><?= __('Incentive Receipt') ?></th>
                <td><?= $this->Form->control('incentive_reciept',array('label'=>''));?></td>
                </tr>
                <tr>
                <th scope="row"><?= __('Purchaser Name') ?></th>
                <td><?= $this->Form->control('purchaser_name',array('label'=>''));?></td>
                </tr>
                <!-- <tr>
                <th scope="row"><?= __('Purchaser Sign.') ?></th>
                <td><?= $this->Form->control('purchaser_sign',array('label'=>''));?></td>
                </tr> -->
                <tr>
                  <th scope="row"><?= __('Order ID') ?></th>
                  <td><?=h($order->id) ?></td>
                </tr>
                <tr>
                <th scope="row"><?= __('Grand Total') ?></th>
                <td><?= $this->Form->control('grand_total',array('label'=>''));?></td>
                </tr>
                <tr>
                <th scope="row"><?= __('Status') ?></th>
                <td><?= $this->Form->control('status',array('label'=>''));?></td>
                </tr>
                <tr>
                <th scope="row"><?= __('Sale Date') ?></th>
                <td><?= $this->Form->control('sale_date',array('label'=>''));?></td>
                </tr>
                <tr>
                <th scope="row"><?= __('Ship Date') ?></th>
                <td><?= $this->Form->control('ship_date',array('label'=>''));?></td>
                </tr>
             </table>
            </fieldset>
			<div class="related">
            <h4><?= __('Order Products') ?></h4>
            <?php if (!empty($order->order_items)): ?>
            <table cellpadding="0" cellspacing="0">
                <tr>
                    <th scope="col"><?= __('Id') ?></th>
                    <th scope="col"><?= __('Order Id') ?></th>
                    <th scope="col"><?= __('Product Id') ?></th>
                    <th scope="col"><?= __('Price') ?></th>
                    <th scope="col"><?= __('Quantity') ?></th>
                    <th scope="col"><?= __('Incentives') ?></th>
                    <th scope="col"><?= __('Total') ?></th>
                    <th scope="col"><?= __('Promo') ?></th>
                    <th scope="col"><?= __('Discount') ?></th>
                    <th scope="col"><?= __('Franklin Enabled') ?></th>
                    <!--th scope="col"><?= __('Created') ?></th>
                    <th scope="col"><?= __('Modified') ?></th -->
                    <th scope="col" class="actions"><?= __('Actions') ?></th>
                </tr>
                <?php foreach ($order->order_items as $orderItems): ?>
                <tr>
                    <td><?= h($orderItems->id) ?></td>
                    <td><?= h($orderItems->order_id) ?></td>
                    <td><?= h($orderItems->product_id) ?></td>
                    <td><?= h($orderItems->price) ?></td>
                    <td><?= h($orderItems->quantity) ?></td>
                    <td><?= h($orderItems->incentives) ?></td>
                    <td><?= h($orderItems->total) ?></td>
                    <td><?= h($orderItems->promo) ?></td>
                    <td><?= h($orderItems->discount) ?></td>
                    <td><?= h($orderItems->franklin_enabled) ?></td>
                    <!--td><?= h($orderItems->created) ?></td>
                    <td><?= h($orderItems->modified) ?></td -->
                    <td class="actions">
                        <?= $this->Html->link(__('View'), ['controller' => 'OrderItems', 'action' => 'view', $orderItems->id]) ?>
                        <?= $this->Html->link(__('Edit'), ['controller' => 'OrderItems', 'action' => 'edit', $orderItems->id]) ?>
                        <?= $this->Form->postLink(__('Delete'), ['controller' => 'OrderItems', 'action' => 'delete', $orderItems->id], ['confirm' => __('Are you sure you want to delete # {0}?', $orderItems->id)]) ?>
                    </td>
                </tr>
                <?php endforeach; ?>
            </table>
            <?php endif; ?>
        </div>
           
            <?= $this->Form->button(__('Submit'),['class' => 'btn waves-effect waves-light border-round gradient-45deg-purple-deep-orange col s12 mt-4']) ?>
            <?= $this->Form->end() ?>
        </div>
    </div>
</div>
