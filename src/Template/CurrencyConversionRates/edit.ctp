<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\CurrencyConversionRate $currencyConversionRate
 */
?>
<div class="customers index large-9 medium-8 columns content">
    <div class="orders index large-9 medium-8 columns content card card card-default scrollspy">
    
        <div class="card-content">

            <?= $this->Form->create($currencyConversionRate) ?>
            <fieldset>
                <legend><?= __('Edit Currency Conversion Rate') ?></legend>
                <?php
                    foreach($currencyConversionRate as $key => $rate){
                        echo $this->Form->control($rate->ckey, ['value' => $rate->cvalue]);
                    }
                ?>
            </fieldset>
            <?= $this->Form->button(__('Submit'),['class' => 'btn waves-effect waves-light border-round col s3 mt-4', 'style' => 'background: #000;', 'name' => 'clicked', 'value' => 'save', 'action'=>'index']) ?>
            <?= $this->Html->link(__('Add New'), ['action' => 'addnew'],['class' => 'btn waves-effect waves-light border-round col s3 mt-4', 'style' => 'background: #000;', 'name' => 'clicked', 'value' => 'save']) ?>
            <?= $this->Form->end() ?>
        </div>
    </div>
</div>
