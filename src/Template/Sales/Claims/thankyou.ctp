<?php

?> 
    <div class="row">
                <div class="col-12 m-0 p-0">
                    <div class="text-center"> 
                      <div class="logo-container p-4">
					 
					   <?= $this->Html->image('logo.png'); ?> 
					</div>		
					<h2 class="m-4">Thank you so much for Credit Application with Primco</h2> 					
              <p>
      					  <?php echo $this->Html->link(
      						'Back to Home',
      						['controller' => 'Users', 'action' => 'login'],
      						['class' => 'btn btn-primary']
      					); ?>

                <?php  echo $this->Html->link(
                  'Edit Claim Form',
                  ['controller' => 'Claims', 'action' => 'editclaim',base64_encode($claim->id )],
                  ['class' => 'btn btn-warning']
                );  
                 ?>
      					
      					
      					<?php 
                if($link){
                    echo $this->Html->link(
                      'Preview and Download',
                      $link,
                      ['class' => 'btn btn-info']
                    ); 
                }
                
                ?>
                        
              </p>
                      <div>
                        <?php if($image_name): ?>
                          
                           <?= $this->Html->image('/img/QRcode/'.$image_name ) ?> 
                        <?php endif; ?>
                      </div>
                      
                    </div>
            </div>
    </div>
