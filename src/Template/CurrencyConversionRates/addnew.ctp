<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\ccr $ccr
 */
?>
<div class="currencyConversionRates form large-9 medium-8 columns content">
    <?= $this->Form->create('ccr') ?>
    <fieldset>
        <legend><?= __('Add Currency Conversion Rate') ?></legend>
        <?php
            echo $this->Form->control('ckey', ['label' => 'Conversion Key']);
            echo $this->Form->control('cvalue',['label' => 'Conversion Value']);

        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit'), ['class' => 'btn waves-effect waves-light border-round col s3 mt-4', 'style' => 'background: #000;','action'=>'edit/1']) ?>
    <?= $this->Html->link(__('Back'), ['action' => 'edit'],['class' => 'btn waves-effect waves-light border-round col s3 mt-4', 'style' => 'background: #000;', 'name' => 'clicked', 'value' => 'save']) ?>
    <?= $this->Form->end() ?>
</div>
