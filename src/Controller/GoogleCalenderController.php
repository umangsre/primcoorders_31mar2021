<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Event\Event;
use Google_Client;
use Google_Service_Calendar;
use Cake\Core\Exception\Exception;
/**
 * GoogleCalender Controller
 *
 *
 * @method \App\Model\Entity\GoogleCalender[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class GoogleCalenderController extends AppController
{
    public function initialize()
    {
        parent::initialize();

        $this->loadComponent('GoogleCalender', [
            'id'=>'45427394210-pf2n906or6b43mdkphn8duo1cga8h928.apps.googleusercontent.com',
            'secret'=>'nY7R8TMIHLOqgJdokBYU0FEV',
            'timeZone'=>'Asia/Kolkata'
         ]);
    } 
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function beforeFilter( Event $event)
    {
        $this->autoRender = false;
    }
    
    
    public function auth()
    {
        if(isset($this->request->query['code']))
        {
            $this->GoogleCalendar->authCode = $this->request->query['code'];
            $this->GoogleCalendar->state = $this->request->query['state'];
        }
        elseif(isset($this->request->query['error']))
            return $this->redirect($this->referer());
        call_user_func(array($this->GoogleCalendar,$this->GoogleCalendar->state));
        if(isset($this->GoogleCalendar->returning))
            return $this->GoogleCalendar->returnData;
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $client = new Google_Client();
        //echo WWW_ROOT ;
        $client->setAuthConfig(WWW_ROOT.'credentials.json');
        $client->setApplicationName("Client_Library_Examples");
        //$client->setDeveloperKey("45427394210-pf2n906or6b43mdkphn8duo1cga8h928.apps.googleusercontent.com");
        $redirect_uri = 'http://' . $_SERVER['HTTP_HOST'] . $_SERVER['PHP_SELF'];
        $client->setRedirectUri($redirect_uri);
        try{

            
            echo '<pre>';
            print_r($array );
            echo '</pre>';
            $service = new Google_Service_Calendar($client);

            // Print the next 10 events on the user's calendar.
            $calendarId = 'primary';
            $optParams = array(
              'maxResults' => 10,
              'orderBy' => 'startTime',
              'singleEvents' => true,
              'timeMin' => date('c'),
            );
            $results = $service->events->listEvents($calendarId, $optParams);
           
            $events = $results->getItems();
            if (empty($events)) {
                print "No upcoming events found.\n";
            } else {
                print "Upcoming events:\n";
                foreach ($events as $event) {
                    $start = $event->start->dateTime;
                    if (empty($start)) {
                        $start = $event->start->date;
                    }
                    printf("%s (%s)\n", $event->getSummary(), $start);
                }
            }
    } catch(Exception $e){
          
    }
       //  $event = array(
       //      'start'=>'2014-12-31 22:00:00',
       //      'end'=>'2015-01-01 03:30:00',
       //      'summary'=>'New Years Eve Dinner',
       //      'description'=>'We will have a great party!!',
       //      'location'=>'Via Nazionale 6 Roma'
       //  );

       // $list =$this->GoogleCalendar->getCalendarList('sarbjit.rndexperts@gmail.com');
       // print_r($list);
    }

    /**
     * View method
     *
     * @param string|null $id Google Calender id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $googleCalender = $this->GoogleCalender->get($id, [
            'contain' => []
        ]);

        $this->set('googleCalender', $googleCalender);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $event = array(
            'start'=>'2020-02-14 22:00:00',
            'end'=>'2020-02-18 03:30:00',
            'summary'=>'New Years Eve Dinner',
            'description'=>'We will have a great party!!',
            'location'=>'Via Nazionale 6 Roma'
        );
        $this->GoogleCalendar->insertEvent('sarbjit.rndexperts@gmail.com', $event);
    }

    /**
     * Edit method
     *
     * @param string|null $id Google Calender id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $googleCalender = $this->GoogleCalender->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $googleCalender = $this->GoogleCalender->patchEntity($googleCalender, $this->request->getData());
            if ($this->GoogleCalender->save($googleCalender)) {
                $this->Flash->success(__('The google calender has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The google calender could not be saved. Please, try again.'));
        }
        $this->set(compact('googleCalender'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Google Calender id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $googleCalender = $this->GoogleCalender->get($id);
        if ($this->GoogleCalender->delete($googleCalender)) {
            $this->Flash->success(__('The google calender has been deleted.'));
        } else {
            $this->Flash->error(__('The google calender could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
