<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\User $user
 */
$roles = ['select' => 'Select', 'admin' => 'Admin', 'admin-claims' => 'Claims Admin', 'miller' => 'Mill', 'sales' => 'Sales', 'dealer' => 'Dealer', 'sdm' => 'SDM'];
?>
<div class="orders index large-9 medium-8 columns content card card card-default scrollspy">
    <div class="card-content">
        <div class="users form large-9 medium-8 columns content">
            <?= $this->Form->create($user) ?>
            <fieldset>
                <legend><?= __('Add User') ?></legend>
                <?php
                echo $this->Form->control('name', ['required']);
                echo $this->Form->control('username', ['label' => 'Email / Username', 'type' => 'email']);
                echo $this->Form->control('password');
                echo $this->Form->control('role', ['options' => $roles, 'onchange' => 'changeUserType(this)']);
                ?>
                <div id="sales" style="display: none;">
                    <?php
                    echo $this->Form->control('sales_phone');
                    echo $this->Form->control('sales_province');
                    echo $this->Form->control('sales_rep');
                    echo $this->Form->control('sd_rep');
                    echo $this->Form->control('spec');
                    echo $this->Form->control('sdm_id', ['options' => $sdmList]);
                    ?>
                </div>
                <div id="dealer" style="display: none;">
                    <?php
                    echo $this->Form->control('dealer_customerid');
                    echo $this->Form->control('dealer_phone');
                    echo $this->Form->control('dealer_contact_name');
                    echo $this->Form->control('dealer_address');
                    echo $this->Form->control('dealer_city');
                    echo $this->Form->control('dealer_province');
                    echo $this->Form->control('dealer_postal_code');
                    echo $this->Form->control('dealer_sin');
                    ?>
                </div>
                <div id="miller" style="display: none;">
                    <?php
                    echo $this->Form->control('mill_contact_name');
                    echo $this->Form->control('mill_phone');
                    echo $this->Form->control('mill_address');
                    echo $this->Form->control('mill_city');
                    echo $this->Form->control('mill_postal_code');
                    ?>
                </div>
            </fieldset>
            <?= $this->Form->button(__('Submit'), ['class' => 'btn waves-effect waves-light border-round gradient-45deg-purple-deep-orange col s12 mt-4 submit-user']) ?>
            <?= $this->Form->end() ?>
        </div>
    </div>
</div>
<script>
    function changeUserType(event) {
        const $userType = event.value;
        const $salesDiv = $('#sales');
        const $dealerDiv = $('#dealer');
        const $millerDiv = $('#miller');
        if ($userType === 'sales') {
            $salesDiv.show();
            $dealerDiv.hide();
            $millerDiv.hide();
        } else if ($userType === 'dealer') {
            $salesDiv.hide();
            $dealerDiv.show();
            $millerDiv.hide();
        } else if ($userType === 'miller') {
            $salesDiv.hide();
            $dealerDiv.hide();
            $millerDiv.show();
        } else {
            $salesDiv.hide();
            $dealerDiv.hide();
            $millerDiv.hide();
        }
    }
</script>
