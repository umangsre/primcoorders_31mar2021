<?php
namespace App\Controller\Api;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;
use Cake\Event\Event;
use Cake\Mailer\Email;
use Cake\Routing\Router;

/**
 * Products Controller
 *
 * @property \App\Model\Table\ProductsTable $Products
 *
 * @method \App\Model\Entity\Product[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class ProductsController extends AppController
{
    public function initialize()
    {
        parent::initialize();
        $this->loadComponent('RequestHandler');
         $this->Auth->allow([ 'view', 'index','getproductname','getitemname','getitemnumber','getproduct','getprovinces','getallsalesmen']);
    }

    /****** Rest api***/

    public function getallcustomers()
    {

        $customers = TableRegistry::get('Customers');
        $customer_data = $customers->find('all');

        $json_records = array();
        foreach ($customer_data->toList() as $key => $value) {
            $json_records[] = [
                'id' => $value->id,
                'customerid' => $value->customerid,
                'name' => $value->name,
                'contact_name' => $value->contact_name,
                'address' => $value->address,
                'city' => $value->city,
                'province' => $value->province,
                'postal_code' => $value->postal_code,
                'sin' => $value->sin,

            ];
        }

        $content = json_encode($json_records);
        $this->response = $this->response->withStringBody($content);
        $this->response = $this->response->withType('json');
        return $this->response;
    }

    public function getallsalesmen()
    {
        $this->request->allowMethod(['post', 'put']);
        $province = $this->request->getData('province');
        $salesmen = TableRegistry::get('Salesmen');
        $salesmen_data = $salesmen->find('all')->where(['province' => $province]);
        $response =['status'=> 0];
        if($salesmen_data){
            $json_records = array();
            foreach ($salesmen_data->toList() as $key => $value) {
                $json_records[] = [
                    'id' => $value->id,
                    'name' => $value->name,
                    'email' => $value->rep_email,
                    'province' =>$value->province,
                    'phone' =>$value->phone,
                    'photo' =>$value->photo

                ];
            }
            $base_urls =  Router::url('/', true) ;
            $sign_link =  $base_urls .'img/SalesRep/';

            $response =['status'=> 1];
             $response['base_url'] = $sign_link;
            $response['json_records'] = $json_records;
        }


        $content = json_encode($response);
        $this->response = $this->response->withStringBody($content);
        $this->response = $this->response->withType('json');
        return $this->response;
    }

    public function allproducts()
    {
        $product = TableRegistry::get('Products');
        $products = $product->find('all');
        $this->set([
            'products' => $products,
            '_serialize' => ['products']
        ]);
    }

    public function getallproducts()
    {
        $this->autoRender = false;
        $product = TableRegistry::get('Products');
        $product_data = $product->find('all');


        $json_records = array();
        foreach ($product_data->toList() as $key => $value) {
            $json_records[] = [
                'id' => $value->id,
                'product_name' => $value->product_name,
                'item_name' => $value->item_name,
                'item_number' => $value->item_number,
                'item_type' => $value->item_type,
                'color_name' => $value->color_name,
                'single_franklin' => $value->single_franklin,
                'bulk_franklin' => $value->bulk_franklin,
                'per_item' => $value->per_item,
                'single_item' => $value->single_item,
                'bulk_item' => $value->bulk_item,
                'franklin_type' => $value->franklin_type,
                'bulk_count' => $value->bulk_count,

            ];
        }

        $content = json_encode($json_records);
        $this->response = $this->response->withStringBody($content);
        $this->response = $this->response->withType('json');
        return $this->response;
    }

    public function getcustomers()
    {
        $this->autoRender = false;
        $customers = TableRegistry::get('Customers');
        $customer_data = $customers->find()
        ->where( [ "name LIKE" => "%".$_POST['name']."%" ] );

        $json_records = array();
        foreach ($customer_data->toList() as $key => $value) {
            $json_records[] = [
                'id' => $value->id,
                'customerid' => $value->customerid,
                'name' => $value->name,
                'address' => $value->address,
                'city' => $value->city,
                'province' => $value->province,
                'postal_code' => $value->postal_code,
                'sin' => $value->sin,

            ];
        }

        $content = json_encode($json_records);
        $this->response = $this->response->withStringBody($content);
        $this->response = $this->response->withType('json');
        return $this->response;
    }

    public function getcustomersdata()
    {
        $this->autoRender = false;
        $customers = TableRegistry::get('Customers');
        $customer_data = $customers->find('all')
        ->where( [ "id" => $_POST['customer_id'] ] );

        $json_records = array();
        foreach ($customer_data->toList() as $key => $value) {
            $json_records[] = [
                'id' => $value->id,
                'customerid' => $value->customerid,
                'name' => $value->name,
                'address' => $value->address,
                'city' => $value->city,
                'province' => $value->province,
                'postal_code' => $value->postal_code,
                'sin' => $value->sin,

            ];
        }

        $content = json_encode($json_records);
        $this->response = $this->response->withStringBody($content);
        $this->response = $this->response->withType('json');
        return $this->response;
    }

    public function getsalesmen()
    {
        $this->autoRender = false;
        $salesmen = TableRegistry::get('Salesmen');
        $salesmen_data = $salesmen->find('all')
        ->where( [ "name LIKE" => "%".$_POST['name']."%" ] );

        $json_records = array();
        foreach ($salesmen_data->toList() as $key => $value) {
            $json_records[] = [
                'id' => $value->id,
                'name' => $value->name,
            ];
        }

        $content = json_encode($json_records);
        $this->response = $this->response->withStringBody($content);
        $this->response = $this->response->withType('json');
        return $this->response;
    }


    public function getprovinces()
    {
        $this->autoRender = false;
        $salesmen = TableRegistry::get('Salesmen');
        $salesmen_data = $salesmen->find('all')
        ->distinct( 'province' )
        ->select( ['province'] );

        $json_records = array();
        foreach ($salesmen_data->toList() as $key => $value) {
            $json_records[] = [
                'name' => $value->province,
            ];
        }

        $content = json_encode($json_records);
        $this->response = $this->response->withStringBody($content);
        $this->response = $this->response->withType('json');
        return $this->response;
    }

    public function getsalesmendata()
    {
        $this->autoRender = false;
        $salesmen = TableRegistry::get('Salesmen');
        $salesmen_data = $salesmen->find('all')
        ->where( [ "id" => $_POST['salesman_id'] ] );

        $json_records = array();
        foreach ($salesmen_data->toList() as $key => $value) {
            $json_records[] = [
                'id' => $value->id,
                'name' => $value->name,
                'email' => $value->rep_email,
            ];
        }

        $content = json_encode($json_records);
        $this->response = $this->response->withStringBody($content);
        $this->response = $this->response->withType('json');
        return $this->response;
    }

    public function getproductname()
    {
        //$this->autoRender = false;
        $this->request->allowMethod(['post', 'put']);
        $products = TableRegistry::get('Products');
        $salesmen_data = $products->find('all')
        ->distinct( 'product_name' )
        ->select( ['product_name'] );
        //->where( [ "product_name" => $_POST['product_name'] ] );

        $json_records = array();
        foreach ($salesmen_data->toList() as $key => $value) {
            $json_records[] = [
                //'id' => $value->id,
                'product_name' => $value->product_name,
            ];
        }

        //print_r($json_records);
        $this->set([
            'json_records' => $json_records,
            '_serialize' => ['json_records']
                ]);

         //$this->set(compact('json_records'));
    }

    public function getitemname()
    {
        $this->request->allowMethod(['post', 'put']);
        //$this->autoRender = false;
        $products = TableRegistry::get('Products');
        $salesmen_data = $products->find('all')
        ->distinct( 'item_name' )
        ->select( ['item_name'] )
        ->where( [ "product_name" => $this->request->getData('product_name') ] );

        $json_records = array();
        foreach ($salesmen_data->toList() as $key => $value) {
            $json_records[] = [
                //'id' => $value->id,
                'item_name' => $value->item_name,
            ];
        }
        //print_r($json_records);
        $this->set([
            'json_records' => $json_records,
            '_serialize' => ['json_records']
                ]);

         //$this->set(compact('json_records'));
    }

    public function getitemnumber()
    {
        $this->request->allowMethod(['post', 'put']);
        $products = TableRegistry::get('Products');
        $products_data = $products->find('all')
        //->select( [ 'id', 'item_number'] )
        ->where( [ "product_name" => $this->request->getData('product_name'), 'item_name' =>$this->request->getData('item_name') ] );

        $json_records = array();
        foreach ($products_data->toList() as $key => $value) {
            $json_records[] = [
                'id' => $value->id,
                'product_name' => $value->product_name,
                'item_name' => $value->item_name,
                'item_number' => $value->item_number,
                'item_type' => $value->item_type,
                'color_name' => $value->color_name,
                'single_franklin' => $value->single_franklin,
                'bulk_franklin' => $value->bulk_franklin,
                'per_item' => $value->per_item,
                'single_item' => $value->single_item,
                'bulk_item' => $value->bulk_item,
                'franklin_type' => $value->franklin_type,
                'bulk_count' => $value->bulk_count,

            ];
        }

         $this->set([
            'json_records' => $json_records,
            '_serialize' => ['json_records']
                ]);


          //$this->set(compact('json_records'));

       /*  $content = json_encode($json_records);
        $this->response = $this->response->withStringBody($content);
        $this->response = $this->response->withType('json');
        return $this->response; */
    }

    public function getproduct()
    {
        $this->request->allowMethod(['post', 'put']);
        $product = TableRegistry::get('Products');
        $product_data = $product->find('all')
        ->where( [ "id" => $this->request->getData('product_id') ] );

        $json_records = array();
        foreach ($product_data->toList() as $key => $value) {
            $json_records[] = [
                'id' => $value->id,
                'product_name' => $value->product_name,
                'item_name' => $value->item_name,
                'item_number' => $value->item_number,
                'item_type' => $value->item_type,
                'color_name' => $value->color_name,
                'single_franklin' => $value->single_franklin,
                'bulk_franklin' => $value->bulk_franklin,
                'per_item' => $value->per_item,
                'single_item' => $value->single_item,
                'bulk_item' => $value->bulk_item,
                'franklin_type' => $value->franklin_type,
                'bulk_count' => $value->bulk_count,

            ];
        }


         $this->set([
            'json_records' => $json_records,
            '_serialize' => ['json_records']
                ]);
    }

    /***** Rest api end *****/
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $products = $this->paginate($this->Products);

        $this->set(compact('products'));
    }

    /**
     * View method
     *
     * @param string|null $id Product id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $product = $this->Products->get($id, [
            'contain' => []
        ]);

        $this->set('product', $product);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $product = $this->Products->newEntity();
        if ($this->request->is('post')) {
            $product = $this->Products->patchEntity($product, $this->request->getData());
            if ($this->Products->save($product)) {
                $this->Flash->success(__('The product has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The product could not be saved. Please, try again.'));
        }
        $this->set(compact('product'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Product id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $product = $this->Products->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $product = $this->Products->patchEntity($product, $this->request->getData());
            if ($this->Products->save($product)) {
                $this->Flash->success(__('The product has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The product could not be saved. Please, try again.'));
        }
        $this->set(compact('product'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Product id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $product = $this->Products->get($id);
        if ($this->Products->delete($product)) {
            $this->Flash->success(__('The product has been deleted.'));
        } else {
            $this->Flash->error(__('The product could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
