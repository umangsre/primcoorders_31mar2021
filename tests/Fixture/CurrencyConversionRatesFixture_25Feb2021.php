<?php
namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * CurrencyConversionRatesFixture
 */
class CurrencyConversionRatesFixture extends TestFixture
{
    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'id' => ['type' => 'biginteger', 'length' => 20, 'unsigned' => true, 'null' => false, 'default' => null, 'comment' => '', 'autoIncrement' => true, 'precision' => null],
        'cad' => ['type' => 'float', 'length' => 20, 'precision' => 4, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => ''],
        'usd_mannington' => ['type' => 'float', 'length' => 20, 'precision' => 4, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => ''],
        'usd_general' => ['type' => 'float', 'length' => 20, 'precision' => 4, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => ''],
        'usd_others' => ['type' => 'float', 'length' => 20, 'precision' => 4, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => ''],
        'usd_custom_1' => ['type' => 'float', 'length' => 20, 'precision' => 4, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => ''],
        'usd_custom_2' => ['type' => 'float', 'length' => 20, 'precision' => 4, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => ''],'created' => ['type' => 'datetime', 'length' => null, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null],
        'updated' => ['type' => 'datetime', 'length' => null, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['id'], 'length' => []],
        ],
        '_options' => [
            'engine' => 'InnoDB',
            'collation' => 'latin1_swedish_ci'
        ],
    ];
    // @codingStandardsIgnoreEnd
    /**
     * Init method
     *
     * @return void
     */
    public function init()
    {
        $this->records = [
            [
                'id' => 1,
                'cad' => 1,
                'usd_mannington' => 1,
                'usd_general' => 1,
                'usd_custom_1' => 1,
                'usd_custom_2' => 1,
                'created' => '2020-02-25 07:19:15',
                'updated' => '2020-02-25 07:19:15'
            ],
        ];
        parent::init();
    }
}
