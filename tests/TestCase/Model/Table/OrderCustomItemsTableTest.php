<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\OrderCustomItemsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\OrderCustomItemsTable Test Case
 */
class OrderCustomItemsTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\OrderCustomItemsTable
     */
    public $OrderCustomItems;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.OrderCustomItems',
        'app.Orders'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('OrderCustomItems') ? [] : ['className' => OrderCustomItemsTable::class];
        $this->OrderCustomItems = TableRegistry::getTableLocator()->get('OrderCustomItems', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->OrderCustomItems);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
