<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Event $event
 */
?>
<div class="orders index large-9 medium-8 columns content card card card-default scrollspy">
    <div class="card-content">
        <div class="events view large-9 medium-8 columns content">
            <h3><?= h($event->name) ?></h3>
            <table class="vertical-table">
                <tr>
                    <th scope="row"><?= __('Name') ?></th>
                    <td><?= h($event->name) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Location') ?></th>
                    <td><?= h($event->location) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Image') ?></th>
                    <td> <img src="<?= $event->image; ?>" height="150px" width="200px"></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Id') ?></th>
                    <td><?= $this->Number->format($event->id) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Dated And Time') ?></th>
                    <td><?= h($event->dated_and_time) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Created') ?></th>
                    <td><?= h($event->created) ?></td>
                </tr>
            </table>
            <div class="row">
                <h4><?= __('Description') ?></h4>
                <?= $this->Text->autoParagraph(h($event->description)); ?>
            </div>
            <div class="related">
                <h4><?= __('Related Sub Events') ?></h4>
                <?php if (!empty($event->sub_events)): ?>
                <table cellpadding="0" cellspacing="0">
                    <tr>
                        <th scope="col"><?= __('Id') ?></th>
                        <th scope="col"><?= __('Event Id') ?></th>
                        <th scope="col"><?= __(' Event Name') ?></th>
                      <!--   <th scope="col"><?= __(' Time') ?></th> -->
                        <th scope="col"><?= __('From Time') ?></th>
                        <th scope="col"><?= __('To Time') ?></th>
                        <th scope="col"><?= __('Event Date') ?></th>
                        <th scope="col" class="actions"><?= __('Actions') ?></th>
                    </tr>
                    <?php foreach ($event->sub_events as $subEvents): ?>
                    <tr>
                        <td><?= h($subEvents->id) ?></td>
                        <td><?= h($subEvents->event_id) ?></td>
                        <td><?= h($subEvents->name) ?></td>
                       <!--  <td><?= h($subEvents->_time) ?></td> -->
                        <td><?= h($subEvents->from_time) ?></td>
                        <td><?= h($subEvents->to_time) ?></td>
                        <td><?= h($subEvents->event_date) ?></td>
                        <td class="actions">
                            <?= $this->Html->link(__('View'), ['controller' => 'SubEvents', 'action' => 'view', $subEvents->id]) ?>
                            <?= $this->Html->link(__('Edit'), ['controller' => 'SubEvents', 'action' => 'edit', $subEvents->id]) ?>
                            <?= $this->Form->postLink(__('Delete'), ['controller' => 'SubEvents', 'action' => 'delete', $subEvents->id], ['confirm' => __('Are you sure you want to delete # {0}?', $subEvents->id)]) ?>
                        </td>
                    </tr>
                    <?php endforeach; ?>
                </table>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>
