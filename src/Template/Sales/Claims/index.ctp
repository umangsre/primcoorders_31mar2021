<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Claim[]|\Cake\Collection\CollectionInterface $claims
 */
?>
<!--nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Claim'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Customers'), ['controller' => 'Customers', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Customer'), ['controller' => 'Customers', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Consumers'), ['controller' => 'Consumers', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Consumer'), ['controller' => 'Consumers', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Claim Products'), ['controller' => 'ClaimProducts', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Claim Product'), ['controller' => 'ClaimProducts', 'action' => 'add']) ?></li>
    </ul>
</nav  -->
<style>
    .message.success {
        margin-top: 1rem;
        background: #0d4219;
        text-align: center;
        color: #fff;
        -webkit-transition: -webkit-box-shadow .25s;
        -moz-transition: box-shadow .25s;
        -o-transition: box-shadow .25s;
        transition: -webkit-box-shadow .25s;
        transition: box-shadow .25s;
        transition: box-shadow .25s, -webkit-box-shadow .25s;
        border-radius: 2px;
    }
</style>
<div class="orders index large-9 medium-8 columns content card card card-default scrollspy">
    <div class="card-content">
        <div class="claims index large-9 medium-8 columns content">

            <table cellpadding="0" cellspacing="0">
                <thead>
                    <tr>
                        <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                        <th scope="col"><?= $this->Paginator->sort('customer_name') ?></th>
                        <th scope="col"><?= $this->Paginator->sort('retailer_name') ?></th>
                        <th scope="col"><?= $this->Paginator->sort('total_claim_products') ?></th>
                        <th scope="col"><?= $this->Paginator->sort('status') ?></th>
                        <th scope="col"><?= $this->Paginator->sort('created_date') ?></th -->
                        <th scope="col" class="actions"><?= __('Actions') ?></th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($claims as $claim): ?>
                    <tr>
                        <td><?= $this->Number->format($claim->id) ?></td>
                        <td><?= h($claim->customer->name) ?></td>
                        <td><?php echo !empty($claim->retailer) ? ($claim->retailer->retailer_name) : '' ?></td>
                        <td><?= is_array($claim->claim_json) ? h(count($claim->claim_json)) : 0 ?></td>
                        <?php
                        $claimStatuses = $claim->claim_status;
                        $totalStatuses = count($claimStatuses);
                        $status = 'Pending';
                        if($totalStatuses > 0) {
                            $status = $claimStatuses[$totalStatuses - 1]['status'];
                        }
                        ?>
                        <td><?= h(ucfirst($status)) ?></td>
                        <td><?= h($claim->created_date) ?></td>
                        <td class="actions">
                            <?= $this->Html->link(__('View'), ['action' => 'view', $claim->id]) ?>
                            <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $claim->id], ['confirm' => __('Are you sure you want to delete # {0}?', $claim->id)]) ?>
                        </td>
                    </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
            <div class="paginator">
                <ul class="pagination">
                    <?= $this->Paginator->first('<< ' . __('first')) ?>
                    <?= $this->Paginator->prev('< ' . __('previous')) ?>
                    <?= $this->Paginator->numbers() ?>
                    <?= $this->Paginator->next(__('next') . ' >') ?>
                    <?= $this->Paginator->last(__('last') . ' >>') ?>
                </ul>
                <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
            </div>
        </div>
    </div>
</div>
