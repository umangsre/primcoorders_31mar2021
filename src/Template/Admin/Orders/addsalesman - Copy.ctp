<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Order $order
 */
?>

<div class="row">
    <div class="col-12 logo-container text-center p-4">
        <?= $this->Html->image('logo.png'); ?>           
    </div>
    <div class="col-sm-12 col-12 full-width-mobile">
        <div class="steps-right-form">
            <div class="steps_nav text-center pt-3 mb-3 row">
                <div class="steps_item col">
                    <p>1<br>
                        <em>Dealer Section</em>
                    </p>
                </div>
                <div class="steps_item active  col">
                    <p>2<br>
                        <em>Sales Section</em>
                    </p>
                </div>
                <div class="steps_item col">
                    <p>3<br>
                        <em>Order Form</em>
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="orders form large-9 medium-8 columns content">
    <?= $this->Form->create($order) ?>
    <div class="row">
        <div class="tab w-100">
            <div class="form-fields">
                <div class="row">
                    <div class="col-sm-6 col-12 mt-3">
                        <label>* Sale Representative</label>
                        <?= $this->Form->control( 'salesman_name', [ 'type' => 'text', 'required' => 'required', 'id' => 'salesman_name', 'class' => 'form-control', 'label' => false ] ) ?>
                        <div id="suggesstion-salesman" class="list_wraper" style="display:none;"></div>
                        <?= $this->Form->control( 'salesman_id', [ 'type' => 'hidden', 'required' => 'required', 'id' => 'salesman_id', 'class' => 'form-control', 'label' => false ] ) ?>
                    </div>
                    <div class="col-sm-6 col-12 mt-3">
                        <label>* Sale Representative Email</label>
                        <?= $this->Form->control( 'rep_email', [ 'type' => 'text', 'required' => 'required', 'id' => 'rep_email', 'class' => 'form-control', 'label' => false ] ) ?>
                        
                    </div>
                    <div class="col-sm-6 col-12 mt-3">
                        <label>* Event</label>
                        <?= $this->Form->control( 'events', [ 'type' => 'text', 'required' => 'required', 'id' => 'events', 'class' => 'form-control', 'label' => false ] ) ?>
                        
                    </div>
                    <div class="col-sm-6 col-12 mt-3">
                        <label>Sale Order #</label>
                        <?= $this->Form->control( 'sale_order_number', [ 'type' => 'text', 'required' => 'required', 'id' => 'sale_order_number', 'class' => 'form-control', 'label' => false ] ) ?>
                        
                    </div>
                    <div class="col-sm-6 col-12 mt-3">
                        <label>Incentive acknowledgement</label>
                        <?= $this->Form->control( 'incentive_reciept', [ 'type' => 'text', 'required' => 'required', 'id' => 'incentive_reciept', 'class' => 'form-control', 'label' => false ] ) ?>
                        
                    </div>
                    <div class="col-sm-6 col-12 mt-3">
                        <label>Sale Date</label>
                        <?= $this->Form->control( 'sale_date', [ 'type' => 'date', 'required' => 'required', 'id' => 'sale_date', 'class' => 'form-control', 'label' => false ] ) ?>
                        
                    </div>
                    <div class="col-sm-6 col-12 mt-3">
                        <label>Purchaser's Name</label>
                        <?= $this->Form->control( 'purchaser_name', [ 'type' => 'text', 'required' => 'required', 'id' => 'purchaser_name', 'class' => 'form-control', 'label' => false ] ) ?>
                        
                    </div>
                    <div class="col-sm-12 col-12 mt-3">
                        <?= $this->Form->button(__('Next'), array( 'class' => 'btn btn-primary' )  ) ?>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>    

    
    <?= $this->Form->end() ?>
</div>
