<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\OnlineUserRegistration $onlineUserRegistration
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List Online User Registration'), ['action' => 'index']) ?></li>
    </ul>
</nav>
<div class="onlineUserRegistration form large-9 medium-8 columns content">
    <?= $this->Form->create($onlineUserRegistration) ?>
    <fieldset>
        <legend><?= __('Add Online User Registration') ?></legend>
        <?php
            echo $this->Form->control('first__last_name');
            echo $this->Form->control('email');
            echo $this->Form->control('user_type');
            echo $this->Form->control('can_see_account_details');
            echo $this->Form->control('can_see_placed_order');
            echo $this->Form->control('can_see_invoiced_order');
            echo $this->Form->control('can_see_credit_memos');
            echo $this->Form->control('can_see_prices');
            echo $this->Form->control('can_see_price_list');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
