<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Claim $claim
 */

?>
<div class="orders index large-9 medium-8 columns content card card card-default scrollspy">
    <div class="card-content">
        <div class="claims form large-9 medium-8 columns content card card-default" style="padding: 2rem;">
            <?= $this->Form->create('send_emails', ['id' => 'send_emails_form']) ?>
            <div>
                <h5 style="margin-bottom: 2rem;"><?= __('Send Email') ?></h5>
                <div id="errors"></div>
                <fieldset style="margin-bottom: 2rem;">
                    <legend><?= __('Send Email') ?></legend>
                    <?php
                    echo $this->Form->control('claim_id', ['type' => 'hidden', 'id' => 'claim_id', 'value' => $claimId]);
                    echo $this->Form->control('email_ids', ['type' => 'textarea', 'id' => 'email_ids']);
                    ?>
                </fieldset>
            </div>
            <?= $this->Form->button(__('Send'), ['class' => 'btn waves-effect waves-light border-round col s3 mt-4', 'style' => 'background: #000;', 'name' => 'clicked', 'value' => 'save', 'action'=>'index']) ?>
            <?= $this->Html->link(__('Back'), ['action' => 'index'],['class' => 'btn waves-effect waves-light border-round col s3 mt-4', 'style' => 'background: #000;', 'name' => 'clicked', 'value' => 'save', 'action'=>'index']) ?>
            <?= $this->Form->end() ?>
        </div>
    </div>
</div>
<script>
    const userRole = '<?php echo $currentUserInfo['role']; ?>';
</script>
