




<div class="orders index large-9 medium-8 columns content card card card-default scrollspy">
    <div class="card-content">
        <div class="families form large-9 medium-8 columns content">
			<div class="content">
			<h3>Upload File</h3>
				<?= $this->Flash->render() ?>
				<div class="upload-frm">
					<?php echo $this->Form->create($uploadData, ['type' => 'file']); ?>
						<?php echo $this->Form->input('file', ['type' => 'file', 'class' => 'form-control']); ?>
						<?php echo $this->Form->button(__('Upload File'), ['type'=>'submit', 'class' => 'form-controlbtn btn-default']); ?>
					<?php echo $this->Form->end(); ?>
					
					<?= $this->Html->link(__('Excel'), ['action' => 'index', '_ext'=>'xlsx']); ?>
				</div>
			</div>
		</div>
	</div>
</div>
