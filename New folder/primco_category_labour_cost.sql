-- phpMyAdmin SQL Dump
-- version 4.9.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Feb 20, 2021 at 07:49 AM
-- Server version: 5.7.32-35-log
-- PHP Version: 7.3.27

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `dbw9kb42bvzy8y`
--

-- --------------------------------------------------------

--
-- Table structure for table `primco_category_labour_cost`
--

CREATE TABLE `primco_category_labour_cost` (
  `id` bigint(20) NOT NULL,
  `parent_category` varchar(767) DEFAULT NULL,
  `category` varchar(767) DEFAULT NULL,
  `uom` varchar(767) DEFAULT NULL,
  `labour_cost_sq_ft` varchar(767) DEFAULT NULL,
  `labour_cost_sq_yards` varchar(767) DEFAULT NULL,
  `labour_cost_linear_ft` varchar(767) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `primco_category_labour_cost`
--

INSERT INTO `primco_category_labour_cost` (`id`, `parent_category`, `category`, `uom`, `labour_cost_sq_ft`, `labour_cost_sq_yards`, `labour_cost_linear_ft`) VALUES
(1, NULL, 'Carpet', 'sq.yards.', '0.88', '7', '2.64'),
(2, NULL, 'Wood', NULL, NULL, NULL, NULL),
(3, NULL, 'Vinyl', NULL, NULL, NULL, NULL),
(4, '2', 'Floating Floor', 'sq.ft.', '3.90', '35.10', '11.7'),
(5, '2', 'Gluedown Floor', 'sq.ft.', '5.85', '52.65', '17.55'),
(6, '2', 'Stapled Floor', 'sq.ft.', '5.20', '46.80', '15.60'),
(7, '3', 'Without Underlayment / Embossing', 'sq.ft.', '3.16', '28.60', NULL),
(9, '3', 'With Underlayment', 'per sheet', '65', NULL, NULL),
(10, '3', 'With Embossing', 'sq.ft.', '4.33', '39', '13'),
(12, NULL, 'Luxury Vinyl', 'sq.ft.', '4.55', '40.95', '13.65'),
(13, NULL, 'Laminate', 'sq.ft.', '3.12', '28.08', '9.36'),
(14, NULL, 'Underlay', 'sq.yards.', '0.88', '7', '2.64'),
(15, NULL, 'Rubber', 'sq.ft.', '0.88', '7', '2.64');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `primco_category_labour_cost`
--
ALTER TABLE `primco_category_labour_cost`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id` (`id`) USING BTREE;

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `primco_category_labour_cost`
--
ALTER TABLE `primco_category_labour_cost`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
