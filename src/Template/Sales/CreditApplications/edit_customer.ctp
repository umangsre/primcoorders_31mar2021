<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\CreditApplication $creditApplication
 */

$options = array(
                    '1' => 'Yes',
                    '0' => 'No'
            );
?>

<div class="row">
    <div class="col-12 logo-container text-center p-4">
        <?= $this->Html->image('logo.png'); ?>           
    </div>
</div>
<div class="orders form large-9 medium-8 columns content container">
    <?= $this->Form->create($creditApplication,['id' => 'customer_form']) ?>
    
	<div class="row">
        <h2 class="_heading"><?= __('CREDIT APPLICATION FOR NEW CUSTOMER') ?></h2>
	</div>
	<div class="row">
		<div class="col-sm-6 col-12 mt-3">
			<?php  echo $this->Form->control('company_legal_name',['class' => 'form-control','required' => 'required', 'label' => 'Company Legal Name:*' ]); ?>
		</div>
		<div class="col-sm-6 col-12 mt-3">
            <?php echo $this->Form->control('company_trade_name',['class' => 'form-control','required' => 'required',  'label' => 'Company Trade Name:*' ]); ?>
		</div>
		<div class="col-sm-6 col-12 mt-3">
            <?php echo $this->Form->control('street_address',['class' => 'form-control','required' => 'required',  'label' => 'Street Address:*' ]); ?>
		</div>
		<div class="col-sm-6 col-12 mt-3">
            <?php echo $this->Form->control('phone',['class' => 'form-control','required' => 'required', 'label' => 'Phone* #' ]); ?>
		</div>
		<div class="col-sm-6 col-12 mt-3">
            <?php echo $this->Form->control('fax',['class' => 'form-control' , 'label' => "Fax #"]); ?>
		</div>
		<div class="col-sm-6 col-12 mt-3">
            <?php echo $this->Form->control('city',['class' => 'form-control','required' => 'required',  'label' => 'City:*' ]); ?>
		</div>
		<div class="col-sm-6 col-12 mt-3">
            <?php echo $this->Form->control('province',['class' => 'form-control','required' => 'required',  'label' => 'Province:*' ]); ?>
		</div>
		<div class="col-sm-6 col-12 mt-3">
            <?php echo $this->Form->control('postal_code',['class' => 'form-control','required' => 'required',  'label' => 'Postal Code:*' ]); ?>
		</div>
		<div class="col-sm-6 col-12 mt-3">
            <?php echo $this->Form->control('email_address',['class' => 'form-control','type' =>'email','required' => 'required',  'label' => 'Email Address:*' ]); ?>
		</div>
		<div class="col-sm-6 col-12 mt-3"></div>
	</div>
	<div class="row">
		<h2  class="_heading"><?= __('Owner Information') ?></h2>
	</div>
	<div class="row">
		<div class="col-sm-6 col-12 mt-3">
			<?php echo $this->Form->control('owner_name',['class' => 'form-control','required' => 'required',  'label' => 'Owners Name:*' ]); ?>
		</div>
		<div class="col-sm-6 col-12 mt-3">
			<?php echo $this->Form->control('owner_home_address',['class' => 'form-control','required' => 'required',  'label' => 'Owners Home Address:*' ]); ?>
		</div>
		<div class="col-sm-6 col-12 mt-3">
			<?php echo $this->Form->control('owner_home_phone',['class' => 'form-control','required' => 'required', 'label' => 'Owners Home Phone* #'  ]); ?>
		</div>
		<div class="col-sm-6 col-12 mt-3">
			<?php echo $this->Form->control('owner_cell_phone',['class' => 'form-control','required' => 'required', 'label' => 'Owners Cell Phone* #'   ]); ?>
		</div>
	</div>
	<div class="row">
		<h2 class="_heading"><?= __('Management Information') ?></h2>
	</div>
	<div class="row">
		<div class="col-sm-6 col-12 mt-3">
            <?php echo $this->Form->control('manager_name',['class' => 'form-control','required' => 'required',  'label' => 'Manager Name:*' ]); ?>
		</div>
		<div class="col-sm-6 col-12 mt-3">
            <?php echo $this->Form->control('manager_phone',['class' => 'form-control','required' => 'required', 'label' => 'Manager Phone* #'   ]); ?>
		</div>
		<div class="col-sm-6 col-12 mt-3">
            <?php echo $this->Form->control('manager_finacial_address',['class' => 'form-control',  'label' => 'Finacial Institution Including Finacial Address:' ]); ?>
		</div>
		<div class="col-sm-6 col-12 mt-3">
            <?php echo $this->Form->control('manager_bank_account',['class' => 'form-control',  'label' => 'Bank Account Information(please provide void cheque):' ]); ?>
		</div>
		<div class="col-sm-6 col-12 mt-3">
            <?php echo $this->Form->control('manager_city',['class' => 'form-control',  'label' => 'City:' ]); ?>
		</div>
		<div class="col-sm-6 col-12 mt-3">
            <?php echo $this->Form->control('manager_province',['class' => 'form-control',  'label' => 'Province:' ]); ?>
		</div>
		<div class="col-sm-6 col-12 mt-3">
            <?php echo $this->Form->control('manager_postal_code',['class' => 'form-control',  'label' => 'Postal Code:*' ]); ?>
		</div>
		<div class="col-sm-6 col-12 mt-3">
            <?php echo $this->Form->control('manager_phone_2',['class' => 'form-control',  'label' => 'Phone #'   ]);	?>
		</div>
		<div class="col-sm-6 col-12 mt-3">
            <?php echo $this->Form->control('manager_gst',['class' => 'form-control','required' => 'required',  'label' => 'GST/HST Registration #*' ]); ?>
		</div>
		<div class="col-sm-6 col-12 mt-3">
            <?php echo $this->Form->control('province_sales_tax',['class' => 'form-control','required' => 'required', 'label' => 'Provincial Sales Tax #:*' ]); ?>
		</div>
		<div class="col-sm-6 col-12 mt-3">
            <?php echo $this->Form->control('amount_credit_required',[ 'type'=> 'number' ,'class' => 'form-control','required' => 'required',  'label' => 'Amount of Credit Required:*' ]); ?>
		</div>
		<div class="col-sm-6 col-12 mt-3">
            <?php echo $this->Form->control('yearly_sales_volume',['type'=> 'number','class' => 'form-control', 'label' => 'Expected Sales Volume Per Year:' ]); ?>
		</div>
		<div class="col-sm-6 col-12 mt-3">
            <?php echo $this->Form->control('fiscal_year_end',['class' => 'form-control','label' => 'Fiscal Year End:' ]);	?>
		</div>
		<div class="col-sm-6 col-12 mt-3">
            <?php echo $this->Form->control('is_group',[ 'id' => 'is_group','class' => 'form-control','options' => $options,'default' => '0',  'label' => 'Are you part of a Buy Group']); ?>
		</div>
		<div class="col-sm-3 col-12 mt-3"></div>
		<div id="group_option" class="col-sm-6 col-12 mt-3">
            <?php echo $this->Form->control('group_name',['class' => 'group_name form-control', 'label' => 'Buy Group Name:']); ?>
		</div>		
		
    </div>
	<div class="col-sm-6 col-12 mt-3 button_right">
    	<?= $this->Form->button(__('Next'),[ 'id' =>'customer_submit','class' => 'btn btn-primary' ]) ?>
	</div>
    <?= $this->Form->end() ?>
</div>
