<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\OnlineUserRegistration $onlineUserRegistration
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Online User Registration'), ['action' => 'edit', $onlineUserRegistration->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Online User Registration'), ['action' => 'delete', $onlineUserRegistration->id], ['confirm' => __('Are you sure you want to delete # {0}?', $onlineUserRegistration->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Online User Registration'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Online User Registration'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Credit Applications'), ['controller' => 'CreditApplications', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Credit Application'), ['controller' => 'CreditApplications', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="onlineUserRegistration view large-9 medium-8 columns content">
    <h3><?= h($onlineUserRegistration->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Credit Application') ?></th>
            <td><?= $onlineUserRegistration->has('credit_application') ? $this->Html->link($onlineUserRegistration->credit_application->id, ['controller' => 'CreditApplications', 'action' => 'view', $onlineUserRegistration->credit_application->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('User Type') ?></th>
            <td><?= h($onlineUserRegistration->user_type) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Submission Date') ?></th>
            <td><?= h($onlineUserRegistration->submission_date) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Customer Number') ?></th>
            <td><?= h($onlineUserRegistration->customer_number) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Company Name') ?></th>
            <td><?= h($onlineUserRegistration->company_name) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Administartor Will See') ?></th>
            <td><?= h($onlineUserRegistration->administartor_will_see) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Standard Name') ?></th>
            <td><?= h($onlineUserRegistration->standard_name) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Standard Email') ?></th>
            <td><?= h($onlineUserRegistration->standard_email) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Standard Name1') ?></th>
            <td><?= h($onlineUserRegistration->standard_name1) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Standard Email1') ?></th>
            <td><?= h($onlineUserRegistration->standard_email1) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Standard Name2') ?></th>
            <td><?= h($onlineUserRegistration->standard_name2) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Standard Email2') ?></th>
            <td><?= h($onlineUserRegistration->standard_email2) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Standard Name3') ?></th>
            <td><?= h($onlineUserRegistration->standard_name3) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Standard Email3') ?></th>
            <td><?= h($onlineUserRegistration->standard_email3) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('First  Last Name') ?></th>
            <td><?= h($onlineUserRegistration->first__last_name) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('User Email') ?></th>
            <td><?= h($onlineUserRegistration->user_email) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Print Name') ?></th>
            <td><?= h($onlineUserRegistration->print_name) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Authorized Signature') ?></th>
            <td><?= h($onlineUserRegistration->authorized_signature) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __(' Date') ?></th>
            <td><?= h($onlineUserRegistration->_date) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __(' Title') ?></th>
            <td><?= h($onlineUserRegistration->_title) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($onlineUserRegistration->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Can See Account Details') ?></th>
            <td><?= $this->Number->format($onlineUserRegistration->can_see_account_details) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Can See Placed Order') ?></th>
            <td><?= $this->Number->format($onlineUserRegistration->can_see_placed_order) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Can See Invoiced Order') ?></th>
            <td><?= $this->Number->format($onlineUserRegistration->can_see_invoiced_order) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Can See Credit Memos') ?></th>
            <td><?= $this->Number->format($onlineUserRegistration->can_see_credit_memos) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Can See Prices') ?></th>
            <td><?= $this->Number->format($onlineUserRegistration->can_see_prices) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Can See Price List') ?></th>
            <td><?= $this->Number->format($onlineUserRegistration->can_see_price_list) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Anti Spam Consent') ?></th>
            <td><?= $this->Number->format($onlineUserRegistration->anti_spam_consent) ?></td>
        </tr>
    </table>
</div>
