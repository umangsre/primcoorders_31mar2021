<?php
namespace App\Controller\Api;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;
use Cake\Event\Event;
use Cake\Mailer\Email;
use Cake\Routing\Router;
use \PHPQRCode\QRcode;

/**
 * Orders Controller
 *
 * @property \App\Model\Table\OrdersTable $Orders
 *
 * @method \App\Model\Entity\Order[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class OrdersController extends AppController
{
    public function initialize()
    {
        parent::initialize();
        $this->loadComponent('RequestHandler');
        $this->Auth->allow([ 'appAdd','pdfView', 'index','base64test']);
    }


    /*** rest api ***/

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
         $this->request->allowMethod(['post', 'put']);
        $order = $this->Orders->newEntity();
        if ($this->request->is('post')) {
          // $jsonData = $this->request->input('json_decode');
            // print_r($this->request->getData());
            // die('here');
            $order = $this->Orders->patchEntity($order, $this->request->getData());
            if ($this->Orders->save($order)) {
                $OrderItems = TableRegistry::get('OrderItems');
                
                foreach($this->request->getData('orderItems') as $item){
                    $OrderItem = $OrderItems->newEntity();
                    $OrderItem = $OrderItems->patchEntity($OrderItem, $item);
                    $OrderItem->order_id =$order->id;
                    if ($OrderItems->save($OrderItem)) {

                    }    
                }
                $this->set('order', $order);

            }
            //$this->Flash->error(__('The order could not be saved. Please, try again.'));
        }
       // $customers = $this->Orders->Customers->find('list', ['limit' => 200]);
       // $salesmen = $this->Orders->Salesmen->find('list', ['limit' => 200]);
        //$this->set(compact('order', 'customers', 'salesmen'));
    }

    public function appAdd()
    {
        $this->request->allowMethod(['post', 'put','delete']);
        $order = $this->Orders->newEntity();
        $this->loadModel('OrderItems');
        $response =['pdf_link' =>'','qrcode_link' => '','sign_link' =>'' ];
         $link='';
        if ($this->request->is(['patch', 'post', 'put'])) {

           // print_r($this->request->getData());

            if($id = $this->request->getData('id')){
               $order = $this->Orders->get($id, [
                    'contain' => ['Customers', 'Salesmen', 'OrderItems']
                ]); 
            }
           
            $order_id ='';
            $output_file='temp.txt';
            $_orderItems = json_decode( $this->request->getData('orderItems'),true);

            $order = $this->Orders->patchEntity($order, $this->request->getData());
            if ($this->Orders->save($order)) {
                $order_id =$order->id;
                $order['call_status'] = 1;
                $OrderItems = TableRegistry::get('OrderItems');
                $orderItemsData = $this->request->getData('orderItems');
                $existingproducts = [];
                if (!empty($order->order_items)){
                   
                    foreach ($order->order_items as $__orderItem){
                         $existingproducts[] =$__orderItem->product_id;
                    }
                }
                if(!empty($_orderItems)){
                foreach( $_orderItems as $key => $item ){
                                       
                    if( $item['franklin_enabled'] == "true" || $item['franklin_enabled'] == "1" ) {

                        $item['franklin_enabled'] = 1;
                    } else {
                        $item['franklin_enabled'] = 0;
                        $item['incentives'] = 0;
                    }
                    
                    if( in_array( $item['product_id'], $existingproducts ) ){
                        
                        $key = array_search( $item['product_id'], $existingproducts); 
                        //die;
                        
                        if( $key >= 0 ) {
                            unset($existingproducts[$key]) ; 
                        }
                        
                        $OrderItem = $this->OrderItems->find('all', [
                                        'contain' => [],
                                        'conditions' =>['order_id' => $order_id ,'product_id' =>$item['product_id']]
                                    ])->first();
                    } else {
                       $OrderItem = $this->OrderItems->newEntity();  
                    }
                   
                    $OrderItem = $this->OrderItems->patchEntity($OrderItem, $item);
                    //print_r($OrderItem);

                    $OrderItem->order_id =$order->id;
                    if ($this->OrderItems->save($OrderItem)) {

                    }    
                }
				}
                if(!empty($existingproducts)){
                     foreach($existingproducts as $product ){
                        $orderItem = $this->OrderItems->find('all', [
                                            'contain' => [],
                                            'conditions' =>['order_id' => $order_id,'product_id' =>$product]
                                      ])->first();
                        if ($this->OrderItems->delete($orderItem)) {
                            
                        }
                    }
                }
               
                $qrcode_img ='';
                if($order_id){
                   
                    $link =  Router::url([ 
                                    'controller' => 'Orders', 
                                    'action' => 'pdf-view',
                                    base64_encode($order_id )
                                  ],
                                  TRUE);               
                    $base_urls =  Router::url('/', true) ;
                    $path_qr = WWW_ROOT .'img/QRcode/'; 
                    $path_sign = WWW_ROOT .'img/Signs/';
                    $qr_image_name = 'qr_codes_'.$order_id."_order.png"; 
                    $qrcode_img = $path_qr.$qr_image_name;
                    $qrcode_link =  $base_urls. 'img/QRcode/' . $qr_image_name;
                    $sign_img = "primco-sign-".$order_id.".jpg";
                    $output_file = $path_sign . $sign_img;
                    $sign_link =  $base_urls .'img/Signs/' .$sign_img;
                    $ecc = 'L'; 
                    $pixel_Size = 10; 
                    $frame_Size = 10;     
                    // Generates QR Code and Stores it in directory given 
                   QRcode::png($link, $qrcode_img, $ecc, $pixel_Size, $frame_Size); 

                   $this->sendMail($image_name,$link, $order_id);
                   if(!empty($this->request->getData()['signature'])){
                        $this->base64test($this->request->getData()['signature'],$output_file);
                   }
                  
                   $response =['pdf_link' =>$link,'qrcode_link' => $qrcode_link,'sign_link' =>$sign_link];
                   // $order = $this->Orders->get($order_id, [
                    // 'contain' => ['Customers', 'Salesmen', 'OrderItems']
                // ]);
                 }
        
            } else {
                $order['call_status'] = 0;
            }

			//print_R($order);
			//exit;
             $this->set([
                'order' => $order,
                 'response' => $response,
                '_serialize' => ['order','response']
            ]);               
        }
    }

    

    public function savesignature()
    {
        
        $path ='';
        $file_name ='';
        $response =['path' =>$path,'file_name' => $file_name ];
        
        
        if($_FILES){
            
            $file = $_FILES['files'];
            //print_r($file);
            $ext = substr(strtolower(strrchr($file['name'], '.')), 1); //get the extension
            $arr_ext = array('jpg', 'jpeg', 'gif'); //set allowed extensions
            
            //only process if the extension is valid
            if(in_array($ext, $arr_ext))
            {
                $path =  WWW_ROOT . 'img/Signs/'. $file['name'];
                $file_name = $file['name'];
                //do the actual uploading of the file. First arg is the tmp name, second arg is
                //where we are putting it
                if(move_uploaded_file($file['tmp_name'], $path)){

                    $response =['path' =>$path,'file_name' =>$file_name ];
                }

                
            }
       }
        // $content = json_encode($response);
        // $this->response = $this->response->withStringBody($content);
        // $this->response = $this->response->withType('json');
        return response;
    }
    
    public function base64_to_jpeg($base64_string, $output_file) {
        $path ='';
        $file_name ='';
        $response =['path' =>$path,'file_name' => $file_name ];
        // open the output file for writing
        $ifp = fopen( $output_file, 'wb' ); 

        // split the string on commas
        // $data[ 0 ] == "data:image/png;base64"
        // $data[ 1 ] == <actual base64 string>
        $data = explode( ',', $base64_string );

        // we could add validation here with ensuring count( $data ) > 1
        fwrite( $ifp, base64_decode( $data[ 1 ] ) );

        // clean up the file resource
        fclose( $ifp ); 

        $output_file; 
        if(is_array($output_file)){
            
            $ext = substr(strtolower(strrchr($output_file['name'], '.')), 1); //get the extension
            $arr_ext = array('jpg', 'jpeg', 'gif'); //set allowed extensions
            
            //only process if the extension is valid
            if(in_array($ext, $arr_ext))
            {
                $path =  WWW_ROOT . 'img/Signs/'. $output_file['name'];
                $file_name = $output_file['name'];
                //do the actual uploading of the file. First arg is the tmp name, second arg is
                //where we are putting it
                if(move_uploaded_file($output_file['tmp_name'], $path)){
                    
                    $response =['path' =>$path,'file_name' =>$file_name ];
                }

                
            }
       }
         return response;
    }


    
    

    public function pdfView($id = null ){
        if ( $id == null ) {
            return $this->redirect( $this->referer() );
        }

        $order_id = $id = base64_decode($id);
        $order = $this->Orders->get($id, [
            'contain' => []
        ]);
        

        $order_item = TableRegistry::get('OrderItems');
        $salesmen = TableRegistry::get('Salesmen');
        $customers = TableRegistry::get('Customers');
        $products = TableRegistry::get('Products');

        $pdf_data = [];
        // print_r($order->toArray());
        if ( !empty( $order->toArray() ) ) {
            //foreach ($order->toArray() as $order_key => $order_value) {
            
            $pdf_data['order'] = [
                'id' => $order->toArray()['id'],
                'grand_total' => $order->toArray()['grand_total'],
                'po_number' => $order->toArray()['po_number'],
                'ship_to' => $order->toArray()['ship_to'],
                'ship_date' => $order->toArray()['ship_date'],
                'ship_via' => $order->toArray()['ship_via'],
                'additional_comments' => $order->toArray()['additional_comments'],
                'events' => $order->toArray()['events'],
                'sale_order_number' => $order->toArray()['sale_order_number'],
                'incentive_reciept' => $order->toArray()['incentive_reciept'],
                'sale_date' => $order->toArray()['sale_date'],
                'purchaser_name' => $order->toArray()['purchaser_name'],
                
            ];

            /*Salesmen*/
            $salesmen_data = $salesmen->find( 'all' )
            ->where( [ 'id' => $order->toArray()['salesman_id'] ] );
            
            if( !empty( $salesmen_data->toList() ) ) {
                foreach ($salesmen_data->toList() as $sm_key => $sm_value) {
                    $pdf_data['salesman'] = [
                        'name' => $sm_value['name'],
                        'sales_rep' => $sm_value['sales_rep'],
                        'sd_rep' => $sm_value['sd_rep'],
                        'rep_email' => $sm_value['rep_email'],
                        'spec' => $sm_value['spec']
                    ];
                }
                
            }
            
            $customer_data = $customers->find( 'all' )
            ->where( [ 'id' => $order->toArray()['customer_id'] ] );
            
            if( !empty( $customer_data->toList() ) ) {
                foreach ($customer_data->toList() as $c_key => $c_value) {
                    $pdf_data['customer'] = [
                        'name' => $c_value['name'],
                        'address' => $c_value['address'],
                        'city' => $c_value['city'],
                        'province' => $c_value['province'],
                        'postal_code' => $c_value['postal_code'],
                        'sin' => $c_value['sin'],
                    ];
                }
                
            }

            $order_item_data = $order_item->find( 'all' )
            ->where( [ 'order_id' => $order->toArray()['id'] ] );
            
            if( !empty( $order_item_data->toList() ) ) {
                foreach ($order_item_data->toList() as $oi_key => $oi_value) {
                    
                    $product = $products->find( )
                    ->where( [ 'id' => $oi_value['product_id'] ] )
                    ->first()
                    ->toArray();
                    
                    $product_name = '<strong>'.$product['item_number'].':</strong> '.$product['product_name'].', '.$product['item_name'];
                    $unit = $product['item_type'];
                    //die;
                    $pdf_data['order_item'][] = [
                        'product_name' => $product_name,
                        'unit' => $unit,
                        'price' => $oi_value['price'],
                        'quantity' => $oi_value['quantity'],
                        'incentives' => $oi_value['incentives'],
                        'promo' => $oi_value['promo'],
                        'discount' => $oi_value['discount'],
                        'total' => $oi_value['total'],
                        
                    ];
                }
                
            }

            
        }
         
        $this->set(compact('pdf_data','order_id'));
        $this->viewBuilder()->layout('ajax');
        $this->response->type('pdf');
    }

    public function sendMail($image_name="",$link, $id){
        $toEmail = [
            'm.malhotra@primco.ca',
        ];

        $email = new Email();
        $email->setTransport('default');
        $email->viewVars(['image_name' => $image_name,'link' =>$link ]);
        $email->viewBuilder()->setTemplate('order');
        $email->setEmailFormat('html');
        $email->setFrom(['support@primco.ca' => 'Primco'])
        ->setTo($toEmail)
        ->setBcc('Juneja.ankit@gmail.com')
        ->setSubject('Primco New Order #'.$id)
        ->send();
      return true;
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {

        $orders = $this->Orders->find('all', [
            'contain' => ['Customers', 'Salesmen']
        ]);
        $this->set([
            'orders' => $orders,
            '_serialize' => ['orders']
        ]);


        // $this->paginate = [
        //     'contain' => ['Customers', 'Salesmen']
        // ];
        // $orders = $this->paginate($this->Orders);

        // $this->set(compact('orders'));
    }

    /**
     * View method
     *
     * @param string|null $id Order id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function _view($id = null)
    {
        $order = $this->Orders->get($id, [
            'contain' => ['Customers', 'Salesmen', 'OrderItems']
        ]);

        $this->set('order', $order);
    }
	
	public function getordersbysalesman()
    {
		$this->request->allowMethod(['post', 'put']);
		$id = $this->request->getData('id');
		//$id = base64_decode($id);
        $orders = $this->Orders->find('all', [
            'contain' => ['Customers', 'Salesmen', 'OrderItems','OrderItems.Products'],
			'conditions' =>['salesman_id' => $id],
        ]);
		if($orders){
			$this->set([
				'status' => 1,
				'orders' => $orders,
				'_serialize' => ['status','orders']
			]);
		}else{
			$this->set([
				'status' => 0,
				'orders' => $orders,
				'_serialize' => ['status','orders']
			]);
		}
        
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function _add()
    {
        $order = $this->Orders->newEntity();
        if ($this->request->is('post')) {
            $order = $this->Orders->patchEntity($order, $this->request->getData());
            if ($this->Orders->save($order)) {
                $this->Flash->success(__('The order has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The order could not be saved. Please, try again.'));
        }
        $customers = $this->Orders->Customers->find('list', ['limit' => 200]);
        $salesmen = $this->Orders->Salesmen->find('list', ['limit' => 200]);
        $this->set(compact('order', 'customers', 'salesmen'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Order id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $order = $this->Orders->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $order = $this->Orders->patchEntity($order, $this->request->getData());
            if ($this->Orders->save($order)) {
                $this->Flash->success(__('The order has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The order could not be saved. Please, try again.'));
        }
        $customers = $this->Orders->Customers->find('list', ['limit' => 200]);
        $salesmen = $this->Orders->Salesmen->find('list', ['limit' => 200]);
        $this->set(compact('order', 'customers', 'salesmen'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Order id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $order = $this->Orders->get($id);
        if ($this->Orders->delete($order)) {
            $this->Flash->success(__('The order has been deleted.'));
        } else {
            $this->Flash->error(__('The order could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
