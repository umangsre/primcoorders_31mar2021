$(document).ready(function () {
    if (window.location.href.indexOf('claims/add') > -1) {
        $('button.add-claim-information').click();
        addPrimcoInvoiceField();
    }
    if (window.location.href.indexOf('claims/view') > -1) {
        triggerViewForm();
    }
});

function triggerViewForm() {
    $('#claimInformation').append(claimRowWithData(0, claim_json_data[0]));
    if (claim_json_data['misc']) {
        miscClaimInformation(claim_json_data['misc']);
    }
    addPrimcoInvoiceFieldWithData();
    const settlementMethod = $('#settlement-method').val();
    settlementMethod == 'cash' ? $('#miscClaimInfoArea').hide() : $('#miscClaimInfoArea').show();
    $('.settlement_amount_div').attr('style', settlementMethod === 'cash' ? 'display: inline-block' : 'display: none');
    $('.replacement_amount_div').attr('style', settlementMethod === 'replacement' ? 'display: inline-block' : 'display: none');
    $('.return_qty_div').attr('style', settlementMethod === 'return' ? 'display: inline-block' : 'display: none');
    if (userRole == 'admin' || userRole == 'miller') {
        addMillerInvoiceFieldWithData();
    }
    if (disableValues) {
        $(`#Category_0`).attr('disabled', 'disabled');
        if (claim_json_data[0].Sub_Category) {
            $(`#Sub_Category_0`).attr('disabled', 'disabled');
        }
        $(`#Product_Number_0`).attr('disabled', 'disabled');
        $(`#Product_Description_0`).attr('disabled', 'disabled');
        $(`#Quantity_Purchased_0`).attr('disabled', 'disabled');
        $(`#Quantity_Purchased_UOM_0`).attr('disabled', 'disabled');
        $(`#Quantity_Affected_0`).attr('disabled', 'disabled');
        $(`#Quantity_Affected_UOM_0`).attr('disabled', 'disabled');
        $(`#Installed_0`).attr('disabled', 'disabled');
        $(`#Quantity_Installed_0`).attr('disabled', 'disabled');
        $(`#Quantity_Installed_UOM_0`).attr('disabled', 'disabled');
        $(`#Rooms_Installed_0`).attr('disabled', 'disabled');
        $(`#Date_Installed_0`).attr('disabled', 'disabled');
        $(`#Detailed_Claim_Reason_0`).attr('disabled', 'disabled');
        $(`#Request_For_Resolution_0`).attr('disabled', 'disabled');
        $(`#Primary_Contact_Name_0`).attr('disabled', 'disabled');
        $(`#Primary_Contact_Number_0`).attr('disabled', 'disabled');
        $(`#Quantity_For_Sub_Category_0`).attr('disabled', 'disabled');
    } else {
        setDropdownValue(0, 'Category', claim_json_data[0].Category);
        if (claim_json_data[0].Sub_Category) {
            setDropdownValue(0, 'Sub_Category', claim_json_data[0].Sub_Category);
        }
        if (claim_json_data[0].Installed) {
            setDropdownValue(0, 'Installed', claim_json_data[0].Installed);
        }
        if (claim_json_data[0].Quantity_Purchased_UOM) {
            setDropdownValue(0, 'Quantity_Purchased_UOM', claim_json_data[0].Quantity_Purchased_UOM);
        }
        if (claim_json_data[0].Quantity_Affected_UOM) {
            setDropdownValue(0, 'Quantity_Affected_UOM', claim_json_data[0].Quantity_Affected_UOM);
        }
        if (claim_json_data[0].Quantity_Installed_UOM) {
            setDropdownValue(0, 'Quantity_Installed_UOM', claim_json_data[0].Quantity_Installed_UOM);
        }
        $(`#Date_Installed_0`).datepicker({
            dateFormat: 'yy-mm-dd',
            maxDate: 0
        });
    }
}

$(document).on('click ', 'button.add-claim-information', function (e) {
    e.preventDefault();
    const lastRow = $('#claimInformation .claim_row:last')[0];
    const lastIndex = !lastRow ? undefined : lastRow.id.replace('claim_row_', '');
    $('#claimInformation').append(lastIndex ? claimRow(parseInt(lastIndex) + 1) : claimRow(0));
    $(`#Date_Installed_0`).datepicker({
        dateFormat: 'yy-mm-dd',
        maxDate: 0
    });
});

function miscClaimInformation(data = {}) {
    const objIsEmpty = Object.keys(data).length === 0;
    const miscItems = [
        { id: '', name: 'Select' },
        { id: 'moldings', name: 'Moldings (pcs)' },
        { id: 'baseBoard', name: 'Base Board (linear ft)' },
        { id: 'nosings', name: 'Nosings (pcs)' },
    ];
    let selectedMiscItem = '';
    if (!objIsEmpty) {
        selectedMiscItem = miscItems.filter(miscItem => miscItem.id == data.Misc_Items)[0].name;
    }
    const index = 'misc';
    let html = `<div class="row">`;
    html += createInput(index, 'Furniture', 'No of Furniture Moved (pcs)', 0, objIsEmpty ? '' : data.Furniture);
    if (userRole == 'admin') {
        const furnitureCost = general.filter(cat => cat.category == 'Furniture')[0].labour_cost;
        const totalFurnitureCost = parseFloat(data.Furniture) * parseFloat(furnitureCost);
        html += createInput(index, 'Furniture_Cost', 'Primco Calculated Furniture Labour Cost (CAD)', 0, totalFurnitureCost);
    }
    html += createInput(index, 'Appliances', 'No of Appliances Moved (pcs)', 0, objIsEmpty ? '' : data.Appliances);
    if (userRole == 'admin') {
        const applianceCost = general.filter(cat => cat.category == 'Appliances')[0].labour_cost;
        const totalAppliancesCost = parseFloat(data.Appliances) * parseFloat(applianceCost);
        html += createInput(index, 'Appliances_Cost', 'Primco Calculated Furniture Labour Cost (CAD)', 0, totalAppliancesCost);
    }
    html += createInput(index, 'Toilets', 'No of Toilet Seats Moved (pcs)', 0, objIsEmpty ? '' : data.Toilets);
    if (userRole == 'admin') {
        const toiletCost = general.filter(cat => cat.category == 'Toilets')[0].labour_cost;
        const totalToiletsCost = parseFloat(data.Toilets) * parseFloat(toiletCost);
        html += createInput(index, 'Toilets_Cost', 'Primco Calculated Furniture Labour Cost (CAD)', 0, totalToiletsCost);
    }
    html += `</div>`;
    html += `<div class="row">`;
    if (disableValues) {
        html += createInput(index, 'Misc_Items', 'Miscellaneous Items', 0, objIsEmpty ? '' : selectedMiscItem);
    } else {
        html += createDropdown(index, 'Misc_Items', 'Select Miscellaneous Items', miscItems);
    }
    html += createInput(index, 'Misc_Items_Value', 'Value of Miscellaneous Items', 0, objIsEmpty ? '' : data.Misc_Items_Value);
    html += `</div>`;
    $('#miscClaimInformation').empty().append(html);
    if (disableValues) {
        $(`#Furniture_${index}`).attr('disabled', 'disabled');
        $(`#Appliances_${index}`).attr('disabled', 'disabled');
        $(`#Toilets_${index}`).attr('disabled', 'disabled');
        $(`#Misc_Items_${index}`).attr('disabled', 'disabled');
        $(`#Misc_Items_Value_${index}`).attr('disabled', 'disabled');
        if (userRole == 'admin') {
            $(`#Furniture_Cost_${index}`).attr('disabled', 'disabled');
            $(`#Appliances_Cost_${index}`).attr('disabled', 'disabled');
            $(`#Toilets_Cost_${index}`).attr('disabled', 'disabled');
        }
    } else {
        !objIsEmpty && setDropdownValue('misc', 'Misc_Items', data.Misc_Items);
    }
}

function claimRowWithData(index, data) {
    let categoriesArray = [];
    let subCategoriesArray = [];
    categoriesArray.push({ id: '', name: 'Select' });
    subCategoriesArray.push({ id: '', name: 'Select' });
    categories.map(category => empty(category.parent_category) ?
        categoriesArray.push({
            id: category.id,
            name: category.category,
        }) :
        subCategoriesArray.push({
            id: category.id,
            name: category.category
        })
    );
    let selectedCategory, selectedSubCategory;
    categories.map(category => {
        if (category.id == data.Category) {
            selectedCategory = category.category;
        }
        if (category.id == data.Sub_Category)
            selectedSubCategory = category.category;
    });
    const unitsOfMeasurement = [
        {id: '', name: 'Select'},
        {id: 'sq_ft', name: 'sq.ft'},
        {id: 'linear_ft', name: 'linear ft'},
        {id: 'sq_yards', name: 'sq.yards'},
    ];
    const affectedUnitsOfMeasurement = [
        {id: '', name: 'Select'},
        {id: 'sq_ft', name: 'sq.ft'},
        {id: 'linear_ft', name: 'linear ft'},
        {id: 'sq_yards', name: 'sq.yards'},
        {id: 'percentage', name: 'Percentage (%)'},
    ];
    const installed = [
        {id: '', name: 'Select'},
        {id: 'yes', name: 'Yes'},
        {id: 'no', name: 'No'}
    ];
    const installedUnitsOfMeasurement = [
        {id: '', name: 'Select'},
        {id: 'sq_ft', name: 'sq.ft'},
		{ id: 'sq_yards', name: 'sq.yards' },
        {id: 'percentage', name: 'Percentage (%)'},
    ];
    const selectedPurchasedUom = unitsOfMeasurement.filter(uom => uom.id == data.Quantity_Purchased_UOM)[0].name;
    const selectedAffectedUom = affectedUnitsOfMeasurement.filter(uom => uom.id == data.Quantity_Affected_UOM)[0].name;
    const selectedInstalled = installed.filter(uom => uom.id == data.Installed)[0].name;
    const selectedInstalledUom = installedUnitsOfMeasurement.filter(uom => uom.id == data.Quantity_Installed_UOM)[0].name;
    let html = `<div id="claim_row_${index}" class="claim_row">`;
    html += `<div class="row">`;
    html += '&nbsp;';
    html += `</div>`;
    html += `<div class="row">`;
    if (disableValues) {
        html += createInput(index, 'Category', 'Category', '', selectedCategory);
        if (claim_json_data[0].Sub_Category) {
            html += createInput(index, 'Sub_Category', 'Sub Category', '', selectedSubCategory);
        }
    } else {
        html += createDropdown(index, 'Category', 'Select Category', categoriesArray);
        html += createDropdown(index, 'Sub_Category', 'Select Sub Category', subCategoriesArray);
    }
    html += '<br /><br />';
    html += createInput(index, 'Product_Number', 'Product Number', '', data.Product_Number);
    html += createInput(index, 'Product_Description', 'Product Description', '', data.Product_Description);
    html += '<br /><br />';
    html += createInput(index, 'Quantity_Purchased', 'Quantity Purchased', 0, data.Quantity_Purchased, '30%');
    if (disableValues) {
        html += createInput(index, 'Quantity_Purchased_UOM', 'UOM', '', selectedPurchasedUom, '20%');
    } else {
        html += createDropdown(index, 'Quantity_Purchased_UOM', 'Select UOM', unitsOfMeasurement, '20%');
    }
    html += createInput(index, 'Quantity_Affected', 'Quantity Affected', 0, data.Quantity_Affected, '30%');
    if (disableValues) {
        html += createInput(index, 'Quantity_Affected_UOM', 'UOM', '', selectedAffectedUom, '20%');
    } else {
        html += createDropdown(index, 'Quantity_Affected_UOM', 'Select UOM', affectedUnitsOfMeasurement, '20%');
    }
    html += '<br /><br />';
    if (disableValues) {
        html += createInput(index, 'Installed', 'Installed', '', selectedInstalled);
    } else {
        html += createDropdown(index, 'Installed', 'Installed', installed);
    }
    html += createInput(index, 'Quantity_Installed', 'Quantity Installed', 0, data.Quantity_Installed, '30%');
    if (disableValues) {
        html += createInput(index, 'Quantity_Installed_UOM', 'UOM', '', selectedInstalledUom, '20%');
    } else {
        html += createDropdown(index, 'Quantity_Installed_UOM', 'Select UOM', installedUnitsOfMeasurement, '20%');
    }
    html += '<br /><br />';
    html += createInput(index, 'Rooms_Installed', 'Where the product was installed?', 'Ex. Bedroom, Living Room, etc.', data.Rooms_Installed);
    html += createInput(index, 'Date_Installed', 'Date Installed', 'Select Date', data.Date_Installed);
    html += '<br /><br />';
    html += createInput(index, 'Detailed_Claim_Reason', 'Detailed Reason of Claim *', '', data.Detailed_Claim_Reason, '100%', true);
    html += '<br /><br />';
    html += createInput(index, 'Request_For_Resolution', 'Request for Resolution *', '', data.Request_For_Resolution, '100%', true);
    html += '<br /><br />';
    html += createInput(index, 'Primary_Contact_Name', 'Primary Contact Name (in case of inspection)', '', data.Primary_Contact_Name);
    html += createInput(index, 'Primary_Contact_Number', 'Primary Contact Number (in case of inspection)', '', data.Primary_Contact_Number);
    html += '<br /><br />';
    if (data.Quantity_For_Sub_Category) {
        html += createInput(index, 'Quantity_For_Sub_Category', 'Quantity for Sub Category', '', data.Quantity_For_Sub_Category);
    }
    html += `</div>`;
    html += `</div>`;
    return html;
}

function claimRow(index) {
    let categoriesArray = [];
    let subCategoriesArray = [];
    categoriesArray.push({id: '', name: 'Select'});
    subCategoriesArray.push({id: '', name: 'Select'});
    categories.map(category => empty(category.parent_category) &&
        categoriesArray.push({
            id: category.id,
            name: category.category,
        })
    );
    const unitsOfMeasurement = [
        {id: '', name: 'Select'},
        {id: 'sq_ft', name: 'sq.ft'},
        {id: 'linear_ft', name: 'linear ft'},
        {id: 'sq_yards', name: 'sq.yards'},
    ];
    const affectedUnitsOfMeasurement = [
        {id: '', name: 'Select'},
        {id: 'sq_ft', name: 'sq.ft'},
        {id: 'linear_ft', name: 'linear ft'},
        {id: 'sq_yards', name: 'sq.yards'},
        {id: 'percentage', name: 'Percentage (%)'},
    ];
    const installedUnitsOfMeasurement = [
        {id: '', name: 'Select'},
        {id: 'sq_ft', name: 'sq.ft'},
		{id: 'sq_yards', name: 'sq.yards'},
        {id: 'percentage', name: 'Percentage (%)'},
    ];
    let html = `<div id="claim_row_${index}" class="claim_row">`;
    html += `<div class="row">`;
    html += '&nbsp;';
    html += `</div>`;
    html += '<br /><br />';
    html += `<div class="row">`;
    html += createDropdown(index, 'Category', 'Select Category', categoriesArray);
    html += createDropdown(index, 'Sub_Category', 'Select Sub Category', subCategoriesArray);
    html += '<br /><br />';
    html += createInput(index, 'Product_Number', 'Product Number', '', '');
    html += createInput(index, 'Product_Description', 'Product Description', '', '');
    html += '<br /><br />';
    html += createInput(index, 'Quantity_Purchased', 'Quantity Purchased', 0, '', '30%');
    html += createDropdown(index, 'Quantity_Purchased_UOM', 'Select UOM', unitsOfMeasurement, '20%');
    html += createInput(index, 'Quantity_Affected', 'Quantity Affected', 0, '', '30%');
    html += createDropdown(index, 'Quantity_Affected_UOM', 'Select UOM', affectedUnitsOfMeasurement, '20%');
    html += '<br /><br />';
    html += createDropdown(index, 'Installed', 'Installed', [{id: '', name: 'Select'}, {
        id: 'yes',
        name: 'Yes'
    }, {id: 'no', name: 'No'}]);
    html += createInput(index, 'Quantity_Installed', 'Quantity Installed', 0, '', '30%');
    html += createDropdown(index, 'Quantity_Installed_UOM', 'Select UOM', installedUnitsOfMeasurement, '20%');
    html += '<br /><br />';
    html += createInput(index, 'Rooms_Installed', 'Where the product was installed?', 'Ex. Bedroom, Living Room, etc.', '');
    html += createInput(index, 'Date_Installed', 'Date Installed', 'Select Date', '');
    html += '<br /><br />';
    html += createInput(index, 'Detailed_Claim_Reason', 'Detailed Reason of Claim *', '', '', '100%', true);
    html += '<br /><br />';
    html += createInput(index, 'Request_For_Resolution', 'Request for Resolution *', '', '', '100%', true);
    html += '<br /><br />';
    html += createInput(index, 'Primary_Contact_Name', 'Primary Contact Name (in case of inspection)', '', '');
    html += createInput(index, 'Primary_Contact_Number', 'Primary Contact Number (in case of inspection)', '', '');
    html += '<br /><br />';
    html += `</div>`;
    html += `</div>`;
    return html;
}

function createInput(index, field, label, placeholder, value, width = '50%', textarea = false) {
    let html = '';
    html += `<div class="input text" id="input_area_${field}_${index}" style="width: ${width}; max-width: ${textarea ? '100%' : '50%'}">`;
    html += `<label for="${field}_${index}" class="">${label}</label>`;
    if (textarea) {
        html += `<textarea id="${field}_${index}" name="claims[${index}][${field}]" style="resize: none; height: 10rem;">${value}</textarea>`;
    } else {
        html += `<input type="text" autocomplete="off" placeholder="${placeholder}" value="${value}" id="${field}_${index}" name="claims[${index}][${field}]" />`;
    }
    html += `</div>`;
    return html;
}

function createDropdown(index, field, label, data, width = '50%') {
    let html = '';
    html += `<div class="input select" id="dropdown-area-${field}-${index}" style="width: ${width}">`;
    html += `<label for="${field}_${index}">${label}</label>`;
    html += `<div class="select-wrapper">`;
    html += `<input id="input-${field}-${index}" class="select-dropdown dropdown-trigger" onclick="showDropdown('${field}', ${!isNaN(index) ? index : `'${index}'`})" type="text" readonly="true" data-target="select-options-ul-${index}">`;
    html += `<ul id="select-${field}-ul-${index}" class="dropdown-content select-dropdown dropdown-ul" tabindex="0">`;
    data.map(d => {
        html += `<li onclick="setDropdownValue(${!isNaN(index) ? index : `'${index}'`}, '${field}', '${d.id}')" id="${field}_${index}_${d.id}" tabindex="0"><span>${d.name}</span></li>`;
    });
    html += `</ul>`;
    html += `<svg class="caret" height="24" viewBox="0 0 24 24" width="24" xmlns="http://www.w3.org/2000/svg"><path d="M7 10l5 5 5-5z"></path><path d="M0 0h24v24H0z" fill="none"></path></svg>`;
    html += `<select class="select-dropdown" type="text" id="${field}_${index}" name="claims[${index}][${field}]" placeholder="Select ${field}">`;
    data.map(d => {
        html += `<option value="${d.id}">${d.name}</option>`
    });
    html += `</select>`;
    html += '</div>';
    html += '</div>';
    return html;
}

function setDropdownValue(index, field, dataId) {
    $(`#select-${field}-ul-${index} li`).map((i, li) => {
        li.removeAttribute('class');
    });
    $(`#${field}_${index}_${dataId}`).attr('class', 'selected');
    $(`#select-${field}-ul-${index}`).removeAttr('style', '');
    $(`#${field}_${index}`).val(dataId);
    const selectedValue = $(`#${field}_${index}_${dataId} span`).html();
    $(`#input-${field}-${index}`).attr('value', selectedValue);
    if (field === 'Category') {
        triggerSubCategoriesChange(index, selectedValue, dataId);
    }
    if (field === 'Sub_Category') {
        if (selectedValue.indexOf('With Underlayment') > -1 || selectedValue.indexOf('With Emboss') > -1) {
            if ($(`#Quantity_For_Sub_Category_${index}`).length === 0) {
                let html = createInput(index, 'Quantity_For_Sub_Category', 'Quantity for Sub Category', 0, '');
                $(`#claim_row_${index}`).append(html)
            }
        } else {
            if ($(`#Quantity_For_Sub_Category_${index}`).length > 0) {
                $(`#input_area_Quantity_For_Sub_Category_${index}`).remove();
            }
        }
    }
	if (field === 'Installed') {
        if (selectedValue == 'Yes') {
            $("#consumer-name").prop('required', true);
            $('#consumer-phone').prop('required', true);
            $('#consumer-address').prop('required', true);
            $('#consumer-city').prop('required', true);
            $('#consumer-postal-code').prop('required', true);
        } else {
            $('#consumer-name').removeAttr('required');
            $('#consumer-phone').removeAttr('required');
            $('#consumer-address').removeAttr('required');
            $('#consumer-city').removeAttr('required');
            $('#consumer-postal-code').removeAttr('required');
        }
    }
}

function triggerSubCategoriesChange(index, selectedValue, selectedCategory) {
    if ($(`#Quantity_For_Sub_Category_${index}`).length > 0) {
        $(`#input_area_Quantity_For_Sub_Category_${index}`).remove();
    }
    $(`#input-Sub_Category-${index}`).attr('value', '');
    const unitsOfMeasurement1 = [
        {id: 'sq_ft', name: 'sq.ft'},
        {id: 'linear_ft', name: 'linear ft'},
        {id: 'sq_yards', name: 'sq.yards'},
		];
    const unitsOfMeasurement3 = [
        { id: 'sq_ft', name: 'sq.ft' },
        { id: 'sq_yards', name: 'sq.yards' },
    ];
    const unitsOfMeasurement2 = [
        {id: 'sq_ft', name: 'sq.ft'}
    ];
    let uomArray = [];
    if (selectedValue.indexOf('Carpet') > -1 || (selectedValue.indexOf('Vinyl') > -1 && selectedValue.indexOf('Luxury Vinyl') < 0)) {
        uomArray = unitsOfMeasurement1;
		} else if (selectedValue.indexOf('Underlay') > -1) {
        uomArray = unitsOfMeasurement3;
    }
    } else {
        uomArray = unitsOfMeasurement2;
    }
    let optionHtml = '';
    uomArray.map(d => optionHtml += `<option value="${d.id}">${d.name}</option>`);
    $(`#Quantity_Purchased_UOM_${index}`).empty().append(optionHtml);
    // $(`#Quantity_Affected_UOM_${index}`).empty().append(optionHtml);
    let liHtml = '';
    uomArray.map(d => {
        liHtml += `<li onclick="setDropdownValue(${index}, 'Quantity_Purchased_UOM', '${d.id}')" id="Quantity_Purchased_UOM_${index}_${d.id}" tabindex="0"><span>${d.name}</span></li>`;
    });
    $(`#select-Quantity_Purchased_UOM-ul-${index}`).empty().append(liHtml);
    /*liHtml = '';
    uomArray.map(d => {
        liHtml += `<li onclick="setDropdownValue(${index}, 'Quantity_Affected_UOM', '${d.id}')" id="Quantity_Affected_UOM_${index}_${d.id}" tabindex="0"><span>${d.name}</span></li>`;
    });
    $(`#select-Quantity_Affected_UOM-ul-${index}`).empty().append(liHtml);*/

    if (selectedValue.indexOf('Carpet') > -1 || selectedValue.indexOf('Luxury Vinyl') > -1 || selectedValue.indexOf('Laminate') > -1 || selectedValue.indexOf('Underlay') > -1 || selectedValue.indexOf('Rubber') > -1) {
        $(`#Sub_Category_${index}`).empty();
        $(`#select-Sub_Category-ul-${index}`).empty();
        $(`#dropdown-area-Sub_Category-${index}`).hide();
    } else {
        const subCategoriesArray = [];
        categories.map(category =>
            category.parent_category === selectedCategory && subCategoriesArray.push({
                id: category.id,
                name: category.category
            })
        );
        let html = '';
        subCategoriesArray.map(d => html += `<option value="${d.id}">${d.name}</option>`);
        $(`#Sub_Category_${index}`).empty().append(html);
        html = '';
        subCategoriesArray.map(d => {
            html += `<li onclick="setDropdownValue(${index}, 'Sub_Category', '${d.id}')" id="Sub_Category_${index}_${d.id}" tabindex="0"><span>${d.name}</span></li>`;
        });
        $(`#select-Sub_Category-ul-${index}`).empty().append(html);
        $(`#dropdown-area-Sub_Category-${index}`).show();
    }
}

function showDropdown(field, index) {
    const elem = $(`#select-${field}-ul-${index}`);
    elem.css({
        "display": "block",
        "width": "418.5px",
        "left": "0px",
        "top": "0px",
        "height": "400px",
        "transform-origin": "100px 100%",
        "opacity": "1",
        "transform": "scaleX(1) scaleY(1);"
    });
}

$(document).mouseup(function (e) {
    var container = $(".dropdown-ul");
    if (!container.is(e.target) && container.has(e.target).length === 0) {
        container.removeAttr('style', '');
    }
});

$('#settlement-method').change(() => {
    const settlementMethod = $('#settlement-method').val();
    $('.settlement_amount_div').attr('style', settlementMethod === 'cash' ? 'display: inline-block' : 'display: none');
    $('.replacement_amount_div').attr('style', settlementMethod === 'replacement' ? 'display: inline-block' : 'display: none');
    $('.return_qty_div').attr('style', settlementMethod === 'return' ? 'display: inline-block' : 'display: none');
    if (settlementMethod === 'replacement') {
        $('#consumer-name').attr('required', 'required');
        $('#consumer-phone').attr('required', 'required');
        $('#consumer-address').attr('required', 'required');
        $('#consumer-city').attr('required', 'required');
        $('#consumer-postal_code').attr('required', 'required');
        $('#miscClaimInfoArea').show();
        miscClaimInformation();
    } else {
        $('#consumer-name').removeAttr('required');
        $('#consumer-phone').removeAttr('required');
        $('#consumer-address').removeAttr('required');
        $('#consumer-city').removeAttr('required');
        $('#consumer-postal_code').removeAttr('required');
        $('#miscClaimInfoArea').hide();
        $('#miscClaimInformation').empty();
    }
    $('.settlement_amount_div .input').attr('style', 'max-width: 100%');
    $('.replacement_amount_div .input').attr('style', 'max-width: 100%');
});

function empty(value) {
    value = value;
    return value === '' || value === null || value === 'null' || value === 'undefined' || value === undefined;
}

$('#sendToMillBtn').on('click ', function (e) {
    e.preventDefault();
    let hasErrors = false;
    let errors = 'Please fill the following fields<ul>';
    if (empty($('#miller-name').val())) {
        errors += '<li>Mill Name</li>';
        hasErrors = true;
    }
    if (empty($('#miller-contact-name').val())) {
        errors += '<li>Mill Contact Name</li>';
        hasErrors = true;
    }
    if (empty($('#miller-email').val())) {
        errors += '<li>Mill Email</li>';
        hasErrors = true;
    }
    if (empty($('#miller-phone').val())) {
        errors += '<li>Mill Phone</li>';
        hasErrors = true;
    }
    if (empty($('#miller-address').val())) {
        errors += '<li>Mill Address</li>';
        hasErrors = true;
    }
    if (empty($('#miller-city').val())) {
        errors += '<li>Miller City</li>';
        hasErrors = true;
    }
    if (empty($('#miller-postal-code').val())) {
        errors += '<li>Mill Postal Code</li>';
        hasErrors = true;
    }
    if (empty($('#miller-invoice-number').val())) {
        errors += '<li>Mill Invoice Number</li>';
        hasErrors = true;
    }
    if (hasErrors) {
        $('#errors').empty().append(errors);
        window.scrollTo({
            top: 100,
            left: 100,
            behavior: 'smooth'
        });
        return;
    }
    const formElem = $('#claim-form');
    const status = $("<input>")
        .attr("type", "hidden")
        .attr("name", "status").val("send to mill");
    formElem.append(status);
    formElem.submit();
    miscClaimInformation();
});

$(document).on('click ', 'button.submit-claim', function (e) {
    e.preventDefault();
    let submitClaim = false;
    const claimRows = $('.claim_row');
    let errors = '';
    if ($('#settlement-method').val() == 'replacement' && (empty($('#consumer-name').val()) || empty($('#consumer-phone').val()) || empty($('#consumer-address').val()) || empty($('#consumer-city').val()) || empty($('#consumer-postal-code').val()))) {
        errors += 'Please fill consumer information';
        errors += '<br />';
    }
    if (isNaN(parseInt($('#customer_id').val()))) {
        errors += 'Please select dealer / retailer';
        errors += '<br />';
    }
    if (claimRows.length === 0) {
        errors += 'Please add at least one claim item';
        errors += '<br />';
    }
    if (empty($('#primco-invoice-number').val())) {
        errors += 'Please enter primco invoice number';
        errors += '<br />';
    }
    if (empty($('#Detailed_Claim_Reason').val())) {
        errors += 'Please enter Detailed Reason of Claim';
        errors += '<br />';
    }
    if (empty($('#Request_For_Resolution').val())) {
        errors += 'Please enter Request for Resolution';
        errors += '<br />';
    }
    const quantityPurchasedUom = $('#Quantity_Purchased_UOM_0').val();
    const quantityPurchased = parseFloat($('#Quantity_Purchased_0').val());
    const quantityInstalledUom = $('#Quantity_Installed_UOM_0').val();
    let quantityInstalled = parseFloat($('#Quantity_Installed_0').val());
	let installedStatus = $('#input-Installed-0').val();

    if (installedStatus == 'Yes') {
        if (empty($('#consumer-name').val())) {
            errors += 'Please enter customer Name';
            errors += '<br />';
        }
        if (empty($('#consumer-phone').val())) {
            errors += 'Please enter customer Phone';
            errors += '<br />';
        }
        if (empty($('#consumer-address').val())) {
            errors += 'Please enter customer Address';
            errors += '<br />';
        }
        if (empty($('#consumer-city').val())) {
            errors += 'Please enter customer City';
            errors += '<br />';
        }
        if (empty($('#consumer-postal-code').val())) {
            errors += 'Please enter customer Postal Code';
            errors += '<br />';
        }
    }
	
    if (quantityPurchasedUom == quantityInstalledUom) {
        if (quantityPurchased < quantityInstalled) {
            errors += 'Quantity purchased cannot be less than Installed Quantity';
            errors += '<br />';
        }
    } else {
        if (quantityInstalledUom == 'percentage' && quantityInstalled > 100) {
            errors += 'Quantity purchased cannot be less than Installed Quantity';
            errors += '<br />';
        }
        if (quantityInstalledUom == 'sq_ft') {
            if (quantityPurchasedUom == 'linear_ft') {
                quantityInstalled = quantityInstalled * 12;
            } else if (quantityPurchasedUom == 'sq_yards') {
                quantityInstalled = quantityInstalled / 9;
            }
            if (quantityPurchased < quantityInstalled) {
                errors += 'Quantity purchased cannot be less than Installed Quantity';
                errors += '<br />';
            }
        }
    }
    let quantityAffected = parseFloat($('#Quantity_Affected_0').val());
    let quantityAffectedUom = $('#Quantity_Affected_UOM_0').val();
    if (quantityPurchasedUom == quantityAffectedUom) {
        if (quantityPurchased < quantityAffected) {
            errors += 'Quantity purchased cannot be less than Affected Quantity';
            errors += '<br />';
        }
    } else {
        if (quantityAffectedUom == 'sq_ft') {
            if (quantityPurchasedUom == 'linear_ft') {
                quantityAffectedUom = quantityAffected * 12;
            } else if (quantityPurchasedUom == 'sq_yards') {
                quantityAffected = quantityAffected / 9;
            }
            if (quantityPurchased < quantityAffected) {
                errors += 'Quantity purchased cannot be less than Affected Quantity';
                errors += '<br />';
            }
        } else if (quantityAffected == 'linear_ft') {
            if (quantityPurchased == 'sq_ft') {
                quantityAffected = quantityAffected / 12;
            } else if (quantityPurchased == 'sq_yards') {
                quantityAffected = quantityAffected / 3;
            }
            if (quantityPurchased < quantityAffected) {
                errors += 'Quantity purchased cannot be less than Affected Quantity';
                errors += '<br />';
            }
        } else if (quantityAffectedUom == 'sq_yards') {
            if (quantityPurchasedUom == 'sq_ft') {
                quantityAffected = quantityAffected * 9;
            } else if (quantityPurchasedUom == 'linear_ft') {
                quantityAffected = quantityAffected * 3;
            }
            if (quantityPurchased < quantityAffected) {
                errors += 'Quantity purchased cannot be less than Affected Quantity';
                errors += '<br />';
            }
        }
    }
    if (!empty(errors)) {
        $('#errors').empty().append(errors);
        window.scrollTo({
            top: 100,
            left: 100,
            behavior: 'smooth'
        });
        return;
    }
    if ($('#settlement-method').val() == 'replacement') {
        let totalPrimcoCost = 0;
        let totalUserCost = 0;
        const category = $('#input-Category-0').val();
        const subCategory = $('#input-Sub_Category-0').val();
        const quantityAffected = $('#Quantity_Affected_0').val();
        const labourCost = $('#Labour_Cost_misc').val();
        if (!isNaN(parseInt(labourCost))) {
            totalUserCost += parseInt(labourCost);
            categories.map(cat => {
                const name = cat.category;
                if (name === category) {
                    switch ($('#Quantity_Affected_UOM_0').val()) {
                        case 'sq_ft':
                            primcoLabourCost = cat.labour_cost_sq_ft;
                            break;
                        case 'sq_yards':
                            primcoLabourCost = cat.labour_cost_sq_yards;
                            break;
                        case 'linear_ft':
                            primcoLabourCost = cat.labour_cost_linear_ft;
                            break;
                    }
                    totalPrimcoCost += parseInt(primcoLabourCost) * quantityAffected;
                }
            });
        }
        general.filter(cat => {
            if (cat.category === 'Toilet') {
                const toiletsAffected = $('#Toilets_misc').val();
                const toiletsAffectedAmount = $('#Toilets_Amount_misc').val();
                if (!isNaN(parseInt(toiletsAffectedAmount))) {
                    totalUserCost += parseInt(toiletsAffectedAmount);
                    totalPrimcoCost += parseInt(cat.labour_cost) * parseInt(toiletsAffected);
                }
            }
            if (cat.category === 'Furniture') {
                const furnitureAffected = $('#Furniture_misc').val();
                const furnitureAffectedAmount = $('#Furniture_Amount_misc').val();
                if (!isNaN(parseInt(furnitureAffectedAmount))) {
                    totalUserCost += parseInt(furnitureAffectedAmount);
                    totalPrimcoCost += parseInt(cat.labour_cost) * parseInt(furnitureAffected);
                }
            }
            if (cat.category === 'Appliances') {
                const appliancesAffected = $('#Appliances_misc').val();
                const appliancesAffectedAmount = $('#Appliances_Amount_misc').val();
                if (!isNaN(parseInt(appliancesAffectedAmount))) {
                    totalUserCost += parseInt(appliancesAffectedAmount);
                    totalPrimcoCost += parseInt(cat.labour_cost) * parseInt(appliancesAffected);
                }
            }
            if (cat.category === 'Moldings') {
                const moldingsAffected = $('#Moldings_misc').val();
                const moldingsAffectedAmount = $('#Moldings_Amount_misc').val();
                if (!isNaN(parseInt(moldingsAffectedAmount))) {
                    totalUserCost += parseInt(moldingsAffectedAmount);
                    totalPrimcoCost += parseInt(cat.labour_cost) * parseInt(moldingsAffected);
                }
            }
        });
        if ((totalUserCost - totalPrimcoCost) > 700) {
            submitClaim = !confirm('The total labour cost mentioned seems to be on higher end. Do you want to revise?');
        } else {
            submitClaim = true;
        }
    } else {
        submitClaim = true;
    }
    if (submitClaim) {
        $('#claims_form').submit();
    }
});

const primcoInvoiceNumbers = [];
const millerInvoiceNumbers = [];

function addPrimcoInvoiceFieldWithData() {
    if (empty(primcoInvoiceNumbersArray)) {
        addPrimcoInvoiceField();
        return;
    }
    const invoiceNumbers = JSON.parse(primcoInvoiceNumbersArray);
    if (empty(invoiceNumbers) || invoiceNumbers.length == 0) {
        addPrimcoInvoiceField();
        return;
    }
    invoiceNumbers.map(invoice => {
        const index = 'primco_' + primcoInvoiceNumbers.length;
        let html = createInput(index, 'Primco_Invoice_Number', 'Primco Invoice Number', '', invoice.invoiceNumber);
        html += createInput(index, 'Primco_Invoice_Date', 'Primco Invoice Date', '', invoice.invoiceDate);
        $('#primcoInvoiceNumbers').append(html);
        setPrimcoInvoiceNumbers(index);
        if (disableValues) {
            $(`#Primco_Invoice_Number_${index}`).attr('disabled', 'disabled');
            $(`#Primco_Invoice_Date_${index}`).attr('disabled', 'disabled');
        } else {
            createKeyPressEventForPrimcoInvoiceNumber(index);
            $(`#Primco_Invoice_Date_${index}`).datepicker({
                dateFormat: 'yy-mm-dd',
                maxDate: 0,
                onSelect: function (date, i) {
                    $(`#Primco_Invoice_Date_${index}`).val(date);
                    setPrimcoInvoiceNumbers(index);
                }
            });
        }
    });
}

function addMillerInvoiceFieldWithData() {
    if (empty(millerInvoiceNumbersArray)) {
        return;
    }
    const invoiceNumbers = JSON.parse(millerInvoiceNumbersArray);
    if (empty(invoiceNumbers) || invoiceNumbers.length == 0) {
        return;
    }
    invoiceNumbers.map(invoice => {
        const index = 'miller_' + millerInvoiceNumbers.length;
        let html = createInput(index, 'Miller_Invoice_Number', 'Mill Invoice Number', '', invoice.invoiceNumber);
        html += createInput(index, 'Miller_Invoice_Date', 'Mill Invoice Date', '', invoice.invoiceDate);
        $('#millerInvoiceNumbers').append(html);
        setMillerInvoiceNumbers(index);
        if (disableValues) {
            $(`#Miller_Invoice_Number_${index}`).attr('disabled', 'disabled');
            $(`#Miller_Invoice_Date_${index}`).attr('disabled', 'disabled');
        } else {
            createKeyPressEventForPrimcoInvoiceNumber(index);
            $(`#Miller_Invoice_Date_${index}`).datepicker({
                dateFormat: 'yy-mm-dd',
                maxDate: 0,
                onSelect: function (date, i) {
                    $(`#Miller_Invoice_Date_${index}`).val(date);
                    setMillerInvoiceNumbers(index);
                }
            });
        }
    });
}

function addPrimcoInvoiceField() {
    const index = 'primco_' + primcoInvoiceNumbers.length;
    let html = createInput(index, 'Primco_Invoice_Number', 'Primco Invoice Number *', '', '');
    html += createInput(index, 'Primco_Invoice_Date', 'Primco Invoice Date', '', '');
    $('#primcoInvoiceNumbers').append(html);
    createKeyPressEventForPrimcoInvoiceNumber(index);
    $(`#Primco_Invoice_Date_${index}`).datepicker({
        dateFormat: 'yy-mm-dd',
        maxDate: 0,
        onSelect: function (date, i) {
            $(`#Primco_Invoice_Date_${index}`).val(date);
            setPrimcoInvoiceNumbers(index);
        }
    });
}

function createKeyPressEventForPrimcoInvoiceNumber(index) {
    const $element = $(`#Primco_Invoice_Number_${index}`);
    $element
        .keyup(() => setPrimcoInvoiceNumbers(index));
}

function setPrimcoInvoiceNumbers(index) {
    const arrayIndex = index.replace('primco_', '');
    console.log(primcoInvoiceNumbers);
    primcoInvoiceNumbers[arrayIndex] = {
        invoiceNumber: $(`#Primco_Invoice_Number_${index}`).val(),
        invoiceDate: $(`#Primco_Invoice_Date_${index}`).val(),
    };
    console.log(primcoInvoiceNumbers);
    $('#primco-invoice-number').val(JSON.stringify(primcoInvoiceNumbers));
}

function addMillerInvoiceField() {
    const index = 'miller_' + millerInvoiceNumbers.length;
    let html = createInput(index, 'Miller_Invoice_Number', 'Mill Invoice Number', '', '');
    html += createInput(index, 'Miller_Invoice_Date', 'Mill Invoice Date', '', '');
    $('#millerInvoiceNumbers').append(html);
    createKeyPressEventForMillerInvoiceNumber(index);
    $(`#Miller_Invoice_Date_${index}`).datepicker({
        dateFormat: 'yy-mm-dd',
        maxDate: 0,
        onSelect: function (date, i) {
            $(`#Miller_Invoice_Date_${index}`).val(date);
            setMillerInvoiceNumbers(index);
        }
    });
}

function createKeyPressEventForMillerInvoiceNumber(index) {
    const $element = $(`#Miller_Invoice_Number_${index}`);
    $element
        .keyup(() => setMillerInvoiceNumbers(index));
}

function setMillerInvoiceNumbers(index) {
    const arrayIndex = index.replace('miller_', '');
    millerInvoiceNumbers[arrayIndex] = {
        invoiceNumber: $(`#Miller_Invoice_Number_${index}`).val(),
        invoiceDate: $(`#Miller_Invoice_Date_${index}`).val(),
    };
    $('#miller-invoice-number').val(JSON.stringify(millerInvoiceNumbers));
}

$('#miller-name').keyup(() => {
    const val = $('#miller-name').val();
    const $suggestionElem = $('#suggestions-millers');
    if (val.length > 0) {
        const filteredMillers = millers.filter(miller => miller.miller_name.toLowerCase().indexOf(val.toLowerCase()) > -1);
        if (filteredMillers.length > 0) {
            $suggestionElem.html('');
            const _ul = $('<ul />')
                .attr('id', 'miller-list');
            filteredMillers.map(miller => $('<li />')
                .attr('onclick', `selectMiller(${miller.id})`)
                .html(miller.miller_name)
                .appendTo(_ul)
            );
            $suggestionElem.append(_ul).show();
        } else {
            $suggestionElem.hide();
            $('#miller-id').val('');
            $('#miller-user-id').val('');
            $('#miller-contact-name').val('');
            $('#miller-email').val('');
            $('#miller-phone').val('');
            $('#miller-address').val('');
            $('#miller-city').val('');
            $('#miller-postal-code').val('');
        }
    } else {
        $suggestionElem.hide();
    }
});

function selectMiller(millerId) {
    const miller = millers.filter(miller => miller.id == millerId)[0];
    if (miller) {
        $('#suggestions-millers').hide();
        $('#miller-id').val(millerId);
        $('#miller-user-id').val(miller.user_id);
        $('#miller-name').val(miller.miller_name);
        $('#miller-contact-name').val(miller.contact_name);
        $('#miller-email').val(miller.email);
        $('#miller-phone').val(miller.phone);
        $('#miller-address').val(miller.address);
        $('#miller-city').val(miller.city);
        $('#miller-postal-code').val(miller.postal_code);
    }
}
