<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * CurrencyConversionRate Entity
 *
 * @property int $id
 * @property float $cad
 * @property float $usd_mannington
 * @property float $usd_general
 * @property float $usd_others
 * @property float $usd_custom_1
 * @property float $usd_custom_2
 * @property float $usd_custom_3
 * @property float $usd_custom_4
 * @property \Cake\I18n\FrozenTime|null $created
 * @property \Cake\I18n\FrozenTime|null $updated
 */
class CurrencyConversionRate extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    /*protected $_accessible = [
        'cad' => true,
        'usd_mannington' => true,
        'usd_general' => true,
        'usd_others' => true,
        'usd_custom_1' => true,
        'usd_custom_2' => true,
        'usd_custom_3' => true,
        'usd_custom_4' => true,
        'created' => true,
        'updated' => true
    ];*/
}
