<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Salesmen Model
 *
 * @property &\Cake\ORM\Association\BelongsTo $Users
 * @property &\Cake\ORM\Association\HasMany $Meetings
 * @property \App\Model\Table\OrdersTable&\Cake\ORM\Association\HasMany $Orders
 *
 * @method \App\Model\Entity\Salesman get($primaryKey, $options = [])
 * @method \App\Model\Entity\Salesman newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Salesman[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Salesman|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Salesman saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Salesman patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Salesman[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Salesman findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class SalesmenTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('salesmen');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Users', [
            'foreignKey' => 'user_id'
        ]);
        $this->hasMany('Meetings', [
            'foreignKey' => 'salesman_id'
        ]);
        $this->hasMany('Orders', [
            'foreignKey' => 'salesman_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('name')
            ->maxLength('name', 250)
            ->requirePresence('name', 'create')
            ->notEmptyString('name');

        $validator
            ->scalar('sales_rep')
            ->maxLength('sales_rep', 20)
            ->allowEmptyString('sales_rep');

        $validator
            ->scalar('sd_rep')
            ->maxLength('sd_rep', 20)
            ->allowEmptyString('sd_rep');

        $validator
            ->scalar('rep_email')
            ->maxLength('rep_email', 50)
            ->allowEmptyString('rep_email');

        $validator
            ->scalar('spec')
            ->maxLength('spec', 100)
            ->allowEmptyString('spec');

        $validator
            ->scalar('province')
            ->maxLength('province', 100)
            ->allowEmptyString('province');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['user_id'], 'Users'));

        return $rules;
    }
}
