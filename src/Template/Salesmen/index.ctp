<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Salesman[]|\Cake\Collection\CollectionInterface $salesmen
 */
?>

<div class="salesmen index large-9 medium-8 columns content">
    <div class="orders index large-9 medium-8 columns content card card card-default scrollspy">
    
        <div class="card-content">
             <?= $this->Form->create('search') ?>
            <fieldset>
                <?php
                    echo $this->Form->input('search',['label' =>false]);
                    echo $this->Form->button(__('Search'));
                ?>
            </fieldset>
           
            <?= $this->Form->end() ?>
            <table cellpadding="0" cellspacing="0" class="table table-stripped">
                <thead>
                    <tr>
                        <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                        <th scope="col"><?= $this->Paginator->sort('name') ?></th>
                        <th scope="col"><?= $this->Paginator->sort('sales_rep') ?></th>
                        <th scope="col"><?= $this->Paginator->sort('sd_rep') ?></th>
                        <th scope="col"><?= $this->Paginator->sort('rep_email') ?></th>
                        <th scope="col"><?= $this->Paginator->sort('spec') ?></th>
                        <th scope="col"><?= $this->Paginator->sort('created') ?></th>
                        <th scope="col"><?= $this->Paginator->sort('updated') ?></th>
                        <th scope="col" class="actions"><?= __('Actions') ?></th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($salesmen as $salesman): ?>
                    <tr>
                        <td><?= $this->Number->format($salesman->id) ?></td>
                        <td><?= h($salesman->name) ?></td>
                        <td><?= h($salesman->sales_rep) ?></td>
                        <td><?= h($salesman->sd_rep) ?></td>
                        <td><?= h($salesman->rep_email) ?></td>
                        <td><?= h($salesman->spec) ?></td>
                        <td><?= h($salesman->created) ?></td>
                        <td><?= h($salesman->updated) ?></td>
                        <td class="actions">
                            <?= $this->Html->link(__('View'), ['action' => 'view', $salesman->id]) ?>
                            <?= $this->Html->link(__('Edit'), ['action' => 'edit', $salesman->id]) ?>
                            <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $salesman->id], ['confirm' => __('Are you sure you want to delete # {0}?', $salesman->id)]) ?>
                        </td>
                    </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
            <div class="paginator">
                <ul class="pagination">
                    <?= $this->Paginator->first('<< ' . __('first')) ?>
                    <?= $this->Paginator->prev('< ' . __('previous')) ?>
                    <?= $this->Paginator->numbers() ?>
                    <?= $this->Paginator->next(__('next') . ' >') ?>
                    <?= $this->Paginator->last(__('last') . ' >>') ?>
                </ul>
                <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
            </div>
        </div>
    </div>


</div>
