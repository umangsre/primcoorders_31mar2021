<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * ToolKits Model
 *
 * @method \App\Model\Entity\ToolKits get($primaryKey, $options = [])
 * @method \App\Model\Entity\ToolKits newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\ToolKits[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\ToolKits|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\ToolKits saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\ToolKits patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\ToolKits[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\ToolKits findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class ToolKitsTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('took_kits');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        /*$this->belongsTo('ToolKitCategory', [
            'className' => 'ToolKitCategory',
            'foreignKey' => 'tool_kit_category_id',
            'propertyName' => 'ToolKitCategory'
        ]);*/
    }
}
