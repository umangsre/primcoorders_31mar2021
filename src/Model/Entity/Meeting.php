<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Meeting Entity
 *
 * @property int $id
 * @property string|null $email
 * @property int|null $salesman_id
 * @property string|null $agenda
 * @property string|null $description
 * @property \Cake\I18n\FrozenTime $date_time
 * @property bool|null $is_customer_meeting
 * @property int|null $created_by
 * @property \Cake\I18n\FrozenTime|null $created
 * @property \Cake\I18n\FrozenTime|null $modified
 *
 * @property \App\Model\Entity\Salesman $salesman
 */
class Meeting extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'email' => true,
        'salesman_id' => true,
        'agenda' => true,
        'description' => true,
        'date_time' => true,
        'is_customer_meeting' => true,
        'created_by' => true,
        'created' => true,
        'modified' => true,
        'salesman' => true
    ];
}
