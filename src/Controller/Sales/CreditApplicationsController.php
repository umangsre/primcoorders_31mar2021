<?php
namespace App\Controller\Sales;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;
use Cake\Event\Event;
use Cake\Mailer\Email;
use Cake\Routing\Router;
use \PHPQRCode\QRcode;



/**
 * CreditApplications Controller
 *
 * @property \App\Model\Table\CreditApplicationsTable $CreditApplications
 *
 * @method \App\Model\Entity\CreditApplication[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class CreditApplicationsController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
	 
/**Allow user wihtout Login**/ 
	 public function beforeFilter(Event $event)
		{
			parent::beforeFilter($event);
			$this->Auth->allow(['viewCustomer', 'addCustomer', 'editCustomer','thankyou']);
		}
/**end here**/	


    public function index()
    {
		$this->viewBuilder()->setLayout('dashboard');
        $query = $this->CreditApplications->find();
        if($this->request->is('post')){
         $search = '%'.$this->request->getData('search').'%';
         $query->where(['OR' =>['company_legal_name LIKE' => '%'.$search.'%','company_trade_name LIKE' => '%'.$search.'%','street_address LIKE' => '%'.$search.'%']]);
       }
        $creditApplications = $this->paginate($query);

        $this->set(compact('creditApplications'));
    }

    /**
     * View method
     *
     * @param string|null $id Credit Application id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
		$this->viewBuilder()->setLayout('dashboard');
        $creditApplication = $this->CreditApplications->get($id, [
            'contain' => []
        ]);

        $this->set('creditApplication', $creditApplication);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
		$this->viewBuilder()->setLayout('dashboard');
        $creditApplication = $this->CreditApplications->newEntity();
        if ($this->request->is('post')) {
            $creditApplication = $this->CreditApplications->patchEntity($creditApplication, $this->request->getData());
            if ($this->CreditApplications->save($creditApplication)) {
                $this->Flash->success(__('The credit application has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The credit application could not be saved. Please, try again.'));
        }
        $this->set(compact('creditApplication'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Credit Application id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
		$this->viewBuilder()->setLayout('dashboard');
        $creditApplication = $this->CreditApplications->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $creditApplication = $this->CreditApplications->patchEntity($creditApplication, $this->request->getData());
            if ($this->CreditApplications->save($creditApplication)) {
                $this->Flash->success(__('The credit application has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The credit application could not be saved. Please, try again.'));
        }
        $this->set(compact('creditApplication'));
    }
	
/*Duplicate Here*/
/*****/
public function thankyou( $id = null )
    {
        // $this->autoRender = false;

        if ( $id == null ) {
            return $this->redirect( $this->referer() );
        }
        $id = base64_decode($id);

        $creditApplication = $this->CreditApplications->get($id, [
            'contain' => []
        ]);
        
        $image_name ='';
        $link ='';
        if($creditApplication){
            // $downLoad_link=$actual_link."/product-form/general_pdf.php?id=".$encoded_id;
             $link =  Router::url([ 'controller' => 'CreditApplications', 'action' => 'viewCustomer',base64_encode($creditApplication->id)
                                  ],TRUE);                
            //$downLoad_link = $site_url.'order_pdfs/'.$file_name;
            $path_qr = WWW_ROOT . "/img/QRcode/"; 
            $image_name = "qrcodes_creditApplication_".$creditApplication->id.".png"; 
            $qrcode_img = $path_qr.$image_name;
            // $ecc stores error correction capability('L') 
            $ecc = 'L'; 
            $pixel_Size = 10; 
            $frame_Size = 10;     
            // Generates QR Code and Stores it in directory given 
           QRcode::png($link, $qrcode_img, $ecc, $pixel_Size, $frame_Size); 
           $this->sendMail($image_name,$link, $creditApplication->id);
         }
        
        
        $this->set(compact('image_name','link','creditApplication'));
    }


 public function viewCustomer($id = null)
    {
        if ( $id == null ) {
            return $this->redirect( $this->referer() );
        }
        $id = base64_decode($id);
        $creditApplication = $this->CreditApplications->get($id, [
            'contain' => ['ContactsList','OnlineUserRegistration']
        ]);

        $contacts = TableRegistry::get('ContactsList');
        $onlineusers = TableRegistry::get('OnlineUserRegistration');
        $contactlist = $contacts->find( 'all' )
            ->where( [ 'credit_application_id' => $id  ] )->toArray();
        $onlineusersList = $onlineusers->find( 'all' )
            ->where( [ 'credit_application_id' => $id  ] )->toArray();    
        
        $this->set('creditApplication', $creditApplication);
        $this->set('contactlist', $contactlist);
        $this->set('onlineusersList', $onlineusersList);
         $this->set(compact('creditApplication'));
        /*echo '<pre>';
        print_r($creditApplication);
         print_r($contactlist);
          print_r($onlineusersList);
        echo '</pre>';*/
        $this->viewBuilder()->layout('ajax');
        $this->response->type('pdf');
    }
    public function sendMail($image_name,$link, $id){
        
        $toEmail = [
            's.nesbitt@primco.ca',
            't.yelland@primco.ca',
            'd.grona@primco.ca',
            'm.malhotra@primco.ca',
        ];
        $email = new Email();
        $email->setTransport('default');
        $email->viewVars(['image_name' => $image_name,'link' =>$link ]);
        $email->viewBuilder()->setTemplate('creditapplication');
        $email->setEmailFormat('html');
        $email->setFrom(['support@primco.ca' => 'Primco'])
        ->setTo($toEmail)
        ->setBcc(['Juneja.ankit@gmail.com','ManishMalhotra1303@gmail.com'])
       // ->setBcc('ManishMalhotra1303@gmail.com')
        ->setSubject('New Customer #'.$id)
        ->send();
      return true;
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function addCustomer()
    {
        $creditApplication = $this->CreditApplications->newEntity();
        if ($this->request->is('post')) {
            $creditApplication = $this->CreditApplications->patchEntity($creditApplication, $this->request->getData());
            if ($this->CreditApplications->save($creditApplication)) {
                $this->Flash->success(__('The credit application has been saved.'));
                return $this->redirect(['controller' =>'ContactsList' ,'action' => 'addContact',base64_encode($creditApplication->id)]);
               // return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The credit application could not be saved. Please, try again.'));
        }
        $this->set(compact('creditApplication'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Credit Application id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function editCustomer($id = null)
    {
		if ( $id == null ) {
            return $this->redirect( $this->referer() );
        }
        $id = base64_decode($id);
        $creditApplication = $this->CreditApplications->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $creditApplication = $this->CreditApplications->patchEntity($creditApplication, $this->request->getData());
            if ($this->CreditApplications->save($creditApplication)) {
                $this->Flash->success(__('The credit application has been saved.'));

                return $this->redirect(['controller' =>'ContactsList' ,'action' => 'editContact',base64_encode($creditApplication->id)]);
            }
            $this->Flash->error(__('The credit application could not be saved. Please, try again.'));
        }
		//print_r( $creditApplication);
        $this->set(compact('creditApplication'));
    }
	
/**Duplicate End here***/

    /**
     * Delete method
     *
     * @param string|null $id Credit Application id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $creditApplication = $this->CreditApplications->get($id);
        if ($this->CreditApplications->delete($creditApplication)) {
            $this->Flash->success(__('The credit application has been deleted.'));
        } else {
            $this->Flash->error(__('The credit application could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
