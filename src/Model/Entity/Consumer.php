<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Retailer Entity
 *
 * @property int $id
 * @property string $consumer_name
 * @property string $phone
 * @property string $address
 * @property string $city
 * @property string $postal_code
 * @property \Cake\I18n\FrozenTime|null $created
 *
 * @property \App\Model\Entity\Claim[] $claims
 */
class Consumer extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'consumer_name' => true,
        'phone' => true,
        'address' => true,
        'city' => true,
        'postal_code' => true,
        'created' => true,
        'claims' => true
    ];
}
