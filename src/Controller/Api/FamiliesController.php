<?php
namespace App\Controller\Api;

use App\Controller\AppController;

/**
 * Families Controller
 *
 *
 * @method \App\Model\Entity\Family[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class FamiliesController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $families = $this->paginate($this->Families);

        $this->set(compact('families'));
    }

    /**
     * View method
     *
     * @param string|null $id family id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $family = $this->Families->get($id, [
            'contain' => []
        ]);

        $this->set('family', $family);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $family = $this->Families->newEntity();
        if ($this->request->is('post')) {
            $family = $this->Families->patchEntity($family, $this->request->getData());
            if ($this->Families->save($family)) {
                $this->Flash->success(__('The family has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The family could not be saved. Please, try again.'));
        }
        $this->set(compact('family'));
    }

    /**
     * Edit method
     *
     * @param string|null $id family id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $family = $this->Families->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $family = $this->Families->patchEntity($family, $this->request->getData());
            if ($this->Families->save($family)) {
                $this->Flash->success(__('The family has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The family could not be saved. Please, try again.'));
        }
        $this->set(compact('family'));
    }

    /**
     * Delete method
     *
     * @param string|null $id family id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $family = $this->Families->get($id);
        if ($this->Families->delete($family)) {
            $this->Flash->success(__('The family has been deleted.'));
        } else {
            $this->Flash->error(__('The family could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    public function getallproductslist()
    {
        $this->autoRender = false;
        $query = $this->Families->find('all', [
            'fields' => ['id', 'family', 'name', 'product_code']
        ]);
        echo json_encode(array("response" => $query));
        exit;
    }

    public function getproductdetail()
    {
        $this->autoRender = false;
        $query = $this->Families->find('all', [
            'conditions' => ['id' => $this->request->getData('id')]
        ]);
        echo json_encode(array("response" => $query));
        exit;
    }

	 public function findproductbyname()
    {
       $this->autoRender = false;
       $query = $this->Families->find();
        if ($this->request->is('post')) {
            // if($id =$this->request->getData('id')){
            // $query->where(['id' => $id])->first(); 
            // }else{
                $family = $this->request->getData('family');
                $name = $this->request->getData('name');
                $product_code = $this->request->getData('product_code');
                $mill = $this->request->getData('mill');
				if(!empty($name)){
					$query = $query->where(['name' => $name]);
				}if(!empty($product_code)){
					$query = $query->where(['product_code' => $product_code]);
				}if(!empty($family)){
					$query = $query->where(['family' => $family]);
				}if(!empty($mill)){
					$query = $query->where(['mill' => $mill]);
				}
				//,'family LIKE' => '%'.$name.'%','product_code LIKE' => '%'.$product_code.'%','mill LIKE' => '%'.$mill.'%']])->all(); 
            //}
			$query->all();
            
        }
        echo  json_encode(array("response"=>$query));
		exit;
       // $this->response = $this->response->withStringBody($content);
       // $this->response = $this->response->withType('json');
      //  return $content;
    }
}
