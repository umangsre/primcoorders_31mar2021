<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Order $order
 */
?>

        <div class="row">
            <div class="col-12 logo-container text-center p-4">
                <?= $this->Html->image('logo.png'); ?>           
            </div>
            <div class="col-sm-12 col-12 full-width-mobile">
                <div class="steps-right-form">
                    <div class="steps_nav text-center pt-3 mb-3 row">
                        <div class="steps_item active col">
                            <p>1<br>
                                <em>Dealer Section</em>
                            </p>
                        </div>
                        <div class="steps_item  col">
                            <p>2<br>
                                <em>Sales Section</em>
                            </p>
                        </div>
                        <div class="steps_item col">
                            <p>3<br>
                                <em>Order Form</em>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="orders index large-9 medium-8 columns content card card card-default scrollspy">
        <div class="orders form large-9 medium-8 columns content">
            <?= $this->Form->create($order) ?>
            <div class="row">
                <div class="tab w-100">
                    <div class="form-fields">
                        <div class="row">
                            <div class="col-sm-6 col-12 mt-3">
                                <label>*Dealer Name</label>
                                <?= $this->Form->control( 'name', [ 'type' => 'text', 'required' => 'required', 'id' => 'customer_name', 'class' => 'form-control', 'label' => false ,'value' =>$customer['name']] ) ?>
                                
                                <div id="suggesstion-dealers" class="list_wraper shadow-sm" style="display:none;"></div>
                               
                              <?= $this->Form->control( 'customer_id', [ 'type' => 'hidden','id' => 'customer_id','value' =>$customer['id'] ] ) ?>  
                            </div>
                            <div class="col-sm-6 col-12 mt-3">
                                <label>*Address</label>
                                <?= $this->Form->control( 'address', [ 'type' => 'text', 'required' => 'required', 'id' => 'address', 'class' => 'form-control', 'label' => false,'value' =>$customer['address'] ] ) ?>
                            </div>
                            <div class="col-sm-6 col-12 mt-3">
                                <label>*City</label>
                                <?= $this->Form->control( 'city', [ 'type' => 'text', 'required' => 'required', 'id' => 'city', 'class' => 'form-control', 'label' => false,'value' =>$customer['city'] ] ) ?>
                            </div>
                            <div class="col-sm-6 col-12 mt-3">
                                <label>*Province</label>
                                <?= $this->Form->control( 'province', [ 'type' => 'text', 'required' => 'required', 'id' => 'province', 'class' => 'form-control', 'label' => false,'value' =>$customer['province'] ] ) ?>
                            </div>
                            <div class="col-sm-6 col-12 mt-3">
                                <label>*Postal Code</label>
                                <?= $this->Form->control( 'postal_code', [ 'type' => 'text', 'required' => 'required', 'id' => 'postal_code', 'class' => 'form-control', 'label' => false,'value' =>$customer['postal_code'] ] ) ?>
                            </div>
                            <div class="col-sm-6 col-12 mt-3">
                             <div id="sin_off_file">
                                <label>SIN</label>
                                <?= $this->Form->control( 'sin', [ 'type' => 'text', 'id' => 'sin', 'class' => 'form-control', 'label' => false ] ) ?>
                                </div>
                                <div id="sin_on_file" style="display:none;" class="medium" > 
                                   <label placeholder="* Sin" alt="Sin"> SIN number is "OnFile" </label>
                                </div>
                            </div>
                            <!-- <div class="col-sm-6 col-12 mt-3">
                                <label>P.O. Number</label>
                                <?= $this->Form->control( 'po_number', [ 'type' => 'text', 'class' => 'form-control', 'label' => false ,'value' =>$order['po_number']] ) ?>
                            </div> -->
                            <div class="col-sm-6 col-12 mt-3">
                                <label>Ship To</label>
                                <?= $this->Form->control( 'ship_to', [ 'type' => 'text',  'class' => 'form-control', 'label' => false,'value' =>$order['ship_to'] ] ) ?>
                            </div>
                             <div class="col-sm-6 col-12 mt-3">
                                <label>Additional Comments</label>
                                <?= $this->Form->control( 'additional_comments', [ 'type' => 'text', 'class' => 'form-control', 'label' => false,'value' =>$order['additional_comments'] ] ) ?>
                            </div>
                           
                            <div class="col-sm-6 col-12 mt-3">
                                <label>Ship Via</label>
                                <?= $this->Form->control( 'ship_via', [ 'type' => 'text',  'class' => 'form-control', 'label' => false,'value' =>$order['ship_via'] ] ) ?>
                            </div>
                             <div class="col-sm-6 col-12 mt-3">
                                <label>Ship Date</label>
                                <?= $this->Form->control( 'ship_date', [ 'type' => 'date', 'class' => 'form-control', 'label' => false,'value' =>$order['ship_date'] ] ) ?>
                            </div>
                            
							<div class="col-sm-6 col-12 mt-3">
                            </div>
                           
                            <div class="col-sm-12 col-12 mt-3">
                                <?= $this->Form->button(__('Next'), array( 'class' => 'btn btn-primary' ,'id' =>'customer_form' )  ) ?>
                            </div>


                        </div>
                    </div>
                </div>
            </div>    

            
            <?= $this->Form->end() ?>
        </div>
    </div>
