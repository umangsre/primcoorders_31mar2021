<?php

namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;

/**
 * Families Controller
 *
 * @property \App\Model\Table\FamiliesTable $Families
 *
 * @method \App\Model\Entity\Family[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class FamiliesController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public $helpers = ['Cewi/Excel.Excel'];

    public $paginate = [
        'Articles' => ['search' => 'families']

    ];

    public function index()
    {
        $this->viewBuilder()->setLayout('dashboard');
        $this->loadModel('CurrencyConversionRates');

        $cadquery = TableRegistry::getTableLocator()->get('ccr')->find('all')->all();

        $arraycad = [];
        foreach ($cadquery as $key => $rate) {
            $arraycad[$rate->ckey] = $rate->cvalue;

        }


        $key = array('id', 'family', 'product_code', 'name', 'role_cut', 'mill_cost', 'usd_cad', 'new_mill_cost', 'brokerage_multiplier', 'brokerage', 'units', 'freight', 'ad_fund_percentage', 'ad_fund', 'landed_cost_per_unit', 'load_percentage', '_load', 'margin_lic', 'sell_price_level1', 'rebate_level1', 'gross_margin_level1', 'sell_price_level2', 'rebate_level2', 'gross_margin_level2', 'sell_price_level3', 'rebate_level3', 'gross_margin_level3', 'mill', 'vendorID', 'sqft_ctn', 'wgt_lbs_ctn', 'ctn_pallet', 'sqft_pallet', 'pallet_container_20', 'sqft_container_20', 'sqft_roll', 'sqyd_roll', 'pces_ctn', 'rolls_pallet', 'pallet_container_40', 'tabs_strip', 'packs_pallet', 'ibs_container', 'm2_ctn', 'lf_ctn', 'qty_ctn');

        $upload_data = [];


        $search2 = $this->request->getQueryParams();

        if (!empty($search2['search'])) {
            $search2 = $search2['search'];
            $query_all = $this->Families->find()->where(['or' => ['name LIKE' => '%' . $search2 . '%', 'product_code LIKE' => '%' . $search2 . '%', 'family LIKE' => '%' . $search2 . '%', 'mill LIKE' => '%' . $search2 . '%']]);
        } else {
            $query_all = $this->Families->find('all')->all();
        }


        foreach ($query_all as $family) {
            $keydata = [];
            for ($k = 0; $k < count($key); $k++) {
                $keydata[str_replace("_", " ", $key[$k])] = $family[$key[$k]];
            }
            $upload_data[] = $keydata;
        }
        $articles = $upload_data;
        // debug($articles);
        // exit;


        $uploadData = '';
        $key = array('family', 'product_code', 'name', 'role_cut', 'mill_cost', 'usd_cad', 'new_mill_cost', 'brokerage_multiplier', 'brokerage', 'units', 'freight', 'ad_fund_percentage', 'ad_fund', 'landed_cost_per_unit', 'load_percentage', '_load', 'margin_lic', 'sell_price_level1', 'rebate_level1', 'gross_margin_level1', 'sell_price_level2', 'rebate_level2', 'gross_margin_level2', 'sell_price_level3', 'rebate_level3', 'gross_margin_level3', 'mill', 'vendorID', 'sqft_ctn', 'wgt_lbs_ctn', 'ctn_pallet', 'sqft_pallet', 'pallet_container_20', 'sqft_container_20', 'sqft_roll', 'sqyd_roll', 'pces_ctn', 'rolls_pallet', 'pallet_container_40', 'tabs_strip', 'packs_pallet', 'ibs_container', 'm2_ctn', 'lf_ctn', 'qty_ctn', 'conversion_rate');

        if ($this->request->is('post')) {
            if (!empty($this->getRequest()->getData('file.name'))) {
                move_uploaded_file($this->getRequest()->getData('file.tmp_name'), TMP . $this->getRequest()->getData('file.name'));

                $data = $this->Import->prepareEntityData(TMP . $this->getRequest()->getData('file.name'));

                for ($i = 0; $i < count($data); $i++) {
                    $value = $data[$i];
                    $arrayvalue = array_values($value);

                    $id = $arrayvalue[0];
                    unset($value["Sr.no"]);
                    unset($value["id"]);
                    if (!empty($value["usd cad"])) {
                        $cad = strtolower($value["usd cad"]);
                        $value["usd cad"] = str_replace("-", "_", $cad);

                        $value["conversion_rate"] = $arraycad[$value["usd cad"]];
                    }
                    if (count($key) == count($value)) {
                        //print_r($key);
                        //print_r($value);
                        $datainfo = array_combine($key, $value);
                        $query = $this->Families->find()->where(['id' => $id]);


                        if (!$query->isEmpty()) {

                            //echo $id.'first';

                            //print_r($query);

                            $family = $this->Families->get($id, [
                                'contain' => []
                            ]);

                            $currencyConversionRate = $this->CurrencyConversionRates->get(1, [
                                'contain' => []
                            ]);
                            $family = $this->Families->patchEntity($family, $datainfo);
                            $this->Families->save($family);


                        } else {
                            $family = $this->Families->newEntity();
                            $family = $this->Families->patchEntity($family, $datainfo);
                            $this->Families->save($family);
                        }
                    } else {

                        $this->Flash->error(__('some error in excel file please try again.'));
                    }

                }
                $this->Flash->success(__('The family has been saved.'));
            } else {
                $this->Flash->error(__('Please choose a file to upload.'));
            }
        }

        //$query = $this->Families->find('all')->all();
        $query = $this->Families->find();

        $search = $this->request->getQueryParams();

        if (!empty($search['search'])) {
            $search = $search['search'];
            $query->where(['OR' => ['name LIKE' => '%' . $search . '%', 'product_code LIKE' => '%' . $search . '%', 'family LIKE' => '%' . $search . '%', 'mill LIKE' => '%' . $search . '%']]);
        }
        if (!empty($search['search_f'])) {
            $search = $search['search_f'];
            $query->where(['family LIKE' => '%' . $search . '%']);
        }


        $families = $this->paginate($query);

        $this->set(compact('families', 'articles', 'uploadData', 'search'));
    }

    public function marginCalculator()
    {
        $this->autoRender = false;
        $user_id = $this->Auth->user('id');
        $query = $this->Families->find();
        if ($this->request->is('post')) {
            $query->where(['id' => $this->request->getData('id')])->first();
        }
        $content = json_encode($query);
        $this->response = $this->response->withStringBody($content);
        $this->response = $this->response->withType('json');
        return $this->response;
    }

    public function findproductbyname()
    {
        $this->autoRender = false;
        $query = $this->Families->find();
        if ($this->request->is('post')) {
            if ($id = $this->request->getData('id')) {
                $query->where(['id' => $id])->first();
            } else {
                $search = '%' . $this->request->getData('search') . '%';
                $query->where(['OR' => ['name LIKE' => '%' . $search . '%', 'family LIKE' => '%' . $search . '%', 'product_code LIKE' => '%' . $search . '%']])->all();
            }

        }
        $content = json_encode($query);
        $this->response = $this->response->withStringBody($content);
        $this->response = $this->response->withType('json');
        return $this->response;
    }

    public function findMarginbyname()
    {
        $this->autoRender = false;
        $query = $this->Families->find();
        if ($this->request->is('post')) {
            if ($id = $this->request->getData('id')) {
                $query->where(['id' => $id])->first();
            } else {
                $search = $this->request->getData('search');
                $query->where(['OR' => ['name LIKE' => '%' . $search . '%', 'product_code LIKE' => '%' . $search . '%']])->all();
            }

        }
        $content = json_encode($query);
        $this->response = $this->response->withStringBody($content);
        $this->response = $this->response->withType('json');
        return $this->response;
    }

    /**
     * View method
     *
     * @param string|null $id Family id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $this->viewBuilder()->setLayout('dashboard');
        $family = $this->Families->get($id, [
            'contain' => []
        ]);

        $this->set('family', $family);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $this->viewBuilder()->setLayout('dashboard');
        $family = $this->Families->newEntity();

        if ($this->request->is('post')) {
            $file = $this->request->getData('image');
            $ext = pathinfo($file['name'], PATHINFO_EXTENSION);
            $newFileName = md5(date('YmdHis')) . '.' . $ext;
            $productImage = '';
            if (move_uploaded_file($file['tmp_name'], WWW_ROOT . 'uploads/Products/' . $newFileName)) {
                $productImage = $newFileName;
            }
            $familiData = $this->request->getData();
            $familiData['product_image'] = $productImage;

            $family = $this->Families->patchEntity($family, $familiData);

            if ($this->Families->save($family)) {
                $this->Flash->success(__('The family has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The family could not be saved. Please, try again.'));
        }

        $this->loadModel('CurrencyConversionRates');
        $rates = [];
        $currencyConversionRate = $this->CurrencyConversionRates->find('all')->all();
        foreach ($currencyConversionRate as $key => $rate) {
            //$rates[$rate->ckey] = $rate->ckey;
            $t = explode('_', $rate->ckey);
            $name = '';
            foreach ($t as $key => $value) {
                if ($key > 0) {
                    $name .= ' ' . ucfirst($value);
                } else {
                    $name = strtoupper($value);
                }
            }
            //$rates[$rate->ckey] = str_replace("_","-",implode('_', array_map('ucfirst', explode('_', $rate->ckey))));
            $rates[$rate->ckey] = $name;
        }
        $currencyConversionRate = $rates;
        $this->set(compact('family', 'currencyConversionRate'));

    }

    /**
     * Edit method
     *
     * @param string|null $id Family id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $this->viewBuilder()->setLayout('dashboard');
        $this->loadModel('CurrencyConversionRates');

        $family = $this->Families->get($id, [
            'contain' => []
        ]);
        $rates = [];
        $currencyConversionRate = $this->CurrencyConversionRates->find('all')->all();
        foreach ($currencyConversionRate as $key => $rate) {
            $t = explode('_', $rate->ckey);
            $name = '';
            foreach ($t as $key => $value) {
                if ($key > 0) {
                    $name .= ' ' . ucfirst($value);
                } else {
                    $name = strtoupper($value);
                }
            }

            $rates[$rate->ckey] = $name;
        }
        $currencyConversionRate = $rates;

        $tmpApiData = $this->getData($family->product_code);
        $apiData = [];
        if ($tmpApiData && is_array($tmpApiData)) {
            foreach ($tmpApiData as $key => $data) {
                $apiData[trim($data['ItemNumber'])] = $data;
            }
        }

        if ($apiData) {
            $families = $this->Families->find('all')->where(['item_code IN' => array_keys($apiData)]);
            if ($families) {
                foreach ($families as $key => $fData) {
                    if (isset($apiData[$fData->item_code])) {
                        unset($apiData[$fData->item_code]);
                    }
                }
            }

            $itemCode = $family->item_code;
            if (isset($apiData[$itemCode])) {
                $family->division = trim($apiData[$itemCode]['Division']);
                $family->class_code = trim($apiData[$itemCode]['ClassCode']);
                $family->item_code = trim($apiData[$itemCode]['ItemNumber']);
                $family->specification = trim($apiData[$itemCode]['Specification']);
                $family->peice_per_carton = trim($apiData[$itemCode]['PiecesInCarton']);
                $family->color = trim($apiData[$itemCode]['Color']);
                unset($apiData[$itemCode]);
            } else {
                $tData = reset($apiData);
                if ($tData) {
                    $family->division = trim($tData['Division']);
                    $family->class_code = trim($tData['ClassCode']);
                    $family->item_code = trim($tData['ItemNumber']);
                    $family->specification = trim($tData['Specification']);
                    $family->peice_per_carton = trim($tData['PiecesInCarton']);
                    $family->color = trim($tData['Color']);
                    next($apiData);
                }
            }
        }

        if ($this->request->is(['patch', 'post', 'put'])) {
            $file = $this->request->getData('image');

            $ext = pathinfo($file['name'], PATHINFO_EXTENSION);
            $newFileName = md5(date('YmdHis')) . '.' . $ext;
            $productImage = '';
            if (move_uploaded_file($file['tmp_name'], WWW_ROOT . 'uploads/Products/' . $newFileName)) {
                $productImage = $newFileName;
            }
            $familiData = $this->request->getData();
            $familiData['product_image'] = $productImage;

            $family = $this->Families->patchEntity($family, $familiData);
            if ($this->Families->save($family)) {
                if ($apiData) {
                    $dataToSave = [];
                    foreach ($apiData as $key => $aData) {
                        $familiData['division'] = trim($aData['Division']);
                        $familiData['class_code'] = trim($aData['ClassCode']);
                        $familiData['item_code'] = trim($aData['ItemNumber']);
                        $familiData['specification'] = trim($aData['Specification']);
                        $familiData['piece_per_carton'] = trim($aData['PiecesInCarton']);
                        $familiData['color'] = trim($aData['Color']);
                        $dataToSave[] = $familiData;
                    }
                    $entities = $this->Families->newEntities($dataToSave);
                    $result = $this->Families->saveMany($entities);
                }
                $this->Flash->success(__('The family has been saved.'));
                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The family could not be saved. Please, try again.'));
        }

        $this->set(compact('family', 'currencyConversionRate'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Family id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $family = $this->Families->get($id);
        if ($this->Families->delete($family)) {
            $this->Flash->success(__('The family has been deleted.'));
        } else {
            $this->Flash->error(__('The family could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    public function initialize()
    {
        parent::initialize();
        $this->loadComponent('Cewi/Excel.Import');
        $this->loadComponent('RequestHandler', [
            'enableBeforeRedirect' => false,
        ]);

        $this->loadComponent('Flash');
        $this->loadModel('Files');
    }

    public function viewCalculator($id = null)
    {
        //$this->viewBuilder()->setLayout('dashboard');
        $this->set(compact('id'));
    }

    public function viewList()
    {


        $this->viewBuilder()->setLayout('dashboard');
        $this->loadModel('CurrencyConversionRates');
        $cadquery = TableRegistry::getTableLocator()->get('CurrencyConversionRates')->find('all')->all();


        $cadquery = TableRegistry::getTableLocator()->get('currency_conversion_rates')->find();
        // $cadquery = TableRegistry::getTableLocator()->get('Ccr')->find();
        $arraycad = "";
        foreach ($cadquery as $article) {
            $arraycad = array("cad" => $article->cad, "usd_mannington" => $article->usd_mannington, "usd_others" => $article->usd_others, "usd_general" => $article->usd_general, "usd_custom_1" => $article->usd_custom_1, "usd_custom_2" => $article->usd_custom_2, "usd_custom_3" => $article->usd_custom_3, "usd_custom_4" => $article->usd_custom_4);
        }

        // foreach ($cadquery as $article) {
        //     $arraycad = array("c_key" => $article->$c_value);
        // }

        $key = array('id', 'family', 'product_code', 'name', 'role_cut', 'mill_cost', 'usd_cad', 'new_mill_cost', 'brokerage_multiplier', 'brokerage', 'units', 'freight', 'ad_fund_percentage', 'ad_fund', 'landed_cost_per_unit', 'load_percentage', '_load', 'margin_lic', 'sell_price_level1', 'rebate_level1', 'gross_margin_level1', 'sell_price_level2', 'rebate_level2', 'gross_margin_level2', 'sell_price_level3', 'rebate_level3', 'gross_margin_level3', 'mill', 'vendorID', 'sqft_ctn', 'wgt_lbs_ctn', 'ctn_pallet', 'sqft_pallet', 'pallet_container_20', 'sqft_container_20', 'sqft_roll', 'sqyd_roll', 'pces_ctn', 'rolls_pallet', 'pallet_container_40', 'tabs_strip', 'packs_pallet', 'ibs_container', 'm2_ctn', 'lf_ctn', 'qty_ctn');
        $upload_data = [];


        $search2 = $this->request->getQueryParams();
        if (!empty($search2['search'])) {
            $search2 = $search2['search'];
            $query_all = $this->Families->find()->where(['or' => ['name LIKE' => '%' . $search2 . '%', 'product_code LIKE' => '%' . $search2 . '%', 'family LIKE' => '%' . $search2 . '%', 'mill LIKE' => '%' . $search2 . '%']]);
        } else {
            $query_all = $this->Families->find();
        };

        foreach ($query_all as $family) {

            $keydata = [];
            for ($k = 0; $k < count($key); $k++) {
                if (!empty($search2['margin'])) {

                    if ($search2['margin'] == 2 || $search2['margin'] == 3) {

                        $dollarprice = $family->conversion_rate;
                        if (!empty($search2['default']) && $search2['default'] == 2) {
                            if (!empty($search2['dollar'])) {
                                $dollarprice = $search2['dollar'];
                            }
                        }

                        $update_marginlic = $family->mill_cost / $dollarprice;
                        $update_brokragepersantagebrokragepersantage = $family->brokerage_multiplier;
                        $update_brokrage = $update_brokragepersantagebrokragepersantage * $update_marginlic;
                        $update_freight = $family->freight;
                        $updtae_ad_fund = $update_marginlic * $family->ad_fund_percentage;
                        $update_landed_cost_per_unit = ($update_marginlic) + ($update_brokrage) + ($update_freight) + ($updtae_ad_fund);
                        $update_load = $update_landed_cost_per_unit * ($family->load_percentage);
                        $update_margin_lic = $update_landed_cost_per_unit + $update_load;

                        if ($search2['margin'] == 2) {
                            if ($key[$k] == 'gross_margin_level1') {
                                $family[$key[$k]] = number_format(((((($family->sell_price_level1) - ($update_margin_lic)) + ($family->rebate_level1)) / ($family->sell_price_level1)) * 100), 2);


                            } else if ($key[$k] == 'gross_margin_level2') {
                                $family[$key[$k]] = number_format(((((($family->sell_price_level2) - ($update_margin_lic)) + ($family->rebate_level2)) / ($family->sell_price_level2)) * 100), 2);
                            } else if ($key[$k] == 'gross_margin_level3') {
                                $family[$key[$k]] = number_format(((((($family->sell_price_level3) - ($update_margin_lic)) + ($family->rebate_level3)) / ($family->sell_price_level3)) * 100), 2);
                            }
                        } else if ($search2['margin'] == 3) {
                            if ($key[$k] == 'sell_price_level1') {


                                $family[$key[$k]] = number_format((($update_margin_lic) / (1 - (($family->gross_margin_level1) / 100))), 2);
                            } else if ($key[$k] == 'sell_price_level2') {
                                $family[$key[$k]] = number_format((($update_margin_lic) / (1 - (($family->gross_margin_level2) / 100))), 2);
                            } else if ($key[$k] == 'sell_price_level3') {
                                $family[$key[$k]] = number_format((($update_margin_lic) / (1 - (($family->gross_margin_level3) / 100))), 2);
                            }
                        }


                    }
                }

                $keydata[str_replace("_", " ", $key[$k])] = $family[$key[$k]];

            }

            $upload_data[] = $keydata;
        }
        $articles = $upload_data;

        $uploadData = '';
        $key = array('family', 'product_code', 'name', 'role_cut', 'mill_cost', 'usd_cad', 'new_mill_cost', 'brokerage_multiplier', 'brokerage', 'units', 'freight', 'ad_fund_percentage', 'ad_fund', 'landed_cost_per_unit', 'load_percentage', '_load', 'margin_lic', 'sell_price_level1', 'rebate_level1', 'gross_margin_level1', 'sell_price_level2', 'rebate_level2', 'gross_margin_level2', 'sell_price_level3', 'rebate_level3', 'gross_margin_level3', 'mill', 'vendorID', 'sqft_ctn', 'wgt_lbs_ctn', 'ctn_pallet', 'sqft_pallet', 'pallet_container_20', 'sqft_container_20', 'sqft_roll', 'sqyd_roll', 'pces_ctn', 'rolls_pallet', 'pallet_container_40', 'tabs_strip', 'packs_pallet', 'ibs_container', 'm2_ctn', 'lf_ctn', 'qty_ctn', 'conversion_rate');
        if ($this->request->is('post')) {
            if (!empty($this->getRequest()->getData('file.name'))) {
                move_uploaded_file($this->getRequest()->getData('file.tmp_name'), TMP . $this->getRequest()->getData('file.name'));

                $data = $this->Import->prepareEntityData(TMP . $this->getRequest()->getData('file.name'));

                for ($i = 0; $i < count($data); $i++) {
                    $value = $data[$i];
                    $arrayvalue = array_values($value);

                    $id = $arrayvalue[0];
                    unset($value["Sr.no"]);
                    unset($value["id"]);
                    if (!empty($value["usd cad"])) {
                        $cad = strtolower($value["usd cad"]);
                        $value["usd cad"] = str_replace("-", "_", $cad);

                        $value["conversion_rate"] = $arraycad[$value["usd cad"]];
                    }
                    if (count($key) == count($value)) {

                        $datainfo = array_combine($key, $value);
                        $query = $this->Families->find()->where(['id' => $id]);
                        if (!$query->isEmpty()) {
                            $family = $this->Families->get($id, [
                                'contain' => []
                            ]);

                            $currencyConversionRate = $this->CurrencyConversionRates->get(1, [
                                'contain' => []
                            ]);
                            $family = $this->Families->patchEntity($family, $datainfo);
                            $this->Families->save($family);


                        } else {
                            $family = $this->Families->newEntity();
                            $family = $this->Families->patchEntity($family, $datainfo);
                            $this->Families->save($family);

                        }
                    } else {

                        $this->Flash->error(__('some error in excel file please try again.'));
                    }

                }
                $this->Flash->success(__('The family has been saved.'));
            } else {
                $this->Flash->error(__('Please choose a file to upload.'));
            }
        }


        $query = $this->Families->find();
        $search = $this->request->getQueryParams();

        if (!empty($search['search'])) {
            $search = $search['search'];
            $query->where(['OR' => ['name LIKE' => '%' . $search . '%', 'product_code LIKE' => '%' . $search . '%', 'family LIKE' => '%' . $search . '%', 'mill LIKE' => '%' . $search . '%']]);
        }


        $families = $this->paginate($query);

        $this->set(compact('families', 'articles', 'uploadData', 'search'));
    }

    public function newUpdate()
    {
        $search2 = $this->request->getQueryParams();
        $key = array('id', 'family', 'product_code', 'name', 'role_cut', 'mill_cost', 'usd_cad', 'new_mill_cost', 'brokerage_multiplier', 'brokerage', 'units', 'freight', 'ad_fund_percentage', 'ad_fund', 'landed_cost_per_unit', 'load_percentage', '_load', 'margin_lic', 'sell_price_level1', 'rebate_level1', 'gross_margin_level1', 'sell_price_level2', 'rebate_level2', 'gross_margin_level2', 'sell_price_level3', 'rebate_level3', 'gross_margin_level3', 'mill', 'vendorID', 'sqft_ctn', 'wgt_lbs_ctn', 'ctn_pallet', 'sqft_pallet', 'pallet_container_20', 'sqft_container_20', 'sqft_roll', 'sqyd_roll', 'pces_ctn', 'rolls_pallet', 'pallet_container_40', 'tabs_strip', 'packs_pallet', 'ibs_container', 'm2_ctn', 'lf_ctn', 'qty_ctn');
        if (!empty($search2['search'])) {
            $search3 = $search2['search'];
            $query_all = $this->Families->find()->where(['or' => ['name LIKE' => '%' . $search3 . '%', 'product_code LIKE' => '%' . $search3 . '%', 'family LIKE' => '%' . $search3 . '%', 'mill LIKE' => '%' . $search3 . '%']]);
        } else {
            $query_all = $this->Families->find();
        }


        $familyid = explode(",", $search2['familyid']);

        foreach ($query_all as $family) {

            if (in_array($family->id, $familyid)) {

                $dollarprice = $family->conversion_rate;
                $dollar = $family->usd_cad;
                if (!empty($search2['default']) && $search2['default'] == 2) {
                    if (!empty($search2['dollar'])) {
                        $dollar = "custom_usd";
                        $dollarprice = $search2['dollar'];
                    }
                }

                $update_marginlic = $family->mill_cost / $dollarprice;
                $update_brokragepersantagebrokragepersantage = $family->brokerage_multiplier;
                $update_brokrage = $update_brokragepersantagebrokragepersantage * $update_marginlic;
                $update_freight = $family->freight;
                $updtae_ad_fund = $update_marginlic * $family->ad_fund_percentage;
                $update_landed_cost_per_unit = ($update_marginlic) + ($update_brokrage) + ($update_freight) + ($updtae_ad_fund);
                $update_load = $update_landed_cost_per_unit * ($family->load_percentage);
                $update_margin_lic = $update_landed_cost_per_unit + $update_load;

                if (!empty($search2['margin'])) {

                    if ($search2['margin'] == 2 || $search2['margin'] == 3) {
                        if ($search2['margin'] == 2) {

                            $gross_margin_level1 = number_format(((((($family->sell_price_level1) - ($update_margin_lic)) + ($family->rebate_level1)) / ($family->sell_price_level1)) * 100), 2);

                            $gross_margin_level2 = number_format(((((($family->sell_price_level2) - ($update_margin_lic)) + ($family->rebate_level2)) / ($family->sell_price_level2)) * 100), 2);

                            $gross_margin_level3 = number_format(((((($family->sell_price_level3) - ($update_margin_lic)) + ($family->rebate_level3)) / ($family->sell_price_level3)) * 100), 2);

                            $sell_price_level1 = $family->sell_price_level1;
                            $sell_price_level2 = $family->sell_price_level2;
                            $sell_price_level3 = $family->sell_price_level3;
                        } else if ($search2['margin'] == 3) {

                            $sell_price_level1 = number_format((($update_margin_lic) / (1 - (($family->gross_margin_level1) / 100))), 2);

                            $sell_price_level2 = number_format((($update_margin_lic) / (1 - (($family->gross_margin_level2) / 100))), 2);

                            $sell_price_level3 = number_format((($update_margin_lic) / (1 - (($family->gross_margin_level3) / 100))), 2);

                            $gross_margin_level1 = $family->gross_margin_level1;
                            $gross_margin_level2 = $family->gross_margin_level2;
                            $gross_margin_level3 = $family->gross_margin_level3;
                        }
                    } else {
                        $sell_price_level1 = $family->sell_price_level1;
                        $sell_price_level2 = $family->sell_price_level2;
                        $sell_price_level3 = $family->sell_price_level3;

                        $gross_margin_level1 = $family->gross_margin_level1;
                        $gross_margin_level2 = $family->gross_margin_level2;
                        $gross_margin_level3 = $family->gross_margin_level3;
                    }
                }


                $Families = TableRegistry::getTableLocator()->get('families');
                $familys = $Families->get($family->id);


                $familys->usd_cad = $dollar;
                $familys->new_mill_cost = $update_marginlic;
                $familys->conversion_rate = $dollarprice;
                $familys->brokerage = $update_brokrage;
                $familys->ad_fund = $updtae_ad_fund;
                $familys->landed_cost_per_unit = $update_landed_cost_per_unit;
                $familys->_load = $update_load;
                $familys->margin_lic = $update_margin_lic;
                $familys->sell_price_level1 = $sell_price_level1;
                $familys->sell_price_level2 = $sell_price_level2;
                $familys->sell_price_level3 = $sell_price_level3;
                $familys->gross_margin_level1 = $gross_margin_level1;
                $familys->gross_margin_level2 = $gross_margin_level2;
                $familys->gross_margin_level3 = $gross_margin_level3;
                $Families->save($familys);
            }

        }
        return $this->redirect(['action' => 'view-list']);
    }

    public function upload()
    {
        $this->viewBuilder()->setLayout('dashboard');
        $uploadData = '';
        $key = array('family', 'product_code', 'name', 'role_cut', 'mill_cost', 'usd_cad', 'new_mill_cost', 'brokerage_multiplier', 'brokerage', 'units', 'freight', 'ad_fund_percentage', 'ad_fund', 'landed_cost_per_unit', 'load_percentage', '_load', 'margin_lic', 'sell_price_level1', 'rebate_level1', 'gross_margin_level1', 'sell_price_level2', 'rebate_level2', 'gross_margin_level2', 'sell_price_level3', 'rebate_level3', 'gross_margin_level3', 'mill', 'vendorID', 'sqft_ctn', 'wgt_lbs_ctn', 'ctn_pallet', 'sqft_pallet', 'pallet_container_20', 'sqft_container_20', 'sqft_roll', 'sqyd_roll');
        if ($this->request->is('post')) {
            if (!empty($this->getRequest()->getData('file.name'))) {
                move_uploaded_file($this->getRequest()->getData('file.tmp_name'), TMP . $this->getRequest()->getData('file.name'));
                $data = $this->Import->prepareEntityData(TMP . $this->getRequest()->getData('file.name'));

                for ($i = 0; $i < count($data); $i++) {
                    $value = $data[$i];
                    $arrayvalue = array_values($value);
                    $id = $arrayvalue[0];
                    unset($value["Sr.no"]);
                    unset($value["id"]);
                    $datainfo = array_combine($key, $value);
                    $query = $this->Families->find()->where(['id' => $id])->first();
                    if (!empty($query)) {
                        $family = $this->Families->get($id, [
                            'contain' => []
                        ]);

                        $family = $this->Families->patchEntity($family, $datainfo);
                        $this->Families->save($family);
                    } else {
                        $family = $this->Families->newEntity();
                        $family = $this->Families->patchEntity($family, $datainfo);
                        $this->Families->save($family);
                    }
                    $this->Flash->success(__('The family has been saved.'));

                }

            } else {
                $this->Flash->error(__('Please choose a file to upload.'));
            }
        }
        $this->set(compact('uploadData'));
    }

    public function getData($productCode = '')
    {
        $url = "http://3.131.62.35:8088/api/data/" . $productCode;
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_URL, $url);
        $result = curl_exec($ch);
        curl_close($ch);
        return json_decode($result, true);
    }
}
