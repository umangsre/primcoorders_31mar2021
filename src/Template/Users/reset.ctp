<div class="col s12 m6 l4 z-depth-4 card-panel border-radius-6 login-card bg-opacity-8">
    <?= $this->Form->create() ?>
    <div class="row">
        <div class="input-field col s12">
            <h5 class="ml-4">Reset Password</h5>
        </div>
    </div>
    <div class="row margin">
        <p><?= $this->Flash->render() ?></p>
    </div>
    <div class="row margin">
        <div class="input-field col s12">
            <i class="material-icons prefix pt-2">lock</i>
            <?= $this->Form->control('new_password', ['type'=>'password', 'label' => false, 'div' => false, 'id' => 'new_password', 'autocomplete' =>'off' ] ) ?>
            <label for="new_password" class="center-align">New Password</label>
        </div>
    </div>
    <div class="row margin">
        <div class="input-field col s12">
            <i class="material-icons prefix pt-2">lock</i>
            <?= $this->Form->control('confirm_password', ['type'=>'password', 'label' => false, 'div' => false, 'id' => 'confirm_password', 'autocomplete' =>'off' ] ) ?>
            <label for="confirm_password" class="center-align">Confirm Password</label>
        </div>
    </div>
    <div class="row">
        <div class="input-field col s12">
            <?= $this->Form->button(__('Submit'), [ 'class' => "btn waves-effect waves-light border-round gradient-45deg-purple-deep-orange col s12" ] ); ?>
        </div>
    </div>
    <?= $this->Form->end() ?>
</div>
