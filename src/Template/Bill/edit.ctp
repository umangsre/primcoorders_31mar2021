<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Bill $bill
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $bill->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $bill->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Bill'), ['action' => 'index']) ?></li>
    </ul>
</nav>
<div class="bill form large-9 medium-8 columns content">
    <?= $this->Form->create($bill) ?>
    <fieldset>
        <legend><?= __('Edit Bill') ?></legend>
        <?php
            echo $this->Form->control('billing_date', ['empty' => true]);
            echo $this->Form->control('billing_type_of_expense');
            echo $this->Form->control('billing_place_of_expense');
            echo $this->Form->control('billing_amount');
            echo $this->Form->control('billing_tax');
            echo $this->Form->control('billing_total_amount');
            echo $this->Form->control('billing_image');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
