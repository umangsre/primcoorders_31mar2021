<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * ContactsList Model
 *
 * @property &\Cake\ORM\Association\BelongsTo $CreditApplications
 *
 * @method \App\Model\Entity\ContactsList get($primaryKey, $options = [])
 * @method \App\Model\Entity\ContactsList newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\ContactsList[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\ContactsList|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\ContactsList saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\ContactsList patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\ContactsList[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\ContactsList findOrCreate($search, callable $callback = null, $options = [])
 */
class ContactsListTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('contacts_list');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('CreditApplications', [
            'foreignKey' => 'credit_application_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('statement_name')
            ->maxLength('statement_name', 150)
            ->requirePresence('statement_name', 'create')
            ->notEmptyString('statement_name');

        $validator
            ->scalar('statement_email')
            ->maxLength('statement_email', 150)
            ->requirePresence('statement_email', 'create')
            ->notEmptyString('statement_email');

        $validator
            ->scalar('invoice_name')
            ->maxLength('invoice_name', 150)
            ->requirePresence('invoice_name', 'create')
            ->notEmptyString('invoice_name');

        $validator
            ->scalar('invoice_email')
            ->maxLength('invoice_email', 150)
            ->requirePresence('invoice_email', 'create')
            ->notEmptyString('invoice_email');

        $validator
            ->scalar('price_pages_name')
            ->maxLength('price_pages_name', 150)
            ->requirePresence('price_pages_name', 'create')
            ->notEmptyString('price_pages_name');

        $validator
            ->scalar('price_pages_email')
            ->maxLength('price_pages_email', 150)
            ->requirePresence('price_pages_email', 'create')
            ->notEmptyString('price_pages_email');

        $validator
            ->scalar('order_confirmation_name')
            ->maxLength('order_confirmation_name', 150)
            ->requirePresence('order_confirmation_name', 'create')
            ->notEmptyString('order_confirmation_name');

        $validator
            ->scalar('order_confirmation_email')
            ->maxLength('order_confirmation_email', 150)
            ->requirePresence('order_confirmation_email', 'create')
            ->notEmptyString('order_confirmation_email');

        $validator
            ->scalar('bank_order_name')
            ->maxLength('bank_order_name', 150)
            ->requirePresence('bank_order_name', 'create')
            ->notEmptyString('bank_order_name');

        $validator
            ->scalar('bank_order_email')
            ->maxLength('bank_order_email', 150)
            ->requirePresence('bank_order_email', 'create')
            ->notEmptyString('bank_order_email');

        $validator
            ->scalar('purchasing_agents_name')
            ->maxLength('purchasing_agents_name', 150)
            ->requirePresence('purchasing_agents_name', 'create')
            ->notEmptyString('purchasing_agents_name');

        $validator
            ->scalar('purchasing_agents_email')
            ->maxLength('purchasing_agents_email', 150)
            ->requirePresence('purchasing_agents_email', 'create')
            ->notEmptyString('purchasing_agents_email');

        $validator
            ->scalar('_date')
            ->maxLength('_date', 150)
            ->allowEmptyString('_date');

        $validator
            ->scalar('authorized_signature')
            ->maxLength('authorized_signature', 150)
            ->allowEmptyString('authorized_signature');

        $validator
            ->scalar('print_name')
            ->maxLength('print_name', 150)
            ->allowEmptyString('print_name');

        $validator
            ->scalar('_title')
            ->maxLength('_title', 150)
            ->allowEmptyString('_title');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['credit_application_id'], 'CreditApplications'));

        return $rules;
    }
}
