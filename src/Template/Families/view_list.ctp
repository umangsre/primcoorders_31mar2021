<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Family[]|\Cake\Collection\CollectionInterface $families
 */
use Cake\Routing\Router;
$link  =Router::url([ 'action' => 'findproductbyname']);
$product_link =  Router::url([ 'action' => 'marginCalculator']);


?>
<style>
    .input {
        width: 45% !IMPORTANT;
    }
    input.checkbox_checked {
        opacity: 1 !important;
        margin-left: -10px;
        margin-top: -5px;
        pointer-events: auto !important;
    }
</style>
<div class="orders index large-9 medium-8 columns content card card card-default scrollspy">
    <div class="card-content">
        <?= $this->Flash->render() ?>
        <div class="families index large-9 medium-8 columns content">
            <?= $this->Form->create('search', ['type' => 'get']) ?>
            <fieldset>
                <div class="input text"><input type="text" name="search" id="search" value="<?=@$_GET['search']?>" placeholder="Search"></div>

                <div class="input select">
                    <select name="default">
                        <option value="1" <?php if(@$_GET['default'] == 1){ echo 'Selected'; } ?>>Default</option>
                        <option value="2" <?php if(@$_GET['default'] == 2){ echo 'Selected'; } ?>>Dollar</option>
                    </select>
                </div>
                <div class="input text"><input type="text" placeholder="Enter doller price" name="dollar" id="dollar" value="<?=@$_GET['dollar']?>"></div>

                <div class="input select">
                    <select name="margin">
                        <option value="1" <?php if(@$_GET['margin'] == 1){ echo 'Selected'; } ?>>Current Dollar Rate Current Margin and Current Selling Price</option>
                        <option value="2" <?php if(@$_GET['margin'] == 2){ echo 'Selected'; } ?>>New Dollar Rate, Old Selling Price & New Margin</option>
                        <option value="3" <?php if(@$_GET['margin'] == 3){ echo 'Selected'; } ?>>New Dollar Rate & New Selling Price and Old Margin</option>
                    </select>
                </div>

                <?php

                echo $this->Form->button(__('Search'));

                ?>
                <a class="waves-effect waves-light btn modal-trigger accept_id" href="/families/new-update?margin=<?=@$_GET['margin']?>&default=<?=@$_GET['default']?>&search=<?=@$_GET['search']?>&dollar=<?=@$_GET['dollar']?>&familyid=">Accept </a>
            </fieldset>

            <?= $this->Form->end() ?>

            <!-- Modal Trigger -->
            <div class="import export right-align">
                <!-- <a class="waves-effect waves-light btn modal-trigger" href="#modal1">Import Data</a>-->
                <?= $this->Html->link(__('Excel'), ['action' => 'view-list', '_ext'=>'xlsx','?'  => array('search' => @$_GET['search'],'default' => @$_GET['default'],'dollar' => @$_GET['dollar'],'margin' => @$_GET['margin'])],array(
                    'id' => 'myId',
                    'class' => 'waves-effect waves-light btn'
                )); ?>
            </div>

            <!-- Modal Structure -->
            <div id="modal1" class="modal">
                <div class="modal-content">
                    <h4>Import Data</h4>
                    <div class="content">


                        <div class="upload-frm">
                            <?php echo $this->Form->create($uploadData, ['type' => 'file']); ?>
                            <?php echo $this->Form->input('file', ['type' => 'file', 'class' => 'form-control']); ?>
                            <?php echo $this->Form->button(__('Upload File'), ['type'=>'submit', 'class' => 'form-controlbtn btn-default']); ?>
                            <?php echo $this->Form->end(); ?>


                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <a href="#!" class="modal-close waves-effect waves-green btn-flat">close</a>
                </div>
            </div>


            <table class="responsive-table">
                <thead>
                <tr>
                    <th></th>
                    <th scope="col"><?= $this->Paginator->sort('family') ?></th>
                    <th scope="col"><?= $this->Paginator->sort('name') ?></th>
                    <th scope="col"><?= $this->Paginator->sort('mill') ?></th>
                    <th scope="col" title="<?= __('Product Code')   ?>"><?= $this->Paginator->sort('product_code','Code') ?></th>
                    <th scope="col"><?= $this->Paginator->sort('sell_price_level1') ?></th>
                    <th scope="col"><?= $this->Paginator->sort('rebate_level1') ?></th>
                    <th scope="col"><?= $this->Paginator->sort('gross_margin_level1') ?></th>
                    <th scope="col"><?= $this->Paginator->sort('sell_price_level2') ?></th>
                    <th scope="col"><?= $this->Paginator->sort('rebate_level2') ?></th>
                    <th scope="col"><?= $this->Paginator->sort('gross_margin_level2') ?></th>
                    <th scope="col"><?= $this->Paginator->sort('sell_price_level3') ?></th>
                    <th scope="col"><?= $this->Paginator->sort('rebate_level3') ?></th>
                    <th scope="col"><?= $this->Paginator->sort('gross_margin_level3') ?></th>
                    <th scope="col" class="actions"><?= __('Actions') ?></th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($families as $family): ?>
                    <tr>
                        <td>

                            <input type="checkbox" class="checkbox_checked" data-value="<?=($family->id)?>" />


                        </td>
                        <!-- <td><?= $this->Number->format($family->id) ?></td>-->
                        <td><?= h($family->family) ?></td>
                        <td><?= h($family->name) ?></td>
                        <td><?= h($family->mill) ?></td>
                        <td><?= h($family->product_code) ?></td>
                        <!--  <td><?= h($family->role_cut) ?></td> -->
                        <!--  <td><?= $this->Number->format($family->mill_cost) ?></td>
                        <td><?php
                        $usd = (h($family->usd_cad));
                        $healthy = ["cad","usd_mannington", "usd_general", "usd_others","usd_custom_1", "usd_custom_2", "usd_custom_3","usd_custom_4"];
                        $yummy   = ["CAD","USD-M", "USD-G", "USD-O", "USD-C-1","USD-C-2", "USD-C-3","USD-C-4"];
                        $newPhrase = str_replace($healthy, $yummy, $usd);
                        echo strtoupper($newPhrase);
                        //echo str_replace("_","-",$usd);
                        ?></td>-->
                        <!--<td><?= $this->Number->format($family->conversion_rate) ?></td>-->
                        <!--<td><?= $this->Number->format($family->new_mill_cost) ?></td>
                        <td><?= ($this->Number->format($family->brokerage_multiplier) * 100)."%"  ?></td>
                        <td><?= $this->Number->format($family->brokerage) ?></td>
                        <td><?= h($family->units) ?></td>
                        <td><?= $this->Number->format($family->freight) ?></td>
                         <td><?= $this->Number->format($family->landed_cost_per_unit) ?></td>
                        <td><?= ($this->Number->format($family->load_percentage) * 100)."%" ?></td>
                        <td><?= $this->Number->format($family->margin_lic) ?></td>-->



                        <?php if(@$_GET['margin'] == 1 || empty($_GET['margin'])){ ?>
                            <td><?= $this->Number->format($family->sell_price_level1) ?></td>
                            <td><?= $this->Number->format($family->rebate_level1) ?></td>
                            <td><?= $this->Number->format($family->gross_margin_level1) ?></td>
                            <td><?= $this->Number->format($family->sell_price_level2) ?></td>
                            <td><?= $this->Number->format($family->rebate_level2) ?></td>
                            <td><?= $this->Number->format($family->gross_margin_level2) ?></td>
                            <td><?= $this->Number->format($family->sell_price_level3) ?></td>
                            <td><?= $this->Number->format($family->rebate_level3) ?></td>
                            <td><?= $this->Number->format($family->gross_margin_level3) ?></td>

                        <?php }else if(@$_GET['margin'] == 2){
                            $dollarprice = $family->mill_cost;
                            if(!empty(@$_GET['default']) && @$_GET['default'] == 2){
                                if(!empty(@$_GET['dollar'])){
                                    $dollarprice = $_GET['dollar'];
                                }
                            }

                            $update_marginlic = $family->mill_cost / $dollarprice;
                            $update_brokragepersantagebrokragepersantage = $family->brokerage_multiplier;
                            $update_brokrage = $update_brokragepersantagebrokragepersantage * $update_marginlic;
                            $update_freight = $family->freight;
                            $updtae_ad_fund = $update_marginlic * $family->ad_fund_percentage;
                            $update_landed_cost_per_unit = ($update_marginlic) + ($update_brokrage) + ($update_freight) + ($updtae_ad_fund);
                            $update_load  =  $update_landed_cost_per_unit * ($family->load_percentage);
                            $update_margin_lic = $update_landed_cost_per_unit +  $update_load;
                            ?>

                            <td><?= $this->Number->format($family->sell_price_level1) ?></td>
                            <td><?= $this->Number->format($family->rebate_level1) ?></td>
                            <td><?= number_format(( (( (($family->sell_price_level1) - ($update_margin_lic) ) + ($family->rebate_level1) )/($family->sell_price_level1))*100),2) ?></td>
                            <td><?= $this->Number->format($family->sell_price_level2) ?></td>
                            <td><?= $this->Number->format($family->rebate_level2) ?></td>
                            <td><?= number_format(( (( (($family->sell_price_level2) - ($update_margin_lic) ) + ($family->rebate_level2) )/($family->sell_price_level2))*100),2) ?></td>
                            <td><?= $this->Number->format($family->sell_price_level3) ?></td>
                            <td><?= $this->Number->format($family->rebate_level3) ?></td>
                            <td><?= number_format(( (( (($family->sell_price_level3) - ($update_margin_lic) ) + ($family->rebate_level3) )/($family->sell_price_level3))*100),2) ?></td>



                        <?php }else if(@$_GET['margin'] == 3){

                            $dollarprice = $family->mill_cost;
                            if(!empty(@$_GET['default']) && @$_GET['default'] == 2){
                                if(!empty(@$_GET['dollar'])){
                                    $dollarprice = $_GET['dollar'];
                                }
                            }

                            $update_marginlic = $family->mill_cost / $dollarprice;
                            $update_brokragepersantagebrokragepersantage = $family->brokerage_multiplier;
                            $update_brokrage = $update_brokragepersantagebrokragepersantage * $update_marginlic;
                            $update_freight = $family->freight;
                            $updtae_ad_fund = $update_marginlic * $family->ad_fund_percentage;
                            $update_landed_cost_per_unit = ($update_marginlic) + ($update_brokrage) + ($update_freight) + ($updtae_ad_fund);
                            $update_load  =  $update_landed_cost_per_unit * ($family->load_percentage);
                            $update_margin_lic = $update_landed_cost_per_unit +  $update_load;
                            //echo "MA".$update_margin_lic;
                            ?>

                            <td><?= number_format( (($update_margin_lic)/ (1-(($family->gross_margin_level1)/100))),2) ?></td>
                            <td><?= $this->Number->format($family->rebate_level1) ?></td>
                            <td><?= $this->Number->format($family->gross_margin_level1) ?></td>
                            <td><?= number_format((($update_margin_lic)/(1-(($family->gross_margin_level2)/100))),2) ?></td>
                            <td><?= $this->Number->format($family->rebate_level2) ?></td>
                            <td><?=$this->Number->format($family->gross_margin_level2) ?></td>
                            <td><?= number_format((($update_margin_lic)/(1-(($family->gross_margin_level3)/100))),2) ?></td>
                            <td><?= $this->Number->format($family->rebate_level3) ?></td>
                            <td><?= $this->Number->format($family->gross_margin_level3) ?></td>

                        <?php } ?>







                        <!--<td><?= h($family->mill) ?></td>
                        <td><?= h($family->vendorID) ?></td>
                        <td><?= h($family->created) ?></td> -->
                        <td class="actions">
                            <?= $this->Html->link('<i class="material-icons dp48">view_list</i>', ['action' => 'view', $family->id],['escape' => false,'title' => __('View')]) ?>

                            <?= $this->Html->link('<i class="material-icons dp48">edit</i>', ['action' => 'edit', $family->id],['escape' => false,'title' => __('Edit')]) ?>
                            <?= $this->Form->postLink('<i class="material-icons dp48">delete</i>', ['action' => 'delete', $family->id], ['escape' => false,'title' => __('Delete'),'confirm' => __('Are you sure you want to delete # {0}?', $family->id)]) ?>

                            <?= $this->Html->link('<i class="material-icons dp48">queue_play_next</i>', ['action' => 'view-calculator', $family->id],['escape' => false,'title' => __('View'),'target' => __('_blank')]) ?>

                            <!--  <a class="pricing-modal-trigger" href="javascript:void(0);" data-id="<?=$family->id ?>" data-link="<?=$product_link ?>" ><i class="material-icons" title="margin Calculator">queue_play_next</i></a>-->
                        </td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
            <div class="paginator">
                <ul class="pagination">
                    <?= $this->Paginator->first('<< ' . __('first')) ?>
                    <?= $this->Paginator->prev('< ' . __('previous')) ?>
                    <?= $this->Paginator->numbers() ?>
                    <?= $this->Paginator->next(__('next') . ' >') ?>
                    <?= $this->Paginator->last(__('last') . ' >>') ?>
                </ul>
                <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
            </div>
        </div>
    </div>
</div>
<!-- Modal Structure -->
<div id="modal1" class="modal modal-m" style="height: 800px;">
    <div class="modal-content" style="height: 90%;    overflow: auto;">
        <h3>Margin Calculator</h3>
        <div class="container mt-2">
            <div class="fgdf">
                <div class="gfg">
                    <div class="row">
                        <!--   <h2><?= __('Margin Calculator') ?></h2> -->
                        <div class="tab w-100">


                            <div class="col m12">
                                <div class='row mt-2'>
                                    <div class="col m3 s12 position-relative">
                                        <?php
                                        echo $this->Form->control('search_name', ['class' =>'search_name' , 'label'=>false,
                                            'placeholder' =>'Search Product Name or Product Code or Product Families','data-link' =>$link]);
                                        ?>
                                        <div id="suggesstion-pricing" class="list_wraper" style="display:none;">
                                        </div>
                                    </div>
                                    <div class="col m3 s12 position-relative">
                                        <?php
                                        echo $this->Form->control('defaut_dropdown', ['class' =>'defaut_dropdown' ,'id' =>'defaut_dropdown' , 'label'=>false,
                                            'options' =>[
                                                'defaut' =>'Defaut',
                                                'manually' =>'Dollar'
                                            ]
                                        ]);

                                        ?>
                                    </div>
                                    <div class="col m2 s12 position-relative">
                                        <?php
                                        echo $this->Form->control('defaut_value', ['class' =>'defaut_value float-number' ,'id' =>'defaut_value' , 'label'=>false, 'readonly' =>true, 'onkeyup'=>'getdefaut()']);
                                        ?>
                                    </div>

                                    <div class="col m4 s12">
                                        <?= $this->Form->button(__('Reset'), array( 'class' => 'btn btn-success pricing_reset', 'type' => 'button' )  ) ?>
                                    </div>
                                </div>
                            </div>
                            <div class="col m4">
                                <div class="border-heading">
                                    <h3>Current Dollar Rate Current Margin and Current Selling Price</h3>
                                    <table class="table table-bordered">
                                        <thead>
                                        <tr>
                                            <th>Slab Type</th>
                                            <th>Sell Price</th>
                                            <th>Rebate</th>
                                            <th>Gross margin(In Percentage)</th>


                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr class="product_row">

                                            <td class="price-ls" id="slab_sm_1"> Slab 1</td>
                                            <td class="price-ls" id="sell_sm_1"> </td>
                                            <td class="price-ls" id="rebate_sm_1"></td>
                                            <td class="price-ls" id="gross_sm_1"></td>

                                        </tr>

                                        <tr class="product_row">

                                            <td class="price-ls" id="slab_sm_2">Slab 2 </td>
                                            <td class="price-ls" id="sell_sm_2">  </td>
                                            <td class="price-ls" id="rebate_sm_2"></td>
                                            <td class="price-ls" id="gross_sm_2"></td>

                                        </tr>
                                        <tr class="product_row">

                                            <td class="price-ls" id="slab_sm_3"> Slab 3</td>
                                            <td class="price-ls" id="sell_sm_3"></td>
                                            <td class="price-ls" id="rebate_sm_3"></td>
                                            <td class="price-ls" id="gross_sm_3"></td>

                                        </tr>


                                        </tbody>
                                    </table>
                                </div>
                            </div>

                            <div class="col m4">
                                <div class="border-heading">
                                    <h3>New Dollar Rate, Old Selling Price & New Margin</h3>
                                    <?= $this->Form->create($family) ?>

                                    <table  class="table table-bordered">
                                        <thead>
                                        <tr>
                                            <!--th>Sr.No.</th-->
                                            <th>Slab Type</th>
                                            <th>Sell Price</th>
                                            <th>Rebate</th>
                                            <th>Gross margin(In Percentage)</th>
                                            <th></th>

                                        </tr>
                                        </thead>
                                        <tbody id="pricing-matrix">
                                        <tr class="product_row">

                                            <td class="price-ls"><?php  echo $this->Form->control('slevel',['value' =>'Slab 1','id' =>'slab_1','label' => false, 'readonly' => true]); ?></td>
                                            <td class="price-ls"> <?php echo $this->Form->control('sell_price_level1',['label' => false,'readonly' => true,'class' =>'sell_price']); ?>
                                            </td>
                                            <td class="price-ls"><?php echo $this->Form->control('rebate_level1',['class' =>'rebate','label' => false,'readonly' => true]); ?></td>
                                            <td class="price-ls"><?php  echo $this->Form->control('gross_margin_level1',['class' =>'gross_margin','label' => false,'readonly' => true]); ?></td>
                                            <td> </td>
                                        </tr>

                                        <tr class="product_row">

                                            <td class="price-ls"><?php  echo $this->Form->control('slevel',['value' =>'Slab 2','id' =>'slab_2','label' => false,'readonly' => true]); ?></td>
                                            <td class="price-ls"> <?php echo $this->Form->control('sell_price_level2',['label' => false,'readonly' => true,'class' =>'sell_price']); ?>
                                            </td>
                                            <td class="price-ls"><?php echo $this->Form->control('rebate_level2',['class' =>'rebate','label' => false,'readonly' => true]); ?></td>
                                            <td class="price-ls"><?php  echo $this->Form->control('gross_margin_level2',['class' =>'gross_margin','label' => false,'readonly' => true]); ?></td>
                                            <td> </td>
                                        </tr>
                                        <tr class="product_row">

                                            <td class="price-ls"><?php  echo $this->Form->control('slevel',['value' =>'Slab 3','id' =>'slab_3','label' => false,'readonly' => true]); ?></td>
                                            <td class="price-ls"> <?php echo $this->Form->control('sell_price_level3',['label' => false,'class' =>'sell_price']); ?>
                                            </td>
                                            <td class="price-ls"><?php echo $this->Form->control('rebate_level3',['class' =>'rebate','label' => false,'readonly' => true]); ?></td>
                                            <td class="price-ls"><?php  echo $this->Form->control('gross_margin_level3',['class' =>'gross_margin','label' => false,'readonly' => true]); ?>

                                            </td>
                                            <td>
                                            </td>
                                        </tr>

                                        <tr class="product_row">

                                            <td class="price-ls"><?php  echo $this->Form->control('slevel',['class' =>'slab_select','label' => false, 'value' =>'Slab 4','readonly' => true ]);
                                                ?>

                                            </td>
                                            <td class="price-ls">
                                                <?php echo $this->Form->control('sell_price',[ 'type' =>'number', 'min' =>'0', 'step' =>'0.01','class' =>'sell_price', 'label' => false]); ?>
                                            </td>
                                            <td class="price-ls">
                                                <?php echo $this->Form->control('rebate',['class' =>'rebate','label' => false,'value' =>'0']); ?>

                                            </td>
                                            <td class="price-ls"><?php  echo $this->Form->control('gross_margin',['class' =>'gross_margin','label' => false]); ?></td>
                                            <td></td>
                                        </tr>
                                        </tbody>
                                    </table>


                                    <?= $this->Form->end() ?>

                                </div>
                            </div>
                            <div class="col m4">
                                <div class="border-heading">
                                    <h3>New Dollar Rate &  New Selling Price and Old Margin</h3>

                                    <table  class="table table-bordered">
                                        <thead>
                                        <tr>
                                            <!--th>Sr.No.</th-->
                                            <th>Slab Type</th>
                                            <th>Sell Price</th>
                                            <th>Rebate</th>
                                            <th>Gross margin(In Percentage)</th>
                                            <th></th>

                                        </tr>
                                        </thead>
                                        <tbody id="pricing-matrix-new">
                                        <tr class="new_product_row">

                                            <td class="price-ls"><?php  echo $this->Form->control('slevel',['value' =>'Slab 1','id' =>'slab_ms_1','label' => false, 'readonly' => true]); ?></td>
                                            <td class="price-ls"> <?php echo $this->Form->control('sell_price_level1',['label' => false,'readonly' => true,'class' =>' new_sell_price','id' =>'sell_ms_1']); ?>
                                            </td>
                                            <td class="price-ls"><?php echo $this->Form->control('rebate_level1',['class' =>' new_rebate','label' => false,'readonly' => true,'id' =>'rebate_ms_1']); ?></td>
                                            <td class="price-ls"><?php  echo $this->Form->control('gross_margin_level1',['class' =>' new_gross_margin','label' => false,'readonly' => true,'id' =>'gross_ms_1']); ?></td>
                                            <td> </td>
                                        </tr>

                                        <tr class="new_product_row">

                                            <td class="price-ls"><?php  echo $this->Form->control('slevel',['value' =>'Slab 2','id' =>'slab_2','label' => false,'readonly' => true]); ?></td>
                                            <td class="price-ls"> <?php echo $this->Form->control('sell_price_level2',['label' => false,'readonly' => true,'class' =>' new_sell_price','id' =>'sell_ms_2']); ?>
                                            </td>
                                            <td class="price-ls"><?php echo $this->Form->control('rebate_level2',['class' =>' new_rebate','label' => false,'readonly' => true,'id' =>'rebate_ms_2']); ?></td>
                                            <td class="price-ls"><?php  echo $this->Form->control('gross_margin_level2',['class' =>' new_gross_margin','label' => false,'readonly' => true,'id' =>'gross_ms_2']); ?></td>
                                            <td> </td>
                                        </tr>
                                        <tr class="new_product_row">

                                            <td class="price-ls"><?php  echo $this->Form->control('slevel',['value' =>'Slab 3','id' =>'slab_3','label' => false,'readonly' => true]); ?></td>
                                            <td class="price-ls"> <?php echo $this->Form->control('sell_price_level3',['label' => false,'class' =>' new_sell_price','readonly' => true,'id' =>'sell_ms_3']); ?>
                                            </td>
                                            <td class="price-ls"><?php echo $this->Form->control('rebate_level3',['class' =>' new_rebate','label' => false,'readonly' => true,'id' =>'rebate_ms_3']); ?></td>
                                            <td class="price-ls"><?php  echo $this->Form->control('gross_margin_level3',['class' =>' new_gross_margin','label' => false,'readonly' => true,'id' =>'gross_ms_3']); ?>

                                            </td>
                                            <td>
                                            </td>
                                        </tr>


                                        </tbody>
                                    </table>




                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <?= $this->Form->button(__('+'), array( 'class' => 'btn btn-info pricing_clone_add', 'type' => 'button' )  ) ?>
        <a href="#!" class="modal-close waves-effect waves-light pink accent-2 white-text btn">Close</a>
    </div>
</div>
<script>
    setInterval(function(){ $(".success").hide(); }, 10000);

</script>
