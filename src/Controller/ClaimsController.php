<?php

namespace App\Controller;

use App\Controller\AppController;
use Cake\Event\Event;
use Cake\I18n\Time;
use Cake\Mailer\Email;

/**
 * Claims Controller
 *
 * @property \App\Model\Table\ClaimsTable $Claims
 *
 * @method \App\Model\Entity\Claim[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class ClaimsController extends AppController
{
    private $currentUser = '';

    public function initialize()
    {
        parent::initialize();
        $this->loadComponent('RequestHandler');
        $this->currentUser = $this->Auth->user();
        $this->viewBuilder()->setLayout('dashboard');
    }

    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);
        $this->Auth->allow(['add']);
    }

    public function pdfView($id = null ) {
        $claimMethods = new ClaimsGenericMethods();
        if ( $id == null ) {
            return $this->redirect( $this->referer() );
        }
        $claim_id = $id = base64_decode($id);
        $claim = $claimMethods->getClaim($claim_id);
        $categoryData = $claimMethods->getAllCategoryLabourCost();
        $generalData = $claimMethods->getAllGeneralLabourCost();
        if ( !empty( $claim )) {
            $this->set(compact('claim','categoryData','generalData'));
            $this->viewBuilder()->layout('ajax');
            $this->response->type('pdf');
        }
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $claimMethods = new ClaimsGenericMethods();
        $searchData = $this->request->getData();
        $claims = ($this->currentUser['role'] == 'admin' && $this->request->is('put')) ?
            $this->paginate($claimMethods->getAllClaims(true, $searchData)) :
            $this->paginate($claimMethods->getAllClaims());
        ($this->currentUser['role'] == 'admin' && $this->request->is('put')) &&
        $this->set(compact('searchData'));
        $claimsArray = [];
        $currentUser = $this->currentUser;
        foreach ($claims as $claim) {
            if ($claimMethods->isUserAllowedToViewClaim($claim, $currentUser)) {
                if ($currentUser['role'] === 'dealer') {
                    $claimStatuses = $claim->claim_status;
                    if (count($claimStatuses) > 0) {
                        $lastStatus = $claimStatuses[count($claimStatuses) - 1];
                        if (strpos($lastStatus->status, 'miller')) {
                            $lastStatus->status = 'Pending';
                        }
                    }
                } else {
                    $claimStatuses = $claim->claim_status;
                    if (count($claimStatuses) > 0) {
                        $lastStatus = $claimStatuses[count($claimStatuses) - 1];
                        if (strpos($lastStatus->status, 'miller')) {
                            $lastStatus->status = str_replace('miller', 'Mill', $lastStatus->status);
                        }
                    }
                }
                $claimsArray[] = $claim;
            }
        }
        $claims = $claimsArray;
        $this->set(compact('claims'));
        $this->set(compact('currentUser'));
    }

    /**
     * View method
     *
     * @param string|null $id Claim id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $claimMethods = new ClaimsGenericMethods();
        $millerMethods = new MillerGenericMethods();
        $claim = $claimMethods->getClaim($id);
        //debug($claim);exit;
        $currentUserInfo = $this->currentUser;
        if ($claimMethods->isUserAllowedToViewClaim($claim, $currentUserInfo)) {
            $millers = [];
            if ($currentUserInfo['role'] === 'admin') {
                $millers = $millerMethods->getAllMillers()->all();
            }
            $claim['role'] = $currentUserInfo['role'];
            $categoryData = $claimMethods->getAllCategoryLabourCost()->all();
            $generalData = $claimMethods->getAllGeneralLabourCost()->all();
            $this->set('claim', $claim);
            $this->set(compact('categoryData'));
            $this->set(compact('generalData'));
            $this->set(compact('millers'));
            if ($this->request->is('put')) {
                $this->modifyClaim($claim, $this->request->getData());
            }
        } else {
            return $this->redirect(['action' => 'index']);
        }
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $claimMethods = new ClaimsGenericMethods();
        $dealerMethods = new DealerGenericMethods();
        $consumerMethods = new ConsumerGenericMethods();
        $currentUserInfo = $this->currentUser;
        $category = json_encode($claimMethods->getAllCategoryLabourCost()->all());
        $this->set(compact('category'));
        $general = json_encode($claimMethods->getAllGeneralLabourCost()->all());
        $this->set(compact('general'));
        $claim = $this->Claims->newEntity();
        $this->set(compact('claim'));
        $dealerDetails = $currentUserInfo['role'] == 'dealer' ? $dealerMethods->getDealerDetails($currentUserInfo['id']) : [];
        $this->set(compact('currentUserInfo'));
        $this->set(compact('dealerDetails'));
        if ($this->request->is('post')) {
            $data = $this->request->getData();
            $dealerData = $data;
            if (empty($data['customer_id']) && array_key_exists('customer_name', $data) && !empty($data['customer_name'])) {
                if (empty($dealerData['customer_email'])) {
                    $this->Flash->error(__('Please enter dealer information.'));
                    return;
                }
                $password = $dealerMethods->createUniquePassword();
                $addDealerToUsers = $dealerMethods->addDealerToUsers($dealerData, $password);
                $data['customerid'] = $addDealerToUsers['id'];
                $addDealer = $dealerMethods->addDealer($dealerData, $addDealerToUsers['id']);
                $data['customer_id'] = $addDealer['id'];
                $this->Flash->success(__('The dealer added successfully with password ' . $password));
            } else {
                $updateDealer = $dealerMethods->updateDealer($data['customer_id'], $data);
            }
            $consumerId = null;
            if (!empty($data['consumer_name'])) {
                $consumerDetails = [
                    'consumer_name' => $data['consumer_name'],
                    'phone' => $data['consumer_phone'],
                    'address' => $data['consumer_address'],
                    'city' => $data['consumer_city'],
                    'postal_code' => $data['consumer_postal_code'],
                    'created' => new Time()
                ];
                $addConsumer = $consumerMethods->addConsumer($consumerDetails);
                $consumerId = $addConsumer['id'];
            }
            $data['consumer_id'] = $consumerId;
            if (array_key_exists('claims', $data)) {
                $claim['claim_json'] = json_encode($data['claims']);
            }
            if ($data['settlement_method'] == 'replacement') {
                $data['total_settlement_amount'] = $data['total_labour_cost_claimed'];
            }
            if ($data['settlement_method'] == 'return') {
                $data['total_settlement_amount'] = $data['return_quantity'];
            }
            $claim['created_by'] = $currentUserInfo['id'];
            $claim = $this->Claims->patchEntity($claim, $data);
            if ($this->Claims->save($claim)) {
                $claimStatusDetails = [
                    'claim_id' => $claim['id'],
                    'status_by' => $currentUserInfo['id'],
                    'status' => $data['clicked'] == 'save' ? 'saved' : 'send to primco',
                    'created_at' => new Time()
                ];
                $addStatus = $claimMethods->addClaimStatus($claimStatusDetails);
                $claimCreatedByRole = $currentUserInfo['role'];
                $mail = '';
                if ($claimCreatedByRole === 'sales' || 'consumer' || 'customer') {
                    // send to admin mail
                }
                $this->Flash->success(__('The claim has been saved.'));
                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The claim could not be saved. Please, try again.'));
        }
    }

    /**
     * Edit method
     *
     * @param $claim
     * @param $data
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     */
    public function modifyClaim($claim, $data)
    {  
        $claimMethods = new ClaimsGenericMethods();
        $millerMethods = new MillerGenericMethods();
        $updateClaimInfo = $data;
        $millerData = $data;
        if ($this->currentUser['role'] == 'admin') {
            if (empty($data['miller_id']) && array_key_exists('miller_name', $data) && !empty($data['miller_name'])) {
                if (empty($millerData['miller_email'])) {
                    $this->Flash->error(__('Please enter mill information.'));
                    return;
                }
                $password = $millerMethods->createUniquePassword();
                $addMillerToUsers = $millerMethods->addMillerToUsers($millerData, $password);
                $updateClaimInfo['miller_user_id'] = $addMillerToUsers['id'];
                $addMiller = $millerMethods->addMiller($millerData, $addMillerToUsers['id']);
                $updateClaimInfo['miller_id'] = $addMiller['id'];
                $this->Flash->success(__('The mill added successfully with password ' . $password));
            } else {
                $updateMiller = $millerMethods->updateMiller($millerData);
                $updateClaimInfo['miller_user_id'] = $millerData['miller_user_id'];
                $updateClaimInfo['miller_id'] = $millerData['miller_id'];
            }
            if (array_key_exists('primco_invoice_number', $data)) {
                $update['primco_invoice_number'] = $data['primco_invoice_number'];
            }
            if (array_key_exists('miller_invoice_number', $data)) {
                $updateClaimInfo['miller_invoice_number'] = $data['miller_invoice_number'];
            }
        }
        if (array_key_exists('claims', $data) && $this->currentUser['role'] == 'dealer') {
            $updateClaimInfo['claim_json'] = json_encode($updateClaimInfo['claims']);
        }
        $updateClaim = $claimMethods->updateClaim($claim, $updateClaimInfo);
        if ($updateClaim) {
            $this->Flash->success(__('The claim has been updated.'));
        }
        $claimStatusDetails = $data;
        $claimStatusDetails['status'] = $data['clicked'] == 'save' ? 'saved' : $data['status'];
        $status = $claimStatusDetails['status'];
        $claimStatusDetails['status_by'] = $this->currentUser['id'];
        $claimStatusDetails['created_at'] = new Time();
        $addClaimStatus = $claimMethods->addClaimStatus($claimStatusDetails);

        if ($addClaimStatus) {
            $statusCreatedByRole = $this->currentUser['role'];
            $mail = '';
            if ($statusCreatedByRole === 'admin') {
                if ($status === 'send to mill') {
                    $claimStatusCount = $claimMethods->checkStatusCount($claim->id, 'send to mill');
                    if ($claimStatusCount === 0) {
                        // send mail to miller for new claim
                        $claimStatus = $claim->claim_status[0];
                        $this->loadModel('Users');
                        if($claim->miller_user_id){
                            $user = $this->Users->find('all')->where(['id'=>$claim->miller_user_id])->select(['id','username','name'])->first();
                            $html = "Hello, ".$user->name."<br/><br/>Claim has been assign by Admin with Claim Id : ".$this->currentUser['id'];
                            $this->sendMailToClaimParticipant($user->username, $html);
                        }
                    } else {
                        // send mail to miller for new comment on claim
                        $claimStatus = $claim->claim_status[0];
                        $this->loadModel('Users');
                        if($claim->miller_user_id){
                            $user = $this->Users->find('all')->where(['id'=>$claim->miller_user_id])->select(['id','username','name'])->first();                            
                            $html = "Hello, ".$user->name."<br/><br/>Claim has been assign by Admin with Claim Id : ".$this->currentUser['id'];
                            $this->sendMailToClaimParticipant($user->username, $html);
                        }
                    }
                } else if($status == 'declined by primco') {
                    // send mail to claim created by id, customer, retailer
                    $claimStatus = $claim->claim_status[0];
                    $userId = $claimStatus['status_by'];
                    $this->loadModel('Users');
                    if($claim->customer->user_id){
                        $user = $this->Users->find('all')->where(['id'=>$claim->customer->user_id])->select(['id','username','name'])->first();
                        $html = "Hello, ".$user->name."<br/><br/>Claim has been rejected by Admin with Claim Id : ".$this->currentUser['id'];
                        $this->sendMailToClaimParticipant($user->username, $html);
                    }
                } else {
                    // send mail to claim created by id, customer, retailer
                }
                
            } else if ($statusCreatedByRole === 'miller') {
                // send mail to admin
                $claimStatus = $claim->claim_status[0];
                $userId = $claimStatus['status_by'];
                $this->loadModel('Users');
                $user = $this->Users->find('all')->where(['id'=>$userId])->select(['id','username','name'])->first();
                $html = "Hello, ".$user->name."<br/><br/>Claim status has been changed for claim ID : ".$claim->id.". Detail is as below <br/><br/> <table><tr><td>Claim Id</td><td>".$claim->id."</td></tr><tr><td>Miller Name</td><td>".$this->currentUser['name']."</td></tr><tr><td>Claim Status</td><td>".$status."</td></tr></table>";
                $this->sendMailToClaimParticipant($user->username, $html);
            } else if ($statusCreatedByRole === 'dealer') {
                // send mail to admin
            }
            $this->Flash->success(__('The claim has been ' . $status . '.'));
            return $this->redirect(['action' => 'index']);
        }
        $this->Flash->error(__('The claim could not be ' . $status . '. Please, try again.'));
    }

    public function sendMail($image_name, $link, $id)
    {
        $toEmail = [
            's.nesbitt@primco.ca',
            't.yelland@primco.ca',
            'd.grona@primco.ca',
            'm.malhotra@primco.ca',
        ];
        $email = new Email();
        $email->setTransport('default');
        $email->viewVars(['image_name' => $image_name, 'link' => $link]);
        $email->viewBuilder()->setTemplate('creditapplication');
        $email->setEmailFormat('html');
        $email->setFrom(['support@primco.ca' => 'Primco'])
            ->setTo($toEmail)
            ->setBcc('Juneja.ankit@gmail.com')
            ->setSubject('New Customer #' . $id)
            ->send();
        return true;
    }

    /**
     * Delete method
     *
     * @param string|null $id Claim id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $claim = $this->Claims->get($id);
        if ($this->Claims->delete($claim)) {
            $this->Flash->success(__('The claim has been deleted.'));
        } else {
            $this->Flash->error(__('The claim could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    public function sendEmails($claimId = 0){
        $this->set(compact('claimId'));
        if($this->getRequest()->is('post')){
            $requestData = $this->getRequest()->getData();
            $emailIds = ($requestData['email_ids']) ? $requestData['email_ids'] : '';
            
            if(!$emailIds){
                $this->Flash->error(__('Please enter email ids'));
                return $this->redirect($this->referer());
            }
            
            $emailIds = explode(",",$emailIds);
            //TODO: check for valid email
            
            $claimMethods = new ClaimsGenericMethods();
            $millerMethods = new MillerGenericMethods();
            $claim = $claimMethods->getClaim($claimId);
            $currentUserInfo = $this->currentUser;
            
            if ($claimMethods->isUserAllowedToViewClaim($claim, $currentUserInfo)) {
                $millers = [];
                if ($currentUserInfo['role'] === 'admin') {
                    $millers = $millerMethods->getAllMillers()->all();
                }
                $claim['role'] = $currentUserInfo['role'];
                $categoryData = $claimMethods->getAllCategoryLabourCost()->all();
                $generalData = $claimMethods->getAllGeneralLabourCost()->all();

                $claimDetils = json_decode($claim->claim_json, true);
               
                $categoryArr = array();
                foreach($categoryData as $key => $data){
                    $categoryArr[$data->id] = $data->category;
                }
               
                $invoiceArr = array();
                $invoiceHtml = '';
                foreach($claimDetils as $key => $claimDetil){
                    if(strpos($key, 'primco') === 0){
                        $invoiceArr['number'] = $claimDetil['Primco_Invoice_Number'];
                        $invoiceArr['date'] = $claimDetil['Primco_Invoice_Date'];
                        $invoiceHtml .= '<tr>
                                    <td>Primco Invoice Number : '.$claimDetil['Primco_Invoice_Number'].'</td>
                                    </tr> <br>
                                   
                                    <td>Primco Invoice Date : '.$claimDetil['Primco_Invoice_Date'].'</td>
                                </tr>';
                    }
                }   
            
                $claimDetils = json_decode($claim->claim_json, true);
                
                
                
                $html = '<table>
                            <tr>
                                <td colspan="2">Retailer / Dealer Information</td>
                            </tr>
                            <tr>
                                <td>Dealer Name</td>
                                <td>' . $claim->customer->name . '</td>
                            </tr>
                            <tr>
                                <td>Contact Name</td>
                                <td>' . $claim->customer->contact_name . '</td>
                            </tr>
                            <tr>
                                <td>Phone</td>
                                <td>' . $claim->customer->phone . '</td>
                            </tr>
                            <tr>
                                <td>Address</td>
                                <td>' . $claim->customer->address . '</td>
                            </tr>
                            <tr>
                                <td>Province</td>
                                <td>' . $claim->customer->province . '</td>
                            </tr>
                            <tr>
                                <td>City</td>
                                <td>' . $claim->customer->city . '</td>
                            </tr>
                            <tr>
                                <td>Postal Code</td>
                                <td>' . $claim->customer->postal_code . '</td>
                            </tr><br>
                            <tr>
                                <td colspan="2">End Consumer Information</td>
                            </tr>
                            <tr>
                                <td>Customer Name</td>
                                <td>' . $claim->consumer->consumer_name . '</td>
                            </tr>
                            <tr>
                                <td>Phone</td>
                                <td>' . $claim->consumer->phone . '</td>
                            </tr>
                            <tr>
                                <td>Address</td>
                                <td>' . $claim->consumer->address . '</td>
                            </tr>
                            <tr>
                                <td>City</td>
                                <td>' . $claim->consumer->city . '</td>
                            </tr>
                            <tr>
                                <td>Postal Code</td>
                                <td>' . $claim->consumer->postal_code . '</td>
                            </tr><br>
                            <tr>
                                <td colspan="2">Claim Information</td>
                            </tr>
                            <tr>'.$invoiceHtml.
                            '</tr>
                           
                            <tr>
                                <td>Category</td>
                                <td>'.$categoryArr[$claimDetils[0]['Category']].'</td>
                            </tr>
                            <tr>
                                <td>Product Number</td>
                                <td>'.$claimDetils[0]['Product_Number'].'</td>
                            </tr>
                            <tr>
                                <td>Product Description</td>
                                <td>'.$claimDetils[0]['Product_Description'].'</td>
                            </tr>
                            <tr>
                                <td>Quantity Purchased</td>
                                <td>'.$claimDetils[0]['Quantity_Purchased'].'</td>
                            </tr>
                            <tr>
                                <td>Quantity Purchased UOM</td>
                                <td>'.$claimDetils[0]['Quantity_Purchased_UOM'].'</td>
                            </tr>
                            <tr>
                                <td>Quantity Affected</td>
                                <td>'.$claimDetils[0]['Quantity_Affected'].'</td>
                            </tr>
                            <tr>
                                <td>Quantity Affected UOM</td>
                                <td>'.$claimDetils[0]['Quantity_Affected_UOM'].'</td>
                            </tr>
                            <tr>
                                <td>Installed</td>
                                <td>'.$claimDetils[0]['Installed'].'</td>
                            </tr>
                            <tr>
                                <td>Quantity Installed</td>
                                <td>'.$claimDetils[0]['Quantity_Installed'].'</td>
                            </tr>
                            <tr>
                                <td>Quantity Installed UOM</td>
                                <td>'.$claimDetils[0]['Quantity_Installed_UOM'].'</td>
                            </tr>
                            <tr>
                                <td>Where the product was installed?</td>
                                <td>'.$claimDetils[0]['Rooms_Installed'].'</td>
                            </tr>
                            <tr>
                                <td>Date Installed</td>
                                <td>'.$claimDetils[0]['Date_Installed'].'</td>
                            </tr>
                            <tr>
                                <td>Detailed Reason of Claim</td>
                                <td>'.$claimDetils[0]['Detailed_Claim_Reason'].'</td>
                            </tr>
                            <tr>
                                <td>Request for Resolution</td>
                                <td>'.$claimDetils[0]['Request_For_Resolution'].'</td>
                            </tr>
                            <tr>
                                <td>Primary Contact Name (in case of inspection)</td>
                                <td>'.$claimDetils[0]['Primary_Contact_Name'].'</td>
                            </tr>
                            <tr>
                                <td>Primary Contact Number (in case of inspection)</td>
                                <td>'.$claimDetils[0]['Primary_Contact_Number'].'</td>
                            </tr><br>
                            <tr>
                                <td colspan="2"></td>
                            </tr>
                            <tr>
                                <td>Settlement Method</td>
                                <td>' . $claim->settlement_method . '</td>
                            </tr>
                            <tr>
                                <td>Total Labour Cost Claimed</td>
                                <td>' . $claim->total_settlement_amount . '</td>
                            </tr><br><br>
                            <tr>
                                <td colspan="2">Additional Information Pertaining to Labour</td>
                            </tr>
                            <tr>
                                <td>No. of Furniture Moved (pcs)</td>
                                <td>' . $claimDetils['misc']['Furniture'] . '</td>
                            </tr>
                            <tr>
                                <td>No. of Appliances Moved (pcs)</td>
                                <td>'.$claimDetils['misc']['Appliances'].'</td>
                            </tr>
                            <tr>
                                <td>No. of Toilet Seats Moved (pcs)</td>
                                <td>'.$claimDetils['misc']['Toilets'].'</td>
                            </tr>
                            <tr>
                                <td>No. of Miscellaneous Items Moved (pcs)</td>
                                <td>'.$claimDetils['misc']['Misc_Items_Value'].'</td>
                            </tr>

                        </table>';

                       /* echo "<pre>";
                    print_r($claim);
                    print_r($claim->total_settlement_amount);
                    exit;*/
                    
                    $email = new Email();
                    $email->setTransport('default');
                    $email->setEmailFormat('html');
                    $email->setFrom(['support@primco.ca' => 'Primco'])
                        ->setTo($emailIds)
                        ->setBcc('Juneja.ankit@gmail.com')
                        ->setSubject('Claim Details')
                        ->send($html);
                        
                    /*if(($this->sendClaimEmail('Primco','support@primco.ca',$emailIds, $html))){
                        $this->Flash->success(__('Mail has been send successfully'));
                    } else {
                        $this->Flash->error(__('Unable to send email, Please try again'));
                    }*/
                    return $this->redirect($this->referer());
            } else {
                $this->Flash->error(__('You are not allowed to view this claim.'));
                return $this->redirect($this->referer());
            }
        }
    }

    public function sendClaimEmail($from_name, $from_address, $to_address, $description)
    {
        $to_address = implode(',', $to_address);
        echo "<pre>";
        
        $domain = 'exchangecore.com';

        //Create Email Headers
        $mime_boundary = "----Meeting Booking----".MD5(TIME());

        //$headers = "From: ".$from_name." <".$from_address.">. '\r\n' .
        //'BCC: somebodyelse@example.com'";

        /* $headers = "From: ".$from_address. "\r\n" .
        "CC: Juneja.ankit@gmail.com"; */

        $headers = 'From: <'.$from_address.'>' . "\r\n";
        $headers .= 'Bcc: Juneja.ankit@gmail.com' . "\r\n";

        $headers .= "Reply-To: ".$from_name." <".$from_address.">\n";
        $headers .= "MIME-Version: 1.0\n";
        $headers .= "Content-Type: multipart/alternative; boundary=\"$mime_boundary\"\n";
        $headers .= "Content-class: urn:content-classes:calendarmessage\n";

        //Create Email Body (HTML)
        $message = "--$mime_boundary\r\n";
        $message .= "Content-Type: text/html; charset=UTF-8\n";
        $message .= "Content-Transfer-Encoding: 8bit\n\n";
        $message .= "<html>\n";
        $message .= "<body>\n";
        //$message .= '<p>Dear '.$to_name.',</p>';
        $message .= '<p>'.$description.'</p>';
        $message .= "</body>\n";
        $message .= "</html>\n";
        $message .= "--$mime_boundary\r\n";

        $mailsent = mail($to_address, 'Claim Details', $message, $headers);
       
        return ($mailsent)?(true):(false);
    }

    public function sendMailToAdmin1($mailData)
    {
        $toEmail = [
            $mailData['admin']['email'],
            'claims@primco.ca'
        ];
        $html = "Hello, ".$mailData['sender']['name']."<br/><br/>Claim status has been changed for claim ID : ".$mailData['claim']['id'].". Detail is as below <br/><br/> <table><tr><td>Claim Id</td><td>".$mailData['claim']['id']."</td></tr><tr><td>Miller Name</td><td>".$mailData['miller']['name']."</td></tr><tr><td>Claim Status</td><td>".$mailData['claim']['status']."</td></tr></table>";
        try{
            $email = new Email();
            $email->setTransport('default');
            $email->setEmailFormat('html');
            $email->setFrom(['support@primco.ca' => 'Primco'])
                ->setTo($toEmail)
                ->setSubject('Claim Status Changes')
                ->send($html);
        } catch(Exception $e){
            //echo "<pre>";
            //print_r($e);
            return false;
        }
        return true;
    }

    public function sendMailToClaimParticipant($emailId, $html)
    {
        $toEmail = [
            $emailId,
            'claims@primco.ca'
        ];
        //$html = "Hello, ".$mailData['sender']['name']."<br/><br/>Claim status has been changed for claim ID : ".$mailData['claim']['id'].". Detail is as below <br/><br/> <table><tr><td>Claim Id</td><td>".$mailData['claim']['id']."</td></tr><tr><td>Miller Name</td><td>".$mailData['miller']['name']."</td></tr><tr><td>Claim Status</td><td>".$mailData['claim']['status']."</td></tr></table>";
        try{
            $email = new Email();
            $email->setTransport('default');
            $email->setEmailFormat('html');
            $email->setFrom(['support@primco.ca' => 'Primco'])
                ->setTo($toEmail)
                ->setSubject('Claim Status Changes')
                ->send($html);
        } catch(Exception $e){
            //echo "<pre>";
            //print_r($e);
            return false;
        }
        return true;
    }
}
