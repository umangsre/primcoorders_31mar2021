$(document).ready(function () {
    //console.log('Yes');
    getproduct($("#family_id").val());

    var product = {};
    var dealer = {};
    var salesman = {};
    var selected_product = {};
    var selected_products = [];
    var products = [];
    var dealers = [];
    var salesmen = [];
    var pricing = {};
    var conversion_rate = {};
    localStorage.clear();


    /* get all customers */
    if (localStorage.getItem("dealers") === null) {

        $.ajax({
            type: 'get',
            url: all_customers_ajax_url,
            headers: {
                'X-CSRF-Token': csrfCustomerToken
            },
            success: function (response) {
                if (response) {
                    dealers = response;
                    localStorage.setItem('dealers', JSON.stringify(dealers));
                }
            },
            error: function (e) {
                alert("An error occurred: " + e.responseText.message);
                console.log(e);
            }
        });
    } else {
        dealers = JSON.parse(localStorage.getItem('dealers'));
    }

    /* get all salesmen */
    if (localStorage.getItem("salesmen") === null) {
        $.ajax({
            type: 'get',
            url: all_salesmen_ajax_url,
            headers: {
                'X-CSRF-Token': csrfCustomerToken
            },
            success: function (response) {
                if (response) {
                    salesmen = response;
                    localStorage.setItem('salesmen', JSON.stringify(salesmen));
                    //console.log('salesmen',salesmen);
                }
            },
            error: function (e) {
                alert("An error occurred: " + e.responseText.message);
                console.log(e);
            }
        });
    } else {
        salesmen = JSON.parse(localStorage.getItem('salesmen'));
    }

    /* get all products */
    if (localStorage.getItem("products") === null) {
        $.ajax({
            type: 'get',
            url: all_products_ajax_url,
            headers: {
                'X-CSRF-Token': csrfCustomerToken
            },
            success: function (response) {
                if (response) {
                    products = response;
                    localStorage.setItem('products', JSON.stringify(products));
                    //console.log('products',products);
                }
            },
            error: function (e) {
                alert("An error occurred: " + e.responseText.message);
                console.log(e);
            }
        });
    } else {
        products = JSON.parse(localStorage.getItem('products'));
    }

    /* get all products */
    if (localStorage.getItem("products") === null) {
        $.ajax({
            type: 'get',
            url: all_products_ajax_url,
            headers: {
                'X-CSRF-Token': csrfCustomerToken
            },
            success: function (response) {
                if (response) {
                    products = response;
                    localStorage.setItem('products', JSON.stringify(products));
                    //console.log('products',products);
                }
            },
            error: function (e) {
                alert("An error occurred: " + e.responseText.message);
                console.log(e);
            }
        });
    } else {
        products = JSON.parse(localStorage.getItem('products'));
    }


    /* get conversion_ratio */
    if (localStorage.getItem("conversion_ratio") === null) {

        $.ajax({
            type: 'get',
            url: all_conversion_rate_ajax_url,
            headers: {
                'X-CSRF-Token': csrfCustomerToken
            },
            success: function (response) {
                if (response) {
                    conversion_rate = response;
                    console.log('conversion_rate ', response);
                    localStorage.setItem('conversion_ratio', JSON.stringify(conversion_rate));
                }
            },
            error: function (e) {
                alert("An error occurred: " + e.responseText.message);
                console.log(e);
            }
        });
    } else {
        conversion_rate = JSON.parse(localStorage.getItem('conversion_ratio'));
    }

    $(document).on('keyup ', '#customer_name', function (e) {
        var name = $(this).val();
        console.log('name', name);
        if (name.length > 1) {
            var response = dealers.filter(function (item) {
                if (!empty(item.customerid)) {
                    if (item.name.toLowerCase().indexOf(name.toLowerCase()) > -1 || item.customerid.toString().indexOf(name) > -1) {
                        return 1;
                    }
                } else {
                    if (item.name.toLowerCase().indexOf(name.toLowerCase()) > -1) {
                        return 1;
                    }
                }
            });

            //var response =filteredValue;
            window.myres = response;
            if (response.length > 0) {

                $('#suggesstion-dealers').html('');
                var _ul = $('<ul />')
                    .attr('id', 'dealer-list');
                $.each(response, function (i, item) {
                    //console.log('item',item);
                    var _li = $('<li />')
                        .addClass('item_dealer')
                        .attr('data-customer_id', item.id)
                        .html('<strong>' + item.customerid + ': </strong>' + item.name)
                        .appendTo(_ul);
                });
                _ul.appendTo('#suggesstion-dealers');
                $('#suggesstion-dealers').show();
            }
        } else {
            //$('#customer_name').val('');
            $('#suggesstion-dealers').hide();
            $('#address').val('');
            $('#city').val('');
            $('#province').val('');
            $('#customer_id').val('');
            $('#postal_code').val('');
            $('#sin').val('');
            $('#sin_off_file').show();
            $('#sin_on_file').hide();
            $('#address').prop('readonly', false);
            $('#city').prop('readonly', false);
            $('#province').prop('readonly', false);
            // $('#customer_id').prop('readonly', false);
            $('#postal_code').prop('readonly', false);
        }
    });


    $(document).on('click ', '.item_dealer', function (e) {
        e.preventDefault();
        var customer_id = $(this).attr('data-customer_id');
        if (customer_id) {
            var response = dealers.filter(function (item) {
                // console.log(item.id,customer_id);
                if (item.id === parseInt(customer_id)) {
                    return 1;
                }
            });
            // console.log('response',response[0]);
            if (response) {
                dealer = response[0];
                $('#suggesstion-dealers').hide();
                $('#customer_name').val(dealer.name);
                $('#contact-name').val(dealer.contact_name);
                $('#address').val(dealer.address);
                $('#city').val(dealer.city);
                $('#province').val(dealer.province);
                $('#customer_id').val(dealer.id);
                $('#postal_code').val(dealer.postal_code);
                $('#address').prop('readonly', true);
                $('#city').prop('readonly', true);
                $('#province').prop('readonly', true);
                //$('#customer_id').prop('readonly', true);
                $('#postal_code').prop('readonly', true);
                if (dealer.sin != '') {
                    $('#sin').val(dealer.sin);
                    $('#sin_on_file').show();
                    $('#sin_off_file').hide();
                } else {
                    $('#sin').val('');
                    $('#sin_off_file').show();
                    $('#sin_on_file').hide();
                    console.log('jere');
                }
                $('#msg').text('');
            }
        } else {
            $('#suggesstion-dealers').html('');
            $('#address').prop('readonly', false);
            $('#city').prop('readonly', false);
            $('#province').prop('readonly', false);
            //$('#customer_id').prop('readonly', false);
            $('#postal_code').prop('readonly', false);
        }
    });

    $(document).on('click ', '#customer_form', function (e) {

        var customer_id = $('#customer_id').val();
        if (customer_id == '') {
            e.preventDefault();
            alert("Please select a Customer from List.");
        }
    });

    //salseman_form
    $(document).on('click ', '#salseman_form', function (e) {

        var salesman_id = $('#salesman_id').val();
        if (salesman_id == '') {
            e.preventDefault();
            alert("Please select a Sales Rep from List.");
        }
    });


    $(document).on('keyup ', '#salesman_name', function (e) {
        e.preventDefault();
        var name = $(this).val();
        //console.log('name',name);
        if (name.length > 1) {
            var response = salesmen.filter(function (item) {
                if (item.name.toLowerCase().indexOf(name.toLowerCase()) > -1) {
                    return 1;
                }
            });
            //console.log('response',response);
            if (response) {
                $('#suggesstion-salesman').html('');
                window.myres = response;
                var _ul = $('<ul />')
                    .attr('id', 'salesman-list');
                if (response.length > 0) {
                    $.each(response, function (i, item) {
                        var _li = $('<li />')
                            .addClass('item_salesman')
                            .attr('data-salesman_id', item.id)
                            .html(item.name)
                            .appendTo(_ul);
                    });
                    _ul.appendTo('#suggesstion-salesman');

                }
                $('#suggesstion-salesman').show();
            }
        } else {
            $('#suggesstion-salesman').hide();
            $('#saleman_id').val('');
            //$('#salesman_name').val();
            $('#sales_rep').val('');
            $('#rep_email').val('');
            $('#sd_rep').val('');
            $('#rep_email').prop('readonly', false);
        }
    });


    $(document).on('click ', '.item_salesman', function (e) {
        e.preventDefault();
        var name = $(this).text();
        console.log('name', name);
        if (name.length > 1) {
            var salesman_id = $(this).attr('data-salesman_id');
            var response = salesmen.filter(function (item) {
                //console.log(item.id,salesman_id);
                if (item.id === parseInt(salesman_id)) {
                    return 1;
                }
            });
            //console.log('response',response);
            if (response) {
                if (response.length > 0) {
                    salesman = response[0];
                    $('#suggesstion-salesman').html('');
                    $('#suggesstion-salesman').hide();
                    $('#salesman_id').val(salesman.id);
                    $('#salesman_name').val(salesman.name);
                    $('#rep_email').val(salesman.email);
                    $('#rep_email').prop('readonly', true);
                    //console.log(salesman.email);
                }
            }

        } else {
            $('#suggesstion-dealers').html('');
            $('#rep_email').prop('readonly', false);

        }

    });


    function grandTotal() {
        var total = 0;
        $('.total').each(function () {
            if ($(this).val() != "")
                total += parseFloat($(this).val());
        });
        return total;
    }


    function grandFranklin() {
        var total = 0;

        $('.franklin').each(function () {
            var parent = $(this).parents('.product_row');
            var ischecked = parent.find('.franklin-checkbox').is(':checked');
            if (ischecked) {
                //console.log('ischecked',ischecked);
                if ($(this).val() != "") {
                    total += parseFloat($(this).val());
                }
            }
        });
        return total;
    }

    function grandPoints() {
        var total = 0;
        $('.promo').each(function () {
            if ($(this).val() != "")
                total += parseFloat($(this).val());
        });
        return total;
    }

    function totalDiscount() {
        var total = 0;
        $('.discount').each(function () {
            if ($(this).val() != "")
                total += parseFloat($(this).val());
        });
        return total;
    }

    function SortByItemName(a, b) {
        var aName = a.item_name.toLowerCase();
        var bName = b.item_name.toLowerCase();
        return ((aName < bName) ? -1 : ((aName > bName) ? 1 : 0));
    }

    $(document).on('change', '.product_name', function (e) {
        e.preventDefault();
        var current = $(this);
        var parent = $(this).parents('.product_row');
        console.log('parent product_name', parent, current.val());
        //console.log( current.val() ,'-----test' );

        if (current.val() != '') {
            //console.log('products',products);
            var product_name = current.val();
            var response = products.filter(function (item) {
                //console.log(item.product_name,product_name);
                if (item.product_name === product_name) {
                    return 1;
                }
            });
            /* filtering duplicate item names */
            var result = [];
            $.each(response, function (i, e) {
                var matchingItems = $.grep(result, function (item) {
                    return item.item_name === e.item_name;
                });
                if (matchingItems.length === 0) {
                    result.push(e);
                }
            });

            var response = result;
            window.myres = response;
            // console.log('response',response);
            if (response.length > 0) {
                response.sort(SortByItemName);
                parent.find('.item_name').children().remove();

                //add empty
                var option = $('<option />')
                    .val("")
                    .html('Select Item')
                    .appendTo(parent.find('.item_name'));
                $.each(response, function (i, item) {
                    var option = $('<option />')
                        .val(item.item_name)
                        .attr('data-item-id', item.id)
                        .html(item.item_name)
                        .appendTo(parent.find('.item_name'));
                });

                $('.item_name').formSelect();
                $('.item_number').formSelect();

                parent.find('.item_name').val('');
                parent.find('.item_number').val('');
                parent.find('.item_type').val('');
                parent.find('.product_id').val('');
                parent.find('.quantity').prop('readonly', true);
                parent.find('.total').val('');
                parent.find('.price').val('');
                parent.find('.desc').val('');
                parent.find('.promo').val('');
                parent.find('.discount').val('');
                parent.find('.quantity').val('');
                parent.find('.franklin').val('');
                parent.find('.franklin-checkbox').prop('checked', false);
                parent.find('.franklin-checkbox').val(2);
                parent.find('.franklin-checkbox-input').val(0);
                $('#msg').text('');
            }

        } else {
            parent.find('.item_name').val('');
            parent.find('.item_number').val('');
            parent.find('.item_type').val('');
            parent.find('.product_id').val('');
            parent.find('.quantity').prop('readonly', true);
            parent.find('.total').val('');
            parent.find('.price').val('');
            parent.find('.desc').val('');
            parent.find('.promo').val('');
            parent.find('.discount').val('');
            parent.find('.quantity').val('');
            parent.find('.franklin').val('');
            parent.find('.franklin-checkbox').prop('checked', false);
            parent.find('.franklin-checkbox').val(2);
            parent.find('.franklin-checkbox-input').val(0);
            $('#msg').text('');

        }

    });


    $(document).on('change', '.item_name', function (e) {
        e.preventDefault();
        var current = $(this);
        var parent = current.parents('.product_row');
        console.log('parent item_name', parent);
        var item_name = current.val();
        if (item_name != '') {
            var response = products.filter(function (item) {
                // console.log(item.item_name,item_name);
                if (item.item_name === item_name) {
                    return 1;
                }
            });
            /* filtering duplicate  item numbers */
            var result = [];
            $.each(response, function (i, e) {
                var matchingItems = $.grep(result, function (item) {
                    return item.item_number === e.item_number;
                });
                if (matchingItems.length === 0) {
                    result.push(e);
                }
            });

            var response = result;
            window.myres = response;
            // console.log('response',response);
            if (response.length > 0) {
                parent.find('.item_number').html('');
                //add empty
                var option = $('<option />')
                    .val("")
                    .html('Select Item Number')
                    .appendTo(parent.find('.item_number'));

                $.each(response, function (i, item) {
                    var option = $('<option />')
                        .val(item.item_number)
                        .attr('data-item-id', item.id)
                        .html(item.item_number)
                        .appendTo(parent.find('.item_number'));
                });


                $('.item_name').formSelect();
                $('.item_number').formSelect();

                parent.find('.item_number').val('');
                parent.find('.item_type').val('');
                parent.find('.product_id').val('');
                parent.find('.quantity').prop('readonly', true);
                parent.find('.total').val('');
                parent.find('.price').val('');
                parent.find('.desc').val('');
                parent.find('.promo').val('');
                parent.find('.discount').val('');
                parent.find('.quantity').val('');
                parent.find('.franklin').val('');
                parent.find('.franklin-checkbox').prop('checked', false);
                parent.find('.franklin-checkbox').val(2);
                parent.find('.franklin-checkbox-input').val(0);
                $('#msg').text('');
            }
        } else {
            parent.find('.item_number').val('');
            parent.find('.item_type').val('');
            parent.find('.product_id').val('');
            parent.find('.quantity').prop('readonly', true);
            parent.find('.total').val('');
            parent.find('.price').val('');
            parent.find('.desc').val('');
            parent.find('.promo').val('');
            parent.find('.discount').val('');
            parent.find('.quantity').val('');
            parent.find('.franklin').val('');
            parent.find('.franklin-checkbox').prop('checked', false);
            parent.find('.franklin-checkbox').val(2);
            parent.find('.franklin-checkbox-input').val(0);
            $('#msg').text('');
        }


    });

    $(document).on('change', '.item_number', function (e) {
        e.preventDefault();
        var current = $(this);
        var parent = current.parents('.product_row');
        console.log('parent item_number', parent);
        var product_name = parent.find('.product_name').val();
        var item_name = parent.find('.item_name').val();
        var item_number = current.val();
        if (item_number != '') {
            var product_id = current.find(':selected').attr('data-item-id');
            //console.log(product_name,item_name,item_number,'here');
            var response = products.filter(function (item) {

                if (item.product_name === product_name && item.item_name === item_name && item.item_number === item_number) {
                    //console.log(item.id,product_id);
                    // console.log(item,product_id);
                    return 1;
                }
            });

            //var response =filteredValue;
            window.myres = response;
            //console.log('response',response);
            if (response) {
                if (response.length > 0) {
                    product = response[0];
                    //selected_products.push(product);
                    parent.find('.product_json').val(JSON.stringify(product));
                    var desc = product.product_name + ' ' + product.item_name + ' ' + product.item_number + ' ' + product.color_name;
                    parent.find('.item_type').val(product.item_type);
                    parent.find('.product_id').val(product.id);
                    parent.find('.quantity').prop('readonly', false);
                    parent.find('.price').val(product.per_item);
                    parent.find('.franklin').val(product.single_franklin);
                    parent.find('.franklin-checkbox').prop('checked', true);
                    parent.find('.franklin-checkbox').trigger('change');
                    parent.find('.total').val('');
                    parent.find('.discount').val('');
                    parent.find('.quantity').val(1);
                    parent.attr('data-filled', 'filled');
                    parent.find('.desc').val(desc);
                    if ((product.product_name === 'Underlay-Edm') || (product.product_name === 'Underlay-Depot') || (product.product_name === 'Underlay-Cgy')) {
                        parent.find('.cash-checkbox').show();
                    } else {
                        parent.find('.cash-checkbox').hide();
                    }
                    parent.find(".quantity").trigger("change");
                    //getGrandTotal( parent );
                    $('#msg').text('');
                }

                //console.log('selected_products',selected_products);
            }
        } else {

            parent.find('.item_type').val('');
            parent.find('.product_id').val('');
            parent.find('.quantity').prop('readonly', true);
            parent.find('.total').val('');
            parent.find('.price').val('');
            parent.find('.desc').val('');
            parent.find('.promo').val('');
            parent.find('.discount').val('');
            parent.find('.quantity').val('');
            parent.find('.franklin').val('');
            parent.find('.franklin-checkbox').prop('checked', false);
            parent.find('.franklin-checkbox').val(2);
            parent.find('.franklin-checkbox-input').val(0);
            $('#msg').text('');
        }

    });

    $(document).on('keyup change', '.mill_cost', function (e) {
        var mill_cost = $('.mill_cost').val();
        var conver_rate = $('.conversion_rate').val();
        if (mill_cost != '' && conver_rate != '') {
            var selling_price = parseFloat(mill_cost) / parseFloat(conver_rate);
            $('.selling_price').val(selling_price.toFixed(3));
            if (conver_rate != '') {
                $('.usd_cad').trigger('change');
            }
        }
    });

    $('.modal').modal();

    $(document).on('change', '.usd_cad', function (e) {
        var currency = $(this).val();
        var mill_cost = $('.mill_cost').val();
        var brokerage_multiplier = $('.brokerage_multiplier').val();
        //console.log('test ', conversion_rate ,conversion_rate[currency])
        var conver_rate = conversion_rate[currency];
        if (currency && conver_rate) {
            $('.conversion_rate').val(conver_rate);
            if (mill_cost != '') {
                var selling_price = parseFloat(mill_cost) / parseFloat(conver_rate);
                $('.selling_price').val(selling_price.toFixed(4));
                if (brokerage_multiplier != '') {
                    $('.brokerage_multiplier').trigger('change');
                }
            }
        } else {
            $('.conversion_rate').val(1);
            var selling_price = parseFloat(mill_cost) / 1.00;
            $('.selling_price').val(selling_price.toFixed(4));
        }

    });

    $(document).on('keyup change', '.brokerage_multiplier', function (e) {
        var brokerage_multiplier = $(this).val();
        var conver_rate = $('.conversion_rate').val();
        var selling_price = $('.selling_price').val();
        var freight = $('.freight').val();
        if (selling_price != '') {
            var brokerage = parseFloat(selling_price) * parseFloat(brokerage_multiplier);
            $('.brokerage').val(brokerage.toFixed(4));
            if (freight != '') {
                $('.ad_fund_percentage').trigger('change');
            }
        }


    });

    $(document).on('keyup change', '.ad_fund_percentage', function (e) {
        var ad_fund_percentage = $(this).val();
        var conver_rate = $('.conversion_rate').val();
        var selling_price = $('.selling_price').val();
        var freight = $('.freight').val();
        if (selling_price != '') {
            var brokerage = parseFloat(selling_price) * parseFloat(ad_fund_percentage);
            $('.ad_fund').val(brokerage.toFixed(4));
            if (freight != '') {
                $('.freight').trigger('change');
            }
        }
    });

    $(document).on('keyup change', '.freight', function (e) {
        var brokerage = $('.brokerage').val();
        var selling_price = $('.selling_price').val();
        var load_pent = $('.load_pent').val();
        var ad_fund_percentage = $('.ad_fund_percentage').val();
        var ad_fund_value = $('.ad_fund').val();
        var freight = $(this).val();
        var load_percentage = $('.load_percentage').val();
        if (brokerage != '' && selling_price != '' && freight != '' && ad_fund_value != '') {
            var landed_cost_per_unit = parseFloat(selling_price) + parseFloat(brokerage) + parseFloat(ad_fund_value) + parseFloat(freight);
            $('.landed_cost_per_unit').val(landed_cost_per_unit.toFixed(4));
            if (load_pent != '') {
                $('.load_pent').trigger('change');
            }
        }

    });

    $(document).on('keyup change', '.load_pent', function (e) {
        var load_pent = $(this).val();
        var brokerage = $('.brokerage').val();
        var landed_cost_per_unit = $('.landed_cost_per_unit').val();
        if (landed_cost_per_unit != '' && load_pent != '') {
            var load_percentage = parseFloat(landed_cost_per_unit) * parseFloat(load_pent);
            $('.load_percentage').val(load_percentage.toFixed(4));
            if (load_percentage != '') {
                $('.load_percentage').trigger('change');
            }
        }
    });

    $(document).on('keyup change', '.load_percentage', function (e) {
        var load_percentage = $(this).val();
        var landed_cost_per_unit = $('.landed_cost_per_unit').val();
        if (landed_cost_per_unit != '' && load_percentage != '') {
            var margin_lic = parseFloat(landed_cost_per_unit) + parseFloat(load_percentage);
            $('.margin_lic').val(margin_lic.toFixed(3));
            gross_margin(1);
            gross_margin(2);
            gross_margin(3);
        }
    });


    $(document).on('keyup change', '.sqft_ctn', function (e) {
        var sqft_ctn = $(this).val();
        var ctn_pallet = $('.ctn_pallet').val();
        if (sqft_ctn != '' && ctn_pallet != '') {
            var sqft_pallet = parseFloat(sqft_ctn) * parseFloat(ctn_pallet);
            $('.sqft_pallet').val(sqft_pallet.toFixed(4));
            $('.pallet_container_20').trigger('change');
        }
    });

    $(document).on('keyup change', '.ctn_pallet', function (e) {
        var ctn_pallet = $(this).val();
        var sqft_ctn = $('.sqft_ctn').val();
        if (sqft_ctn != '' && ctn_pallet != '') {
            var sqft_pallet = parseFloat(sqft_ctn) * parseFloat(ctn_pallet);
            $('.sqft_pallet').val(sqft_pallet.toFixed(4));
            $('.pallet_container_20').trigger('change');
        }
    });

    //pallet_container_20

    $(document).on('keyup change', '.pallet_container_20', function (e) {
        var pallet_container_20 = $(this).val();
        var sqft_pallet = $('.sqft_pallet').val();
        if (pallet_container_20 != '' && sqft_pallet != '') {
            var sqft_container_20 = parseFloat(pallet_container_20) * parseFloat(sqft_pallet);
            $('.sqft_container_20').val(sqft_container_20.toFixed(4));
        }
    });


    $(document).on('keyup change', '.sell_price_level1', function (e) {
        gross_margin(1)
    });
    $(document).on('keyup change', '.sell_price_level2', function (e) {
        gross_margin(2)
    });
    $(document).on('keyup change', '.sell_price_level3', function (e) {
        gross_margin(3)
    });
    $(document).on('keyup change', '.rebate_level1', function (e) {
        gross_margin(1)
    });
    $(document).on('keyup change', '.rebate_level2', function (e) {
        gross_margin(2)
    });
    $(document).on('keyup change', '.rebate_level3', function (e) {
        gross_margin(3)
    });

    function gross_margin(key) {
        console.log('key', key);
        if (key) {
            var sell_price_level = $('.sell_price_level' + key).val();
            var rebate_level = $('.rebate_level' + key).val();
            var margin_lic = $('.margin_lic').val();
            if (margin_lic != '' && sell_price_level != '' && rebate_level != '') {
                var gross_margin_level = (((parseFloat(sell_price_level) - parseFloat(margin_lic)) + parseFloat(rebate_level)) / parseFloat(sell_price_level)) * 100;
                $('.gross_margin_level' + key).val(gross_margin_level.toFixed(3));
            }
            return true;
        }

        return false;

    }

    $(document).on('keyup', '.search_name', function (e) {
        e.preventDefault();
        var search_name = $(this).val();
        var url = $(this).attr('data-link');
        console.log('search_name', search_name, url);
        if (search_name != '' && url != '') {
            $.ajax({
                url: url,
                headers: {
                    'X-CSRF-Token': csrfCustomerToken
                },
                type: 'post',
                // async:true,
                crossDomain: true,
                //contentType: false,
                //processData: false,
                //dataType: 'json',
                data: {search: search_name},

                success: function (response) {
                    //console.log('response ',response);
                    if (response) {
                        //window.myres = response;

                        if (response.length > 0) {
                            var _ul = $('<ul />')
                                .attr('id', 'pricing-list');
                            $.each(response, function (i, item) {
                                var code = item.product_code ? item.product_code + ' ' : '';
                                var _li = $('<li />')
                                    .addClass('item_pricing')
                                    .attr('data-id', item.id)
                                    .html(code + item.name)
                                    .appendTo(_ul);
                            });
                            _ul.appendTo('#suggesstion-pricing');

                        }
                        $('#suggesstion-pricing').show();
                    }
                },
                beforeSend: function () {
                    //console.log('search_name');
                    $('#suggesstion-pricing').html('');
                },
                error: function (a, b, c) {
                    console.log('Error: ' + c);
                }

            });
        } else {
            $('#suggesstion-pricing').html('');
            $('#suggesstion-pricing').hide();
            var $table = $("#pricing-matrix");
            var $child = $table.children('.product_row');
            console.log('count', $child.length);
            if ($child.length > 4) {
                console.log('count', $child.length);
                $table.find('tr.product_row:nth-child(4)').nextAll().remove();
            }
            // var $last_row = $('#pricing-matrix > tr:last');
            //var $clone = $tr.clone();
            $table.find('input[type="number"]').not(':input[name="slevel"]').val('');
            $table.find(':input[name="sell_price"]').val('');
            pricing = {};
        }
    });
    $(document).on('keyup', '.search_name_margin', function (e) {
        e.preventDefault();

        var search_name = $(this).val();
        var url = $(this).attr('data-link');
        console.log('search_name', search_name, url);
        if (search_name != '' && url != '') {
            setTimeout(function () {
                $.ajax({
                    url: url,
                    headers: {
                        'X-CSRF-Token': csrfCustomerToken
                    },
                    type: 'post',
                    // async:false,
                    crossDomain: true,
                    //contentType: false,
                    //processData: false,
                    //dataType: 'json',
                    data: {search: search_name},

                    success: function (response) {
                        //console.log('response ',response);
                        if (response) {
                            //window.myres = response;

                            if (response.length > 0) {
                                $('#suggesstion-pricing').html('');
                                var _ul = $('<ul />')
                                    .attr('id', 'pricing-list');
                                $.each(response, function (i, item) {
                                    var code = item.product_code ? item.product_code + ' ' : '';
                                    var _li = $('<li />')
                                        .addClass('item_pricing')
                                        .attr('data-id', item.id)
                                        .html(code + item.name)
                                        .appendTo(_ul);
                                });
                                _ul.appendTo('#suggesstion-pricing');

                            }
                            $('#suggesstion-pricing').show();
                        }
                    },
                    beforeSend: function () {
                        //console.log('search_name');
                        $('#suggesstion-pricing').html('');
                    },
                    error: function (a, b, c) {
                        console.log('Error: ' + c);
                    }

                });
            }, 1000);
        } else {
            $('#suggesstion-pricing').html('');
            $('#suggesstion-pricing').hide();
            var $table = $("#pricing-matrix");
            var $child = $table.children('.product_row');
            console.log('count', $child.length);
            if ($child.length > 4) {
                console.log('count', $child.length);
                $table.find('tr.product_row:nth-child(4)').nextAll().remove();
            }
            // var $last_row = $('#pricing-matrix > tr:last');
            //var $clone = $tr.clone();
            $table.find('input[type="number"]').not(':input[name="slevel"]').val('');
            $table.find(':input[name="sell_price"]').val('');
            pricing = {};
        }

    });

    $(document).on('click', '.pricing_reset', function (e) {
        e.preventDefault();
        $('.search_name, .search_name_margin').val('');
        $('.search_name, .search_name_margin').trigger('keyup');
    });

    $(document).on('click', '.item_pricing', function (e) {
        e.preventDefault();
        var name = $(this).text();
        var url = $('.search_name').attr('data-link');
        if (typeof (url) === "undefined") {
            var url = $('.search_name_margin').attr('data-link');
        }
        //console.log(url);
        //return false;
        var salesman_id = $(this).attr('data-id');
        console.log('salesman_id ', salesman_id);
        if (salesman_id) {

            $.ajax({
                url: url,
                headers: {
                    'X-CSRF-Token': csrfCustomerToken
                },
                type: 'post',
                //async:true,
                crossDomain: true,
                data: {id: salesman_id},
                success: function (response) {
                    if (response) {
                        if (response.length > 0) {
                            pricing = {};
                            pricing = response[0];

                            localStorage.setItem('pricingcal', JSON.stringify(pricing));

                            //$('#suggesstion-pricing').html('');


                            $('#slab_1').prop('readonly', true);
                            $('#slab_2').prop('readonly', true);
                            $('#slab_3').prop('readonly', true);

                            $('#sell-price-level1').prop('readonly', true).val(pricing.sell_price_level1);
                            $('#sell_tms_1').val(pricing.sell_price_level1);
                            $('#sell_ms_1').prop('readonly', true).val(pricing.sell_price_level1);
                            $('#rebate-level1, #rebate_ms_1, #rebate_tms_1').val(pricing.rebate_level1);

                            $('#sell_sm_1').text(pricing.sell_price_level1);
                            $('#sell_sm_2').text(pricing.sell_price_level2);
                            $('#sell_sm_3').text(pricing.sell_price_level3);

                            $('#rebate_sm_1').text(pricing.rebate_level1);
                            $('#rebate_sm_2').text(pricing.rebate_level2);
                            $('#rebate_sm_3').text(pricing.rebate_level3);

                            $('#gross_sm_1').text(pricing.gross_margin_level1);
                            $('#gross_sm_2').text(pricing.gross_margin_level2);
                            $('#gross_sm_3').text(pricing.gross_margin_level3);

                            $('#gross-margin-level1').prop('readonly', true).val(pricing.gross_margin_level1);
                            $('#gross_ms_1').prop('readonly', true).val(pricing.gross_margin_level1);

                            $('#defaut_value').prop('readonly', true).val(pricing.conversion_rate);

                            $('#sell-price-level2').prop('readonly', true).val(pricing.sell_price_level2);
                            $('#sell_tms_2').val(pricing.sell_price_level2);
                            $('#sell_ms_2').prop('readonly', true).val(pricing.sell_price_level2);

                            $('#rebate-level2, #rebate_tms_2').val(pricing.rebate_level2);
                            $('#rebate_ms_2').prop('readonly', true).val(pricing.rebate_level2);


                            $('#gross-margin-level2').prop('readonly', true).val(pricing.gross_margin_level2);
                            $('#gross_ms_2').prop('readonly', true).val(pricing.gross_margin_level2);

                            $('#sell-price-level3').prop('readonly', true).val(pricing.sell_price_level3);
                            $('#sell_tms_3').val(pricing.sell_price_level3);

                            $('#sell_ms_3').prop('readonly', true).val(pricing.sell_price_level3);

                            $('#rebate-level3, #rebate_tms_3').val(pricing.rebate_level3);
                            $('#rebate_ms_3').prop('readonly', true).val(pricing.rebate_level3);

                            $('#gross-margin-level3').prop('readonly', true).val(pricing.gross_margin_level3);
                            $('#gross_ms_3').prop('readonly', true).val(pricing.gross_margin_level3);

                            $('#gross_tms_1').val(pricing.gross_margin_level1);
                            $('#gross_tms_2').val(pricing.gross_margin_level2);
                            $('#gross_tms_3').val(pricing.gross_margin_level3);

                            $('.search_name, .search_name_margin').val(pricing.name);
                            $('#suggesstion-pricing').hide();
                            console.log(response);
                            getdefaut();
                            pattern();
                        }
                    }
                },
                beforeSend: function () {
                    console.log('sign_ajax_url')
                },
                error: function (a, b, c) {
                    console.log('Error: ' + c);
                }

            });
        }

    });

    $(document).click(function () {
        $('#suggesstion-pricing').hide();
    });
    $('.search_name_margin').keydown(function (e) {
        console.log('keyup called');
        var code = e.keyCode || e.which;
        console.log(code);
        if (code == 9) {
            //alert('sss');
            $('#suggesstion-pricing').hide();
        }
    });

    $('.checkbox_checked').on('click', function () {
        var check_kbox = [];
        var accept_id = $(".accept_id").attr('href');
        accept_id = accept_id.substring(0, accept_id.indexOf('familyid'));
        $(".checkbox_checked").each(function () {
            if ($(this).is(":checked")) {
                check_kbox.push($(this).attr('data-value'));
            }
        });
        var checkid = check_kbox.join(',')
        $(".accept_id").attr('href', accept_id + 'familyid=' + checkid);

    });

    $('.modal').modal();


    $(document).on('click', '.pricing-modal-trigger', function (e) {
        e.preventDefault();
        var url = $('.search_name').attr('data-link');
        if (url.length > 1) {
            var salesman_id = $(this).attr('data-id');
            $.ajax({
                url: url,
                headers: {
                    'X-CSRF-Token': csrfCustomerToken
                },
                type: 'post',
                async: true,
                crossDomain: true,
                data: {id: salesman_id},
                success: function (response) {
                    if (response) {
                        if (response.length > 0) {
                            //alert(pricing.margin_lic);
                            pricing = {};
                            pricing = response[0];

                            //console.log('yes');
                            //console.log(pricing);
                            localStorage.setItem('pricingcal', JSON.stringify(pricing));

                            $('#suggesstion-pricing').html('');
                            $('#suggesstion-pricing').hide();
                            $('.search_name').val(pricing.name);
                            $('#slab_1').prop('readonly', true);
                            $('#slab_2').prop('readonly', true);
                            $('#slab_3').prop('readonly', true);

                            $('#sell-price-level1').prop('readonly', true).val(pricing.sell_price_level1);
                            $('#sell_ms_1').prop('readonly', true).val(pricing.sell_price_level1);
                            $('#rebate-level1, #rebate_ms_1').val(pricing.rebate_level1);

                            $('#sell_sm_1').text(pricing.sell_price_level1);
                            $('#sell_sm_2').text(pricing.sell_price_level2);
                            $('#sell_sm_3').text(pricing.sell_price_level3);

                            $('#rebate_sm_1').text(pricing.rebate_level1);
                            $('#rebate_sm_2').text(pricing.rebate_level2);
                            $('#rebate_sm_3').text(pricing.rebate_level3);

                            $('#gross_sm_1').text(pricing.gross_margin_level1);
                            $('#gross_sm_2').text(pricing.gross_margin_level2);
                            $('#gross_sm_3').text(pricing.gross_margin_level3);

                            $('#gross-margin-level1').prop('readonly', true).val(pricing.gross_margin_level1);
                            $('#gross_ms_1').prop('readonly', true).val(pricing.gross_margin_level1);

                            $('#defaut_value').prop('readonly', true).val(pricing.conversion_rate);

                            $('#sell-price-level2').prop('readonly', true).val(pricing.sell_price_level2);
                            $('#sell_ms_2').prop('readonly', true).val(pricing.sell_price_level2);

                            $('#rebate-level2').val(pricing.rebate_level2);
                            $('#rebate_ms_2').val(pricing.rebate_level2);


                            $('#gross-margin-level2').prop('readonly', true).val(pricing.gross_margin_level2);
                            $('#gross_ms_2').prop('readonly', true).val(pricing.gross_margin_level2);

                            $('#sell-price-level3').prop('readonly', true).val(pricing.sell_price_level3);
                            $('#sell_ms_3').prop('readonly', true).val(pricing.sell_price_level3);

                            $('#rebate-level3').val(pricing.rebate_level3);
                            $('#rebate_ms_3').prop('readonly', true).val(pricing.rebate_level3);

                            $('#gross-margin-level3').prop('readonly', true).val(pricing.gross_margin_level3);
                            $('#gross_ms_3').prop('readonly', true).val(pricing.gross_margin_level3);

                            $('.modal-m').modal('open');
                            $('.search_name').val(pricing.name);
                            $('#suggesstion-pricing').hide();
                            console.log(response);
                            getdefaut();

                            //getdefaut();

                        }
                    }
                },
                beforeSend: function () {
                    console.log('sign_ajax_url')
                },
                error: function (a, b, c) {
                    console.log('Error: ' + c);
                }

            });
        }

    });


    //$('.slab_select').formSelect();
    $(document).on('change', '.defaut_dropdown', function (e) {
        var defautdropdown = $(this).val();

        if (defautdropdown == 'defaut') {
            var pricing = localStorage.getItem("pricingcal");
            var pricing = JSON.parse(pricing);
            $('#defaut_value').prop('readonly', true).val(parseFloat(pricing.conversion_rate));
        } else {
            $('#defaut_value').prop('readonly', false).val('0');
        }
        getdefaut();
    });
    $(document).on('change', '.slab_select', function (e) {
        var parent = $(this).parents('.product_row');
        var select_val = $(this).val();
        //console.log('pricing_product', parent, select_val);
        if (!jQuery.isEmptyObject(pricing)) {
            if (select_val == 1) {

                parent.find('.sell_price').val(pricing.sell_price_level1);
                parent.find('.rebate').val(pricing.rebate_level1);
                parent.find('.gross_margin').prop('readonly', true).val(pricing.gross_margin_level1);
            }
            if (select_val == 2) {

                parent.find('.sell_price').val(pricing.sell_price_level2);
                parent.find('.rebate').val(pricing.rebate_level2);
                parent.find('.gross_margin').prop('readonly', true).val(pricing.gross_margin_level2);
            }
            if (select_val == 3) {

                parent.find('.sell_price').val(pricing.sell_price_level3);
                parent.find('.rebate').val(pricing.rebate_level3);
                parent.find('.gross_margin').prop('readonly', true).val(pricing.gross_margin_level3);
            }

            //console.log('pricing_product',pricing);
        }
    });

    $(document).on('keyup', '.sell_price', function (e) {
        getdefaut();

    });

    $(document).on('keyup', '.rebate', function (e) {

        getdefaut();

        /* var parent = $(this).parents('.product_row');
  	var sell_price = parent.find('.sell_price').val();
  	var rebate     = parent.find('.rebate').val();
  	var select_val = parent.find('.slab_select').val();

	var marginlic = parseFloat(pricing.mill_cost) / $('#defaut_value').val();
			var brokragepersantage = pricing.brokerage_multiplier;
			var brokrage = brokragepersantage * marginlic;
			var freight = pricing.freight;
			var ad_fund = marginlic * brokrage;
			var landed_cost_per_unit = parseFloat(marginlic) + parseFloat(brokrage) + parseFloat(freight) + parseFloat(ad_fund);
			var load  =  landed_cost_per_unit * parseFloat(pricing.load_percentage);
			var margin_lic = landed_cost_per_unit + load;

  	if(!jQuery.isEmptyObject(pricing)){
  		if(sell_price != '' && rebate != ''){

  		var gross_margin_level = ( parseFloat(sell_price) -parseFloat(pricing.margin_lic) + parseFloat(rebate) )/parseFloat(sell_price)*100;
			parent.find('.gross_margin').prop('readonly', true).val(gross_margin_level.toFixed(2))
		}

  	} */
    });

    $(document).on('click ', 'button.pricing_clone_add', function (e) {
        e.preventDefault();
        var $table = $("#pricing-matrix");
        var $child = $table.children('.product_row');
        var $index = $child.length + 1;
        var index_text = 'Slab ' + $index;
        //console.log('count',$child.length);
        var $tr = $(this).parents('.product_row');
        var $last_row = $('#pricing-matrix > tr:last');
        var $clone = $last_row.clone(true);
        $clone.find('.slab_select').prop('readonly', true).val(index_text);
        $clone.find('.sell_price').val('');
        $clone.find('.rebate').val(0);
        $clone.find('.gross_margin').prop('readonly', true).val('');
        $last_row.after($clone);
        //$('.slab_select').formSelect();
        //console.log('herrere',$clone);
    });

    $(document).on('click ', 'button.tr_clone_add', function (e) {
        e.preventDefault();
        var $tr = $('#myTabledata > tr:last').closest('.product_row');
        var $clone = $tr.clone();
        $clone.find(':text').val('');
        $clone.find('.item_type').val('');
        $clone.find('.product_id').val('');
        $clone.find('.price').val('');
        $clone.find('.discount').val('');
        $clone.find('.quantity').prop('readonly', true);
        $clone.find('.franklin-checkbox').prop('checked', false);
        $clone.find('.total').val('');
        $clone.find('.quantity').val('');
        $tr.after($clone);
        // console.log('herrere',$tr);
    });

    $(document).on('change', ".franklin-checkbox", function (e) {
        //e.preventDefault();
        var parent = $(this).parents('.product_row');
        var ischecked = parent.find('.franklin-checkbox').is(':checked');
        if (!ischecked) {
            //$(this).val('off');
            parent.find('.franklin').hide();
            parent.find('.franklin-checkbox').val(2);
            parent.find('.franklin-checkbox-input').val(0);
            //parent.find('.franklin-checkbox').prop('checked',false);
            //console.log('unchecked',parent.find('.franklin-checkbox').val());
            parent.find('.quantity').trigger('change');
        } else {
            parent.find('.franklin').show();
            parent.find('.franklin-checkbox').val(1);
            parent.find('.franklin-checkbox-input').val(1);
            parent.find('.quantity').trigger('change');
            //parent.find('.franklin-checkbox').prop('checked',true);
        }
    });

    $(document).on('keyup change', '.quantity', function (e) {
        var bulk_flag = false;
        var parent = $(this).parents('.product_row');
        if (parent.find('.product_json').val() != '') {
            var current_product = JSON.parse(parent.find('.product_json').val());
            var total_quantity = bulkQuantityUpdate(current_product);
            if (total_quantity >= parseInt(current_product.bulk_count)) {
                bulk_flag = true;
                $('.product_row').each(function () {
                    var parent = $(this);
                    if (parent.find('.product_json').val() != '') {

                        var product = JSON.parse(parent.find('.product_json').val());
                        if (product.item_name === current_product.item_name) {
                            bulk_flag = (typeof bulk_flag !== 'undefined') ? bulk_flag : false;
                            if ($(this).find('.product_json').val() != '') {
                                var product = JSON.parse($(this).find('.product_json').val());
                                var quantity = $(this).find('.quantity').val();
                                var price = $(this).find('.price').val();
                                var discount = $(this).find('.discount').val() ? $(this).find('.discount').val() : 0;


                                if (quantity !== '' && price !== '') {
                                    if ((product.product_name === 'Underlay-Edm') || (product.product_name === 'Underlay-Depot') || (product.product_name === 'Underlay-Cgy')) {
                                        var ischecked = $(this).find('.cash-checkbox').is(':checked');
                                        if (ischecked) {
                                            var total_cost = parseFloat(quantity) * parseFloat(product.single_item) * parseFloat(product.per_item);

                                            if (discount !== '') {
                                                var total_cost = total_cost - discount;
                                            }
                                            $(this).find('.price').val(product.single_item);

                                            if (parseInt(quantity) >= parseInt(product.bulk_count)) {
                                                if (product.bulk_count != 0) {
                                                    var multiplier = Math.floor(parseInt(quantity) / parseInt(product.bulk_count));
                                                    multiplier = multiplier > 1 ? multiplier : 1;
                                                } else {
                                                    var multiplier = 1;
                                                }
                                                console.log('multiplier', Math.floor(multiplier), multiplier);
                                                var franklin = multiplier * parseFloat(product.single_franklin);
                                            } else {
                                                var franklin = 0;
                                            }

                                        } else {
                                            $(this).find('.price').val(product.bulk_item);
                                            var total_cost = parseFloat(quantity) * parseFloat(product.bulk_item) * parseFloat(product.per_item);

                                            if (discount !== '') {
                                                var total_cost = total_cost - discount;
                                            }
                                            var franklin = 0;
                                        }


                                    } else if (product.product_name === 'Adhesive') {
                                        if (bulk_flag) {

                                            $(this).find('.price').val(product.bulk_item);
                                            var total_cost = parseFloat(quantity) * parseFloat(product.bulk_item);
                                            if (discount !== '') {
                                                var total_cost = total_cost - discount;
                                            }
                                        } else if (parseInt(quantity) < parseInt(product.bulk_count)) {
                                            var total_cost = parseFloat(quantity) * parseFloat(product.single_item);
                                            if (discount !== '') {
                                                var total_cost = total_cost - discount;
                                            }
                                            $(this).find('.price').val(product.single_item);
                                        } else {

                                            $(this).find('.price').val(product.bulk_item);
                                            var total_cost = parseFloat(quantity) * parseFloat(product.bulk_item);

                                            if (discount !== '') {
                                                var total_cost = total_cost - discount;
                                            }
                                        }

                                        if (parseInt(quantity) >= parseInt(product.bulk_count)) {
                                            if (product.bulk_count != 0) {
                                                var multiplier = Math.floor(parseInt(quantity) / parseInt(product.bulk_count));
                                                multiplier = multiplier > 1 ? multiplier : 1;
                                            } else {
                                                var multiplier = 1;
                                            }
                                            var franklin = multiplier * parseFloat(product.bulk_franklin);
                                        } else {
                                            var franklin = 0;
                                        }


                                    } else if (bulk_flag) {
                                        $(this).find('.price').val(product.bulk_item);
                                        console.log('here bulk ');
                                        var total_cost = parseFloat(quantity) * parseFloat(product.bulk_item) * parseFloat(product.per_item);
                                        var franklin = parseFloat(quantity) * parseFloat(product.bulk_franklin);
                                        if (discount !== '') {
                                            var total_cost = total_cost - discount;
                                        }

                                    } else if (parseInt(quantity) < parseInt(product.bulk_count)) {
                                        var total_cost = parseFloat(quantity) * parseFloat(product.single_item) * parseFloat(product.per_item);
                                        var franklin = parseFloat(quantity) * parseFloat(product.single_franklin);
                                        if (discount !== '') {
                                            total_cost = total_cost - discount;
                                        }
                                        $(this).find('.price').val(product.single_item);
                                    } else {
                                        var total_cost = parseFloat(quantity) * parseFloat(product.bulk_item) * parseFloat(product.per_item);
                                        var franklin = parseFloat(quantity) * parseFloat(product.bulk_franklin);
                                        if (discount !== '') {
                                            var total_cost = total_cost - discount;
                                        }
                                        $(this).find('.price').val(product.bulk_item);
                                    }

                                    $(this).find('.total').val(total_cost.toFixed(2));
                                    $(this).find('.franklin').val(franklin.toFixed(2));


                                    var total = grandTotal();
                                    var total_franklin = grandFranklin();
                                    var total_points = grandPoints();
                                    var grand_discount = totalDiscount();
                                    var gross_total = total + grand_discount;
                                    $('#order-grand-total').html(total.toFixed(2));
                                    $('#order-gross-total').html(gross_total.toFixed(2));
                                    $('#order-points-total').html(total_points.toFixed(2));

                                    $('.input-order-grand-total').val(total.toFixed(2));
                                    $('.input-order-gross-total').val(gross_total.toFixed(2));
                                    $('#order-franklin-total').val(total_franklin.toFixed(2));
                                    $('.input-order-points-total').val(total_points.toFixed(2));
                                    $('.input-order-discount-total').val(grand_discount.toFixed(2));

                                }
                            }
                        }
                    }
                });
            } else {
                $('.product_row').each(function () {
                    var parent = $(this);
                    if (parent.find('.product_json').val() != '') {

                        var product = JSON.parse(parent.find('.product_json').val());
                        if (product.item_name === current_product.item_name) {
                            bulk_flag = (typeof bulk_flag !== 'undefined') ? bulk_flag : false;
                            if ($(this).find('.product_json').val() != '') {
                                var product = JSON.parse($(this).find('.product_json').val());
                                var quantity = $(this).find('.quantity').val();
                                var price = $(this).find('.price').val();
                                var discount = $(this).find('.discount').val() ? $(this).find('.discount').val() : 0;


                                if (quantity !== '' && price !== '') {
                                    if ((product.product_name === 'Underlay-Edm') || (product.product_name === 'Underlay-Depot') || (product.product_name === 'Underlay-Cgy')) {
                                        var ischecked = $(this).find('.cash-checkbox').is(':checked');
                                        if (ischecked) {
                                            var total_cost = parseFloat(quantity) * parseFloat(product.single_item) * parseFloat(product.per_item);

                                            if (discount !== '') {
                                                var total_cost = total_cost - discount;
                                            }
                                            $(this).find('.price').val(product.single_item);

                                            if (parseInt(quantity) >= parseInt(product.bulk_count)) {
                                                if (product.bulk_count != 0) {
                                                    var multiplier = Math.floor(parseInt(quantity) / parseInt(product.bulk_count));
                                                    multiplier = multiplier > 1 ? multiplier : 1;
                                                } else {
                                                    var multiplier = 1;
                                                }
                                                console.log('multiplier', Math.floor(multiplier), multiplier);
                                                var franklin = multiplier * parseFloat(product.single_franklin);
                                            } else {
                                                var franklin = 0;
                                            }

                                        } else {
                                            $(this).find('.price').val(product.bulk_item);
                                            var total_cost = parseFloat(quantity) * parseFloat(product.bulk_item) * parseFloat(product.per_item);

                                            if (discount !== '') {
                                                var total_cost = total_cost - discount;
                                            }
                                            var franklin = 0;
                                        }


                                    } else if (product.product_name === 'Adhesive') {
                                        if (bulk_flag) {

                                            $(this).find('.price').val(product.bulk_item);
                                            var total_cost = parseFloat(quantity) * parseFloat(product.bulk_item);
                                            if (discount !== '') {
                                                var total_cost = total_cost - discount;
                                            }
                                        } else if (parseInt(quantity) < parseInt(product.bulk_count)) {
                                            var total_cost = parseFloat(quantity) * parseFloat(product.single_item);
                                            if (discount !== '') {
                                                var total_cost = total_cost - discount;
                                            }
                                            $(this).find('.price').val(product.single_item);
                                        } else {

                                            $(this).find('.price').val(product.bulk_item);
                                            var total_cost = parseFloat(quantity) * parseFloat(product.bulk_item);

                                            if (discount !== '') {
                                                var total_cost = total_cost - discount;
                                            }
                                        }

                                        if (parseInt(quantity) >= parseInt(product.bulk_count)) {
                                            if (product.bulk_count != 0) {
                                                var multiplier = Math.floor(parseInt(quantity) / parseInt(product.bulk_count));
                                                multiplier = multiplier > 1 ? multiplier : 1;
                                            } else {
                                                var multiplier = 1;
                                            }
                                            var franklin = multiplier * parseFloat(product.bulk_franklin);
                                        } else {
                                            var franklin = 0;
                                        }


                                    } else if (bulk_flag) {
                                        $(this).find('.price').val(product.bulk_item);
                                        console.log('here bulk ');
                                        var total_cost = parseFloat(quantity) * parseFloat(product.bulk_item) * parseFloat(product.per_item);
                                        var franklin = parseFloat(quantity) * parseFloat(product.bulk_franklin);
                                        if (discount !== '') {
                                            var total_cost = total_cost - discount;
                                        }

                                    } else if (parseInt(quantity) < parseInt(product.bulk_count)) {
                                        var total_cost = parseFloat(quantity) * parseFloat(product.single_item) * parseFloat(product.per_item);
                                        var franklin = parseFloat(quantity) * parseFloat(product.single_franklin);
                                        if (discount !== '') {
                                            total_cost = total_cost - discount;
                                        }
                                        $(this).find('.price').val(product.single_item);
                                    } else {
                                        var total_cost = parseFloat(quantity) * parseFloat(product.bulk_item) * parseFloat(product.per_item);
                                        var franklin = parseFloat(quantity) * parseFloat(product.bulk_franklin);
                                        if (discount !== '') {
                                            var total_cost = total_cost - discount;
                                        }
                                        $(this).find('.price').val(product.bulk_item);
                                    }

                                    $(this).find('.total').val(total_cost.toFixed(2));
                                    $(this).find('.franklin').val(franklin.toFixed(2));


                                    var total = grandTotal();
                                    var total_franklin = grandFranklin();
                                    var total_points = grandPoints();
                                    var grand_discount = totalDiscount();
                                    var gross_total = total + grand_discount;
                                    $('#order-grand-total').html(total.toFixed(2));
                                    $('#order-gross-total').html(gross_total.toFixed(2));
                                    $('#order-points-total').html(total_points.toFixed(2));

                                    $('.input-order-grand-total').val(total.toFixed(2));
                                    $('.input-order-gross-total').val(gross_total.toFixed(2));
                                    $('#order-franklin-total').val(total_franklin.toFixed(2));
                                    $('.input-order-points-total').val(total_points.toFixed(2));
                                    $('.input-order-discount-total').val(grand_discount.toFixed(2));

                                }
                            }
                        }
                    }
                });
            }
        }


    });

    function bulkQuantityUpdate(current_product) {
        var same_item_products = [];
        var quantity = 0;
        $('.product_row').each(function () {
            var parent = $(this);
            if (parent.find('.product_json').val() != '') {

                var product = JSON.parse(parent.find('.product_json').val());
                if (product.item_name === current_product.item_name) {
                    quantity = quantity + parseInt(parent.find('.quantity').val());
                    same_item_products.push(product);
                }
            }
        });
        return quantity;
    }


    $(document).on('keyup change', '.promo', function (e) {
        e.preventDefault();
        var parent = $(this).parents('.product_row');
        parent.find('.quantity').trigger('change');
    });
    $(document).on('keyup change', '.discount', function (e) {
        e.preventDefault();
        var parent = $(this).parents('.product_row');
        parent.find('.quantity').trigger('change');
    });

    $(document).on('change', '.cash-checkbox', function (e) {
        console.log('event triggered');
        var parent = $(this).parents('.product_row');
        if (parent.find('.product_json').val() != '') {
            var product = JSON.parse(parent.find('.product_json').val());
            if ((product.product_name === 'Underlay-Edm') || (product.product_name === 'Underlay-Depot') || (product.product_name === 'Underlay-Cgy')) {
                //getGrandTotal( parent );
                parent.find(".quantity").trigger("change");
            }
        }
    });


    var signature_image = "";
    if ($('.order_id').length > 0) {

        var order_id = $('.order_id').val();
        if (order_id != "" && parseInt(order_id) > 0) {
            signature_image = BASE_URL + 'webroot/img/Signs/primco-sign-' + order_id + '.jpg';
            $.get(signature_image)
                .done(function () {
                    // exists code
                }).fail(function () {
                // not exists code
            })

        }
    }
    document.addEventListener('DOMContentLoaded', function () {
        /*Your chartist initialization code here*/
        console.log('DOMContentLoaded');
    });

    var wrapper = document.getElementById("signature-pad");
    if (wrapper) {
        var clearButton = wrapper.querySelector("[data-action=clear]");
        window.orientation = resizeCanvas;
        var canvas = wrapper.querySelector("canvas");
        var signaturePad = new SignaturePad(canvas, {
            // It's Necessary to use an opaque color when saving image as JPEG;
            // this option can be omitted if only saving as PNG or SVG
            backgroundColor: 'rgb(255, 255, 255)',
            minWidth: 1
        });
        console.log(signature_image);
        console.log('here', BASE_URL);
        if (signature_image != "") {
            signaturePad.fromDataURL(signature_image);
        }

        clearButton.addEventListener("click", function (event) {
            signaturePad.clear();
        });

        var windowWidth = $(window).width();

        $(window).resize(function () {
            if ($(window).width() != windowWidth) {
                window.onresize = resizeCanvas;
                resizeCanvas();
            }
        });
        window.orientation = resizeCanvas;
        resizeCanvas();
    }

    // Adjust canvas coordinate space taking into account pixel ratio,
    // to make it look crisp on mobile devices.
    // This also causes canvas to be cleared.
    function resizeCanvas() {
        // When zoomed out to less than 100%, for some very strange reason,
        // some browsers report devicePixelRatio as less than 1
        // and only part of the canvas is cleared then.
        var ratio = Math.max(window.devicePixelRatio || 1, 1);

        // This part causes the canvas to be cleared
        canvas.width = canvas.offsetWidth * ratio;
        canvas.height = canvas.offsetHeight * ratio;
        canvas.getContext("2d").scale(ratio, ratio);

        // This library does not listen for canvas changes, so after the canvas is automatically
        // cleared by the browser, SignaturePad#isEmpty might still return false, even though the
        // canvas looks empty, because the internal data of this library wasn't cleared. To make sure
        // that the state of this library is consistent with visual state of the canvas, you
        // have to clear it manually.
        //signaturePad.clear();
    }

    // On mobile devices it might make more sense to listen to orientation change,
    // rather than window resize events.
    function download(dataURL, filename) {
        if (navigator.userAgent.indexOf("Safari") > -1 && navigator.userAgent.indexOf("Chrome") === -1) {
            window.open(dataURL);
        } else {
            var blob = dataURLToBlob(dataURL);
            var url = window.URL.createObjectURL(blob);

            var a = document.createElement("a");
            a.style = "display: none";
            a.href = url;
            a.download = filename;

            document.body.appendChild(a);
            a.click();

            window.URL.revokeObjectURL(url);
        }
    }

    // One could simply use Canvas#toBlob method instead, but it's just to show
    // that it can be done using result of SignaturePad#toDataURL.
    function dataURLToBlob(dataURL) {
        // Code taken from https://github.com/ebidel/filer.js
        var parts = dataURL.split(';base64,');
        var contentType = parts[0].split(":")[1];
        var raw = window.atob(parts[1]);
        var rawLength = raw.length;
        var uInt8Array = new Uint8Array(rawLength);

        for (var i = 0; i < rawLength; ++i) {
            uInt8Array[i] = raw.charCodeAt(i);
        }

        return new Blob([uInt8Array], {type: contentType});
    }

    $(document).on('click', '#submit-form', function (e) {
        // e.preventDefault();
        console.log('here');
        var order_id = $('.order_id').val();
        console.log('order_id', order_id);

        if (0) {
            alert("Please provide a signature first.");
        } else {
            var dataURL = signaturePad.toDataURL();
            var file = dataURLtoBlob(dataURL);
            var filename = 'primco-sign-' + order_id + '.jpg';
            var formData = new FormData();
            formData.append('files', file, filename);
            formData.append('order_id', order_id);

            $.ajax({
                url: sign_ajax_url,
                headers: {
                    'X-CSRF-Token': csrfCustomerToken
                },
                type: 'post',
                async: true,
                crossDomain: true,
                mimeType: 'multipart/form-data',
                contentType: false,
                processData: false,
                dataType: 'json',
                data: formData,

                success: function (response) {
                    $('#regForm').submit();
                    //localStorage.clear();
                },
                beforeSend: function () {
                    console.log('sign_ajax_url')
                },
                error: function (a, b, c) {
                    console.log('Error: ' + c);
                }

            });
        }
        return false;

    });

    function delete_browse_history() {
        window.history.pushState(null, "", window.location.href);
        window.onpopstate = function () {
            console.log('history', window.location.href);
            window.history.pushState(null, "", window.location.href);
        };
    }

    function dataURLtoBlob(dataURL) {
        var binary = atob(dataURL.split(',')[1]);
        var array = [];
        var i = 0;
        while (i < binary.length) {
            array.push(binary.charCodeAt(i));
            i++
        }
        return new Blob([new Uint8Array(array)], {type: 'image/png'})
    };

    function dataURLtoFile(dataurl, filename) {
        var arr = dataurl.split(',');
        var mime = arr[0].match(/:(.*?);/)[1];
        var bstr = atob(arr[1]);
        var n = bstr.length;
        var u8arr = new Uint8Array(n);
        while (n--) {
            u8arr[n] = bstr.charCodeAt(n);
        }
        return new File([u8arr], filename, {type: mime});
    }

    $(document).on('change', ".is_installed", function (e) {
        //e.preventDefault();
        var parent = $(this).parents('.product_row');
        var ischecked = parent.find('.is_installed').is(':checked');
        console.log('checked value', parent.find(".is_installed").val());
        var checked_value = parent.find(".is_installed").val();
        if (checked_value === 'no') {
            console.log('not checked');
            parent.find('.installed_area').prop('readonly', true);
            parent.find('.installed_date').prop('readonly', true);
            parent.find('.installed_area').val('');
            parent.find('.installed_date').val('');
            parent.find('.installed_date').datepicker("destroy");
        } else {
            console.log('checked');
            parent.find('.installed_area').prop('readonly', false);
            parent.find('.installed_date').prop('readonly', false);
            parent.find('.installed_date').datepicker({dateFormat: "yy-mm-dd"});
        }
    });

    $(document).on('click ', '.statement_add', function (e) {
        e.preventDefault();
        $('<div class="statement_list statement_list_pos"><div class="row"><div class="col-md-6 mt-3"><input type="text" name="statement_name[]" class="form-control" maxlength="150" id="statement-name" value=""></div><div class="col-md-6 mt-3"><input type="text" name="statement_email[]" class="form-control" maxlength="150" id="statement-email" value=""></div></div></div>').appendTo('.statement_main');
    });

    $(document).on('click ', '.invoice_add', function (e) {
        e.preventDefault();
        $('<div class="invoice_list statement_list_pos"><div class="row"><div class="col-md-6 mt-3"><input type="text" name="invoice_name[]" class="form-control" maxlength="150" id="invoice-name" value=""></div><div class="col-md-6 mt-3"><input type="text" name="invoice_email[]" class="form-control" maxlength="150" id="invoice-email" value=""></div></div></div>').appendTo('.invoice_main');
    });


    $(document).on('click ', '.price_pages_add', function (e) {
        e.preventDefault();
        $('<div class="price_pages_list statement_list_pos"><div class="row"><div class="col-md-6 mt-3"><input type="text" name="price_pages_name[]" class="form-control" maxlength="150" id="price-pages-name" value=""></div><div class="col-md-6 mt-3"><input type="text" name="price_pages_email[]" class="form-control" maxlength="150" id="price-pages-email" value=""></div></div></div>').appendTo('.price_pages_main');
    });

    $(document).on('click ', '.order_confirmation_add', function (e) {
        e.preventDefault();
        $('<div class="order_confirmation_list statement_list_pos"><div class="row"><div class="col-md-6 mt-3"><input type="text" name="order_confirmation_name[]" class="form-control" maxlength="150" id="order-confirmation-name" value=""></div><div class="col-md-6 mt-3"><input type="text" name="order_confirmation_email[]" class="form-control" maxlength="150" id="order-confirmation-email" value=""></div></div></div>').appendTo('.order_confirmation_main');
    });

    $(document).on('click ', '.bank_order_add', function (e) {
        e.preventDefault();
        $('<div class="bank_order_list statement_list_pos"><div class="row"><div class="col-md-6 mt-3"><input type="text" name="bank_order_name[]" class="form-control" maxlength="150" id="bank-order-name" value=""></div><div class="col-md-6 mt-3"><input type="text" name="bank_order_email[]" class="form-control" maxlength="150" id="bank-order-email" value=""></div></div></div>').appendTo('.bank_order_main');
    });

    $(document).on('click ', '.purchasing_agents_add', function (e) {
        e.preventDefault();
        $('<div class="purchasing_agents_list statement_list_pos"><div class="row"><div class="col-md-6 mt-3"><input type="text" name="purchasing_agents_name[]" class="form-control" maxlength="150" id="purchasing-agents-name" value=""></div><div class="col-md-6 mt-3"><input type="text" name="purchasing_agents_email[]" class="form-control" maxlength="150" id="purchasing-agents-email" value=""></div></div></div>').appendTo('.purchasing_agents_main');
    });

    $(document).on('click ', '.user_options_add', function (e) {
        e.preventDefault();
        var $div = $('.user_options_list').last();
        var $clone = $div.clone();
        $clone.appendTo('.user_options_main');
        // console.log('herrere',$tr);
    });


    var wrapper = document.getElementById("authorized_signature-pad");
    if (wrapper) {
        var clearButton = wrapper.querySelector("[data-action=clear]");
        window.orientation = resizeCanvas;
        var canvas = wrapper.querySelector("canvas");
        var authorized_signature_pad = new SignaturePad(canvas, {
            // It's Necessary to use an opaque color when saving image as JPEG;
            // this option can be omitted if only saving as PNG or SVG
            backgroundColor: 'rgb(255, 255, 255)',
            minWidth: 1
        });
        var signature_image = "";
        if ($('#order_id').length > 0) {

            var order_id = $('#order_id').val();

            if (order_id != "" && parseInt(order_id) > 0) {
                var filename = 'authorized_signature_' + order_id + '.jpg';
                signature_image = BASE_URL + 'webroot/img/Signs/' + filename;
            }
        }
        console.log(signature_image);
        console.log(BASE_URL);
        if (signature_image != "") {
            authorized_signature_pad.fromDataURL(signature_image);
        }

        clearButton.addEventListener("click", function (event) {
            authorized_signature_pad.clear();
        });

        var windowWidth = $(window).width();

        $(window).resize(function () {
            if ($(window).width() != windowWidth) {
                window.onresize = resizeCanvas;
                resizeCanvas();
            }
        });
        window.orientation = resizeCanvas;
        resizeCanvas();
    }

    //submission_date
    $('#dated').datepicker({dateFormat: "yy-mm-dd"});
    $('#submission_date').datepicker({dateFormat: "yy-mm-dd"});


    $(document).on('click', '#user_submit', function (e) {
        if (0) {
            alert("Please provide a signature first.");
        } else {
            var dataURL = authorized_signature_pad.toDataURL();
            var order_id = $('#order_id').val();
            var file = dataURLtoBlob(dataURL);
            var filename = 'authorized_signature_' + order_id + '.jpg';
            var formData = new FormData();
            console.log('order_id', order_id);

            formData.append('files', file, filename);
            formData.append('order_id', order_id);

            $.ajax({
                url: sign_ajax_url,
                headers: {
                    'X-CSRF-Token': csrfCustomerToken
                },
                type: 'post',
                async: true,
                crossDomain: true,
                mimeType: 'multipart/form-data',
                contentType: false,
                processData: false,
                dataType: 'json',
                data: formData,

                success: function (response) {
                    $('#authorized_signature').val(response.file_name);
                    $('#user_form').submit();
                    //localStorage.clear();
                },
                beforeSend: function () {
                    console.log('sign_ajax_url')
                },
                error: function (a, b, c) {
                    console.log('Error: ' + c);
                }

            });
        }
        return false;

    });

    if ($('#is_group').length > 0) {
        if ($('#is_group').val() == 0) {
            $('#group_option').hide();
        } else {
            $('#group_option').show();
        }
    }

    $(document).on('change keyup', '#is_group', function (e) {
        if ($('#is_group').val() == 0) {
            $('#group_option').hide();
        } else {
            $('#group_option').show();
        }

    });

    $('#date_installed').datepicker({dateFormat: "dd-mm-yy"});


    $(".update-trigger").click(function () {
        //alert('s');
        var id = $(this).attr('data-id');
        $("#mybill").attr('action', $("#mybill").attr('action')+'/edit/' + id);
    });
    $(".done-trigger").click(function () {
        if (confirm("Are you sure you want approve this bill?")) {
            var id = $(this).attr('data-id');
            //$("#mybillaccept").attr('action', 'bill/edit/' + id);
            $("#mybillaccept").attr('action', $("#mybillaccept").attr('action')+'/edit/' + id);
            document.getElementById("mybillaccept").submit();
        }
    });
    $(".close-trigger").click(function () {
        if (confirm("Do you want to close this bill?")) {
            var id = $(this).attr('data-id');
            //$("#billclose").attr('action', 'bill/edit/' + id);
            $("#billclose").attr('action', $("#mybillaccept").attr('action')+'/edit/' + id);
            document.getElementById("billclose").submit();
        }
    });
    $('.float-number').keypress(function (event) {
        if ((event.which != 46 || $(this).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57)) {
            event.preventDefault();
        }
    });

    $(".input label").each(function () {
        //$(this).siblings().attr('for', [input=name].val)
        //console.log($(this).text());
        var str = $(this).text();
        var res = str.replace("Billing", "");
        console.log(res);
        if (res.trim() == "Place Of Expense") {
            $(this).text("City of Expense");
        } else {
            $(this).text(res);
        }


    });
    $("tr th a").each(function () {
        //$(this).siblings().attr('for', [input=name].val)
        //console.log($(this).text());
        var str = $(this).text();
        var res = str.replace("Billing", "");
        if (res.trim() == "Place Of Expense") {
            $(this).text("City of Expense");
        } else {
            $(this).text(res);
        }
        //$(this).text(res);

    });
    $('input[name="is_test"]').attr("type", "checkbox");
});

function pattern() {

    $("input").each(function () {


        var input = $(this);
        var oldVal = input.val();
        if ($.isNumeric(oldVal)) {
            // alert('sss');
            if (oldVal != 0) {
                input.val(parseFloat(oldVal).toFixed(4));
            }
            //(function(el){el.value=parseFloat(el.value).toFixed(2);})(this)
        }

    });
}

function getamount() {
    var bill_amount = $("#billing-amount").val();
    var bill_tax = $("#billing-tax-amount").val();
    $("#billing-total-amount").val(parseFloat(bill_tax) + parseFloat(bill_amount));
}

function getdefaut() {
    $("#pricing-matrix tr.product_row").each(function () {

        var pricing = localStorage.getItem("pricingcal");
        var pricing = JSON.parse(pricing);
        //console.log(pricing.mill_cost);

        var parent2 = $(this);
        var update_sell_price = parent2.find('.sell_price').val();
        var update_rebate = parent2.find('.rebate').val();
        var update_select_val = parent2.find('.slab_select').val();
        var update_marginlic = parseFloat(pricing.mill_cost) / $('#defaut_value').val();

        //console.log("New Mill Cost --- "+update_marginlic);

        //	console.log($('#defaut_value').val());
        var update_brokragepersantagebrokragepersantage = pricing.brokerage_multiplier;

        //console.log("Brokarage Persantage --- "+update_brokragepersantagebrokragepersantage);
        var update_brokrage = update_brokragepersantagebrokragepersantage * update_marginlic;

        //console.log("Brokarage  --- "+update_brokrage);
        var update_freight = pricing.freight;

        //console.log("freight  --- "+update_freight);

        var updtae_ad_fund = update_marginlic * pricing.ad_fund_percentage;
        //
        //	console.log("ad_fund  --- "+updtae_ad_fund);


        var update_landed_cost_per_unit = parseFloat(update_marginlic) + parseFloat(update_brokrage) + parseFloat(update_freight) + parseFloat(updtae_ad_fund);

        //console.log("landed_cost_per_unit  --- "+update_landed_cost_per_unit);

        var update_load = update_landed_cost_per_unit * parseFloat(pricing.load_percentage);

        //console.log("load  --- "+update_load);


        var update_margin_lic = update_landed_cost_per_unit + update_load;

        //console.log("margin_lic" + update_margin_lic);


        if (update_sell_price != '' && update_rebate != '') {
            //console.log( 'sell_price=',sell_price,'margin_lic=',pricing.margin_lic );
            var update_gross_margin_level = (((parseFloat(update_sell_price) - (update_margin_lic)) + parseFloat(update_rebate)) / parseFloat(update_sell_price)) * 100;
            parent2.find('.gross_margin').prop('readonly', true).val(update_gross_margin_level.toFixed(2))
        }

    });

    get_defaut();

}

function get_defaut() {
    $("#pricing-matrix-new tr.new_product_row").each(function () {

        var pricing = localStorage.getItem("pricingcal");
        var pricing = JSON.parse(pricing);
        //console.log(pricing.mill_cost);

        var parent2 = $(this);
        var update_new_gross_margin = parent2.find('.new_gross_margin').val();
        var new_update_rebate = parent2.find('.rebate').val();
        //var update_select_val = parent2.find('.slab_select').val();
        var update_marginlic = parseFloat(pricing.mill_cost) / $('#defaut_value').val();

        //console.log("New Mill Cost --- "+update_marginlic);

        //console.log($('#defaut_value').val());
        var update_brokragepersantagebrokragepersantage = pricing.brokerage_multiplier;

        //console.log("Brokarage Persantage --- "+update_brokragepersantagebrokragepersantage);
        var update_brokrage = update_brokragepersantagebrokragepersantage * update_marginlic;

        //console.log("Brokarage  --- "+update_brokrage);
        var update_freight = pricing.freight;

        //console.log("freight  --- "+update_freight);

        var updtae_ad_fund = update_marginlic * pricing.ad_fund_percentage;
        //
        //console.log("ad_fund  --- "+updtae_ad_fund);


        var update_landed_cost_per_unit = parseFloat(update_marginlic) + parseFloat(update_brokrage) + parseFloat(update_freight) + parseFloat(updtae_ad_fund);

        //console.log("landed_cost_per_unit  --- "+update_landed_cost_per_unit);

        var update_load = update_landed_cost_per_unit * parseFloat(pricing.load_percentage);

        //console.log("load  --- "+update_load);


        var update_margin_lic = update_landed_cost_per_unit + update_load;

        //console.log("margin_lic" + update_margin_lic);


        if (update_new_gross_margin != '' && new_update_rebate != '') {
            //console.log( 'sell_price=',sell_price,'margin_lic=',pricing.margin_lic );

            var selprice = (parseFloat(update_margin_lic) / (1 - (parseFloat(update_new_gross_margin) / 100)));
            parent2.find('.new_sell_price').prop('readonly', true).val(selprice.toFixed(2))
            // var update_gross_margin_level = (( (parseFloat(update_sell_price) - (update_margin_lic) ) + parseFloat(update_rebate) )/parseFloat(update_sell_price))*100;
            // parent2.find('.gross_margin').prop('readonly', true).val(update_gross_margin_level.toFixed(2))
        }

    });

}

function tms_get_defaut() {
    $("#pricing-matrix-tms tr.tms_product_row").each(function () {

        var pricing = localStorage.getItem("pricingcal");
        var pricing = JSON.parse(pricing);
        //console.log(pricing.mill_cost);

        var parent2 = $(this);
        var update_tms_gross_margin = parent2.find('.tms_gross_margin').val();
        var tms_update_rebate = parent2.find('.tms_rebate').val();
        //var update_select_val = parent2.find('.slab_select').val();
        var update_marginlic = parseFloat(pricing.mill_cost) / $('#defaut_value').val();

        //console.log("New Mill Cost --- "+update_marginlic);

        //console.log($('#defaut_value').val());
        var update_brokragepersantagebrokragepersantage = pricing.brokerage_multiplier;

        //console.log("Brokarage Persantage --- "+update_brokragepersantagebrokragepersantage);
        var update_brokrage = update_brokragepersantagebrokragepersantage * update_marginlic;

        //console.log("Brokarage  --- "+update_brokrage);
        var update_freight = pricing.freight;

        //console.log("freight  --- "+update_freight);

        var updtae_ad_fund = update_marginlic * pricing.ad_fund_percentage;
        //
        //console.log("ad_fund  --- "+updtae_ad_fund);


        var update_landed_cost_per_unit = parseFloat(update_marginlic) + parseFloat(update_brokrage) + parseFloat(update_freight) + parseFloat(updtae_ad_fund);

        //console.log("landed_cost_per_unit  --- "+update_landed_cost_per_unit);

        var update_load = update_landed_cost_per_unit * parseFloat(pricing.load_percentage);

        //console.log("load  --- "+update_load);


        var update_margin_lic = update_landed_cost_per_unit + update_load;

        //console.log("margin_lic" + update_margin_lic);


        if (update_tms_gross_margin != '' && tms_update_rebate != '') {
            //console.log( 'sell_price=',sell_price,'margin_lic=',pricing.margin_lic );

            var tmsselprice = (parseFloat(update_margin_lic) / (1 - (parseFloat(update_tms_gross_margin) / 100)));
            parent2.find('.tms_sell_price').val(tmsselprice.toFixed(2))

        }

    });
}

function tmsgetdefaut() {
    $("#pricing-matrix-tms tr.tms_product_row").each(function () {

        var pricing = localStorage.getItem("pricingcal");
        var pricing = JSON.parse(pricing);
        //console.log(pricing.mill_cost);

        var parent2 = $(this);
        var update_tms_sell = parent2.find('.tms_sell_price').val();
        var tms_update_rebate = parent2.find('.tms_rebate').val();
        //var update_select_val = parent2.find('.slab_select').val();
        var update_marginlic = parseFloat(pricing.mill_cost) / $('#defaut_value').val();

        //console.log("New Mill Cost --- "+update_marginlic);

        //console.log($('#defaut_value').val());
        var update_brokragepersantagebrokragepersantage = pricing.brokerage_multiplier;

        //console.log("Brokarage Persantage --- "+update_brokragepersantagebrokragepersantage);
        var update_brokrage = update_brokragepersantagebrokragepersantage * update_marginlic;

        //console.log("Brokarage  --- "+update_brokrage);
        var update_freight = pricing.freight;

        //console.log("freight  --- "+update_freight);

        var updtae_ad_fund = update_marginlic * pricing.ad_fund_percentage;
        //
        //console.log("ad_fund  --- "+updtae_ad_fund);


        var update_landed_cost_per_unit = parseFloat(update_marginlic) + parseFloat(update_brokrage) + parseFloat(update_freight) + parseFloat(updtae_ad_fund);

        //console.log("landed_cost_per_unit  --- "+update_landed_cost_per_unit);

        var update_load = update_landed_cost_per_unit * parseFloat(pricing.load_percentage);

        //console.log("load  --- "+update_load);


        var update_margin_lic = update_landed_cost_per_unit + update_load;

        //console.log("margin_lic" + update_margin_lic);


        if (update_tms_sell != '' && tms_update_rebate != '') {
            //console.log( 'sell_price=',sell_price,'margin_lic=',pricing.margin_lic );

            //var tmsselprice = (parseFloat(update_margin_lic)/(1-(parseFloat(update_tms_gross_margin)/100)));
            //parent2.find('.tms_sell_price').prop('readonly', true).val(tmsselprice.toFixed(2))


            var update_gross_margin_level = (((parseFloat(update_tms_sell) - (update_margin_lic)) + parseFloat(tms_update_rebate)) / parseFloat(update_tms_sell)) * 100;
            parent2.find('.tms_gross_margin').val(update_gross_margin_level.toFixed(3));
        }

    });
}

function gettmsdefaut() {
    var cars = [10, 15, 20];

    //console.log(cars[1]);
    //return false;
    var i = 0;
    $("#pricing-matrix-tms tr.tms_product_row").each(function () {
        var pricing = localStorage.getItem("pricingcal");
        var pricing = JSON.parse(pricing);
        //console.log(pricing.mill_cost);

        var parent2 = $(this);
        var update_tms_gross_margin = parent2.find('.tms_gross_margin').val();
        var tms_update_rebate = parent2.find('.tms_rebate').val();
        //var update_select_val = parent2.find('.slab_select').val();
        var update_marginlic = parseFloat(pricing.mill_cost) / $('#defaut_value').val();

        //console.log("New Mill Cost --- "+update_marginlic);

        //console.log($('#defaut_value').val());
        var update_brokragepersantagebrokragepersantage = pricing.brokerage_multiplier;

        //console.log("Brokarage Persantage --- "+update_brokragepersantagebrokragepersantage);
        var update_brokrage = update_brokragepersantagebrokragepersantage * update_marginlic;

        //console.log("Brokarage  --- "+update_brokrage);
        var update_freight = pricing.freight;

        //console.log("freight  --- "+update_freight);

        var updtae_ad_fund = update_marginlic * pricing.ad_fund_percentage;
        //
        //console.log("ad_fund  --- "+updtae_ad_fund);


        var update_landed_cost_per_unit = parseFloat(update_marginlic) + parseFloat(update_brokrage) + parseFloat(update_freight) + parseFloat(updtae_ad_fund);

        //console.log("landed_cost_per_unit  --- "+update_landed_cost_per_unit);

        var update_load = update_landed_cost_per_unit * parseFloat(pricing.load_percentage);

        //console.log("load  --- "+update_load);


        var update_margin_lic = update_landed_cost_per_unit + update_load;

        //console.log("margin_lic" + update_margin_lic);

        var newupdate_margin_lic = (update_margin_lic * cars[i] / 100) + update_margin_lic
        if (update_tms_gross_margin != '' && tms_update_rebate != '') {
            //console.log( 'sell_price=',sell_price,'margin_lic=',pricing.margin_lic );

            var tmsselprice = (parseFloat(newupdate_margin_lic) / (1 - (parseFloat(update_tms_gross_margin) / 100)));
            parent2.find('.tms_sell_price').val(tmsselprice.toFixed(2))

        }


        i++;
    });
    setTimeout(function () {
        tmsgetdefaut();
    }, 2000);
}

function getproduct(salesman_id) {
    if (salesman_id) {
        var url = $('.search_name_margin').attr('data-link');
        //console.log(url);
        //return false;
        $.ajax({
            url: url,
            headers: {
                'X-CSRF-Token': csrfCustomerToken
            },
            type: 'post',
            //async:true,
            crossDomain: true,
            data: {id: salesman_id},
            success: function (response) {
                if (response) {
                    if (response.length > 0) {
                        pricing = {};
                        pricing = response[0];

                        localStorage.setItem('pricingcal', JSON.stringify(pricing));

                        //$('#suggesstion-pricing').html('');

                        $('#slab_1').prop('readonly', true);
                        $('#slab_2').prop('readonly', true);
                        $('#slab_3').prop('readonly', true);

                        $('#sell-price-level1').prop('readonly', true).val(pricing.sell_price_level1);
                        $('#sell_tms_1').val(pricing.sell_price_level1);
                        $('#sell_ms_1').prop('readonly', true).val(pricing.sell_price_level1);
                        $('#rebate-level1, #rebate_ms_1, #rebate_tms_1').val(pricing.rebate_level1);

                        $('#sell_sm_1').text(pricing.sell_price_level1);
                        $('#sell_sm_2').text(pricing.sell_price_level2);
                        $('#sell_sm_3').text(pricing.sell_price_level3);

                        $('#rebate_sm_1').text(pricing.rebate_level1);
                        $('#rebate_sm_2').text(pricing.rebate_level2);
                        $('#rebate_sm_3').text(pricing.rebate_level3);

                        $('#gross_sm_1').text(pricing.gross_margin_level1);
                        $('#gross_sm_2').text(pricing.gross_margin_level2);
                        $('#gross_sm_3').text(pricing.gross_margin_level3);

                        $('#gross-margin-level1').prop('readonly', true).val(pricing.gross_margin_level1);
                        $('#gross_ms_1').prop('readonly', true).val(pricing.gross_margin_level1);

                        $('#defaut_value').prop('readonly', true).val(pricing.conversion_rate);

                        $('#sell-price-level2').prop('readonly', true).val(pricing.sell_price_level2);
                        $('#sell_tms_2').val(pricing.sell_price_level2);
                        $('#sell_ms_2').prop('readonly', true).val(pricing.sell_price_level2);

                        $('#rebate-level2, #rebate_tms_2').val(pricing.rebate_level2);
                        $('#rebate_ms_2').prop('readonly', true).val(pricing.rebate_level2);


                        $('#gross-margin-level2').prop('readonly', true).val(pricing.gross_margin_level2);
                        $('#gross_ms_2').prop('readonly', true).val(pricing.gross_margin_level2);

                        $('#sell-price-level3').prop('readonly', true).val(pricing.sell_price_level3);
                        $('#sell_tms_3').val(pricing.sell_price_level3);

                        $('#sell_ms_3').prop('readonly', true).val(pricing.sell_price_level3);

                        $('#rebate-level3, #rebate_tms_3').val(pricing.rebate_level3);
                        $('#rebate_ms_3').prop('readonly', true).val(pricing.rebate_level3);

                        $('#gross-margin-level3').prop('readonly', true).val(pricing.gross_margin_level3);
                        $('#gross_ms_3').prop('readonly', true).val(pricing.gross_margin_level3);

                        $('#gross_tms_1').val(pricing.gross_margin_level1);
                        $('#gross_tms_2').val(pricing.gross_margin_level2);
                        $('#gross_tms_3').val(pricing.gross_margin_level3);

                        $('.search_name, .search_name_margin').val(pricing.name);
                        $('#suggesstion-pricing').hide();
                        console.log(response);
                        getdefaut();
                        pattern();
                    }
                }
            },
            beforeSend: function () {
                console.log('sign_ajax_url')
            },
            error: function (a, b, c) {
                console.log('Error: ' + c);
            }

        });
    }
}

function empty(value) {
    value = value;
    return value === '' || value === null || value === 'null' || value === 'undefined' || value === undefined;
}
