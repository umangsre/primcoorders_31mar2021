<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\CurrencyConversionRate $currencyConversionRate
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Currency Conversion Rate'), ['action' => 'edit', $currencyConversionRate->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Currency Conversion Rate'), ['action' => 'delete', $currencyConversionRate->id], ['confirm' => __('Are you sure you want to delete # {0}?', $currencyConversionRate->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Currency Conversion Rates'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Currency Conversion Rate'), ['action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="currencyConversionRates view large-9 medium-8 columns content">
    <h3><?= h($currencyConversionRate->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($currencyConversionRate->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Cad') ?></th>
            <td><?= $this->Number->format($currencyConversionRate->cad) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Usd Mannington') ?></th>
            <td><?= $this->Number->format($currencyConversionRate->usd_mannington) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Usd General') ?></th>
            <td><?= $this->Number->format($currencyConversionRate->usd_general) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Usd Others') ?></th>
            <td><?= $this->Number->format($currencyConversionRate->usd_others) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Usd Custom 1') ?></th>
            <td><?= $this->Number->format($currencyConversionRate->usd_custom_1) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Usd Custom 2') ?></th>
            <td><?= $this->Number->format($currencyConversionRate->usd_custom_2) ?></td>
        </tr>
		<tr>
            <th scope="row"><?= __('Usd Custom 3') ?></th>
            <td><?= $this->Number->format($currencyConversionRate->usd_custom_3) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Usd Custom 4') ?></th>
            <td><?= $this->Number->format($currencyConversionRate->usd_custom_4) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Created') ?></th>
            <td><?= h($currencyConversionRate->created) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Updated') ?></th>
            <td><?= h($currencyConversionRate->updated) ?></td>
        </tr>
    </table>
</div>
