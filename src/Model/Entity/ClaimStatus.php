<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Retailer Entity
 *
 * @property int $id
 * @property string|null $claim_id
 * @property string|null $status
 * @property string|null $status_comments
 * @property string|null $status_by
 * @property \Cake\I18n\FrozenTime|null $created_at
 *
// * @property \App\Model\Entity\Claim[] $claims
 */
class ClaimStatus extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'claim_id' => true,
        'status' => true,
        'status_comments' => true,
        'status_by' => true,
        'created_at' => true,
    ];
}
