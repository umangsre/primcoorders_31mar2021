<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\CurrencyConversionRatesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\CurrencyConversionRatesTable Test Case
 */
class CurrencyConversionRatesTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\CurrencyConversionRatesTable
     */
    public $CurrencyConversionRates;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.CurrencyConversionRates'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('CurrencyConversionRates') ? [] : ['className' => CurrencyConversionRatesTable::class];
        $this->CurrencyConversionRates = TableRegistry::getTableLocator()->get('CurrencyConversionRates', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->CurrencyConversionRates);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
