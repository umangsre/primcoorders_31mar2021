<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\CreditApplication $creditApplication
 */
?>
<!--nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $creditApplication->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $creditApplication->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Credit Applications'), ['action' => 'index']) ?></li>
		<li><?= $this->Html->link(__('Edit ContactsList',['class'=>'btn']), ['controller' =>'ContactsList','action' => 'edit',base64_encode($creditApplication->id)]) ?>
	</li>
	<li> <?= $this->Html->link(__('Edit Online Users',['class'=>'btn']), ['controller' =>'OnlineUserRegistration','action' => 'edit',base64_encode($creditApplication->id)]) ?> </li>
    </ul>
</nav -->
<div class="orders index large-9 medium-8 columns content card card card-default scrollspy">
    <div class="card-content">
        <div class="creditApplications form large-9 medium-8 columns content">
            <?= $this->Form->create($creditApplication) ?>
            <fieldset>
                <legend><?= __('Edit Credit Application') ?></legend>
                <?php
                    echo $this->Form->control('company_legal_name');
                    echo $this->Form->control('company_trade_name');
                    echo $this->Form->control('street_address');
                    echo $this->Form->control('phone');
                    echo $this->Form->control('fax');
                    echo $this->Form->control('city');
                    echo $this->Form->control('province');
                    echo $this->Form->control('postal_code');
                    echo $this->Form->control('email_address');
                    echo $this->Form->control('owner_name');
                    echo $this->Form->control('owner_home_address');
                    echo $this->Form->control('owner_home_phone');
                    echo $this->Form->control('owner_cell_phone');
                    echo $this->Form->control('manager_name');
                    echo $this->Form->control('manager_phone');
                    echo $this->Form->control('manager_finacial_address');
                    echo $this->Form->control('manager_bank_account');
                    echo $this->Form->control('manager_city');
                    echo $this->Form->control('manager_province');
                    echo $this->Form->control('manager_postal_code');
                    echo $this->Form->control('manager_phone_2');
                    echo $this->Form->control('manager_gst');
                    echo $this->Form->control('province_sales_tax');
                    echo $this->Form->control('amount_credit_required');
                    echo $this->Form->control('yearly_sales_volume');
                    echo $this->Form->control('fiscal_year_end');
                    echo $this->Form->control('is_group');
                    echo $this->Form->control('group_name');
                   
                ?>
            </fieldset>
            <?= $this->Form->button(__('Submit'),['class' => 'btn waves-effect waves-light border-round gradient-45deg-purple-deep-orange col s12 mt-4']) ?>
        	
            <?= $this->Form->end() ?>
        </div>
    </div>
</div>
