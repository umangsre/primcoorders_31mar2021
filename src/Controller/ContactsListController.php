<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;
use Cake\Event\Event;
use Cake\Mailer\Email;
use Cake\Routing\Router;
use \PHPQRCode\QRcode;

/**
 * ContactsList Controller
 *
 * @property \App\Model\Table\ContactsListTable $ContactsList
 *
 * @method \App\Model\Entity\ContactsList[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class ContactsListController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
	 
	public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);
        $this->Auth->allow(['addContact', 'viewContact', 'editContact', 'indexContact']);
    }
	
	public function indexContact()
    {
        $contactsList = $this->paginate($this->ContactsList);

        $this->set(compact('contactsList'));
    }

    /**
     * View method
     *
     * @param string|null $id Contacts List id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function viewContact($id = null)
    {
        $contactsList = $this->ContactsList->get($id, [
            'contain' => []
        ]);

        $this->set('contactsList', $contactsList);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function addContact($id = null)
    {
		if ( $id == null ) {
            return $this->redirect( $this->referer() );
        }
        $id = base64_decode($id);

        $contactsList = $this->ContactsList->newEntity();
        if ($this->request->is('post')) {
			$contact_array =[];
			foreach($this->request->getData() as $key => $val){
				$contact_array[$key] = serialize( $val);
			   }
			$contactsList->credit_application_id =$id;   
            $contactsList = $this->ContactsList->patchEntity($contactsList,$contact_array);
           
            if ($this->ContactsList->save($contactsList)) {
                $this->Flash->success(__('The contacts list has been saved.'));
				
				
                return $this->redirect(['controller' =>'OnlineUserRegistration' ,'action' => 'addUser',base64_encode($id)]);
               // return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The contacts list could not be saved. Please, try again.'));
        }
        $this->set(compact('contactsList','id'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Contacts List id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function editContact($id = null)
    {
		if ( $id == null ) {
            return $this->redirect( $this->referer() );
        }
        $id = base64_decode($id);
		
        $contactsList = $this->ContactsList->find('all', [
			'conditions' => ['credit_application_id' => $id],
            'contain' => []
        ])->first();
		
        /* $contactsList = $this->ContactsList->get($id, [
            'contain' => []
        ]); */
        if ($this->request->is(['patch', 'post', 'put'])) {
			$contact_array =[];
			foreach($this->request->getData() as $key => $val){
				$contact_array[$key] = serialize( $val);
			   }
            $contactsList = $this->ContactsList->patchEntity($contactsList, $contact_array);
			
            if ($this->ContactsList->save($contactsList)) {
                $this->Flash->success(__('The contacts list has been saved.'));

                return $this->redirect(['controller' =>'OnlineUserRegistration' ,'action' => 'editUser',base64_encode($id)]);
            }else{
				
			}
            $this->Flash->error(__('The contacts list could not be saved. Please, try again.'));
        }
        $this->set(compact('contactsList','id'));
    }
	
	
	
    public function index()
    {
        $contactsList = $this->paginate($this->ContactsList);

        $this->set(compact('contactsList'));
    }

    /**
     * View method
     *
     * @param string|null $id Contacts List id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $contactsList = $this->ContactsList->get($id, [
            'contain' => []
        ]);

        $this->set('contactsList', $contactsList);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $contactsList = $this->ContactsList->newEntity();
        if ($this->request->is('post')) {
            $contactsList = $this->ContactsList->patchEntity($contactsList, $this->request->getData());
			
            if ($this->ContactsList->save($contactsList)) {
                $this->Flash->success(__('The contacts list has been saved.'));

                return $this->redirect(['action' => 'index']);
            }else{
				
			}
            $this->Flash->error(__('The contacts list could not be saved. Please, try again.'));
        }
        $this->set(compact('contactsList'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Contacts List id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
		$this->viewBuilder()->setLayout('dashboard');
		if ( $id == null ) {
            return $this->redirect( $this->referer() );
        }
        $id = base64_decode($id);
		
        $contactsList = $this->ContactsList->find('all', [
			'conditions' => ['credit_application_id' => $id],
            'contain' => []
        ])->first();
        if ($this->request->is(['patch', 'post', 'put'])) {
			$contact_array =[];
			foreach($this->request->getData() as $key => $val){
				$contact_array[$key] = serialize( $val);
			}
            $contactsList = $this->ContactsList->patchEntity($contactsList, $contact_array);
            if ($this->ContactsList->save($contactsList)) {
                $this->Flash->success(__('The contacts list has been saved.'));

                return $this->redirect(['controller' =>'CreditApplications','action' => 'index']);
            }else{
			}
            $this->Flash->error(__('The contacts list could not be saved. Please, try again.'));
        }
        $this->set(compact('contactsList'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Contacts List id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $contactsList = $this->ContactsList->get($id);
        if ($this->ContactsList->delete($contactsList)) {
            $this->Flash->success(__('The contacts list has been deleted.'));
        } else {
            $this->Flash->error(__('The contacts list could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
