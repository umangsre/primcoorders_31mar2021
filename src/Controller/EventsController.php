<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Routing\Router;
/**
 * Events Controller
 *
 * @property \App\Model\Table\EventsTable $Events
 *
 * @method \App\Model\Entity\Event[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class EventsController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $this->viewBuilder()->setLayout('dashboard');
         $query = $this->Events->find();
        if($this->request->is('post')){
         $search = '%'.$this->request->getData('search').'%';
         $query->where(['OR' =>['name LIKE' => '%'.$search.'%','description LIKE' => '%'.$search.'%','location LIKE' => '%'.$search.'%']]);
       }
        $events = $this->paginate($query);

        $this->set(compact('events'));
    }

    /**
     * View method
     *
     * @param string|null $id Event id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $this->viewBuilder()->setLayout('dashboard');
        $event = $this->Events->get($id, [
            'contain' => ['SubEvents']
        ]);

        $this->set('event', $event);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $this->viewBuilder()->setLayout('dashboard');
        $event = $this->Events->newEntity();
        if ($this->request->is('post')) {
            $event = $this->Events->patchEntity($event, $this->request->getData());
             // print_r( $this->request->getData());
             // print_r( $_FILES);
           
            if($_FILES){
                $file = $_FILES['image'];
                //print_r($file);
                $ext = substr(strtolower(strrchr($file['name'], '.')), 1); //get the extension
                $arr_ext = array('jpg', 'jpeg', 'gif','png'); //set allowed extensions
                $file_name = 'events_'.$file['name'];
                //only process if the extension is valid
                if(in_array($ext, $arr_ext))
                {
                     //die('here ');
                    $path =  WWW_ROOT . 'img/'. $file_name;
                    //do the actual uploading of the file. First arg is the tmp name, second arg is
                    //where we are putting it
                    if(move_uploaded_file($file['tmp_name'], $path)){
                         $base_urls =  Router::url('/', true) ;
                         $sign_link =  $base_urls .'img/' .$file_name;  
                         $event->image = $sign_link;
                    }else{
                        $event->image=''; 
                    }

                    
                }else{
                    $event->image='';
                }
                
           }

            if ($this->Events->save($event)) {
                $this->Flash->success(__('The event has been saved.'));

                return $this->redirect(['action' => 'index']);
            }else{
                print_r($event->getErrors());
            }
            $this->Flash->error(__('The event could not be saved. Please, try again.'));
        }
        $this->set(compact('event'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Event id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $this->viewBuilder()->setLayout('dashboard');
        $ev = $event = $this->Events->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $event = $this->Events->patchEntity($event, $this->request->getData());
            print_r($_FILES);
             if( $_FILES['image']['size'] > 0 ){

               // die('if');

           
                $file = $_FILES['image'];
                //print_r($file);
                $ext = substr(strtolower(strrchr($file['name'], '.')), 1); //get the extension
                $arr_ext = array('jpg', 'jpeg', 'gif','png'); //set allowed extensions
                $file_name = 'events_'.$file['name'];
                //only process if the extension is valid
                if(in_array($ext, $arr_ext))
                {
                    $path =  WWW_ROOT . 'img/'. $file_name;
                    //do the actual uploading of the file. First arg is the tmp name, second arg is
                    //where we are putting it
                    if(move_uploaded_file($file['tmp_name'], $path)){
                         $base_urls =  Router::url('/', true) ;
                         $sign_link =  $base_urls .'img/' .$file_name;  
                         $event->image = $sign_link;
                    }

                    
                }else{
                   $event->image ='';  
                }
                
           }else{
                $ev = $this->Events->get($id, [
                    'contain' => []
                ]);
                $event->image =$ev->image;
                // die('else');
           }
            if ($this->Events->save($event)) {
                $this->Flash->success(__('The event has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            //dd($this->Events->errors());
            $this->Flash->error(__('The event could not be saved. Please, try again.'));
        }
        $this->set(compact('event'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Event id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->viewBuilder()->setLayout('dashboard');
        $this->request->allowMethod(['post', 'delete']);
        $event = $this->Events->get($id);
        if ($this->Events->delete($event)) {
            $this->Flash->success(__('The event has been deleted.'));
        } else {
            $this->Flash->error(__('The event could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
