<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * SubEvent Entity
 *
 * @property int $id
 * @property int $event_id
 * @property string $_event
 * @property string $_time
 * @property \Cake\I18n\FrozenTime|null $from_time
 * @property \Cake\I18n\FrozenTime|null $to_time
 * @property \Cake\I18n\FrozenTime|null $created
 *
 * @property \App\Model\Entity\Event $event
 */
class SubEvent extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'event_id' => true,
        'name' => true,
        'description' => true,
        'event_date' => true,
        'from_time' => true,
        'to_time' => true,
        'created' => true,
        'event' => true
    ];
}
