<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Family[]|\Cake\Collection\CollectionInterface $families
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Family'), ['action' => 'add']) ?></li>
    </ul>
</nav>
<div class="families index large-9 medium-8 columns content">
    <h3><?= __('Families') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('family') ?></th>
                <th scope="col"><?= $this->Paginator->sort('name') ?></th>
                <th scope="col"><?= $this->Paginator->sort('product_code') ?></th>
                <th scope="col"><?= $this->Paginator->sort('role_cut') ?></th>
                <th scope="col"><?= $this->Paginator->sort('mill_cost') ?></th>
                <th scope="col"><?= $this->Paginator->sort('usd_cad') ?></th>
                <th scope="col"><?= $this->Paginator->sort('conversion_rate') ?></th>
                <th scope="col"><?= $this->Paginator->sort('selling_price') ?></th>
                <th scope="col"><?= $this->Paginator->sort('brokerage_multiplier') ?></th>
                <th scope="col"><?= $this->Paginator->sort('brokerage') ?></th>
                <th scope="col"><?= $this->Paginator->sort('units') ?></th>
                <th scope="col"><?= $this->Paginator->sort('freight') ?></th>
                <th scope="col"><?= $this->Paginator->sort('ad_fund_percentage') ?></th>
                <th scope="col"><?= $this->Paginator->sort('ad_fund') ?></th>
                <th scope="col"><?= $this->Paginator->sort('landed_cost_per_unit') ?></th>
                <th scope="col"><?= $this->Paginator->sort('load_percentage') ?></th>
                <th scope="col"><?= $this->Paginator->sort('_load') ?></th>
                <th scope="col"><?= $this->Paginator->sort('margin_lic') ?></th>
                <th scope="col"><?= $this->Paginator->sort('sell_price_level1') ?></th>
                <th scope="col"><?= $this->Paginator->sort('rebate_level1') ?></th>
                <th scope="col"><?= $this->Paginator->sort('gross_margin_level1') ?></th>
                <th scope="col"><?= $this->Paginator->sort('sell_price_level2') ?></th>
                <th scope="col"><?= $this->Paginator->sort('rebate_level2') ?></th>
                <th scope="col"><?= $this->Paginator->sort('gross_margin_level2') ?></th>
                <th scope="col"><?= $this->Paginator->sort('sell_price_level3') ?></th>
                <th scope="col"><?= $this->Paginator->sort('rebate_level3') ?></th>
                <th scope="col"><?= $this->Paginator->sort('gross_margin_level3') ?></th>
                <th scope="col"><?= $this->Paginator->sort('mill') ?></th>
                <th scope="col"><?= $this->Paginator->sort('vendorID') ?></th>
                <th scope="col"><?= $this->Paginator->sort('sqft_ctn') ?></th>
                <th scope="col"><?= $this->Paginator->sort('wgt_lbs_ctn') ?></th>
                <th scope="col"><?= $this->Paginator->sort('ctn_pallet') ?></th>
                <th scope="col"><?= $this->Paginator->sort('sqft_pallet') ?></th>
                <th scope="col"><?= $this->Paginator->sort('pallet_container_20') ?></th>
                <th scope="col"><?= $this->Paginator->sort('sqft_container_20') ?></th>
                <th scope="col"><?= $this->Paginator->sort('sqft_roll') ?></th>
                <th scope="col"><?= $this->Paginator->sort('sqyd_roll') ?></th>
                <th scope="col"><?= $this->Paginator->sort('created') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($families as $family): ?>
            <tr>
                <td><?= $this->Number->format($family->id) ?></td>
                <td><?= h($family->family) ?></td>
                <td><?= h($family->name) ?></td>
                <td><?= h($family->product_code) ?></td>
                <td><?= h($family->role_cut) ?></td>
                <td><?= $this->Number->format($family->mill_cost) ?></td>
                <td><?= h($family->usd_cad) ?></td>
                <td><?= $this->Number->format($family->conversion_rate) ?></td>
                <td><?= $this->Number->format($family->selling_price) ?></td>
                <td><?= $this->Number->format($family->brokerage_multiplier) ?></td>
                <td><?= $this->Number->format($family->brokerage) ?></td>
                <td><?= h($family->units) ?></td>
                <td><?= $this->Number->format($family->freight) ?></td>
                <td><?= $this->Number->format($family->ad_fund_percentage) ?></td>
                <td><?= $this->Number->format($family->ad_fund) ?></td>
                <td><?= $this->Number->format($family->landed_cost_per_unit) ?></td>
                <td><?= $this->Number->format($family->load_percentage) ?></td>
                <td><?= $this->Number->format($family->_load) ?></td>
                <td><?= $this->Number->format($family->margin_lic) ?></td>
                <td><?= $this->Number->format($family->sell_price_level1) ?></td>
                <td><?= $this->Number->format($family->rebate_level1) ?></td>
                <td><?= $this->Number->format($family->gross_margin_level1) ?></td>
                <td><?= $this->Number->format($family->sell_price_level2) ?></td>
                <td><?= $this->Number->format($family->rebate_level2) ?></td>
                <td><?= $this->Number->format($family->gross_margin_level2) ?></td>
                <td><?= $this->Number->format($family->sell_price_level3) ?></td>
                <td><?= $this->Number->format($family->rebate_level3) ?></td>
                <td><?= $this->Number->format($family->gross_margin_level3) ?></td>
                <td><?= h($family->mill) ?></td>
                <td><?= h($family->vendorID) ?></td>
                <td><?= $this->Number->format($family->sqft_ctn) ?></td>
                <td><?= $this->Number->format($family->wgt_lbs_ctn) ?></td>
                <td><?= $this->Number->format($family->ctn_pallet) ?></td>
                <td><?= $this->Number->format($family->sqft_pallet) ?></td>
                <td><?= $this->Number->format($family->pallet_container_20) ?></td>
                <td><?= $this->Number->format($family->sqft_container_20) ?></td>
                <td><?= $this->Number->format($family->sqft_roll) ?></td>
                <td><?= $this->Number->format($family->sqyd_roll) ?></td>
                <td><?= h($family->created) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $family->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $family->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $family->id], ['confirm' => __('Are you sure you want to delete # {0}?', $family->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div>
