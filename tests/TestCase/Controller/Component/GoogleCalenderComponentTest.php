<?php
namespace App\Test\TestCase\Controller\Component;

use App\Controller\Component\GoogleCalenderComponent;
use Cake\Controller\ComponentRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Controller\Component\GoogleCalenderComponent Test Case
 */
class GoogleCalenderComponentTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Controller\Component\GoogleCalenderComponent
     */
    public $GoogleCalender;

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $registry = new ComponentRegistry();
        $this->GoogleCalender = new GoogleCalenderComponent($registry);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->GoogleCalender);

        parent::tearDown();
    }

    /**
     * Test initial setup
     *
     * @return void
     */
    public function testInitialization()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
