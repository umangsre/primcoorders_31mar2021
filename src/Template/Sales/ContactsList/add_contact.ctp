<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\ContactsList $contactsList
 */
$options = array(
                    '1' => 'Yes',
                    '0' => 'No'
                  );
?>
<div class="row">
    <div class="col-12 logo-container text-center p-4">
        <?= $this->Html->image('logo.png'); ?>           
    </div>
</div>

<div class="contactsList form large-9 medium-8 columns content container">
    <?= $this->Form->create($contactsList,['id' =>'contact_form']) ?>
    <div class="row">
        <h2 class="_heading"><?= __('Add Contact List') ?></h2>
	</div>

	<div class="statement_main statement_main_pos">		
		<div class="statement_list statement_list_pos">
			<h6 class="mr_10">Statements</h6>
			<div class="row">
				
				<div class="col-md-6 mt-3">
				<?php
					echo $this->Form->control('statement_name[]',['class' => 'form-control','value' =>'','required' => 'required', 'label' => 'Name*:' ]); ?>
				</div>
				<div class="col-md-6 mt-3">				
					<?php echo $this->Form->control('statement_email[]',['class' => 'form-control','value' =>'','type' =>'email','required' => 'required', 'label' => 'Email*:'  ]); ?>
				</div>
			</div>
		</div>
		<button type="button" class="btn btn-info statement_add statement_add_pos" style="top: 68px;">+</button>
	 </div>		
	<div class="invoice_main statement_main_pos">	
		<div class="invoice_list statement_list_pos">
			<h6 class="mr1_10">Invoices</h6>
			<div class="row">
				<div class="col-sm-6 col-12 mt-3">
					<?php echo $this->Form->control('invoice_name[]',['class' => 'form-control','value' =>'','required' => 'required', 'label' => 'Name*:'  ]); ?>
				</div>
				<div class="col-sm-6 col-12 mt-3">	
					<?php echo $this->Form->control('invoice_email[]',['class' => 'form-control','value' =>'','type' =>'email','required' => 'required', 'label' => 'Email*:'  ]); ?>
				</div>
			</div>
		</div>
		<button type="button"  class="btn btn-info invoice_add statement_add_pos">+</button>
	</div>
	<div class="price_pages_main statement_main_pos">		
		<div class="price_pages_list statement_list_pos">
			<h6 class="mr1_10">Price Pages</h6>
			<div class="row">	
				<div class="col-sm-6 col-12 mt-3">	
					<?php echo $this->Form->control('price_pages_name[]',['class' => 'form-control','value' =>'','required' => 'required', 'label' => 'Name*:'  ]);?>
				</div>
				<div class="col-sm-6 col-12 mt-3">	
					<?php echo $this->Form->control('price_pages_email[]',['class' => 'form-control','value' =>'','type' =>'email','required' => 'required', 'label' => 'Email*:'  ]); ?>
				</div>
			</div>
		</div>
		<button type="button" class="btn btn-info price_pages_add statement_add_pos">+</button>
	</div>
    <div class="order_confirmation_main statement_main_pos">		
		<div class="order_confirmation_list statement_list_pos">
			<h6 class="mr1_10">Order Confirmation</h6>
			<div class="row">
					<div class="col-sm-6 col-12 mt-3">	
			           <?php echo $this->Form->control('order_confirmation_name[]',['class' => 'form-control','value' =>'','required' => 'required', 'label' => 'Name*:'  ]); ?>
					  </div>
					<div class="col-sm-6 col-12 mt-3"> 
			            <?php echo $this->Form->control('order_confirmation_email[]',['class' => 'form-control','value' =>'','type' =>'email','required' => 'required', 'label' => 'Email*:'  ]); ?>
					</div>
			</div>
	     </div>
		 <button type="button" class="btn btn-info order_confirmation_add statement_add_pos">+</button>
	</div>

	<div class="bank_order_main statement_main_pos">	
		<div class="bank_order_list statement_list_pos">
			<h6 class="mr1_10">Notification of Back Order Arrival</h6>
			<div class="row">
				<div class="col-sm-6 col-12 mt-3">	
		            <?php echo $this->Form->control('bank_order_name[]',['class' => 'form-control','value' =>'','required' => 'required', 'label' => 'Name*:'  ]); ?>
				</div>
				<div class="col-sm-6 col-12 mt-3">	
		            <?php echo $this->Form->control('bank_order_email[]',['class' => 'form-control','value' =>'','type' =>'email','required' => 'required', 'label' => 'Email*:'  ]); ?>
				</div>
	         </div>
		</div>
		<button type="button" class="btn btn-info bank_order_add statement_add_pos">+</button>
	</div>
	<div class="purchasing_agents_main statement_main_pos">	
		<div class="purchasing_agents_list statement_list_pos">
			<h6 class="mr1_10">Purchaging Agents</h6>
			<div class="row">
				<div class="col-sm-6 col-12 mt-3">	
		            <?php echo $this->Form->control('purchasing_agents_name[]',['class' => 'form-control','value' =>'','required' => 'required', 'label' => 'Name*:'  ]); ?>
				</div>
				<div class="col-sm-6 col-12 mt-3">	
		            <?php echo $this->Form->control('purchasing_agents_email[]',['class' => 'form-control','value' =>'','type' =>'email','required' => 'required', 'label' => 'Email*:'  ]); ?>
				</div>
	    	</div>
		</div>
		<button type="button" class="btn btn-info purchasing_agents_add statement_add_pos">+</button>
	</div>
	
	
	<div class="col-sm-6 col-12 mt-3 button_right">
		<?php 
		/*
		echo $this->Html->link(
                  'Back',
                  ['controller' => 'CreditApplications', 'action' => 'addCustomer',base64_encode($id )],
                  ['class' => 'btn btn-success']
                ); 
                */ 
         ?>	
		<?= $this->Form->button(__('Next'),['contact_submit' => '', 'class' => 'btn btn-primary btn_right']) ?>
	</div>
    <?= $this->Form->end() ?>
</div>

