<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * SubEvents Controller
 *
 * @property \App\Model\Table\SubEventsTable $SubEvents
 *
 * @method \App\Model\Entity\SubEvent[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class SubEventsController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $this->viewBuilder()->setLayout('dashboard');
        $this->paginate = [
            'contain' => ['Events']
        ];
        $subEvents = $this->paginate($this->SubEvents);

        $this->set(compact('subEvents'));
    }

    /**
     * View method
     *
     * @param string|null $id Sub Event id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $this->viewBuilder()->setLayout('dashboard');
        $subEvent = $this->SubEvents->get($id, [
            'contain' => ['Events']
        ]);

        $this->set('subEvent', $subEvent);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $this->viewBuilder()->setLayout('dashboard');
        $subEvent = $this->SubEvents->newEntity();
        if ($this->request->is('post')) {
            $subEvent = $this->SubEvents->patchEntity($subEvent, $this->request->getData());
            if ($this->SubEvents->save($subEvent)) {
                $this->Flash->success(__('The sub event has been saved.'));

                return $this->redirect(['controller' =>'Events', 'action' => 'index']);
            }
            dd($subEvent);
            $this->Flash->error(__('The sub event could not be saved. Please, try again.'));
        }
        $events = $this->SubEvents->Events->find('list', ['limit' => 200]);
        $this->set(compact('subEvent', 'events'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Sub Event id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $this->viewBuilder()->setLayout('dashboard');
        $subEvent = $this->SubEvents->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $subEvent = $this->SubEvents->patchEntity($subEvent, $this->request->getData());
            if ($this->SubEvents->save($subEvent)) {
                $this->Flash->success(__('The sub event has been saved.'));

                 return $this->redirect(['controller' =>'Events', 'action' => 'index']);
            }
            $this->Flash->error(__('The sub event could not be saved. Please, try again.'));
        }
        $events = $this->SubEvents->Events->find('list', ['limit' => 200]);
        $this->set(compact('subEvent', 'events'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Sub Event id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $subEvent = $this->SubEvents->get($id);
        if ($this->SubEvents->delete($subEvent)) {
            $this->Flash->success(__('The sub event has been deleted.'));
        } else {
            $this->Flash->error(__('The sub event could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
