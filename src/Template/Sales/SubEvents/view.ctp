<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\SubEvent $subEvent
 */
?>
<div class="orders index large-9 medium-8 columns content card card card-default scrollspy">
    <div class="card-content">

        <div class="subEvents view large-9 medium-8 columns content">
            <h3><?= h($subEvent->id) ?></h3>
            <table class="vertical-table">
                <tr>
                    <th scope="row"><?= __('Event') ?></th>
                    <td><?= $subEvent->has('event') ? $this->Html->link($subEvent->event->name, ['controller' => 'Events', 'action' => 'view', $subEvent->event->id]) : '' ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __(' Event') ?></th>
                    <td><?= h($subEvent->_event) ?></td>
                </tr>
               <!--  <tr>
                    <th scope="row"><?= __(' Time') ?></th>
                    <td><?= h($subEvent->_time) ?></td>
                </tr> -->
                <tr>
                    <th scope="row"><?= __('Id') ?></th>
                    <td><?= $this->Number->format($subEvent->id) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('From Time') ?></th>
                    <td><?= h($subEvent->from_time) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('To Time') ?></th>
                    <td><?= h($subEvent->to_time) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Event Date') ?></th>
                    <td><?= h($subEvent->event_date) ?></td>
                </tr>
            </table>
        </div>
    </div>
</div>
