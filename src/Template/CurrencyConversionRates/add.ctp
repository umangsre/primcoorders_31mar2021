<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\ccr $ccr
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li><?= $this->Html->link(__('Add Currency Conversion Rates'), ['action' => 'index']) ?></li>
    </ul>
</nav>
<div class="currencyConversionRates form large-9 medium-8 columns content">
    <?= $this->Form->create($ccr) ?>
    <fieldset>
        <legend><?= __('Add Currency Conversion Rate') ?></legend>
        <?php
            echo $this->Form->control('Conversion key');
            echo $this->Form->control('Conversion Value ');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit'),['class' => 'btn waves-effect waves-light border-round col s3 mt-4', 'style' => 'background: #000;']) ?>
    <?= $this->Form->button(__('Back'),['class' => 'btn waves-effect waves-light border-round col s3 mt-4', 'style' => 'background: #000;', 'index'=>'edit']) ?>
    <?= $this->Form->end() ?>
</div>
