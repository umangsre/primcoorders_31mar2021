<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * ccr Entity
 *
 * @property int $id
 * @property int $name
 * @property \Cake\I18n\FrozenTime|null $created
 * @property \Cake\I18n\FrozenTime|null $updated
 */
class ccr extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'tool' => true,
        'tool_kit_category_id' => true,
        'item_code' => true,
        'quantity' => true,
        'created' => true,
        'updated' => true
    ];
}
