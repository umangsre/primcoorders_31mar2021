<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Bill $bill
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Bill'), ['action' => 'edit', $bill->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Bill'), ['action' => 'delete', $bill->id], ['confirm' => __('Are you sure you want to delete # {0}?', $bill->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Bill'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Bill'), ['action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="bill view large-9 medium-8 columns content">
    <h3><?= h($bill->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Billing Type Of Expense') ?></th>
            <td><?= h($bill->billing_type_of_expense) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Billing Place Of Expense') ?></th>
            <td><?= h($bill->billing_place_of_expense) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Billing Tax') ?></th>
            <td><?= h($bill->billing_tax) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Billing Status') ?></th>
            <td><?= h($bill->billing_status) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($bill->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Billing Amount') ?></th>
            <td><?= $this->Number->format($bill->billing_amount) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Billing Total Amount') ?></th>
            <td><?= $this->Number->format($bill->billing_total_amount) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Billing Userid') ?></th>
            <td><?= $this->Number->format($bill->billing_userid) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Billing Date') ?></th>
            <td><?= h($bill->billing_date) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Created') ?></th>
            <td><?= h($bill->created) ?></td>
        </tr>
    </table>
    <div class="row">
        <h4><?= __('Billing Image') ?></h4>
        <?= $this->Text->autoParagraph(h($bill->billing_image)); ?>
    </div>
    <div class="row">
        <h4><?= __('Billing Comment') ?></h4>
        <?= $this->Text->autoParagraph(h($bill->billing_comment)); ?>
    </div>
</div>
