<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Claim $claim
 * @var \App\Model\Entity\PrimcoCategoryLabourCost $categoryData
 * @var \App\Model\Entity\PrimcoGeneralLabourCost $generalData
 */
$options = ['select' => 'Select', 'yes' => 'Yes', 'no' => 'No'];
$categories = ['select' => 'Select', 'carpet' => 'Carpet', 'wood' => 'Wood', 'vinyl' => 'Vinyl', 'laminates' => 'Laminates', 'luxury_vinyl' => 'Luxury Vinyl Plank', 'underlay' => 'Underlay', 'rubber' => 'Rubber'];
$appliances = ['select' => 'Select', 'dishwasher' => 'Dishwasher', 'mouldings' => 'Mouldings'];
$uom = ['select' => 'Select', 'sq.ft.' => 'sq.ft.', 'sq.yd.' => 'sq.yd.'];
$settlement_methods = ['select' => 'Select', 'cash' => 'Cash Settlement', 'replacement' => 'Replacement', 'return' => 'Return'];
?>
<script>
    const categories = <?php echo json_encode($categoryData) ?>;
    const general = <?php echo json_encode($generalData) ?>;
    const millers = <?php echo json_encode($millers) ?>;
</script>
<style>
    #suggesstion-dealers {
        background: #fff;
        box-shadow: 3px 3px 3px rgba(0, 0, 0, 0.5);
        padding: 5px;
    }

    #suggesstion-dealers ul li {
        margin: 5px;
        border-bottom: 1px solid #000;
    }

    #suggestions-millers {
        background: #fff;
        box-shadow: 3px 3px 3px rgba(0, 0, 0, 0.5);
        padding: 5px;
    }

    #suggestions-millers ul li {
        margin: 5px;
        border-bottom: 1px solid #000;
    }
</style>
<div class="orders index large-9 medium-8 columns content card card card-default scrollspy">
    <div class="card-content">
        <div class="claims view large-9 medium-8 columns content">
            <h3>Claim Number: <?= h($claim->id) ?></h3>
            <?= $this->Form->create($claim, ['id' => 'claim-form']) ?>
            <table class="vertical-table">
                <?php
                $disabled = true;
                if ($claim->role == 'dealer') {
                    if (count($claim->claim_status) > 0) {
                        $lastStatus = $claim->claim_status[count($claim->claim_status) - 1];
                        if ($lastStatus['status'] == 'send to dealer' || $lastStatus['status'] == 'saved') {
                            $disabled = false;
                        }
                    }
                }
                ?>
                <div id="errors"></div>
                <fieldset style="margin-bottom: 2rem;">
                    <legend><?= __('Retailer / Dealer Information') ?></legend>
                    <?php
                    echo $this->Form->control('name', ['disabled' => true, 'value' => $claim->customer->name, 'label' => 'Dealer Name']);
                    echo $this->Form->control('contact_name', ['disabled' => true, 'value' => $claim->customer->contact_name, 'label' => 'Contact Name']);
                    echo $this->Form->control('phone', ['disabled' => true, 'value' => $claim->customer->phone, 'label' => 'Phone']);
                    echo $this->Form->control('address', ['disabled' => true, 'value' => $claim->customer->address]);
                    echo $this->Form->control('province', ['disabled' => true, 'value' => $claim->customer->province]);
                    echo $this->Form->control('city', ['disabled' => true, 'value' => $claim->customer->city]);
                    echo $this->Form->control('postal_code', ['disabled' => true, 'value' => $claim->customer->postal_code]);
                    ?>
                </fieldset>
                <fieldset style="margin-bottom: 2rem;">
                    <legend><?= __('End Consumer Information') ?></legend>
                    <?php
                    echo $this->Form->control('consumer_name', ['disabled' => $disabled, 'value' => $claim->consumer->consumer_name, 'label' => 'Customer Name']);
                    echo $this->Form->control('consumer_phone', ['disabled' => $disabled, 'value' => $claim->consumer->phone, 'label' => 'Phone']);
                    echo $this->Form->control('consumer_address', ['disabled' => $disabled, 'value' => $claim->consumer->address, 'label' => 'Address']);
                    echo $this->Form->control('consumer_city', ['disabled' => $disabled, 'value' => $claim->consumer->city, 'label' => 'City']);
                    echo $this->Form->control('consumer_postal_code', ['disabled' => $disabled, 'value' => $claim->consumer->postal_code, 'label' => 'Postal Code']);
                    ?>
                </fieldset>
                <fieldset style="margin-bottom: 2rem;">
                    <legend><?= __('Claim Information') ?></legend>
                    <div id="primcoInvoiceNumbers"></div>
                    <?php if (!$disabled) { ?>
                        <button type="button" onclick="addPrimcoInvoiceField()">Add More Invoice Numbers</button>
                    <?php } ?>
                    <input type="hidden" name="primco_invoice_number" id="primco-invoice-number"/>
                    <?php
                    if ($claim->role === 'admin' || $claim->role === 'miller') {
                        ?>
                        <div id="millerInvoiceNumbers"></div>
                        <?php if ($claim->role === 'admin') { ?>
                            <button type="button" onclick="addMillerInvoiceField()">Add More Mill Invoice Numbers
                            </button>
                        <?php } ?>
                        <input type="hidden" name="miller_invoice_number" id="miller-invoice-number"/>
                    <?php } else if ($claim->role === 'miller') { ?>
                        <div id="millerInvoiceNumbers"></div>
                    <?php } ?>
                    <div id="claimInformation"></div>
                </fieldset>
                <fieldset style="margin-bottom: 2rem;">
                    <?php
                    if ($disabled) {
                        echo $this->Form->control('settlement_method', ['disabled' => $disabled, 'value' => $claim->settlement_method]);
                    } else {
                        echo $this->Form->control('settlement_method', [
                            'options' => $settlement_methods,
                            'placeholder' => 'select',
                            'controller' => 'total_settlement_amount'
                        ]);
                    }
                    ?>
                    <?php
                    if ($disabled) {
                        if ($claim->settlement_method === 'cash') {
                            echo $this->Form->control('total_settlement_amount', ['disabled' => $disabled, 'value' => $claim->total_settlement_amount, 'label' => 'Total Settlement Amount (in CAD)']);
                        } else if ($claim->settlement_method === 'replacement') {
                            echo $this->Form->control('total_labour_cost_claimed', ['disabled' => $disabled, 'value' => $claim->total_settlement_amount, 'label' => 'Total Labour Cost Claimed (in CAD)']);
                        } else {
                            echo $this->Form->control('return_quantity', ['disabled' => $disabled, 'value' => $claim->total_settlement_amount, 'label' => 'Return Quantity']);
                        }
                    } else {
                        ?>
                        <div class="settlement_amount_div" style="display: none;">
                            <?php
                            echo $this->Form->control('total_settlement_amount', ['label' => 'Total Settlement Amount (in CAD)', 'value' => $claim->total_settlement_amount]);
                            ?>
                        </div>
                        <div class="replacement_amount_div" style="display: none;">
                            <?php
                            echo $this->Form->control('total_labour_cost_claimed', ['label' => 'Total Labour Cost Claimed (in CAD)', 'value' => $claim->total_settlement_amount]);
                            ?>
                        </div>
                        <div class="return_qty_div" style="display: none;">
                            <?php
                            echo $this->Form->control('return_quantity', ['label' => 'Return Quantity']);
                            ?>
                        </div>
                    <?php } ?>
                    <?php
                    $claimData = json_decode($claim->claim_json, true);
                 

            //    echo $claim;
            //     //    echo "<br>";
            //     //    echo $claim->Category;
            //     //    exit;
            //     echo $claim->Category;
            //     exit;
                    if ($claim->role === 'admin' && $claim->settlement_method === 'replacement') {
                        
                        $quantityUom = $claimData[0]['Quantity_Installed_UOM'];
                        $quantityAffected = $claimData[0]['Quantity_Affected'];
                        $quantityAffectedUOM = $claimData[0]['Quantity_Affected_UOM'];
                        
                        $selectedCategory = $claimData[0]['Category'];
                         
                        $selectedSubCategory = $claimData[0]['Sub_Category'];
                        echo $selectedSubCategory;
                       
                        $quantityPurchased = $claimData[0]['Quantity_Purchased'];
                        $furnitureMoved=$claimData['misc']['Furniture'];
                        if ($furnitureMoved==''){
                            $furnitureMoved=0;
                        }
                        $toiletsMoved=$claimData['misc']['Toilets'];
                        if ($toiletsMoved==''){
                            $toiletsMoved=0;
                        }
                        $appliancesMoved=$claimData['misc']['Appliances'];
                        if ( $appliancesMoved==''){
                            $appliancesMoved=0;
                        }
                        $moldingsMoved=$claimData['misc']['Misc_Items_Value'];
                        if ( $moldingsMoved==''){
                            $moldingsMoved=0;
                        }
                        
                        if ($quantityUom == 'percentage') {
                            $quantityAffected = $quantityAffected * $quantityPurchased / 100;
                        }
                        $quantityPurchasedUom = $claimData[0]['Quantity_Purchased_UOM'];
                       // $quantityedUom = $claimData[0]['Quantity_Purchased_UOM'];
                        $categoryAmount = 0;
                        $subCategoryAmount = 0;
                        $categoriesTotalAmount = 0;
                       
                        foreach ($categoryData as $category) {
                            
                            if ($category['id'] == $selectedCategory) {
                                $categoryAmount = (float)$category['labour_cost_' . $quantityAffectedUOM];
                            }
                            if (!empty($selectedSubCategory) && $category['id'] == $selectedSubCategory) {
                                $subCategoryAmount = (float)$category['labour_cost_' . $quantityAffectedUOM];
                            }
                            $categoriesTotalAmount = (float)$categoryAmount + (float)$subCategoryAmount;
                        }

                      
                  
                          
                        $totalAmount = (float)$categoriesTotalAmount * (float)$quantityAffected + $furnitureMoved*45.50 + $toiletsMoved*71.50 + $appliancesMoved*32.50 + $moldingsMoved*1.30;
                        
                        echo $this->Form->control('primco_total_settlement_amount', ['disabled' => $disabled, 'value' => number_format($totalAmount,2)]);
                    }
                    echo '<div class="row"><h6 style="text-align: center;">Claim Pictures</h6></div>';
                    echo '<div class="row"><label for="billing-image">Pictures - Full Room Pictures / Area(s) where the issue took place</label></div>';
                    $explodeimage = explode(",", $claim->claim_pictures_1);
                    if (!empty($explodeimage)) {
                        foreach ($explodeimage as $key => $value) {
                            if (strpos($value, 'pdf')) {
                                echo "<a href='$value' target='_blank' ><img src='/img/pdf-icon.png' class='billing_image' style='width: 200px'></a>";
                            } else {
                                echo "<a href='$value' target='_blank' ><img src='$value' class='billing_image' style='width: 200px'></a>";
                            }
                        }
                        echo '<br /><br />';
                    }
                    echo '<div class="row"><label for="billing-image">Pictures of the issue - Closeup</label></div>';
                    $explodeimage = explode(",", $claim->claim_pictures_2);
                    if (!empty($explodeimage)) {
                        foreach ($explodeimage as $key => $value) {
                            if (strpos($value, 'pdf')) {
                                echo "<a href='$value' target='_blank' ><img src='/img/pdf-icon.png' class='billing_image' style='width: 200px'></a>";
                            } else {
                                echo "<a href='$value' target='_blank' ><img src='$value' class='billing_image' style='width: 200px'></a>";
                            }
                        }
                        echo '<br /><br />';
                    }
                    echo '<div class="row"><label for="billing-image">Labour Bill / Quote, if applicable</label></div>';
                    $explodeimage = explode(",", $claim->claim_pictures_3);
                    if (!empty($explodeimage)) {
                        foreach ($explodeimage as $key => $value) {
                            if (strpos($value, 'pdf')) {
                                echo "<a href='$value' target='_blank' ><img src='/img/pdf-icon.png' class='billing_image' style='width: 200px'></a>";
                            } else {
                                echo "<a href='$value' target='_blank' ><img src='$value' class='billing_image' style='width: 200px'></a>";
                            }
                        }
                        echo '<br /><br />';
                    }
                    echo '<div class="row"><label for="billing-image">Consumer Proof of Purchase</label></div>';
                    $explodeimage = explode(",", $claim->claim_pictures_4);
                    if (!empty($explodeimage)) {
                        foreach ($explodeimage as $key => $value) {
                            if (strpos($value, 'pdf')) {
                                echo "<a href='$value' target='_blank' ><img src='/img/pdf-icon.png' class='billing_image' style='width: 200px'></a>";
                            } else {
                                echo "<a href='$value' target='_blank' ><img src='$value' class='billing_image' style='width: 200px'></a>";
                            }
                        }
                        echo '<br /><br />';
                    }
                    ?>
                </fieldset>
                <fieldset style="margin-bottom: 2rem;" id="miscClaimInfoArea">
                    <legend><?= __('Additional Information Pertaining to Labour (if applicable)') ?></legend>
                    <div id="miscClaimInformation"></div>
                </fieldset>
                <?php if ($claim->role === 'admin') { ?>
                    <fieldset style="margin-bottom: 2rem;">
                        <legend><?= __('Mill Information') ?></legend>
                        <?php
                        $millerDisabled = !empty($claim->miller);
                        echo $this->Form->control('miller_id', ['type' => 'hidden', 'value' => $millerDisabled ? $claim->miller->miller_id : '']);
                        echo $this->Form->control('miller_user_id', ['type' => 'hidden', 'value' => $millerDisabled ? $claim->miller->miller_user_id : '']);
                        echo $this->Form->control('miller_name', ['disabled' => false, 'value' => $millerDisabled ? $claim->miller->miller_name : '', 'label' => 'Mill Name', 'autocomplete' => 'off']);
                        ?>
                        <div id="suggestions-millers" class="list_wraper shadow-sm"
                             style="display:none; height: 10rem; overflow-y: scroll;"></div>
                        <?php
                        echo $this->Form->control('miller_contact_name', ['disabled' => false, 'value' => $millerDisabled ? $claim->miller->contact_name : '', 'label' => 'Contact Name']);
                        echo $this->Form->control('miller_email', ['disabled' => false, 'value' => $millerDisabled ? $claim->miller->email : '', 'label' => 'Email']);
                        echo $this->Form->control('miller_phone', ['disabled' => false, 'value' => $millerDisabled ? $claim->miller->phone : '', 'label' => 'Phone']);
                        echo $this->Form->control('miller_address', ['disabled' => false, 'value' => $millerDisabled ? $claim->miller->address : '', 'label' => 'Address']);
                        echo $this->Form->control('miller_city', ['disabled' => false, 'value' => $millerDisabled ? $claim->miller->city : '', 'label' => 'City']);
                        echo $this->Form->control('miller_postal_code', ['disabled' => false, 'value' => $millerDisabled ? $claim->miller->postal_code : '', 'label' => 'Postal Code']);
                        ?>
                    </fieldset>
                <?php } ?>
                <?php
                echo $this->Form->control('status_comments', ['label' => 'Add your Comments/Details', 'type' => 'textarea']);
                echo $this->Form->control('claim_id', ['type' => 'hidden', 'value' => h($claim->id)]);
                $claimStatuses = $claim->claim_status;
                $lastStatus = $claimStatuses[0];
                echo '<div class="row">&nbsp;</div>';
                ?>
                <?php
                $checkClosure = $lastStatus['status'] === 'closed' || $lastStatus['status'] === 'approved by primco' || $lastStatus['status'] === 'declined by primco';
                $noStatus = count($claimStatuses) === 0;
                if ($claim->role === 'admin') {
                    if (!$checkClosure) {
                        $buttonsDisabled = false;
                        echo $this->Form->button(__('Approve'), ['disabled' => $buttonsDisabled, 'class' => 'btn waves-effect waves-light border-round col s5 ml-4 mr-4', 'style' => '    background: #000;','name' => 'status', 'value' => 'approved by primco']);
                        echo $this->Form->button(__('Reject'), ['disabled' => $buttonsDisabled, 'class' => 'btn waves-effect waves-light border-round  col s5', 'style' => '    background: #000;', 'name' => 'status', 'value' => 'declined by primco']);
                        echo $this->Form->button(__('Send to Mill'), ['disabled' => $buttonsDisabled, 'class' => 'btn waves-effect waves-light border-round  col s5 ml-4 mr-4', 'style' => '    background: #000;', 'name' => 'status', 'value' => 'send to mill', 'id' => 'sendToMillBtn']);
                        echo $this->Form->button(__('Send to Dealer'), ['disabled' => $buttonsDisabled, 'class' => 'btn waves-effect waves-light border-round  col s5', 'style' => ' background: #000;', 'name' => 'status', 'value' => 'send to dealer']);
                    }
                } else if ($claim->role === 'dealer') {
                    if (!$checkClosure && !$noStatus) {
                        $buttonsDisabled = true;
                        switch ($lastStatus['status']) {
                            case 'saved':
                            case 'send to dealer':
                            case 'declined by primco':
                                $buttonsDisabled = false;
                                break;
                            default:
                                $buttonsDisabled = true;
                                break;
                        }
                        if (!$buttonsDisabled) {
                            echo $this->Form->button(__('Save'), ['class' => 'save-claim btn waves-effect waves-light border-round gradient-45deg-purple-deep-orange col s5 ml-4 mr-4', 'name' => 'clicked', 'value' => 'save']);
                        }
                        echo $this->Form->button(__('Send to Primco'), ['disabled' => $buttonsDisabled, 'class' => 'btn waves-effect waves-light border-round gradient-45deg-purple-deep-orange col s5 ml-4 mr-4', 'name' => 'status', 'value' => 'send to admin']);
                    }
                } else if ($claim->role === 'miller') {
                    if (!$checkClosure && !$noStatus) {
                        $buttonsDisabled = true;
                        switch ($lastStatus['status']) {
                            case 'send to mill':
                                $buttonsDisabled = false;
                                break;
                            default:
                                $buttonsDisabled = true;
                                break;
                        }
                        echo $this->Form->button(__('Approve'), ['disabled' => $buttonsDisabled, 'class' => 'btn waves-effect waves-light border-round gradient-45deg-purple-deep-orange col s5 ml-4 mr-4', 'name' => 'status', 'value' => 'Approved by Mill']);
                        echo $this->Form->button(__('Reject'), ['disabled' => $buttonsDisabled, 'class' => 'btn waves-effect waves-light border-round gradient-45deg-purple-deep-orange col s5', 'name' => 'status', 'value' => 'Declined by Mill']);
                        echo $this->Form->button(__('Send to Primco'), ['disabled' => $buttonsDisabled, 'class' => 'btn waves-effect waves-light border-round gradient-45deg-purple-deep-orange col s5 ml-4 mr-4', 'name' => 'status', 'value' => 'Send to Primco']);
                    }
                }
                ?>
                <?php
                echo '<div class="row">&nbsp;</div>';
                echo '<div class="row">&nbsp;</div>';
                foreach ($claimStatuses as $claimStatus) {
                    $claimStatusName = str_replace('miller', 'mill', $claimStatus['status']);
                    echo '<div style="margin-bottom: 2rem; padding: 5px; border: 1px solid #000; width: 95%; margin-right: 3%; display: inline-block;">';
                    echo $claimStatusName . ' on ' . $claimStatus['created_at'] . ' by ' . $claimStatus['username'] . '<br />';
                    echo $claimStatus['status_comments'] . '<br />';
                    echo '</div>';
                }
                ?>
            </table>
            <?= $this->Form->end() ?>
        </div>
    </div>
</div>
<script>
    const disableValues = '<?php echo $disabled; ?>';
</script>
<script>
    const claim_json_data = <?php echo $claim->claim_json; ?>;
</script>
<script>
    const userRole = '<?php echo $claim->role; ?>';
</script>
<script>
    const primcoInvoiceNumbersArray = '<?php echo $claim->primco_invoice_number; ?>';
    const millerInvoiceNumbersArray = '<?php echo $claim->miller_invoice_number; ?>';
</script>
