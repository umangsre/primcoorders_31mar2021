<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Family Entity
 *
 * @property int $id
 * @property string $family
 * @property string $name
 * @property string|null $product_code
 * @property string|null $role_cut
 * @property float|null $mill_cost
 * @property string $usd_cad
 * @property float|null $conversion_rate
 * @property float|null $selling_price
 * @property float|null $brokerage_multiplier
 * @property float $brokerage
 * @property string $units
 * @property float $freight
 * @property float|null $ad_fund_percentage
 * @property float|null $ad_fund
 * @property float $landed_cost_per_unit
 * @property float $load_percentage
 * @property float|null $_load
 * @property float $margin_lic
 * @property float|null $sell_price_level1
 * @property float|null $rebate_level1
 * @property float|null $gross_margin_level1
 * @property float|null $sell_price_level2
 * @property float|null $rebate_level2
 * @property float|null $gross_margin_level2
 * @property float|null $sell_price_level3
 * @property float|null $rebate_level3
 * @property float|null $gross_margin_level3
 * @property string $mill
 * @property string|null $vendorID
 * @property float|null $sqft_ctn
 * @property float|null $wgt_lbs_ctn
 * @property float|null $ctn_pallet
 * @property float|null $sqft_pallet
 * @property float|null $pallet_container_20
 * @property float|null $sqft_container_20
 * @property float|null $sqft_roll
 * @property float|null $sqyd_roll
 * @property float|null $sku_code
 * @property float|null $color
 * @property float|null $length
 * @property float|null $width
 * @property float|null $thickness
 * @property float|null $warranty
 * @property float|null $image_url
 * @property \Cake\I18n\FrozenTime|null $created
 */
class Family extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'family' => true,
        'name' => true,
        'province' => true,
        'product_code' => true,
        'role_cut' => true,
        'mill_cost' => true,
        'usd_cad' => true,
        'new_mill_cost' => true,
        'conversion_rate' => true,
        'selling_price' => true,
        'brokerage_multiplier' => true,
        'brokerage' => true,
        'units' => true,
        'freight' => true,
        'ad_fund_percentage' => true,
        'ad_fund' => true,
        'landed_cost_per_unit' => true,
        'load_percentage' => true,
        '_load' => true,
        'margin_lic' => true,
        'sell_price_level1' => true,
        'rebate_level1' => true,
        'gross_margin_level1' => true,
        'sell_price_level2' => true,
        'rebate_level2' => true,
        'gross_margin_level2' => true,
        'sell_price_level3' => true,
        'rebate_level3' => true,
        'gross_margin_level3' => true,
        'mill' => true,
        'vendorID' => true,
        'sqft_ctn' => true,
        'wgt_lbs_ctn' => true,
        'ctn_pallet' => true,
        'sqft_pallet' => true,
        'pallet_container_20' => true,
        'sqft_container_20' => true,
        'sqft_roll' => true,
        'sku_code' => true,
        'color' => true,
        'length' => true,
        'width' => true,
        'thickness' => true,
        'warranty' => true,
        'product_image' => true,
        'pces_ctn' => true,
        'rolls_pallet' => true,
        'pallet_container_40' => true,
        'tabs_strip' => true,
        'packs_pallet' => true,
        'ibs_container' => true,
        'm2_ctn' => true,
        'lf_ctn' => true,
        'qty_ctn' => true,
        'division' => true,
        'class_code' => true,
        'item_code' => true,
        'specification' => true,
        'piece_per_carton' => true,
        'created' => true
    ];
}
