<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Family[]|\Cake\Collection\CollectionInterface $families
 */
use Cake\Routing\Router;
$link  =Router::url([ 'action' => 'findproductbyname']);
$product_link =  Router::url([ 'action' => 'marginCalculator']);
?>
<div class="orders index large-9 medium-8 columns content card card card-default scrollspy">
    <div class="card-content">
<div class="families index large-9 medium-8 columns content">
     <?= $this->Form->create('search') ?>
            <fieldset>
                <?php
                    echo $this->Form->input('search',['label' =>false]);
                    echo $this->Form->button(__('Search'));
                ?>
            </fieldset>

            <?= $this->Form->end() ?>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('family') ?></th>
                <th scope="col"><?= $this->Paginator->sort('name') ?></th>
                <th scope="col" title="<?= __('Product Code')   ?>"><?= $this->Paginator->sort('product_code','Code') ?></th>
                <!-- <th scope="col"><?= $this->Paginator->sort('role_cut') ?></th> -->
                <th scope="col" title="<?= __('Mill Cost')   ?>"><?= $this->Paginator->sort('mill_cost','MC') ?></th>
                <th scope="col"  title="<?= __('USD/CAD Currency')   ?>"><?= $this->Paginator->sort('usd_cad','CY') ?></th>
                <th scope="col" title="<?= __('Conversion Rate')   ?>"><?= $this->Paginator->sort('conversion_rate','CR') ?></th>
                <th scope="col" title="<?= __('Selling Price')   ?>"><?= $this->Paginator->sort('selling_price','SP') ?></th>
                <th scope="col" title="<?= __('Brokerage Multiplier')   ?>"><?= $this->Paginator->sort('brokerage_multiplier','BM') ?></th>
                <th scope="col" title="<?= __('Brokerage')   ?>"><?= $this->Paginator->sort('BR') ?></th>
                <th scope="col" title="<?= __('Units')   ?>"><?= $this->Paginator->sort('units') ?></th>
                <th scope="col" title="<?= __('Freight')   ?>"><?= $this->Paginator->sort('freight') ?></th>
                <th scope="col" title="<?= __('Landed Cost Per Unit')   ?>"><?= $this->Paginator->sort('landed_cost_per_unit','LCPU') ?></th>
                <th scope="col"  title="<?= __('Load Percentage')   ?>" ><?= $this->Paginator->sort('load_percentage','LP') ?></th>
                <th scope="col" title="<?= __('Margin lIC')   ?>"><?= $this->Paginator->sort('MLIC') ?></th>
               
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($families as $family): ?>
            <tr>
                <td><?= $this->Number->format($family->id) ?></td>
                <td><?= h($family->family) ?></td>
                <td><?= h($family->name) ?></td>
                <td><?= h($family->product_code) ?></td>
              <!--   <td><?= h($family->role_cut) ?></td> -->
                <td><?= $this->Number->format($family->mill_cost) ?></td>
                <td><?= h($family->usd_cad) ?></td>
                <td><?= $this->Number->format($family->conversion_rate) ?></td>
                <td><?= $this->Number->format($family->selling_price) ?></td>
                <td><?= $this->Number->format($family->brokerage_multiplier) ?></td>
                <td><?= $this->Number->format($family->brokerage) ?></td>
                <td><?= h($family->units) ?></td>
                <td><?= $this->Number->format($family->freight) ?></td>
                <td><?= $this->Number->format($family->landed_cost_per_unit) ?></td>
                <td><?= $this->Number->format($family->load_percentage) ?></td>
                <td><?= $this->Number->format($family->margin_lic) ?></td>
               <!--  <td><?= $this->Number->format($family->sell_price_level1) ?></td>
                <td><?= $this->Number->format($family->rebate_level1) ?></td>
                <td><?= $this->Number->format($family->gross_margin_level1) ?></td>
                <td><?= $this->Number->format($family->sell_price_level2) ?></td>
                <td><?= $this->Number->format($family->rebate_level2) ?></td>
                <td><?= $this->Number->format($family->gross_margin_level2) ?></td>
                <td><?= $this->Number->format($family->sell_price_level3) ?></td>
                <td><?= $this->Number->format($family->rebate_level3) ?></td>
                <td><?= $this->Number->format($family->gross_margin_level3) ?></td>
                <td><?= h($family->mill) ?></td>
                <td><?= h($family->vendorID) ?></td>
                <td><?= h($family->created) ?></td> -->
                <td class="actions">
                    <!-- <?= $this->Html->link(__('View'), ['action' => 'view', $family->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $family->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $family->id], ['confirm' => __('Are you sure you want to delete # {0}?', $family->id)]) ?> -->
                    <?= $this->Html->link('<i class="material-icons dp48">view_list</i>', ['action' => 'view', $family->id], ['title' => __('View'),'escape' => false]) ?>
                    <?= $this->Html->link('<i class="material-icons dp48">queue_play_next</i>', ['action' => 'view-calculator', $family->id], ['escape' => false, 'title' => __('View')]) ?>

                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div>
</div>
</div>

 <!-- Modal Structure -->
  <div id="modal1" class="modal">
    <div class="modal-content">
     <h3>Margin Calculator</h3>
     <div class="container mt-2">
    <div class="fgdf">
        <div class="gfg">
            <div class="row">
                <!--   <h2><?= __('Margin Calculator') ?></h2> -->
                <div class="tab w-100">
                      <?= $this->Form->create($family) ?>
                    <div class="form-fields">
                            <div class='row mt-2'>
                                <div class="col m8 s12 position-relative">
                                    <?php
                                 echo $this->Form->control('search_name', ['class' =>'search_name' , 'label'=>false,
                                 'placeholder' =>'Search Product Name or Product Code or Product Families','data-link' =>$link]);
                                  ?>
                                  <div id="suggesstion-pricing" class="list_wraper" style="display:none;">
                                  </div>
                                </div>

                                 <div class="col m4 s12">
                                    <?= $this->Form->button(__('Reset'), array( 'class' => 'btn btn-success pricing_reset', 'type' => 'button' )  ) ?>
                                 </div>
                            </div>

                        </div>
                        <br>
                            <table id="pricing-matrix-table" class="table table-bordered">
                             <thead>
                            <tr>
                                <!--th>Sr.No.</th-->
                                <th>Slab Type</th>
                                <th>Sell Price</th>
                                <th>Rebate</th>
                                <th>Gross margin(In Percentage)</th>
                                <th></th>

                            </tr>
                        </thead>
                        <tbody id="pricing-matrix">
                             <tr class="product_row">

                                <td class="price-ls"><?php  echo $this->Form->control('slevel',['value' =>'Slab 1','id' =>'slab_1','label' => false, 'readonly' => true]); ?></td>
                                <td class="price-ls"> <?php echo $this->Form->control('sell_price_level1',['label' => false,'readonly' => true]); ?>
                                </td>
                                <td class="price-ls"><?php echo $this->Form->control('rebate_level1',['label' => false,'readonly' => true]); ?></td>
                                <td class="price-ls"><?php  echo $this->Form->control('gross_margin_level1',['label' => false,'readonly' => true]); ?></td>
                                <td> </td>
                             </tr>

                             <tr class="product_row">

                                <td class="price-ls"><?php  echo $this->Form->control('slevel',['value' =>'Slab 2','id' =>'slab_2','label' => false,'readonly' => true]); ?></td>
                                <td class="price-ls"> <?php echo $this->Form->control('sell_price_level2',['label' => false,'readonly' => true]); ?>
                                </td>
                                <td class="price-ls"><?php echo $this->Form->control('rebate_level2',['label' => false,'readonly' => true]); ?></td>
                                <td class="price-ls"><?php  echo $this->Form->control('gross_margin_level2',['label' => false,'readonly' => true]); ?></td>
                                 <td> </td>
                             </tr>
                             <tr class="product_row">

                                <td class="price-ls"><?php  echo $this->Form->control('slevel',['value' =>'Slab 3','id' =>'slab_3','label' => false,'readonly' => true]); ?></td>
                                <td class="price-ls"> <?php echo $this->Form->control('sell_price_level3',['label' => false]); ?>
                                </td>
                                <td class="price-ls"><?php echo $this->Form->control('rebate_level3',['label' => false,'readonly' => true]); ?></td>
                                <td class="price-ls"><?php  echo $this->Form->control('gross_margin_level3',['label' => false,'readonly' => true]); ?>

                                </td>
                                 <td>
                                  </td>
                             </tr>

                             <tr class="product_row">

                                <td class="price-ls"><?php  echo $this->Form->control('slevel',['class' =>'slab_select','label' => false, 'type' =>'select',
                                    'options' =>['0' =>'Select Slab','1' =>'slab 1','2' =>'slab 2', '3' =>'slab 3']
                                    ] ); ?></td>
                                <td class="price-ls"> <?php echo $this->Form->control('sell_price',[ 'type' =>'number', 'min' =>'0', 'step' =>'0.01','class' =>'sell_price', 'label' => false]); ?>
                                </td>
                                <td class="price-ls">
                                    <?php echo $this->Form->control('rebate',['class' =>'rebate','type' =>'number', 'min' =>'0', 'step' =>'0.001','label' => false]); ?>
                                </td>
                                <td class="price-ls"><?php  echo $this->Form->control('gross_margin',['class' =>'gross_margin','label' => false]); ?></td>
                                 <td>
                                     <?= $this->Form->button(__('+'), array( 'class' => 'btn btn-info pricing_clone_add', 'type' => 'button' )  ) ?>
                                 </td>
                             </tr>
                            </tbody>
                        </table>


                        <?= $this->Form->end() ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
    <div class="modal-footer">
      <a href="#!" class="modal-close waves-effect waves-light pink accent-2 white-text btn">Close</a>
    </div>
  </div>
