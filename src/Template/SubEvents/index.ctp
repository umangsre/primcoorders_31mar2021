<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\SubEvent[]|\Cake\Collection\CollectionInterface $subEvents
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Sub Event'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Events'), ['controller' => 'Events', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Event'), ['controller' => 'Events', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="subEvents index large-9 medium-8 columns content">
    <h3><?= __('Sub Events') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('event_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('_event') ?></th>
                <th scope="col"><?= $this->Paginator->sort('_time') ?></th>
                <th scope="col"><?= $this->Paginator->sort('from_time') ?></th>
                <th scope="col"><?= $this->Paginator->sort('to_time') ?></th>
                <th scope="col"><?= $this->Paginator->sort('created') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($subEvents as $subEvent): ?>
            <tr>
                <td><?= $this->Number->format($subEvent->id) ?></td>
                <td><?= $subEvent->has('event') ? $this->Html->link($subEvent->event->name, ['controller' => 'Events', 'action' => 'view', $subEvent->event->id]) : '' ?></td>
                <td><?= h($subEvent->_event) ?></td>
                <td><?= h($subEvent->_time) ?></td>
                <td><?= h($subEvent->from_time) ?></td>
                <td><?= h($subEvent->to_time) ?></td>
                <td><?= h($subEvent->created) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $subEvent->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $subEvent->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $subEvent->id], ['confirm' => __('Are you sure you want to delete # {0}?', $subEvent->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div>
