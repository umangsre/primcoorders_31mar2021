<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\User $user
 */
?>
<div class="orders index large-9 medium-8 columns content card card card-default scrollspy">
    <div class="card-content">
        <div class="users view large-9 medium-8 columns content">
            <h3><?= h($user->id) ?></h3>
            <table class="vertical-table">
                <tr>
                    <th scope="row"><?= __('Name') ?></th>
                    <td><?= h($user->name) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Username') ?></th>
                    <td><?= h($user->username) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Role') ?></th>
                    <td><?= h($user->role) ?></td>
                </tr>
                <?php
                if ($user->role == 'sales') {
                    $salesman = $user->salesmen[0];
                    ?>
                    <tr>
                        <th scope="row"><?= __('Phone') ?></th>
                        <td><?= h($salesman['phone']) ?></td>
                    </tr>
                    <tr>
                        <th scope="row"><?= __('Province') ?></th>
                        <td><?= h($salesman['province']) ?></td>
                    </tr>
                    <tr>
                        <th scope="row"><?= __('Sales Rep') ?></th>
                        <td><?= h($salesman['sales_rep']) ?></td>
                    </tr>
                    <tr>
                        <th scope="row"><?= __('Sd Rep') ?></th>
                        <td><?= h($salesman['sd_rep']) ?></td>
                    </tr>
                    <tr>
                        <th scope="row"><?= __('Spec') ?></th>
                        <td><?= h($salesman['spec']) ?></td>
                    </tr>
                    <?php
                } else if ($user->role == 'dealer') {
                    $dealer = $user->customers[0];
                    ?>
                    <tr>
                        <th scope="row"><?= __('Customerid') ?></th>
                        <td><?= h($dealer['customerid']) ?></td>
                    </tr>
                    <tr>
                        <th scope="row"><?= __('Phone') ?></th>
                        <td><?= h($dealer['phone']) ?></td>
                    </tr>
                    <tr>
                        <th scope="row"><?= __('Contact Name') ?></th>
                        <td><?= h($dealer['contact_name']) ?></td>
                    </tr>
                    <tr>
                        <th scope="row"><?= __('Address') ?></th>
                        <td><?= h($dealer['address']) ?></td>
                    </tr>
                    <tr>
                        <th scope="row"><?= __('City') ?></th>
                        <td><?= h($dealer['city']) ?></td>
                    </tr>
                    <tr>
                        <th scope="row"><?= __('Province') ?></th>
                        <td><?= h($dealer['province']) ?></td>
                    </tr>
                    <tr>
                        <th scope="row"><?= __('Postal Code') ?></th>
                        <td><?= h($dealer['postal_code']) ?></td>
                    </tr>
                    <tr>
                        <th scope="row"><?= __('SIN') ?></th>
                        <td><?= h($dealer['sin']) ?></td>
                    </tr>
                    <?php
                } else if ($user->role == 'miller') {
                $miller = $user->millers[0];
                ?>
                <tr>
                    <th scope="row"><?= __('Contact Name') ?></th>
                    <td><?= h($miller['contact_name']) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Phone') ?></th>
                    <td><?= h($miller['phone']) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Address') ?></th>
                    <td><?= h($miller['address']) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('City') ?></th>
                    <td><?= h($miller['city']) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Postal Code') ?></th>
                    <td><?= h($miller['postal_code']) ?></td>
                </tr>
                <?php
                }
                ?>
                <tr>
                    <th scope="row"><?= __('Created') ?></th>
                    <td><?= h($user->created) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Modified') ?></th>
                    <td><?= h($user->modified) ?></td>
                </tr>
            </table>
        </div>
    </div>
</div>
