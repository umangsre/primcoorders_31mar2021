<?php
namespace App\Controller\Api;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;
use Cake\Event\Event;
use Cake\Mailer\Email;
use Cake\Routing\Router;
use \PHPQRCode\QRcode;

/**
 * OnlineUserRegistration Controller
 *
 * @property \App\Model\Table\OnlineUserRegistrationTable $OnlineUserRegistration
 *
 * @method \App\Model\Entity\OnlineUserRegistration[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class OnlineUserRegistrationController extends AppController
{
    public function initialize()
    {
        parent::initialize();
        $this->loadComponent('RequestHandler');
         $this->Auth->allow([ 'viewonlineuser', 'addonlineuser','index']);
    }
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['CreditApplications']
        ];
        $onlineUserRegistration = $this->paginate($this->OnlineUserRegistration);

        $this->set(compact('onlineUserRegistration'));
    }

    /**
     * View method
     *
     * @param string|null $id Online User Registration id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function viewonlineuser()
    {
        $this->request->allowMethod(['post', 'put']);
        $id =$this->request->getData('id');
        $onlineUserRegistration = $this->OnlineUserRegistration->get($id, [
            'contain' => []
        ]);

        $this->set([
                    'status' => 1,
                    'onlineUserRegistration' => $onlineUserRegistration,
                    '_serialize' => ['status', 'onlineUserRegistration']
                ]);

        //$this->set('onlineUserRegistration', $onlineUserRegistration);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function addonlineuser()
    {
        $this->request->allowMethod(['post', 'put']);
        $onlineUserRegistration = $this->OnlineUserRegistration->newEntity();
        if ($this->request->is('post')) {
            //print_r($this->request->getData());
            $data_array = $this->request->getData();
            foreach( json_decode( $this->request->getData('standar_user_contact'),true) as $key => $val){
                $jj =0;
                if($jj == 0){
                    $data_array['standard_name'] = $val['name'];
                    $data_array['standard_email'] = $val['email'];
                }else{
                    $data_array['standard_name'.$jj] = $val['name'];
                    $data_array['standard_email'.$jj] = $val['email'];
                }
                $jj++;
            }

           //$onlineUserRegistration->credit_application_id =$id;
	       $onlineUserRegistration = $this->OnlineUserRegistration->patchEntity ($onlineUserRegistration, $data_array);

            if ($this->OnlineUserRegistration->save($onlineUserRegistration)) {
               $qrcode_img ='';
               $id=0;
                if($id = $this->request->getData('credit_application_id')){

                      $link =  Router::url([
                                    'controller' => 'CreditApplications',
                                    'action' => 'viewCustomer',
                                    base64_encode($id)
                                  ],
                                  TRUE);
                    $base_urls =  Router::url('/', true) ;
                    $path_qr = WWW_ROOT .'img/QRcode/';
                    $path_sign = WWW_ROOT .'img/Signs/';
                    $qr_image_name = "qrcodes_creditApplication_".$id.".png";
                    $qrcode_img = $path_qr.$qr_image_name;
                    $qrcode_link =  $base_urls. 'img/QRcode/' . $qr_image_name;
                    $sign_img = "primco-sign-credit-application-".$id.".jpg";
                    $output_file = $path_sign . $sign_img;
                    $sign_link =  $base_urls .'img/Signs/' .$sign_img;
                    $ecc = 'L';
                    $pixel_Size = 10;
                    $frame_Size = 10;
                    // Generates QR Code and Stores it in directory given
                   QRcode::png($link, $qrcode_img, $ecc, $pixel_Size, $frame_Size);

                   $this->sendMail($qr_image_name,$link, $order_id);
                   if($this->request->getData('signature')){
                         $this->base64test($this->request->getData('signature'),$output_file);
                   }

                   $response =['pdf_link' =>$link,'qrcode_link' => $qrcode_link,'sign_link' =>$sign_link];
                 }
               $this->set([
                    'status' => 1,
                    'response'=> $response,
                    'onlineUserRegistration' => $onlineUserRegistration,
                    '_serialize' => ['status', 'onlineUserRegistration','response']
                ]);
            }else{
                 $this->set([
                    'status' => 0,
                    'onlineUserRegistration' => $onlineUserRegistration,
                    '_serialize' => ['status', 'onlineUserRegistration']
                ]);

            }


        }

    }


    public function sendMail($image_name,$link, $id){

        $toEmail = [
            'ankit@primco.ca',
//            's.nesbitt@primco.ca',
//            't.yelland@primco.ca',
//            'd.grona@primco.ca',
//            'm.malhotra@primco.ca',
        ];
        $email = new Email();
        $email->setTransport('default');
        $email->viewVars(['image_name' => $image_name,'link' =>$link ]);
        $email->viewBuilder()->setTemplate('creditapplication');
        $email->setEmailFormat('html');
        $email->setFrom(['support@primco.ca' => 'Primco'])
        ->setTo($toEmail)
        ->setBcc('Juneja.ankit@gmail.com')
        ->setSubject('New Customer #'.$id)
        ->send();
      return true;
    }

    /**
     * Edit method
     *
     * @param string|null $id Online User Registration id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $onlineUserRegistration = $this->OnlineUserRegistration->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $onlineUserRegistration = $this->OnlineUserRegistration->patchEntity($onlineUserRegistration, $this->request->getData());
            if ($this->OnlineUserRegistration->save($onlineUserRegistration)) {
                $this->Flash->success(__('The online user registration has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The online user registration could not be saved. Please, try again.'));
        }
        $creditApplications = $this->OnlineUserRegistration->CreditApplications->find('list', ['limit' => 200]);
        $this->set(compact('onlineUserRegistration', 'creditApplications'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Online User Registration id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $onlineUserRegistration = $this->OnlineUserRegistration->get($id);
        if ($this->OnlineUserRegistration->delete($onlineUserRegistration)) {
            $this->Flash->success(__('The online user registration has been deleted.'));
        } else {
            $this->Flash->error(__('The online user registration could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
