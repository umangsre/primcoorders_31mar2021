<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\ContactsList $contactsList
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List Contacts List'), ['action' => 'index']) ?></li>
    </ul>
</nav>
<div class="contactsList form large-9 medium-8 columns content">
    <?= $this->Form->create($contactsList) ?>
    <fieldset>
        <legend><?= __('Add Contacts List') ?></legend>
        <?php
            echo $this->Form->control('statement_name');
            echo $this->Form->control('statement_email');
            echo $this->Form->control('invoice_name');
            echo $this->Form->control('invoice_email');
            echo $this->Form->control('price_pages_name');
            echo $this->Form->control('price_pages_email');
            echo $this->Form->control('order_confirmation_name');
            echo $this->Form->control('order_confirmation_email');
            echo $this->Form->control('bank_order_name');
            echo $this->Form->control('bank_order_email');
            echo $this->Form->control('purchasing_agents_name');
            echo $this->Form->control('purchasing_agents_email');
            echo $this->Form->control('_date');
            echo $this->Form->control('authorized_signature');
            echo $this->Form->control('print_name');
            echo $this->Form->control('_title');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
