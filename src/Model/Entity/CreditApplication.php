<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * CreditApplication Entity
 *
 * @property int $id
 * @property string $company_legal_name
 * @property string $company_trade_name
 * @property string $street_address
 * @property string $phone
 * @property string|null $fax
 * @property string $city
 * @property string $province
 * @property string $postal_code
 * @property string $email_address
 * @property string $owner_name
 * @property string $owner_home_address
 * @property string $owner_home_phone
 * @property string $owner_cell_phone
 * @property string $manager_name
 * @property string $manager_phone
 * @property string|null $manager_finacial_address
 * @property string|null $manager_bank_account
 * @property string|null $manager_city
 * @property string|null $manager_province
 * @property string|null $manager_postal_code
 * @property string|null $manager_phone_2
 * @property string $manager_gst
 * @property string $province_sales_tax
 * @property string $amount_credit_required
 * @property string|null $yearly_sales_volume
 * @property string|null $fiscal_year_end
 * @property int|null $is_group
 * @property string|null $group_name
 * @property string|null $_date
 * @property string|null $authorized_signature
 * @property string|null $print_name
 * @property string|null $_title
 *
 * @property \App\Model\Entity\ContactsList[] $contacts_list
 * @property \App\Model\Entity\OnlineUserRegistration[] $online_user_registration
 */
class CreditApplication extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'company_legal_name' => true,
        'company_trade_name' => true,
        'street_address' => true,
        'phone' => true,
        'fax' => true,
        'city' => true,
        'province' => true,
        'postal_code' => true,
        'email_address' => true,
        'owner_name' => true,
        'owner_home_address' => true,
        'owner_home_phone' => true,
        'owner_cell_phone' => true,
        'manager_name' => true,
        'manager_phone' => true,
        'manager_finacial_address' => true,
        'manager_bank_account' => true,
        'manager_city' => true,
        'manager_province' => true,
        'manager_postal_code' => true,
        'manager_phone_2' => true,
        'manager_gst' => true,
        'province_sales_tax' => true,
        'amount_credit_required' => true,
        'yearly_sales_volume' => true,
        'fiscal_year_end' => true,
        'is_group' => true,
        'group_name' => true,
        '_date' => true,
        'authorized_signature' => true,
        'print_name' => true,
        '_title' => true,
        'contacts_list' => true,
        'online_user_registration' => true
    ];
}
