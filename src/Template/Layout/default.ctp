<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 */

$cakeDescription = 'CakePHP: the rapid development php framework';
use Cake\Routing\Router;
?>
<!DOCTYPE html>
<html>
<head>
    <?= $this->Html->charset() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>
        Primco
    </title>
    <?= $this->Html->meta('icon') ?>

    <?= $this->Html->css('bootstrap.min.css') ?>
	<?= $this->Html->css('jquery-ui.min.css') ?>
    <?= $this->Html->css('form.css') ?>

    

    <?= $this->fetch('meta') ?>
    <?= $this->fetch('css') ?>
    <?= $this->fetch('script') ?>
	<script src="https://cdn.jsdelivr.net/npm/signature_pad@2.3.2/dist/signature_pad.min.js"></script>
    <script type="text/javascript">
        var csrfCustomerToken = <?= json_encode($this->request->getParam('_csrfToken')) ?>;
       var customer_ajax_url = '<?= Router::url([ 'controller' => 'Orders', 'action' => 'getcustomers', 'prefix' => false ]) ?>';
        var customer_data_ajax_url = '<?= Router::url([ 'controller' => 'Orders', 'action' => 'getcustomersdata', 'prefix' => false ]) ?>';
        var salesmen_ajax_url = '<?= Router::url([ 'controller' => 'Orders', 'action' => 'getsalesmen', 'prefix' => false ]) ?>';
        var salesmen_data_ajax_url = '<?= Router::url([ 'controller' => 'Orders', 'action' => 'getsalesmendata', 'prefix' => false ]) ?>';
        var item_name_ajax_url = '<?= Router::url([ 'controller' => 'Orders', 'action' => 'getitemname', 'prefix' => false ]) ?>';
        var item_number_ajax_url = '<?= Router::url([ 'controller' => 'Orders', 'action' => 'getitemnumber', 'prefix' => false ]) ?>';
        var product_ajax_url = '<?= Router::url([ 'controller' => 'Orders', 'action' => 'getproduct', 'prefix' => false ]) ?>';
        var all_customers_ajax_url = '<?= Router::url([ 'controller' => 'Orders', 'action' => 'getallcustomers', 'prefix' => false ]) ?>';
        var all_salesmen_ajax_url = '<?= Router::url([ 'controller' => 'Orders', 'action' => 'getallsalesmen', 'prefix' => false ]) ?>';
        var all_products_ajax_url = '<?= Router::url([ 'controller' => 'Orders', 'action' => 'getallproducts', 'prefix' => false ]) ?>';
        var sign_ajax_url = '<?= Router::url([ 'controller' => 'Orders', 'action' => 'savesignature' , 'prefix' => false]) ?>';

        var all_conversion_rate_ajax_url = '<?=Router::url([ 'controller' => 'CurrencyConversionRates', 'action' => 'getConverionRate', 'prefix' => false ]) ?>';
		var BASE_URL = ' <?= BASE_URL ?>';
    </script>
</head>
<body>
    <div class="body-wrapper">
        <div class="application-from p-0 m-0">
            
            <div class="container11">
                <?= $this->fetch('content') ?>
            </div>
        </div>
    </div>
    <footer>
    </footer>
    <?= $this->Html->script('jquery.min.js') ?>
    <?= $this->Html->script('bootstrap.min.js') ?>
	<?= $this->Html->script('jquery-ui.min.js') ?>
    <?= $this->Html->script('form.js') ?>
    <?= $this->Html->script('form_validate.js') ?>
</body>
</html>
