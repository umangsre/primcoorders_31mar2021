<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Salesman Entity
 *
 * @property int $id
 * @property int|null $user_id
 * @property string $name
 * @property string|null $sales_rep
 * @property string|null $sd_rep
 * @property string|null $rep_email
 * @property string|null $spec
 * @property string|null $province
 * @property \Cake\I18n\FrozenTime|null $created
 * @property \Cake\I18n\FrozenTime|null $updated
 *
 * @property \App\Model\Entity\Order[] $orders
 */
class Salesman extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'user_id' => true,
        'name' => true,
        'sales_rep' => true,
        'sd_rep' => true,
        'rep_email' => true,
        'spec' => true,
        'province' => true,
        'phone' => true,
        'photo' => true,
        'created' => true,
        'updated' => true,
        'user' => true,
        'meetings' => true,
        'orders' => true
    ];
}
