<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Order[]|\Cake\Collection\CollectionInterface $orders
 */
?>
<!--nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Order'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Customers'), ['controller' => 'Customers', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Customer'), ['controller' => 'Customers', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Salesmen'), ['controller' => 'Salesmen', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Salesman'), ['controller' => 'Salesmen', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Order Items'), ['controller' => 'OrderItems', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Order Item'), ['controller' => 'OrderItems', 'action' => 'add']) ?></li>
    </ul>
</nav -->
<div class="orders index large-9 medium-8 columns content card card card-default scrollspy">

    <div class="orders index large-9 medium-8 columns content">
         <div class="card-content">
             <?= $this->Form->create('search') ?>
            <fieldset>
                <?php
                    echo $this->Form->input('search',['label' =>false]);
                    echo $this->Form->button(__('Search'));
                ?>
            </fieldset>

            <?= $this->Form->end() ?>
            <table cellpadding="0" cellspacing="0">
                <thead>
                    <tr>
                        <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                        <th scope="col"><?= $this->Paginator->sort('customer_id') ?></th>
                        <th scope="col"><?= $this->Paginator->sort('salesman_id') ?></th>
                        <th scope="col"><?= $this->Paginator->sort('grand_total') ?></th>
                        <th scope="col"><?= $this->Paginator->sort('status') ?></th>
                        <!--th scope="col"><?= $this->Paginator->sort('po_number') ?></th>
                        <th scope="col"><?= $this->Paginator->sort('ship_to') ?></th>
                        <th scope="col"><?= $this->Paginator->sort('ship_via') ?></th>
                        <th scope="col"><?= $this->Paginator->sort('additional_comments') ?></th>
                        <th scope="col"><?= $this->Paginator->sort('events') ?></th>
                        <th scope="col"><?= $this->Paginator->sort('sale_order_number') ?></th>
                        <th scope="col"><?= $this->Paginator->sort('incentive_reciept') ?></th>
                        <th scope="col"><?= $this->Paginator->sort('sale_date') ?></th>
                        <th scope="col"><?= $this->Paginator->sort('ship_date') ?></th>
                        <th scope="col"><?= $this->Paginator->sort('purchaser_name') ?></th>
                        <th scope="col"><?= $this->Paginator->sort('purchaser_sign') ?></th>
                        <th scope="col"><?= $this->Paginator->sort('created') ?></th>
                        <th scope="col"><?= $this->Paginator->sort('updated') ?></th -->
                        <th scope="col" class="actions"><?= __('Actions') ?></th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($orders as $order): ?>
                    <tr>
                        <td><?= $this->Number->format($order->id) ?></td>
                        <td><?= $order->has('customer') ? $this->Html->link($order->customer->name, ['controller' => 'Customers', 'action' => 'view', $order->customer->id]) : '' ?></td>
                        <td><?= $order->has('salesman') ? $this->Html->link($order->salesman->name, ['controller' => 'Salesmen', 'action' => 'view', $order->salesman->id]) : '' ?></td>
                        <td><?= $this->Number->format($order->grand_total) ?></td>
                         <td><?= $this->Number->format($order->status) ?></td>
                        <!--td><?= h($order->po_number) ?></td>
                        <td><?= h($order->ship_to) ?></td>
                        <td><?= h($order->ship_via) ?></td>
                        <td><?= h($order->additional_comments) ?></td>
                        <td><?= h($order->events) ?></td>
                        <td><?= h($order->sale_order_number) ?></td>
                        <td><?= h($order->incentive_reciept) ?></td>
                        <td><?= h($order->sale_date) ?></td>
                        <td><?= h($order->ship_date) ?></td>
                        <td><?= h($order->purchaser_name) ?></td>

                        <td><?= h($order->purchaser_sign) ?></td>
                        <td><?= h($order->created) ?></td>
                        <td><?= h($order->updated) ?></td -->
                        <td class="actions">
                            <?= $this->Html->link(__('View'), ['action' => 'view', $order->id], ['class' => 'mr-2 btn','style' => 'background-color:#ffb300;']) ?>
                            <?= $this->Html->link(__('Edit'), ['action' => 'add', base64_encode($this->Number->format($order->id))], ['class' => 'mr-2 btn']) ?>
                            <?= $this->Html->link(__('Download'), ['action' => 'pdf-view', base64_encode($this->Number->format($order->id))], ['escape' => false, 'class' => 'mr-2 btn', 'style' => 'background-color:#303F9F;']) ?>
                            <?= $this->Form->postLink(__($this->Html->tag('i', '', ['class' => 'fas fa-trash']).' Delete'), ['action' => 'delete', $order->id], ['escape' => false, 'style' => 'color:#f44336;'], ['confirm' => __('Are you sure you want to delete # {0}?', $order->id)]) ?>
                        </td>
                    </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
            <div class="paginator">
                <ul class="pagination">
                    <?= $this->Paginator->first('<< ' . __('first')) ?>
                    <?= $this->Paginator->prev('< ' . __('previous')) ?>
                    <?= $this->Paginator->numbers() ?>
                    <?= $this->Paginator->next(__('next') . ' >') ?>
                    <?= $this->Paginator->last(__('last') . ' >>') ?>
                </ul>
                <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
            </div>
        </div>
    </div>
        </div>
