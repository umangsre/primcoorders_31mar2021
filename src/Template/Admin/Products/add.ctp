<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Product $product
 */
?>
<!--nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List Products'), ['action' => 'index']) ?></li>
    </ul>
</nav -->
<div class="orders index large-9 medium-8 columns content card card card-default scrollspy">
    <div class="card-content">
        <div class="products form large-9 medium-8 columns content">
            <?= $this->Form->create($product) ?>
            <fieldset>
                <legend><?= __('Add Product') ?></legend>
                <?php
                    echo $this->Form->control('product_name');
                    echo $this->Form->control('item_name');
                    echo $this->Form->control('item_number');
                    echo $this->Form->control('color_name');
                    echo $this->Form->control('single_item');
                    echo $this->Form->control('bulk_item');
                    echo $this->Form->control('single_franklin');
                    echo $this->Form->control('bulk_franklin');
                    echo $this->Form->control('per_item');
                    echo $this->Form->control('bulk_count');
                    echo $this->Form->control('item_type');
                    echo $this->Form->control('franklin_type');
                    //echo $this->Form->control('created_at');
                    //echo $this->Form->control('updated_on', ['empty' => true]);
                ?>
            </fieldset>
            <?= $this->Form->button(__('Submit'),['class' => 'btn waves-effect waves-light border-round gradient-45deg-purple-deep-orange col s12 mt-4']) ?>
            <?= $this->Form->end() ?>
        </div>
    </div>
</div>
