<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Orders Model
 *
 * @property \App\Model\Table\CustomersTable&\Cake\ORM\Association\BelongsTo $Customers
 * @property \App\Model\Table\SalesmenTable&\Cake\ORM\Association\BelongsTo $Salesmen
 * @property \App\Model\Table\OrderCustomItemsTable&\Cake\ORM\Association\HasMany $OrderCustomItems
 * @property \App\Model\Table\OrderItemsTable&\Cake\ORM\Association\HasMany $OrderItems
 *
 * @method \App\Model\Entity\Order get($primaryKey, $options = [])
 * @method \App\Model\Entity\Order newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Order[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Order|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Order saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Order patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Order[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Order findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class OrdersTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('orders');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Customers', [
            'foreignKey' => 'customer_id'
        ]);
        $this->belongsTo('Salesmen', [
            'foreignKey' => 'salesman_id'
        ]);
        $this->hasMany('OrderCustomItems', [
            'foreignKey' => 'order_id'
        ]);
        $this->hasMany('OrderItems', [
            'foreignKey' => 'order_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->allowEmptyString('id', null, 'create');

        $validator
            ->numeric('grand_total')
            ->allowEmptyString('grand_total');

        $validator
            ->numeric('franklin_total')
            ->allowEmptyString('franklin_total');

        $validator
            ->numeric('points_total')
            ->allowEmptyString('points_total');

        $validator
            ->numeric('discount_total')
            ->allowEmptyString('discount_total');

        $validator
            ->numeric('gross_total')
            ->allowEmptyString('gross_total');

        $validator
            ->scalar('po_number')
            ->maxLength('po_number', 50)
            ->allowEmptyString('po_number');

        $validator
            ->scalar('ship_to')
            ->maxLength('ship_to', 255)
            ->allowEmptyString('ship_to');

        $validator
            ->scalar('ship_via')
            ->maxLength('ship_via', 50)
            ->allowEmptyString('ship_via');

        $validator
            ->scalar('additional_comments')
            ->maxLength('additional_comments', 255)
            ->allowEmptyString('additional_comments');

        $validator
            ->scalar('events')
            ->maxLength('events', 50)
            ->allowEmptyString('events');

        $validator
            ->scalar('sale_order_number')
            ->maxLength('sale_order_number', 50)
            ->allowEmptyString('sale_order_number');

        $validator
            ->scalar('incentive_reciept')
            ->maxLength('incentive_reciept', 50)
            ->allowEmptyString('incentive_reciept');

        // $validator
        //     ->date('sale_date')
        //     ->allowEmptyDate('sale_date');

        // $validator
        //     ->date('ship_date')
        //     ->allowEmptyDate('ship_date');

        $validator
            ->scalar('purchaser_name')
            ->maxLength('purchaser_name', 100)
            ->allowEmptyString('purchaser_name');

        $validator
            ->allowEmptyString('status');

        $validator
            ->scalar('purchaser_sign')
            ->maxLength('purchaser_sign', 50)
            ->allowEmptyString('purchaser_sign');

        $validator
            ->allowEmptyString('is_test');

        $validator
            ->allowEmptyString('created_by');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['customer_id'], 'Customers'));
        $rules->add($rules->existsIn(['salesman_id'], 'Salesmen'));

        return $rules;
    }
}
