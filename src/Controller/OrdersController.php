<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;
use Cake\Event\Event;
use Cake\Mailer\Email;
use Cake\Routing\Router;
use \PHPQRCode\QRcode;
/**
 * Orders Controller
 *
 * @property \App\Model\Table\OrdersTable $Orders
 *
 * @method \App\Model\Entity\Order[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class OrdersController extends AppController
{
    //public $uses = [ 'Order', 'Customer' ];
     public $paginate = [
        'limit' => 25,
        'order' => [
            'Orders.id' => 'desc'
        ]
    ];
    public function initialize()
    {
        parent::initialize();
        $this->loadComponent('RequestHandler');
    }

    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);
        $this->Auth->allow(['add', 'addsalesman', 'addorderitem', 'thankyou', 'getcustomers', 'getcustomersdata', 'getsalesmen', 'getsalesmendata', 'getitemname', 'getitemnumber', 'getproduct', 'thankyou','pdfView','savesignature','editorder','getallproducts','getallcustomers','getallsalesmen']);
    }

    public function pdfView($id = null ){
        if ( $id == null ) {
            return $this->redirect( $this->referer() );
        }

        $order_id = $id = base64_decode($id);
        $order = $this->Orders->get($order_id, [
            'contain' => []
        ]);


        $order_item = TableRegistry::get('OrderItems');
        $salesmen = TableRegistry::get('Salesmen');
        $customers = TableRegistry::get('Customers');
		$products = TableRegistry::get('Products');

        $pdf_data = [];
        // print_r($order->toArray());
        if ( !empty( $order->toArray() ) ) {
            //foreach ($order->toArray() as $order_key => $order_value) {

            $pdf_data['order'] = [
                'id' => $order->toArray()['id'],
                'grand_total' => $order->toArray()['grand_total'],
                'franklin_total' => $order->toArray()['franklin_total'],
                'points_total' => $order->toArray()['points_total'],
                'discount_total' => $order->toArray()['discount_total'],
                'gross_total' => $order->toArray()['gross_total'],
                'po_number' => $order->toArray()['po_number'],
                'ship_to' => $order->toArray()['ship_to'],
                'ship_date' => $order->toArray()['ship_date'],
                'ship_via' => $order->toArray()['ship_via'],
                'additional_comments' => $order->toArray()['additional_comments'],
                'events' => $order->toArray()['events'],
                'sale_order_number' => $order->toArray()['sale_order_number'],
                'incentive_reciept' => $order->toArray()['incentive_reciept'],
                'sale_date' => $order->toArray()['sale_date'],
                'purchaser_name' => $order->toArray()['purchaser_name'],

            ];

            /*Salesmen*/
            $salesmen_data = $salesmen->find( 'all' )
            ->where( [ 'id' => $order->toArray()['salesman_id'] ] );

            if( !empty( $salesmen_data->toList() ) ) {
                foreach ($salesmen_data->toList() as $sm_key => $sm_value) {
                    $pdf_data['salesman'] = [
                        'name' => $sm_value['name'],
                        'sales_rep' => $sm_value['sales_rep'],
                        'sd_rep' => $sm_value['sd_rep'],
                        'rep_email' => $sm_value['rep_email'],
                        'spec' => $sm_value['spec']
                    ];
                }

            }

            $customer_data = $customers->find( 'all' )
            ->where( [ 'id' => $order->toArray()['customer_id'] ] );

            if( !empty( $customer_data->toList() ) ) {
                foreach ($customer_data->toList() as $c_key => $c_value) {
                    $pdf_data['customer'] = [
                        'name' => $c_value['name'],
                        'address' => $c_value['address'],
                        'city' => $c_value['city'],
                        'province' => $c_value['province'],
                        'postal_code' => $c_value['postal_code'],
                        'sin' => $c_value['sin'],
                    ];
                }

            }

            $order_item_data = $order_item->find( 'all' )
            ->where( [ 'order_id' => $order->toArray()['id'] ] );

            if( !empty( $order_item_data->toList() ) ) {
                foreach ($order_item_data->toList() as $oi_key => $oi_value) {

					$product = $products->find( )
					->where( [ 'id' => $oi_value['product_id'] ] )
					->first()
					->toArray();

					$product_name = '<strong>'.$product['item_number'].':</strong> '.$product['product_name'].', '.$product['item_name'];
					$unit = $product['item_type'];
					//die;
                    $pdf_data['order_item'][] = [
						'product_name' => $product_name,
                        'unit' => $unit,
                        'price' => $oi_value['price'],
                        'quantity' => $oi_value['quantity'],
                        'incentives' => $oi_value['incentives'],
						'promo' => $oi_value['promo'],
						'discount' => $oi_value['discount'],
                        'total' => $oi_value['total'],

                    ];
                }

            }
        }

        $this->set(compact('pdf_data','order_id'));
		$this->viewBuilder()->layout('ajax');
        $this->response->type('pdf');
    }


    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $this->viewBuilder()->setLayout('dashboard');
	    $query = $this->Orders->find('all',['contain' => ['Customers', 'Salesmen']])->where(['is_test' =>0]);
		if($this->request->is('post')){
         $search = '%'.$this->request->getData('search').'%';
         $query->where(['OR' =>['Customers.name LIKE' => '%'.$search.'%','Salesmen.name LIKE' => '%'.$search.'%']])->andWhere(['is_test' =>0]);
       }
        $orders = $this->paginate($query);

        $this->set(compact('orders'));
    }

    public function test()
    {
        $this->viewBuilder()->setLayout('dashboard');
        $query = $this->Orders->find('all',['contain' => ['Customers', 'Salesmen']])->where(['is_test' =>1]);
        if($this->request->is('post')){
         $search = '%'.$this->request->getData('search').'%';
         $query->where(['OR' =>['Customers.name LIKE' => '%'.$search.'%','Salesmen.name LIKE' => '%'.$search.'%']])->andWhere(['is_test' =>1]);
       }
        $orders = $this->paginate($query);

        $this->set(compact('orders'));
    }

    /**
     * View method
     *
     * @param string|null $id Order id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $this->viewBuilder()->setLayout('dashboard');
        $order = $this->Orders->get($id, [
            'contain' => ['Customers', 'Salesmen', 'OrderItems']
        ]);

        $this->set('order', $order);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add($id =null)
    {
        $this->loadModel('Customers');
        $customer = $this->Customers->newEntity();
        if($id != null){
            $id = base64_decode($id);
            $order = $this->Orders->get($id, [
                'contain' => []
            ]);
            $action_type = 'edit';
            $customers = TableRegistry::get('Customers');
            if($order !=null){
                $customer_id =$order['customer_id'];
                if($customer_id){
                    $customer = $customers->get($customer_id, [
                        'contain' => []
                    ])->toArray();
                }
            }
        }else{
           $order = $this->Orders->newEntity();
        }

		$customers = TableRegistry::get('Customers');
         $action_type = 'add';
        if ($this->request->is(['patch', 'post', 'put'])) {

            /*Update date*/
            $this->request->data['ship_date'] = $this->request->data['ship_date']['year'].'-'.$this->request->data['ship_date']['month'].'-'.$this->request->data['ship_date']['day'].' 00:00:00';
            $order = $this->Orders->patchEntity($order, $this->request->getData());

			// echo '<pre>';
           //          print_r($this->request->getData());
           //          echo '</pre>';
           //          die;

            try {
				$customer_id =$this->request->data['customer_id'];
				if($customer_id){
					// $customer = $customers->get($customer_id, [
					// 'contain' => []
					// ]);
					// $customer = $customers->patchEntity($customer, $this->request->getData());
					// if ($customers->save($customer)) {
					// }else{

					// }
				}else{
					// $customer = $customers->newEntity();
					// $customer = $customers->patchEntity($customer, $this->request->getData());
					// if ($customers->save($customer)) {
					// 	$customer_id =$customer->id;
					// 	$order->customer_id = $customer_id;
					// }

				}

				$orderdata = $this->Orders->saveOrFail($order);
                return $this->redirect(['action' => 'addsalesman', base64_encode($orderdata->id) ,'add']);
            } catch (\Cake\ORM\Exception\PersistenceFailedException $e) {
                 $this->Flash->error(__('The order could not be saved. Please, try again.'));
            }

        }


       $this->set(compact('order','customer'));
    }


	public function editorder($id = null )
    {
        $id = base64_decode($id);
        $order = $this->Orders->get($id, [
            'contain' => []
        ]);

		$customer ='';
        $action_type = 'edit';
		$customers = TableRegistry::get('Customers');
		if($order != null){

			$customer_id =$order['customer_id'];
			if($customer_id){
						$customer = $customers->get($customer_id, [
							'contain' => []
						])->toArray();
			}
		}
        if ($this->request->is(['patch', 'post', 'put'])) {

            /*Update date*/
            $this->request->data['ship_date'] = $this->request->data['ship_date']['year'].'-'.$this->request->data['ship_date']['month'].'-'.$this->request->data['ship_date']['day'].' 00:00:00';
            $order = $this->Orders->patchEntity($order, $this->request->getData());
            try {
				$customer_id =$this->request->data['customer_id'];
				if($customer_id){
					// $customer = $customers->get($customer_id, [
					// 'contain' => []
					// ]);
					// $customer = $customers->patchEntity($customer, $this->request->getData());
					// if ($customers->save($customer)) {

					// }
				}else{
					// $customer = $customers->newEntity();
					// $customer = $customers->patchEntity($customer, $this->request->getData());
					// if ($customers->save($customer)) {
					// 	$customer_id =$customer->id;
					// 	$order->customer_id = $customer_id;
					// }

				}

				$orderdata = $this->Orders->saveOrFail($order);

                return $this->redirect(['action' => 'addsalesman', base64_encode($orderdata->id) ,'edit']);
            } catch (\Cake\ORM\Exception\PersistenceFailedException $e) {
                 $this->Flash->error(__('The order could not be saved. Please, try again.'));
            }

        }

        $this->set(compact('order','customer'));
    }

    public function addsalesman( $id = null, $action_type =null )
    {
		$salesmen = TableRegistry::get('Salesmen');
        if ($id == null ) {
            return $this->redirect(['action' => 'add']);
        }
        $id = base64_decode($id);
        $order = $this->Orders->get($id, [
            'contain' => []
        ]);

		$salesman ='';
		if($order !=null){

			$salesman_id =$order['salesman_id'];
			if($salesman_id){
				$salesman = $salesmen->get($salesman_id, [
					'contain' => []
				]);
			}
		}else{
            $this->loadModel('Salesmen');
            $salesman = $this->Salesmen->newEntity();
        }

        if ($this->request->is(['patch', 'post', 'put'])) {
            $this->request->data['sale_date'] = $this->request->data['sale_date']['year'].'-'.$this->request->data['sale_date']['month'].'-'.$this->request->data['sale_date']['day'].' 00:00:00';
            $order = $this->Orders->patchEntity($order, $this->request->getData());

            try {

				$salesman_id =$this->request->data['salesman_id'];
				if($salesman_id){
					// $salesman = $salesmen->get($salesman_id, [
					// 	'contain' => []
					// ]);
					// $salesman = $salesmen->patchEntity($salesman, $this->request->getData());
					// if ($salesmen->save($salesman)) {
					// }
				}else{
					// $salesman = $salesmen->newEntity();
					// $salesman = $salesmen->patchEntity($salesman, $this->request->getData());
					// 	if ($salesmen->save($salesman)) {
					// 	$salesman_id =$salesman->id;
					// 	$order->salesman_id = $salesman_id;
					// 	}

				}

                $this->Orders->saveOrFail($order);
                //$this->Flash->success(__('The order has been saved.'));

                return $this->redirect(['action' => 'addorderitem', base64_encode($id) ]);
            } catch (\Cake\ORM\Exception\PersistenceFailedException $e) {
                $this->Flash->error(__('The order could not be saved. Please, try again.'));
            }


        }
        $this->set(compact('order','salesman', 'id','action_type'));

    }

    public function addorderitem( $id = null )
    {
        if ($id == null ) {
            return $this->redirect(['action' => 'add']);
        }

        $id = base64_decode($id);

        $order_items = TableRegistry::get('OrderItems');
        $product = TableRegistry::get('Products');

        $order = $this->Orders->get($id, [
            'contain' => []
        ]);
		$order_item_data ='';
		$data = array();

		if($order !=null){

		$order_item_data = $order_items->find()
            ->where( [ 'order_id' => $order['id'] ] )
			->contain(['Products'])
			->all()
			->toArray();

            $item_names_arr =[];
            foreach ($order_item_data as $value) {


                if($value->product->product_name){

                    $salesmen_data = $product->find('all')
                        ->distinct( 'item_name' )
                        ->select( ['item_name'] )
                        ->where( [ "product_name" => $value->product->product_name ] );

                        $json_records = array();
                        foreach ($salesmen_data->toList() as $key => $val) {
                            $json_records[] =$val->item_name;
                        }
                    $item_names_arr[$value->product->id]['item_names'] =$json_records;
                    $products_data = $product->find('all')
                    ->select( [ 'id', 'item_number'] )
                    ->where( [ "product_name" => $value->product->product_name, 'item_name' => $value->product->item_name ] );

                    $json_records = array();
                    foreach ($products_data->toList() as $key => $value1) {
                        $json_records[] = $value1->item_number;
                    }
                     $item_names_arr[$value->product->id]['item_numbers'] =$json_records;
                }

            }
            /*echo '<pre>';
             print_r($item_names_arr);
             echo '</pre>';
             die;*/
		}



        $all_products = $product->find('all')
        ->select( ['product_name'] )
        ->distinct('product_name');
         $product_records = [];
        if (!empty($all_products->toList())) {

            foreach ($all_products->toList() as $key => $value) {
                $product_records[] = [
                    'id' => $value->id,
                    'name' => $value->product_name,
                ];
            }
        }

        if ($this->request->is(['patch', 'post', 'put'])) {

            $records = [];
			$grand_total =0;
           // $order_item_table = TableRegistry::getTableLocator()->get('OrderItems');
            foreach ($order_item_data as $val) {

                if($product_id =$val->product_id){

                     $order_item_delete = $order_items->find('all')
                        ->where( [ 'order_id' => $order['id'], 'product_id' =>$product_id ] )->first();
                    if($order_item_delete != null){
                        $missingLexicon = $order_items->get ( $order_item_delete->id );
                         $order_items->delete ( $missingLexicon );
                    }

                }

            }
            //die('here');


            foreach ( $this->request->getData()['product_id'] as $key => $value ) {

                if ( $this->request->getData()['quantity'][$key] <= 0 || $this->request->getData()['product_name'][$key] == "" || $this->request->getData()['item_name'][$key] == "" || $this->request->getData()['product_id'][$key] == "" || $this->request->getData()['total'][$key] <= 0 ) {
                    continue;
                }

                $product_id = $this->request->getData()['product_id'][$key];
                $order_item = $order_items->newEntity();
				$order_item->order_id = $id;
                $order_item->product_id = $this->request->getData()['product_id'][$key];
                $order_item->price = $this->request->getData()['price'][$key];
                $order_item->quantity = $this->request->getData()['quantity'][$key];
                $order_item->discount = $this->request->getData()['discount'][$key];

				if($this->request->getData()['franklin_enabled_input'][$key] ==1){
					$order_item->incentives = $this->request->getData()['incentives'][$key];
					$order_item->franklin_enabled = 1;
				}else{
					$order_item->incentives = '';
					$order_item->franklin_enabled = '';
				}

                $order_item->total = $this->request->getData()['total'][$key];
                $order_item->promo = $this->request->getData()['promo'][$key];
                $order_item =$order_items->patchEntity($order_item, $order_item->toArray());
                if( $order_items->save($order_item)){

                }

            }

             $order = $this->Orders->patchEntity($order, $this->request->getData());
             $order->status = 1;
			if ($this->Orders->save($order)){
                $this->Flash->success(__(' Order Saved. Successful Products:') );
                return $this->redirect(['action' => 'thankyou', base64_encode($order->id) ]);
			}
            $this->Flash->error(__('The order could not be saved. Please, try again.'));

        }



        $this->set(compact( 'id', 'order', 'item_names_arr','order_item_data', 'product_records'));

    }


    public function thankyou( $id = null )
    {
        // $this->autoRender = false;
        if ( $id == null ) {
            return $this->redirect( $this->referer() );
        }

        $order_id = $id = base64_decode($id);
        $order = $this->Orders->get($id, [
            'contain' => []
        ]);


        $order_item = TableRegistry::get('OrderItems');
        $salesmen = TableRegistry::get('Salesmen');
        $customers = TableRegistry::get('Customers');

        $pdf_data = [];
        // print_r($order->toArray());
        if ( !empty( $order->toArray() ) ) {
            //foreach ($order->toArray() as $order_key => $order_value) {

            $pdf_data['order'] = [
                'grand_total' => $order->toArray()['grand_total'],
                'po_number' => $order->toArray()['po_number'],
                'ship_to' => $order->toArray()['ship_to'],
                'ship_date' => $order->toArray()['ship_date'],
                'ship_via' => $order->toArray()['ship_via'],
                'additional_comments' => $order->toArray()['additional_comments'],
                'events' => $order->toArray()['events'],
                'sale_order_number' => $order->toArray()['sale_order_number'],
                'incentive_reciept' => $order->toArray()['incentive_reciept'],
                'sale_date' => $order->toArray()['sale_date'],
                'purchaser_name' => $order->toArray()['purchaser_name'],

            ];

            /*Salesmen*/
            $salesmen_data = $salesmen->find( 'all' )
            ->where( [ 'id' => $order->toArray()['salesman_id'] ] );

            if( !empty( $salesmen_data->toList() ) ) {
                foreach ($salesmen_data->toList() as $sm_key => $sm_value) {
                    $pdf_data['salesman'] = [
                        'name' => $sm_value['name'],
                        'sales_rep' => $sm_value['sales_rep'],
                        'sd_rep' => $sm_value['sd_rep'],
                        'rep_email' => $sm_value['rep_email'],
                        'spec' => $sm_value['spec']
                    ];
                }

            }

            $customer_data = $customers->find( 'all' )
            ->where( [ 'id' => $order->toArray()['customer_id'] ] );

            if( !empty( $customer_data->toList() ) ) {
                foreach ($customer_data->toList() as $c_key => $c_value) {
                    $pdf_data['customer'] = [
                        'name' => $c_value['name'],
                        'address' => $c_value['address'],
                        'city' => $c_value['city'],
                        'province' => $c_value['province'],
                        'postal_code' => $c_value['postal_code'],
                        'sin' => $c_value['sin'],
                    ];
                }

            }

            $order_item_data = $order_item->find( 'all' )
            ->where( [ 'order_id' => $order->toArray()['id'] ] );

            if( !empty( $order_item_data->toList() ) ) {
                foreach ($order_item_data->toList() as $oi_key => $oi_value) {
                    $pdf_data['order_item'][] = [
                        'price' => $oi_value['price'],
                        'quantity' => $oi_value['quantity'],
                        'incentives' => $oi_value['incentives'],
						'promo' => $oi_value['promo'],
                        'total' => $oi_value['total'],

                    ];
                }

            }


        }

       $image_name ='';
        $link ='';
        if($order_id){

             $link =  Router::url([ 'controller' => 'Orders', 'action' => 'pdf-view',base64_encode($order_id )
                                  ],TRUE);

            $path_qr = WWW_ROOT .'/img/QRcode/';
            $image_name = 'qr_codes_'.$order_id."_order.png";
            $qrcode_img = $path_qr.$image_name;
            // $ecc stores error correction capability('L')
            $ecc = 'L';
            $pixel_Size = 10;
            $frame_Size = 10;
            // Generates QR Code and Stores it in directory given
           QRcode::png($link, $qrcode_img, $ecc, $pixel_Size, $frame_Size);
         }
		$this->sendMail($image_name,$link, $order_id);

        $this->set(compact('pdf_data','order_id','image_name','link'));
    }


	public function sendMail($image_name,$link, $id){
		$toEmail = [
            'm.malhotra@primco.ca',
            'test-2nqlz4pjw@mail-tester.com',
            'support@primco.ca'

            //
        ];

        $email = new Email();
        $email->setTransport('default');
		$email->viewVars(['image_name' => $image_name,'link' =>$link ]);
        $email->viewBuilder()->setTemplate('order');
        $email->setEmailFormat('html');
        $email->setFrom(['support@primco.ca' => 'Primco'])
        ->setTo($toEmail)
        ->setBcc(['Juneja.ankit@gmail.com','sarbjit.rndexperts@gmail.com','ManishMalhotra1303@gmail.com'])
       // ->setBcc('sarbjit.rndexperts@gmail.com')
       // ->setBcc('ManishMalhotra1303@gmail.com')
        ->setSubject('Primco New Order #'.$id)
        ->send();
      return true;
    }

    /**
     * Edit method
     *
     * @param string|null $id Order id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
		$this->viewBuilder()->setLayout('dashboard');
        $order = $this->Orders->get($id, [
            'contain' => []
        ]);
         //print_r($order);
           // die;
        if ($this->request->is(['patch', 'post', 'put'])) {
            // print_r($this->request->getData());
           //die;
            $order = $this->Orders->patchEntity($order, $this->request->getData());
            if ($this->Orders->save($order)) {
                $this->Flash->success(__('The order has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The order could not be saved. Please, try again.'));
        }
        $customers = $this->Orders->Customers->find('list', ['limit' => 200]);
        $salesmen = $this->Orders->Salesmen->find('list', ['limit' => 200]);
        $this->set(compact('order', 'customers', 'salesmen'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Order id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
		$this->viewBuilder()->setLayout('dashboard');
        $this->request->allowMethod(['post', 'delete']);
        $order = $this->Orders->get($id);
        if ($this->Orders->delete($order)) {
            $this->Flash->success(__('The order has been deleted.'));
        } else {
            $this->Flash->error(__('The order could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }

	public function savesignature()
    {
        $this->autoRender = false;
		$path ='';
		$file_name ='';
		$response =['path' =>$path,'file_name' => $file_name ];


		if($_FILES){

			$file = $_FILES['files'];
			//print_r($file);
			$ext = substr(strtolower(strrchr($file['name'], '.')), 1); //get the extension
			$arr_ext = array('jpg', 'jpeg', 'gif'); //set allowed extensions

			//only process if the extension is valid
			if(in_array($ext, $arr_ext))
			{
				$path =  WWW_ROOT . 'img/Signs/'. $file['name'];
				$file_name = $file['name'];
				//do the actual uploading of the file. First arg is the tmp name, second arg is
				//where we are putting it
				if(move_uploaded_file($file['tmp_name'], $path)){
					$response =['path' =>$path,'file_name' =>$file_name ];
				}


			}
       }
        $content = json_encode($response);
        $this->response = $this->response->withStringBody($content);
        $this->response = $this->response->withType('json');
        return $this->response;
    }

	function base64_to_jpeg($base64_string, $output_file) {
		// open the output file for writing
		$ifp = fopen( $output_file, 'wb' );
		$data = explode( ',', $base64_string );

		// we could add validation here with ensuring count( $data ) > 1
		fwrite( $ifp, base64_decode( $data[ 1 ] ) );

		// clean up the file resource
		fclose( $ifp );

		return $output_file;
	}

	public function getallcustomers()
    {
        $this->autoRender = false;
        $customers = TableRegistry::get('Customers');
        $customer_data = $customers->find('all');

        $json_records = array();
        foreach ($customer_data->toList() as $key => $value) {
            $json_records[] = [
                'id' => $value->id,
                'customerid' => $value->customerid,
                'name' => $value->name,
                'contact_name' => $value->contact_name,
                'address' => $value->address,
                'city' => $value->city,
                'province' => $value->province,
                'postal_code' => $value->postal_code,
                'sin' => $value->sin,

            ];
        }

        $content = json_encode($json_records);
        $this->response = $this->response->withStringBody($content);
        $this->response = $this->response->withType('json');
        return $this->response;
    }

    public function getallsalesmen()
    {
        $this->autoRender = false;
        $salesmen = TableRegistry::get('Salesmen');
        $salesmen_data = $salesmen->find('all');

        $json_records = array();
        foreach ($salesmen_data->toList() as $key => $value) {
            $json_records[] = [
                'id' => $value->id,
                'name' => $value->name,
                'email' => $value->rep_email
            ];
        }

        $content = json_encode($json_records);
        $this->response = $this->response->withStringBody($content);
        $this->response = $this->response->withType('json');
        return $this->response;
    }

    public function getallproducts()
    {
        $this->autoRender = false;
        $product = TableRegistry::get('Products');
        $product_data = $product->find('all');
         //$product_data = $product->find('all')->group('item_name','item_number');


        $json_records = array();
        foreach ($product_data->toList() as $key => $value) {
            $json_records[] = [
                'id' => $value->id,
                'product_name' => $value->product_name,
                'item_name' => $value->item_name,
                'item_number' => $value->item_number,
                'item_type' => $value->item_type,
                'color_name' => $value->color_name,
                'single_franklin' => $value->single_franklin,
                'bulk_franklin' => $value->bulk_franklin,
                'per_item' => $value->per_item,
                'single_item' => $value->single_item,
                'bulk_item' => $value->bulk_item,
                'franklin_type' => $value->franklin_type,
                'bulk_count' => $value->bulk_count,

            ];
        }

        $content = json_encode($json_records);
        $this->response = $this->response->withStringBody($content);
        $this->response = $this->response->withType('json');
        return $this->response;
    }

    public function getcustomers()
    {
        $this->autoRender = false;
        $customers = TableRegistry::get('Customers');
        $customer_data = $customers->find()
        ->where( [ "name LIKE" => "%".$_POST['name']."%" ] );

        $json_records = array();
        foreach ($customer_data->toList() as $key => $value) {
            $json_records[] = [
                'id' => $value->id,
                'customerid' => $value->customerid,
                'name' => $value->name,
                'address' => $value->address,
                'city' => $value->city,
                'province' => $value->province,
                'postal_code' => $value->postal_code,
                'sin' => $value->sin,

            ];
        }

        $content = json_encode($json_records);
        $this->response = $this->response->withStringBody($content);
        $this->response = $this->response->withType('json');
        return $this->response;
    }

    public function getcustomersdata()
    {
        $this->autoRender = false;
        $customers = TableRegistry::get('Customers');
        $customer_data = $customers->find('all')
        ->where( [ "id" => $_POST['customer_id'] ] );

        $json_records = array();
        foreach ($customer_data->toList() as $key => $value) {
            $json_records[] = [
                'id' => $value->id,
                'customerid' => $value->customerid,
                'name' => $value->name,
                'address' => $value->address,
                'city' => $value->city,
                'province' => $value->province,
                'postal_code' => $value->postal_code,
                'sin' => $value->sin,

            ];
        }

        $content = json_encode($json_records);
        $this->response = $this->response->withStringBody($content);
        $this->response = $this->response->withType('json');
        return $this->response;
    }

    public function getsalesmen()
    {
        $this->autoRender = false;
        $salesmen = TableRegistry::get('Salesmen');
        $salesmen_data = $salesmen->find('all')
        ->where( [ "name LIKE" => "%".$_POST['name']."%" ] );

        $json_records = array();
        foreach ($salesmen_data->toList() as $key => $value) {
            $json_records[] = [
                'id' => $value->id,
                'name' => $value->name,
            ];
        }

        $content = json_encode($json_records);
        $this->response = $this->response->withStringBody($content);
        $this->response = $this->response->withType('json');
        return $this->response;
    }

    public function getsalesmendata()
    {
        $this->autoRender = false;
        $salesmen = TableRegistry::get('Salesmen');
        $salesmen_data = $salesmen->find('all')
        ->where( [ "id" => $_POST['salesman_id'] ] );

        $json_records = array();
        foreach ($salesmen_data->toList() as $key => $value) {
            $json_records[] = [
                'id' => $value->id,
                'name' => $value->name,
                'email' => $value->rep_email,
            ];
        }

        $content = json_encode($json_records);
        $this->response = $this->response->withStringBody($content);
        $this->response = $this->response->withType('json');
        return $this->response;
    }

    public function getitemname()
    {
        $this->autoRender = false;
        $products = TableRegistry::get('Products');
        $salesmen_data = $products->find('all')
        ->distinct( 'item_name' )
        ->select( ['item_name'] )
        ->where( [ "product_name" => $_POST['product_name'] ] );

        $json_records = array();
        foreach ($salesmen_data->toList() as $key => $value) {
            $json_records[] = [
                'id' => $value->id,
                'item_name' => $value->item_name,
            ];
        }

        $content = json_encode($json_records);
        $this->response = $this->response->withStringBody($content);
        $this->response = $this->response->withType('json');
        return $this->response;
    }

    public function getitemnumber()
    {
        $this->autoRender = false;
        $products = TableRegistry::get('Products');
        $products_data = $products->find('all')
        ->select( [ 'id', 'item_number'] )
        ->where( [ "product_name" => $_POST['product_name'], 'item_name' => $_POST['item_name'] ] );

        $json_records = array();
        foreach ($products_data->toList() as $key => $value) {
            $json_records[] = [
                'id' => $value->id,
                'item_number' => $value->item_number,
            ];
        }

        $content = json_encode($json_records);
        $this->response = $this->response->withStringBody($content);
        $this->response = $this->response->withType('json');
        return $this->response;
    }

    public function getproduct()
    {
        $this->autoRender = false;
        $product = TableRegistry::get('Products');
        $product_data = $product->find('all')
        ->where( [ "id" => $_POST['product_id'] ] );

        $json_records = array();
        foreach ($product_data->toList() as $key => $value) {
            $json_records[] = [
                'id' => $value->id,
                'product_name' => $value->product_name,
                'item_name' => $value->item_name,
                'item_number' => $value->item_number,
                'item_type' => $value->item_type,
                'color_name' => $value->color_name,
                'single_franklin' => $value->single_franklin,
                'bulk_franklin' => $value->bulk_franklin,
                'per_item' => $value->per_item,
                'single_item' => $value->single_item,
                'bulk_item' => $value->bulk_item,
                'franklin_type' => $value->franklin_type,
                'bulk_count' => $value->bulk_count,

            ];
        }

        $content = json_encode($json_records);
        $this->response = $this->response->withStringBody($content);
        $this->response = $this->response->withType('json');
        return $this->response;
    }
}
