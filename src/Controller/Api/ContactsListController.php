<?php
namespace App\Controller\Api;

use App\Controller\AppController;

/**
 * ContactsList Controller
 *
 * @property \App\Model\Table\ContactsListTable $ContactsList
 *
 * @method \App\Model\Entity\ContactsList[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class ContactsListController extends AppController
{
     public function initialize()
    {
        parent::initialize();
        $this->loadComponent('RequestHandler');
         $this->Auth->allow([ 'viewcontact', 'addcontact','index']);
    }
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['CreditApplications']
        ];
        $contactsList = $this->paginate($this->ContactsList);

        $this->set(compact('contactsList'));
    }

    /**
     * View method
     *
     * @param string|null $id Contacts List id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function viewcontact()
    {
        $this->request->allowMethod(['post', 'put']);
        $id = $this->request->getData('id');
        $contactsList = $this->ContactsList->get($id, [
            'contain' => []
        ]);
        $this->set([
                    'status' => 1,
                    'contactsList' => $contactsList,
                    '_serialize' => ['status', 'contactsList']
                ]);
        //$this->set('contactsList', $contactsList);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function addcontact()
    {
        $this->request->allowMethod(['post', 'put']);

        $contactsList = $this->ContactsList->newEntity();
        if ($this->request->is('post')) {
            $contactsList_arr=[];
            $statement_name =[];
            $statement_email =[];
            $invoices_name =[];
            $invoices_email =[];
            $price_pages_name=[];
            $price_pages_email=[];
            $order_confirmation_name=[];
            $order_confirmation_email=[];
            $back_order_name =[];
            $back_order_email =[];
            $purchase_list_email = [];
            $purchase_list_name = [];
           
            //statement
            foreach(json_decode( $this->request->getData('statements'),true) as $k => $v){
               $statement_name[] = $v['name'];
               $statement_email[] = $v['email'];
            }
            $contactsList_arr['statement_name'] = serialize($statement_name);
            $contactsList_arr['statement_email'] = serialize($statement_email);
            //invoices_name
            foreach( json_decode($this->request->getData('invoices'),true) as $k => $v){
               $invoices_name[] = $v['name'];
               $invoices_email[] = $v['email'];
            }
            $contactsList_arr['invoice_name'] = serialize($invoices_name);
            $contactsList_arr['invoice_email'] = serialize($invoices_email);
            //price_pages_name
            foreach( json_decode( $this->request->getData('price_pages'),true) as $k => $v){
               $price_pages_name[] = $v['name'];
               $price_pages_email[] = $v['email'];
            }
            $contactsList_arr['price_pages_name'] = serialize($price_pages_name);
            $contactsList_arr['price_pages_email'] = serialize($price_pages_email);
            //order_confirmation_name
            foreach( json_decode( $this->request->getData('order_confirmation'),true) as $k => $v){
               $order_confirmation_name[] = $v['name'];
               $order_confirmation_email[] = $v['email'];
            }
            $contactsList_arr['order_confirmation_name'] = serialize($order_confirmation_name);
            $contactsList_arr['order_confirmation_email'] = serialize($order_confirmation_email);
            //back_order_name
            foreach( json_decode( $this->request->getData('back_order'),true) as $k => $v){
               $back_order_name[] = $v['name'];
               $back_order_email[] = $v['email'];
            }
            $contactsList_arr['bank_order_name'] = serialize($back_order_name);
            $contactsList_arr['bank_order_email'] = serialize($back_order_email);
            //purchase_list_email
             foreach(  json_decode( $this->request->getData('purchase_list'),true) as $k => $v){
               $purchase_list_name[] = $v['name'];
               $purchase_list_email[] = $v['email'];
            }

            $contactsList_arr['purchasing_agents_name'] = serialize($purchase_list_name);
            $contactsList_arr['purchasing_agents_email'] = serialize($purchase_list_email);
            $contactsList = $this->ContactsList->patchEntity($contactsList, $contactsList_arr);
            if ($this->ContactsList->save($contactsList)) {
                 $this->set([
                    'status' => 1,
                    'contactsList' => $contactsList,
                    '_serialize' => ['status', 'contactsList']
                ]);
            }else{
              // dd($contactsList->errors());
                 $this->set([
                    'status' => 0,
                    'contactsList' => $contactsList,
                    '_serialize' => ['status', 'contactsList']
                ]);
            }
           
        }
        
    }

    /**
     * Edit method
     *
     * @param string|null $id Contacts List id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $contactsList = $this->ContactsList->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $contactsList = $this->ContactsList->patchEntity($contactsList, $this->request->getData());
            if ($this->ContactsList->save($contactsList)) {
                $this->Flash->success(__('The contacts list has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The contacts list could not be saved. Please, try again.'));
        }
        $creditApplications = $this->ContactsList->CreditApplications->find('list', ['limit' => 200]);
        $this->set(compact('contactsList', 'creditApplications'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Contacts List id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $contactsList = $this->ContactsList->get($id);
        if ($this->ContactsList->delete($contactsList)) {
            $this->Flash->success(__('The contacts list has been deleted.'));
        } else {
            $this->Flash->error(__('The contacts list could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
