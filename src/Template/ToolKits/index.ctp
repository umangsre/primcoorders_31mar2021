<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Bill[]|\Cake\Collection\CollectionInterface $bill
 */
?>
<!--<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Tool Kit'), ['action' => 'add']) ?></li>
    </ul>
</nav>-->

<style>
input[type="submit"] {
    background: #2c323f;
    border: none;
    padding: 10px 20px;
    margin-top: 15px;
    color: #fff;
}
.ui-accordion .ui-accordion-content{
    height: auto !important;
}
</style>
<div class="bill index large-9 medium-8 columns content card card card-default scrollspy">
    <div class="card-content">
        <!--<h3><?= __('Tool Kits') ?></h3>-->
        <?php echo $this->Form->create('ToolKits');?>
        <div id="accordion">
            <?php foreach ($toolkitsCategories as $toolkitsCategory): ?>
                <h3><?php echo $toolkitsCategory->name?></h3>
                <div>
                    <table cellpadding="0" cellspacing="0">
                        <tbody>
                            <tr>
                                <th>Product Name</th>
                                <th>Product Code</th>
                                <th>Quantity</th>
                                <th>Required / Not Required</th>
                            </tr>
                            <?php foreach ($toolkitsCategory->ToolKits as $toolKit): ?>
                                <tr>
                                    <td><?php echo $toolKit->tool?></td>
                                    <td><?php echo $toolKit->item_code?></td>
                                    <td>
                                        <?php
                                            echo $this->Form->control('qty['.$toolKit->id.']', ['options' => array(0, 1,2,3,4,5,6,7,8,9,10),'label' => false,'class' => 'qty']);
                                        ?>
                                    </td>
                                    <td>
                                        <?php
                                            echo $this->Form->control('status['.$toolKit->id.']', ['type' => 'checkbox','label' => false,'class' => 'status','style'=>'position: unset;pointer-events: all;opacity: 1;cursor:pointer;']);
                                        ?>
                                    </td>
                                </tr>                                
                            <?php endforeach; ?>                            
                        </tbody>                        
                    </table>
                   <!-- <?= $this->Html->link(__('Add Product'), ['action' => 'addproduct'],['class' => 'btn waves-effect waves-light border-round col s3 mt-4','style' => 'background: #000; opacity:10;', 'name' => 'clicked', 'value' => 'save']) ?> -->
                </div>                
            <?php endforeach; ?>
        </div>
        
		<div style="clear: both;"><br/>
		  <div style="width: 50%; float: left;">
	          <?php
	          echo $this->Form->control('salesmanName',['label'=>'Salesman Name', 'readonly' => 'readonly', 'value'=>$user_name]);
	          ?>
		    </div>
		    <div class="form-group">
			<?php
            	echo $this->Form->control('sample_acct', ['options' => $salesRep,'label' => 'Sample Account Number','class' => 'qty']);
            ?>
		    </div>

		</div>
		
        
        <?php
        echo $this->Form->submit('Submit');
        echo $this->Form->end();
        ?>
    </div>
</div>