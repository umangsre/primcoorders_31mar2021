<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Salesman $salesman
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Salesman'), ['action' => 'edit', $salesman->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Salesman'), ['action' => 'delete', $salesman->id], ['confirm' => __('Are you sure you want to delete # {0}?', $salesman->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Salesmen'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Salesman'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Orders'), ['controller' => 'Orders', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Order'), ['controller' => 'Orders', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="salesmen view large-9 medium-8 columns content">
    <h3><?= h($salesman->name) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Name') ?></th>
            <td><?= h($salesman->name) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Sales Rep') ?></th>
            <td><?= h($salesman->sales_rep) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Sd Rep') ?></th>
            <td><?= h($salesman->sd_rep) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Rep Email') ?></th>
            <td><?= h($salesman->rep_email) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Spec') ?></th>
            <td><?= h($salesman->spec) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($salesman->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Created') ?></th>
            <td><?= h($salesman->created) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Updated') ?></th>
            <td><?= h($salesman->updated) ?></td>
        </tr>
    </table>
    <div class="related">
        <h4><?= __('Related Orders') ?></h4>
        <?php if (!empty($salesman->orders)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Customer Id') ?></th>
                <th scope="col"><?= __('Salesman Id') ?></th>
                <th scope="col"><?= __('Grand Total') ?></th>
                <th scope="col"><?= __('Po Number') ?></th>
                <th scope="col"><?= __('Ship To') ?></th>
                <th scope="col"><?= __('Ship Via') ?></th>
                <th scope="col"><?= __('Additional Comments') ?></th>
                <th scope="col"><?= __('Events') ?></th>
                <th scope="col"><?= __('Sale Order Number') ?></th>
                <th scope="col"><?= __('Incentive Reciept') ?></th>
                <th scope="col"><?= __('Sale Date') ?></th>
                <th scope="col"><?= __('Ship Date') ?></th>
                <th scope="col"><?= __('Purchaser Name') ?></th>
                <th scope="col"><?= __('Status') ?></th>
                <th scope="col"><?= __('Purchaser Sign') ?></th>
                <th scope="col"><?= __('Signature') ?></th>
                <th scope="col"><?= __('Created') ?></th>
                <th scope="col"><?= __('Updated') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($salesman->orders as $orders): ?>
            <tr>
                <td><?= h($orders->id) ?></td>
                <td><?= h($orders->customer_id) ?></td>
                <td><?= h($orders->salesman_id) ?></td>
                <td><?= h($orders->grand_total) ?></td>
                <td><?= h($orders->po_number) ?></td>
                <td><?= h($orders->ship_to) ?></td>
                <td><?= h($orders->ship_via) ?></td>
                <td><?= h($orders->additional_comments) ?></td>
                <td><?= h($orders->events) ?></td>
                <td><?= h($orders->sale_order_number) ?></td>
                <td><?= h($orders->incentive_reciept) ?></td>
                <td><?= h($orders->sale_date) ?></td>
                <td><?= h($orders->ship_date) ?></td>
                <td><?= h($orders->purchaser_name) ?></td>
                <td><?= h($orders->status) ?></td>
                <td><?= h($orders->purchaser_sign) ?></td>
                <td><?= h($orders->signature) ?></td>
                <td><?= h($orders->created) ?></td>
                <td><?= h($orders->updated) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Orders', 'action' => 'view', $orders->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'Orders', 'action' => 'edit', $orders->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Orders', 'action' => 'delete', $orders->id], ['confirm' => __('Are you sure you want to delete # {0}?', $orders->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>
