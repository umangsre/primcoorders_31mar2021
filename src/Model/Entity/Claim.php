<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Claim Entity
 *
 * @property int $id
 * @property string|null $consumer_id
 * @property string|null $customer_id
 * @property string|null $installed
 * @property string|null $date_installed
 * @property string|null $rooms_installed
 * @property string|null $claim_json
 * @property string|null $primco_invoice_number
 * @property string|null $miller_invoice_number
 * @property string|null $settlement_method
 * @property string|null $claim_pictures_1
 * @property string|null $claim_pictures_2
 * @property string|null $claim_pictures_3
 * @property string|null $claim_pictures_4
 * @property string|null $total_settlement_amount
 * @property string|null $created_by
 * @property string|null $miller_id
 * @property string|null $miller_user_id
 * @property \Cake\I18n\FrozenTime|null $created_date
 *
 */
class Claim extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'consumer_id' => true,
        'customer_id' => true,
        'installed' => true,
        'date_installed' => true,
        'rooms_installed' => true,
        'claim_json' => true,
        'primco_invoice_number' => true,
        'miller_invoice_number' => true,
        'settlement_method' => true,
        'claim_pictures_1' => true,
        'claim_pictures_2' => true,
        'claim_pictures_3' => true,
        'claim_pictures_4' => true,
        'total_settlement_amount' => true,
        'created_by' => true,
        'miller_id' => true,
        'miller_user_id' => true,
        'created_date' => true,
    ];
}
