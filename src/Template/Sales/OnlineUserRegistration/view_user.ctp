<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\OnlineUserRegistration $onlineUserRegistration
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Online User Registration'), ['action' => 'edit', $onlineUserRegistration->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Online User Registration'), ['action' => 'delete', $onlineUserRegistration->id], ['confirm' => __('Are you sure you want to delete # {0}?', $onlineUserRegistration->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Online User Registration'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Online User Registration'), ['action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="onlineUserRegistration view large-9 medium-8 columns content">
    <h3><?= h($onlineUserRegistration->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('First  Last Name') ?></th>
            <td><?= h($onlineUserRegistration->first__last_name) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Email') ?></th>
            <td><?= h($onlineUserRegistration->email) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($onlineUserRegistration->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('User Type') ?></th>
            <td><?= $this->Number->format($onlineUserRegistration->user_type) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Can See Account Details') ?></th>
            <td><?= $this->Number->format($onlineUserRegistration->can_see_account_details) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Can See Placed Order') ?></th>
            <td><?= $this->Number->format($onlineUserRegistration->can_see_placed_order) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Can See Invoiced Order') ?></th>
            <td><?= $this->Number->format($onlineUserRegistration->can_see_invoiced_order) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Can See Credit Memos') ?></th>
            <td><?= $this->Number->format($onlineUserRegistration->can_see_credit_memos) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Can See Prices') ?></th>
            <td><?= $this->Number->format($onlineUserRegistration->can_see_prices) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Can See Price List') ?></th>
            <td><?= $this->Number->format($onlineUserRegistration->can_see_price_list) ?></td>
        </tr>
    </table>
</div>
