<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Event Entity
 *
 * @property int $id
 * @property string $name
 * @property string|null $description
 * @property string|null $location
 * @property string|null $image
 * @property \Cake\I18n\FrozenTime $dated_and_time
 * @property \Cake\I18n\FrozenTime $created
 */
class Event extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'name' => true,
        'description' => true,
        'location' => true,
        'image' => true,
        'dated_and_time' => true,
        'created' => true
    ];
}
