<?php
namespace App\Controller\Api;
use Cake\I18n\Time;
use Cake\ORM\TableRegistry;
use App\Controller\AppController;

/**
 * Claims Controller
 *
 *
 * @method \App\Model\Entity\Claim[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class ClaimsController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
		$bill = $this->Claim->find('all');
		echo json_encode(array("response"=>$bill));
		exit;

    }

    /**
     * View method
     *
     * @param string|null $id Bill id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $bill = $this->Bill->get($id, [
            'contain' => []
        ]);

        $this->set('Bill', $bill);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
	 public function add()
    {
        $bill = $this->Bill->newEntity();
		$this->request->allowMethod(['post', 'put']);
        if ($this->request->is('post')) {

			$data = $this->request->getData();

			if (!empty($data['billing_image'])) {
				$dataimage = $data['billing_image'];
				list($type, $dataimage) = explode(';', $dataimage);
				list(, $dataimage)      = explode(',', $dataimage);
				$dataimage = base64_decode($dataimage);
				$newfilename = uniqid(). '.png';
				file_put_contents('bill_image/'.$newfilename, $dataimage);
				$data['billing_image'] = $newfilename;
			}



            $bill = $this->Bill->patchEntity($bill, $this->request->getData());
            if ($this->Bill->save($bill)) {
				echo json_encode(array("response"=>"Done"));
				exit;
			   $this->set('bill', $bill);
            }else{
				echo json_encode(array("response"=>"Error"));
				exit;
			}
        }
    }
    /* public function add()
    {
        $bill = $this->Bill->newEntity();
        if ($this->request->is('post')) {
            $bill = $this->Bill->patchEntity($Bill, $this->request->getData());
            if ($this->Bill->save($Bill)) {
                $this->Flash->success(__('The Bill has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The Bill could not be saved. Please, try again.'));
        }
        $this->set(compact('Bill'));
    } */

    /**
     * Edit method
     *
     * @param string|null $id Bill id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $bill = $this->Bill->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
			$data =  $this->request->getData();
			if (!empty($data['billing_image'])) {
				$dataimage = $data['billing_image'];
				list($type, $dataimage) = explode(';', $dataimage);
				list(, $dataimage)      = explode(',', $dataimage);
				$dataimage = base64_decode($dataimage);
				$newfilename = uniqid(). '.png';
				file_put_contents('bill_image/'.$newfilename, $dataimage);
				$data['billing_image'] = $newfilename;
			}
			if(!empty($data['billing_status'])){
			if($data['billing_status'] == 'D'){
				$data['billing_status'] = 'R';

				$articlesTable = TableRegistry::getTableLocator()->get('BillComment');
				$article = $articlesTable->newEntity();

				$article->comment = $data['billing_comment'];
				$article->billid = $id;

				if ($articlesTable->save($article)) {
					// The $article entity contains the id now
					$id = $article->id;
				}
				unset($data['billing_comment']);
			}
			}
            $bill = $this->Bill->patchEntity($bill,$data);
            if ($this->Bill->save($bill)) {
                echo json_encode(array("response"=>"Done"));
				exit;
            }
            //$this->Flash->error(__('The Bill could not be saved. Please, try again.'));
        }
        //$this->set(compact('Bill'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Bill id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $bill = $this->Bill->get($id);
        if ($this->Bill->delete($bill)) {

			echo json_encode(array("response"=>"Done"));
				exit;
           // $this->Flash->success(__('The Bill has been deleted.'));
        } else {
           // $this->Flash->error(__('The Bill could not be deleted. Please, try again.'));
        }

        //return $this->redirect(['action' => 'index']);
    }
}
