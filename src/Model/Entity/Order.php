<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Order Entity
 *
 * @property int $id
 * @property int|null $customer_id
 * @property int|null $salesman_id
 * @property float|null $grand_total
 * @property float|null $franklin_total
 * @property float|null $points_total
 * @property float|null $discount_total
 * @property float|null $gross_total
 * @property string|null $po_number
 * @property string|null $ship_to
 * @property string|null $ship_via
 * @property string|null $additional_comments
 * @property string|null $events
 * @property string|null $sale_order_number
 * @property string|null $incentive_reciept
 * @property \Cake\I18n\FrozenDate|null $sale_date
 * @property \Cake\I18n\FrozenDate|null $ship_date
 * @property string|null $purchaser_name
 * @property int|null $status
 * @property string|null $purchaser_sign
 * @property int|null $is_test
 * @property int|null $created_by
 * @property \Cake\I18n\FrozenTime|null $created
 * @property \Cake\I18n\FrozenTime|null $updated
 *
 * @property \App\Model\Entity\Customer $customer
 * @property \App\Model\Entity\Salesman $salesman
 * @property \App\Model\Entity\OrderCustomItem[] $order_custom_items
 * @property \App\Model\Entity\OrderItem[] $order_items
 */
class Order extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'customer_id' => true,
        'salesman_id' => true,
        'grand_total' => true,
        'franklin_total' => true,
        'points_total' => true,
        'discount_total' => true,
        'gross_total' => true,
        'po_number' => true,
        'ship_to' => true,
        'ship_via' => true,
        'additional_comments' => true,
        'events' => true,
        'sale_order_number' => true,
        'incentive_reciept' => true,
        'sale_date' => true,
        'ship_date' => true,
        'purchaser_name' => true,
        'status' => true,
        'purchaser_sign' => true,
        'is_test' => true,
        'created_by' => true,
        'created' => true,
        'updated' => true,
        'customer' => true,
        'salesman' => true,
        'order_custom_items' => true,
        'order_items' => true
    ];
}
