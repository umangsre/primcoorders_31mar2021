<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Bill $bill
 */
?>
<style>
    img.billing_image {
        height: 150px;
        width: 150px;
        margin-left: 10px;
    }
</style>
<div class="bill index large-9 medium-8 columns content card card card-default scrollspy">
    <div class="card-content">
        <div class="bill view large-9 medium-8 columns content">
            <!--<h3><?= h($bill->id) ?></h3>-->
            <table class="vertical-table">
                <tr>
                    <th scope="row"><?= __('Billing Type Of Expense') ?></th>
                    <td><?= h($bill->billing_type_of_expense) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Billing Place Of Expense') ?></th>
                    <td><?= h($bill->billing_place_of_expense) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Billing Tax') ?></th>
                    <td><?= h($bill->billing_tax) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Id') ?></th>
                    <td><?= $this->Number->format($bill->id) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Billing Amount') ?></th>
                    <td><?= $this->Number->format($bill->billing_amount) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Billing Total Amount') ?></th>
                    <td><?= $this->Number->format($bill->billing_total_amount) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Billing Date') ?></th>
                    <td><?= h($bill->billing_date) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Company Credit Card Used?') ?></th>
                    <td><?= h($bill->company_credit_card) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Billing Status') ?></th>
                    <td><?php if ($bill->billing_status == 'P') {
                            echo 'In Process';
                        } elseif ($bill->billing_status == 'A') {
                            echo 'Approve';
                        } elseif ($bill->billing_status == 'D') {
                            echo 'Decline';
                        } elseif ($bill->billing_status == 'R') {
                            echo 'Reupdate';
                        } ?></td>
                </tr>
            </table>
            <div class="row">
                <h4><?= __('Billing Image') ?></h4>
                <?php
                $explodeimage = explode(",", $bill->billing_image);
                $explodeimage = array_filter($explodeimage);
                if (!empty($explodeimage)) {
                    foreach ($explodeimage as $key => $value) {
                        echo "<a href='$value' target='_blank' ><img src='$value' class='billing_image' style='width: 100px;'></a>";
                    }
                }
                ?>

            </div>
            <div class="row">
                <h4><?= __('Billing History') ?></h4>

                <?php
                foreach ($billcommnets as $billcommnet) {
                    echo '<p style="border: 1px solid #eee;padding: 7px 30px;">' . $billcommnet->comment . ' <small style="float: right;">' . $billcommnet->created . '</small></p>';
                }
                if ($bill->billing_status == 'A') {
                    echo '<p style="border: 1px solid #eee;padding: 7px 30px;">The bill is approved </p>';
                }

                ?>

            </div>
        </div>
    </div>
</div>
