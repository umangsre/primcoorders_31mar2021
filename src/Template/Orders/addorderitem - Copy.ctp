<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Order $order
 */
print_r($order);
?>

<div class="row">
    <div class="col-12 logo-container text-center p-4">
        <?= $this->Html->image('logo.png'); ?>           
    </div>
    <div class="col-sm-12 col-12 full-width-mobile">
        <div class="steps-right-form">
            <div class="steps_nav text-center pt-3 mb-3 row">
                <div class="steps_item col">
                    <p>1<br>
                        <em>Dealer Section</em>
                    </p>
                </div>
                <div class="steps_item   col">
                    <p>2<br>
                        <em>Sales Section</em>
                    </p>
                </div>
                <div class="steps_item active col">
                    <p>3<br>
                        <em>Order Form</em>
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="orders form large-9 medium-8 columns content">
    <?= $this->Form->create($order) ?>
    <div class="row">
        <div class="tab w-100">
            <div class="form-fields">
                <div class="table-responsive">
                    <table id='userTable' class="table table-bordered">
                        <thead>
                            <tr>
                                <!--th>Sr.No.</th-->
                                <th>Product</th>
                                <th>Item Name</th>
                                <th>Item number</th>
                                <th>Description</th>
                                <th>Unit of Measure</th>
                                <th>Price</th>
                                <th>Quantity</th>
                                <th>Total</th>
                                <th>Enable Franklin</th>
                                <th>Cash Incentives</th>
                                <th>promotions Points/UOM</th>
                                <th>Discount</th>
                            </tr>
                        </thead>
                        <tbody id="myTabledata">
                            <?php
                                $j=1;
                                while ($j < 13) {
                                ?>
                            <tr class="product_row" data-index="<?php  echo $j; ?>" data-filled=""  data-order_id="" >
                                <input type="hidden" name="product_json" class="product_json">
                                <!--td class="list_item" ><?php  echo $j; ?> </td -->
                                <td class="list_item item_ls" >
                                    <select class="product_name"  name="product_name[]"  >
                                        <option value="">Select Product</option>
                                        <?php foreach($product_records as $item_name):?>
                                        <option value="<?=$item_name['name']?>"><?=$item_name['name']?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </td>
                                <td class="list_item item_ls" >
                                    <select class="item_name"  name="item_name[]"  >
                                        <option>Select Item</option>
                                    </select>
                                </td>
                                <td class="list_item item_ls" >
                                    <select class="item_number"  name="item_number[]"  >
                                        <option>Select Item Number</option>
                                    </select>
                                </td>
                                <td class="list_item" >
                                    <input class="desc"  name="desc[]"  />
                                    <div class="suggesstion-products list_view"></div>
                                    <input type="hidden" class="product_id"  name="product_id[]"  />
                                </td>
                                <td class="list_item" >
                                    <input class="item_type"  name="item_type[]"  />
                                </td>
                                <td class="list_item" ><input type="text"  min="1" class="price" name="price[]"  />
                                </td>
                                <td class="list_item"> 
                                <input  type="number"  min="1"  class="quantity"  name="quantity[]"  />
                                </td>
                                
                                <td class="list_item" >
                                    <input class="total"  name="total[]"  />
                                </td>
                                <td class="list_item" >
                                    <input type="checkbox" class="franklin-checkbox"  name="franklin_enabled[]" value="1" />
                                    <input type="hidden" class="franklin-checkbox-input"  name="franklin_enabled_input[]" value="0" />
                                </td>
                                <td class="list_item" ><input class="franklin d-none"   name="incentives[]"  /></td>
                                <td class="list_item" ><input  min="0"   type="number"class="promo"  name="promo[]"  /></td>
                                <td class="list_item"> 
                                <input  type="number"  min="1"  class="discount"  name="discount[]"  />
                                </td>
                                
                            </tr>
                            <?php
                                $j++;
                                }
                                ?>
                        </tbody>
                    </table>
                    <div class="col-12">
                        <p>
                            <span>Grand Total: $</span>
                            <span id="order-grand-total">0.00</span>
                        </p>
                        
                    </div>
                    <div class="btn btn-default add-morebtn">
                        <?= $this->Form->button(__('Add More'), array( 'class' => 'btn btn-info tr_clone_add' , 'type' => 'button' )  ) ?>

                        <?= $this->Form->button(__('Submit'), array( 'class' => 'btn btn-primary' )  ) ?>
                        
                        <!-- <button type="button" class="btn btn-info tr_clone_add">
                            Add More
                        </button> -->
                    </div>
                </div>
            </div>
        </div>
    </div>    

    
    <?= $this->Form->end() ?>
</div>
