<?php


namespace App\Controller;


use Cake\I18n\Time;

class ConsumerGenericMethods extends AppController
{
    public function initialize()
    {
        parent::initialize();
        $this->loadModel('Consumers');
    }

    public function getAllConsumers($search = false, $searchData = '') {
        $query = $this->Consumers->find('all');
        return $query;
    }

    public function addConsumer($consumerDetails) {
        $addConsumer = $this->Consumers->newEntity();
        $addConsumer = $this->Consumers->patchEntity($addConsumer, $consumerDetails);
        if ($this->Consumers->save($addConsumer)) {
            return $addConsumer;
        }
    }
}
