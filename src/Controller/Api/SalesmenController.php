<?php
namespace App\Controller\Api;

use App\Controller\AppController;

/**
 * Salesmen Controller
 *
 * @property \App\Model\Table\SalesmenTable $Salesmen
 *
 * @method \App\Model\Entity\Salesman[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class SalesmenController extends AppController
{
    public function initialize()
    {
        parent::initialize();
        $this->loadComponent('RequestHandler');
        $this->Auth->allow(['index']);
    }
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $salesmen = $this->Salesmen->find('all');
         $this->set([
            'salesmen' => $salesmen,
            '_serialize' => ['salesmen']
                ]); 
        
        // $salesmen = $this->paginate($this->Salesmen);

        // $this->set(compact('salesmen'));
    }

    /**
     * View method
     *
     * @param string|null $id Salesman id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $salesman = $this->Salesmen->get($id, [
            'contain' => ['Orders']
        ]);

        $this->set('salesman', $salesman);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $salesman = $this->Salesmen->newEntity();
        if ($this->request->is('post')) {
            $salesman = $this->Salesmen->patchEntity($salesman, $this->request->getData());
            if ($this->Salesmen->save($salesman)) {
                $this->Flash->success(__('The salesman has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The salesman could not be saved. Please, try again.'));
        }
        $this->set(compact('salesman'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Salesman id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $salesman = $this->Salesmen->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $salesman = $this->Salesmen->patchEntity($salesman, $this->request->getData());
            if ($this->Salesmen->save($salesman)) {
                $this->Flash->success(__('The salesman has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The salesman could not be saved. Please, try again.'));
        }
        $this->set(compact('salesman'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Salesman id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $salesman = $this->Salesmen->get($id);
        if ($this->Salesmen->delete($salesman)) {
            $this->Flash->success(__('The salesman has been deleted.'));
        } else {
            $this->Flash->error(__('The salesman could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
