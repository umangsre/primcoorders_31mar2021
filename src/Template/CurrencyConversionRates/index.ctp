<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\ccr[]|\Cake\Collection\CollectionInterface $currencyConversionRates
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Currency Conversion Rate'), ['action' => 'add']) ?></li>
    </ul>
</nav>
<div class="currencyConversionRates index large-9 medium-8 columns content">
    <h3><?= __('Currency Conversion Rates') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('cad') ?></th>
                <th scope="col"><?= $this->Paginator->sort('usd_mannington') ?></th>
                <th scope="col"><?= $this->Paginator->sort('usd_general') ?></th>
                <th scope="col"><?= $this->Paginator->sort('usd_others') ?></th>
                <th scope="col"><?= $this->Paginator->sort('usd_custom_1') ?></th>
                <th scope="col"><?= $this->Paginator->sort('usd_custom_2') ?></th>
				<th scope="col"><?= $this->Paginator->sort('usd_custom_3') ?></th>
                <th scope="col"><?= $this->Paginator->sort('usd_custom_4') ?></th>
                <th scope="col"><?= $this->Paginator->sort('created') ?></th>
                <th scope="col"><?= $this->Paginator->sort('updated') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($currencyConversionRates as $currencyConversionRate): ?>
            <tr>
                <td><?= $this->Number->format($currencyConversionRate->id) ?></td>
                <td><?= $this->Number->format($currencyConversionRate->cad) ?></td>
                <td><?= $this->Number->format($currencyConversionRate->usd_mannington) ?></td>
                <td><?= $this->Number->format($currencyConversionRate->usd_general) ?></td>
                <td><?= $this->Number->format($currencyConversionRate->usd_others) ?></td>
                <td><?= $this->Number->format($currencyConversionRate->usd_custom_1) ?></td>
                <td><?= $this->Number->format($currencyConversionRate->usd_custom_2) ?></td>
				<td><?= $this->Number->format($currencyConversionRate->usd_custom_3) ?></td>
                <td><?= $this->Number->format($currencyConversionRate->usd_custom_4) ?></td>
                <td><?= h($currencyConversionRate->created) ?></td>
                <td><?= h($currencyConversionRate->updated) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $currencyConversionRate->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $currencyConversionRate->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $currencyConversionRate->id], ['confirm' => __('Are you sure you want to delete # {0}?', $currencyConversionRate->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div>
