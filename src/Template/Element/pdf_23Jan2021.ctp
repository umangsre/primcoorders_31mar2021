<?php
     $tr = "";
	$dealer_name =$pdf_data['customer']['name']?:'';
	$sin =$pdf_data['customer']['sin']? 'OnFile':'Required';
	$city = $pdf_data['customer']['city']?:'';
	$province =$pdf_data['customer']['province']?:'';
	$sm_name= $pdf_data['salesman']['name']?:'';
	$grand_total = 0;
	$franklin_total =0;
	$points_total =0;
	$discount_total =0;
	$gross_total =0;
	$po_number = $pdf_data['order']['po_number']?:'';
	$ship_to = $pdf_data['order']['ship_to']?:'';
	$ship_date = $pdf_data['order']['ship_date']?:'';
	$ship_via = $pdf_data['order']['ship_via']?:'';
	$additional_comments =$pdf_data['order']['additional_comments']?:'';
	$events = $pdf_data['order']['events']?:'';
	$sale_order_number = $pdf_data['order']['sale_order_number']?:'';
	$incentive_ack = $pdf_data['order']['incentive_reciept']?:'';
	$sale_date = $pdf_data['order']['sale_date']?:'';
	$purchaser_name = $pdf_data['order']['purchaser_name']?:'';
	$signature = $this->Url->image('/img/Signs/primco-sign-'.$pdf_data['order']['id'].'.jpg');
	$_signature = BASE_URL.'webroot/img/Signs/primco-sign-'.$pdf_data['order']['id'].'.jpg';
	$logo = BASE_URL.'webroot/img/primco-logo.png';
	
	/*echo $_signature ; 
	die();*/
	$line =1;           
		foreach($pdf_data['order_item'] as $key =>$order_row){
			//echo $order_row['total'];
				
			      $grand_total += $order_row['total'];
				  $franklin_total +=$order_row['incentives']?:0;
				  $points_total +=$order_row['promo']?:0;
				  $discount =$order_row['discount']?:0;
				  $discount_total+=$order_row['discount'];
					$inc = $order_row['incentives'] > 0 ? '$'.$order_row['incentives'] : '';
					$tr .= "<tr>
							<td>".$line++."</td>
							<td>".$order_row['quantity']."</td>
							<td>".$order_row['unit']."</td>
							<td>".$order_row['product_name']."</td>
							<td>$".$order_row['price']."</td>
							<td>$".$discount."</td>
							<td>$".$order_row['total']."</td>
							<td>".$order_row['promo']."</td>
							<td>". $inc ."</td>
						    </tr>";
		}
		$gross_total =$grand_total + $discount_total;
		if($pdf_data['order']['grand_total']){
			$grand_total = $pdf_data['order']['grand_total'];
		}

		if($pdf_data['order']['franklin_total']){
			$franklin_total = $pdf_data['order']['franklin_total'];
		}

		if($pdf_data['order']['points_total']){
			$points_total = $pdf_data['order']['points_total'];
		}

		if($pdf_data['order']['discount_total']){
			$discount_total = $pdf_data['order']['discount_total'];
		}

		if($pdf_data['order']['gross_total']){
			$gross_total = $pdf_data['order']['gross_total'];
		}
		




// Set some content to print
echo $html = <<<EOD
<!DOCTYPE html>
<html>
<head>
	<title>Print &amp; Preview</title>
	<link href="https://fonts.googleapis.com/css?family=Montserrat:400,600,800&display=swap" rel="stylesheet">
</head>
<body>
<style type="text/css">
body {
    background: #555;
    font-family: 'Montserrat', sans-serif;
    font-size: 12px;
}
table {
    border: 1px solid black;
    border-collapse: separate;
    border-spacing: 0;
}
table td, table th {
    border: 1px solid red;
	
}

table tr td {
    border-right: 0;
}
table tr:last-child td {
    border-bottom: 0;
}
table tr td:first-child,
table tr th:first-child {
    border-left: 0;
}
table tr td{
    border-top: 0;
}

table {
    width: 100%;
}
table th {
    font-weight: bold;

}
table th, table td {
    padding: 5px;
    vertical-align: top;
}
.main-table{
	width: 665px;
	margin: 0 auto;
	background: #fff;
	padding: 5px;
	height:auto;
}

table tr,table tr th,table tr td {
    border: none !important;
}

th {
    
	font-weight: bold;
	font-size: 12px;
}
.border {
   
	border-collapse: collapse;
}
table.border tr td, table.border tr th {
    border:1px solid #cecece;
	border-collapse: collapse;
}
table.border tr td:last-child, table.border tr th:last-child {
    border-right: none;
}
table.border tr {
    margin: 0;
    border: none;
    border-spacing: 0px;
}
table.border tr:last-child td, table.border tr:last-child th {
    border-bottom: none !important;
}
.print-button-container{
	width: 920px;
	margin: 0 auto;
	text-align: center;
	padding: 10px 0px;
	background: #fff;
	display: block;
}
.print-button{
	padding: 10px 20px;
	font-size: 14px;
	background-color: #1f555f;
	color: #fff;
	border: 1px solid #1f555f;
}
@media print{
	.print-button-container{
		display: none;
	}
}
</style>

<table class="main-table" cellspacing="0" id="order-form">
	<tr>
		<td>
			<table cellspacing="0" border="0">
				<tr style="height: 10px; padding: 2px;">
					<td></td>
					<td></td>
					<td style="padding: 0px;vertical-align: bottom; padding-left: 10px;"><strong>Phone</strong></td>
					<td style="padding: 0px;vertical-align: bottom; padding-left: 10px;"><strong>Fax</strong></td>
				</tr>
				<tr>
					<td>
						<img style="width: 250px;" src="{$logo}">
					</td>
					<td>

						<p style="font-size:10px">12300 - 44th Street SE, Calgary, AB T2Z 4A2<br>
						14320 - 112th Avenue, Edmonton, AB T5M 2T9<br>
						704 - 47th Street East, Saskatoon, SK S7K 0X3<br>
						645 Mcleod Street, Regina, SK S4N 4Y2<br>
						Winnipeg, MB<br>
						Vancouver, BC</p>
					
					</td>
					<td>
						
						<p style="font-size:10px">(403) 730-0222<br>
						(780) 454-0316<br>
						(306) 934-6121<br>
						(306) 721-8484<br>
						(204) 6337256<br>
						(604) 278-7771</p>

					</td>
					<td style="font-size:10px">(403) 295-8313<br>
						(780) 452-3377<br>
						(306) 934-1833<br>
						<h2>Order #<br>Surfaces202000{$order_id}</h2>

					</td>

				</tr>
			</table>
		</td>
		
	</tr>
	<tr>
		
		<td>
			<table class="border" border="0" cellspacing="0">
				<tr>
					<th>Dealer's Name</th>
					<td>{$dealer_name}</td>
					<th>P.O. Number</th>
					<td># {$po_number} </td>
				</tr>
				<tr>
					<th>Ship to</th>
					<td> {$ship_to} </td>
					<th>Ship Date</th>
					<td>{$ship_date }</td>
				</tr>
				<tr>
					<th>City &amp; Prov</th>
					<td>{$city}, {$province}</td>
					<th>Ship via</th>
					<td>{$ship_via}</td>
				</tr>
			</table>
		</td>

	</tr>
	<tr>
		<td>
			<table cellspacing="0"  class="border">
				<tr>
					<th>Additional Comments (Tag and/or Special Quotes):</th>
				</tr>
				<tr>
					<td>
					{$additional_comments}
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td>
			<table class="border" cellspacing="0">
				<tr>
					<th style="width:30px;"></th>
					<th style="width:50px">Qty</th>
					<th>Unit of Mes.</th>
					<th style="width:196px">Description</th>
					<th style="width:75px">Price</th>
					<th style="width:62px">Discount</th>
					<th style="width:64px">Total</th>
					<th style="width:54px">Points</th>
					<th style="width:58px">Franklin</th>
				</tr>
				{$tr}
				
				<tr>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td><small><strong>G.S.T. Reg. No. 821316346RT0001</strong></small></td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>Grand Total is before GST, other taxes, and additional freight charges.</td>
					<td>&nbsp;</td>
					<td>$ {$discount_total}</td>
					<td>$ {$grand_total}</td>
					<td>{$points_total}</td>
					<td> {$franklin_total}</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>Gross Total (Grand Total + Discount)</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>$ {$gross_total}</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td style="font-size:10px">
			<center>DUE TO SPECIAL PRICING AND INCENTIVES, ALL ITEMS WILL SHIP UPON ARRIVAL AT PRIMCO LOCAL WAREHOUSE (NO EXCEPTIONS). ORDER MAY BE PARTIALLY SHIPPED, DEPENDING ON ARRIVAL OF GOODS. PAYMENT IS DUE AS PER CREDIT TERMS IN PLACE. <br><br>
			Purchaser's Initials__________________</center>
		</td>
	</tr>
	<tr>
		<td> 
			<table class="border" cellspacing="0" style="width:80%;">
				<tr>
					<th style="width: 50%;">Events (Conventions, Etc.)</th>
					<td colspan="3">{$events}</td>
				</tr>
				<tr>
					<th>Primco Sales Representative</th>
					<td>{$sm_name}</td>
					<th>Sales Order #</th>
					<td>SURFACES202000{$order_id}</td>
				</tr>
				<tr>
					<th>I have received an incentive in the amount of and acknowledge that I will receive a T4 slip from Primco</th>
					<td>{$incentive_ack}</td>
					<th>Date</th>
					<td>{$sale_date}</td>
				</tr>
				<tr>
					<th>SIN</th>
					<td colspan="3">{$sin}</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td style="font-size:10px">I agree that if goods are not shipped within the terms agreed upon, I will be charged back any funds given as an incentive for this order.<br><br>
			Purchaser's Initials ____________________________
		</td>
	</tr>
	<tr>
		<td>
			<table class="border" cellspacing="0">
				<tr>
					<td  style="width: 50%">Purchaser's Name: <strong>{$purchaser_name}</strong></td>
					<td rowspan="2" class="border">
						<ul style="font-size:10px" >
							<li>PRICE DO NOT INCLUDE G.S.T.</li>
							<li>THIS ORDER IS NOT SUBJECT TO COUNTERMAND</li>
							<li>PRICES F.O.B. LOCAL WAREHOUSE</li>
							<li>NO GOODS SOLD ON CONSIGNMENT</li>
							<li>NO REPRESENTATIONS, AGREEMENTS OR PROMISES OF SALESMAN (NOT APPEARING ON THE ORIGINAL OF THIS ORDER)</li>
							<li>WHETHER VERBAL OR IN WRITING SHALL BE VALID, EXCEPT WHEN CONFIRMED IN WRITING BY THE COMPANY. EXECUTION OF</li>
							<li>THIS ORDER IS CONTINGENT UPON STRIKE, ACCIDENTS, DELAYS OF CARRIERS AND OTHER DELAYS UNAVOIDABLE OR BEYOND</li>
							<li>OUR CONTROL. COVERING CAPACITY ESTIMATED, NOT GUARANTEED.</li>
						</ul>
	
					</td>
				</tr>
				<tr>
					<td class="border" style="width: 50%">
						Purchaser's Signature <br /><img height="150" src="{$_signature}">
						
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>


</body>
</html>
EOD;
?>