<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * ContactsList Entity
 *
 * @property int $id
 * @property int|null $credit_application_id
 * @property string $statement_name
 * @property string $statement_email
 * @property string $invoice_name
 * @property string $invoice_email
 * @property string $price_pages_name
 * @property string $price_pages_email
 * @property string $order_confirmation_name
 * @property string $order_confirmation_email
 * @property string $bank_order_name
 * @property string $bank_order_email
 * @property string $purchasing_agents_name
 * @property string $purchasing_agents_email
 * @property string|null $_date
 * @property string|null $authorized_signature
 * @property string|null $print_name
 * @property string|null $_title
 */
class ContactsList extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'credit_application_id' => true,
        'statement_name' => true,
        'statement_email' => true,
        'invoice_name' => true,
        'invoice_email' => true,
        'price_pages_name' => true,
        'price_pages_email' => true,
        'order_confirmation_name' => true,
        'order_confirmation_email' => true,
        'bank_order_name' => true,
        'bank_order_email' => true,
        'purchasing_agents_name' => true,
        'purchasing_agents_email' => true,
        '_date' => true,
        'authorized_signature' => true,
        'print_name' => true,
        '_title' => true
    ];
}
