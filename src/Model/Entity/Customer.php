<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Customer Entity
 *
 * @property int $id
 * @property int|null $customerid
 * @property string $name
 * @property string $contact_name
 * @property string $phone
 * @property string $address
 * @property string $city
 * @property string $province
 * @property string $postal_code
 * @property string|null $sin
 * @property int|null $created_by
 * @property \Cake\I18n\FrozenTime|null $created
 * @property \Cake\I18n\FrozenTime|null $updated
 */
class Customer extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'user_id' => true,
        'customerid' => true,
        'name' => true,
        'contact_name' => true,
        'phone' => true,
        'address' => true,
        'city' => true,
        'province' => true,
        'postal_code' => true,
        'sin' => true,
        'created_by' => true,
        'created' => true,
        'updated' => true
    ];
}
