<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Retailer Entity
 *
 * @property int $id
 * @property string|null $parent_category
 * @property string|null $category
 * @property string|null $labour_cost_sq_ft
 * @property string|null $labour_cost_sq_yards
 * @property string|null $labour_cost_linear_ft
 *
// * @property \App\Model\Entity\Claim[] $claims
 */
class PrimcoCategoryLabourCost extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'parent_category' => true,
        'category' => true,
        'labour_cost_sq_ft' => true,
        'labour_cost_sq_yards' => true,
        'labour_cost_linear_ft' => true,
    ];
}
