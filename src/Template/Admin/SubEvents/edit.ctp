<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\SubEvent $subEvent
 */
?>
<div class="orders index large-9 medium-8 columns content card card card-default scrollspy">
    <div class="card-content">
        <div class="subEvents form large-9 medium-8 columns content">
            <?= $this->Form->create($subEvent) ?>
            <fieldset>
                <legend><?= __('Edit Sub Event') ?></legend>
                <?php
                    echo $this->Form->control('event_id', ['options' => $events]);
                    echo $this->Form->control('name');
                    echo $this->Form->control('description');
                    echo $this->Form->control('event_date');
                    echo $this->Form->control('from_time', ['empty' => true]);
                    echo $this->Form->control('to_time', ['empty' => true]);
                ?>
            </fieldset>
            <?= $this->Form->button(__('Submit')) ?>
            <?= $this->Form->end() ?>
        </div>
    </div>
</div>
