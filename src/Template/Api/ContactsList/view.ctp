<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\ContactsList $contactsList
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Contacts List'), ['action' => 'edit', $contactsList->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Contacts List'), ['action' => 'delete', $contactsList->id], ['confirm' => __('Are you sure you want to delete # {0}?', $contactsList->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Contacts List'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Contacts List'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Credit Applications'), ['controller' => 'CreditApplications', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Credit Application'), ['controller' => 'CreditApplications', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="contactsList view large-9 medium-8 columns content">
    <h3><?= h($contactsList->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Credit Application') ?></th>
            <td><?= $contactsList->has('credit_application') ? $this->Html->link($contactsList->credit_application->id, ['controller' => 'CreditApplications', 'action' => 'view', $contactsList->credit_application->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Statement Name') ?></th>
            <td><?= h($contactsList->statement_name) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Statement Email') ?></th>
            <td><?= h($contactsList->statement_email) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Invoice Name') ?></th>
            <td><?= h($contactsList->invoice_name) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Invoice Email') ?></th>
            <td><?= h($contactsList->invoice_email) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Price Pages Name') ?></th>
            <td><?= h($contactsList->price_pages_name) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Price Pages Email') ?></th>
            <td><?= h($contactsList->price_pages_email) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Order Confirmation Name') ?></th>
            <td><?= h($contactsList->order_confirmation_name) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Order Confirmation Email') ?></th>
            <td><?= h($contactsList->order_confirmation_email) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Bank Order Name') ?></th>
            <td><?= h($contactsList->bank_order_name) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Bank Order Email') ?></th>
            <td><?= h($contactsList->bank_order_email) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Purchasing Agents Name') ?></th>
            <td><?= h($contactsList->purchasing_agents_name) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Purchasing Agents Email') ?></th>
            <td><?= h($contactsList->purchasing_agents_email) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __(' Date') ?></th>
            <td><?= h($contactsList->_date) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Authorized Signature') ?></th>
            <td><?= h($contactsList->authorized_signature) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Print Name') ?></th>
            <td><?= h($contactsList->print_name) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __(' Title') ?></th>
            <td><?= h($contactsList->_title) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($contactsList->id) ?></td>
        </tr>
    </table>
</div>
