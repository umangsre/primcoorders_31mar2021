<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Meeting $meeting
 */
?>
<!-- <nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List Meetings'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Salesmen'), ['controller' => 'Salesmen', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Salesman'), ['controller' => 'Salesmen', 'action' => 'add']) ?></li>
    </ul>
</nav> -->
<div class="orders index large-9 medium-8 columns content card card card-default scrollspy">
    <div class="card-content">
        <div class="meetings form large-9 medium-8 columns content">
            <?= $this->Form->create($meeting) ?>
            <fieldset>
                <legend><?= __('Meeting By Sale Rep') ?></legend>
                <?php
                 echo $this->Form->control('salesman_id', ['options' => $salesmen, 'empty' => true]);
                    echo $this->Form->control('email',['label' =>'Customer\'s email']);
                    echo $this->Form->control('agenda');
                    echo $this->Form->control('description',['rows' => '10', 'cols' => '10']);
                    echo $this->Form->control('date_time');
                   // echo $this->Form->control('is_customer_meeting');
                ?>
            </fieldset>
            <?= $this->Form->button(__('Submit')) ?>
            <?= $this->Form->end() ?>
        </div>
    </div>
</div>
