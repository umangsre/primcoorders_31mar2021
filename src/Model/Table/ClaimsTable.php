<?php

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Claims Model
 *
 * @property &\Cake\ORM\Association\BelongsTo $Customers
 * @property &\Cake\ORM\Association\BelongsTo $Consumers
 * @property \App\Model\Table\ClaimProductsTable&\Cake\ORM\Association\HasMany $ClaimStatus
 *
 * @method \App\Model\Entity\Claim get($primaryKey, $options = [])
 * @method \App\Model\Entity\Claim newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Claim[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Claim|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Claim saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Claim patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Claim[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Claim findOrCreate($search, callable $callback = null, $options = [])
 */
class ClaimsTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('claims');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');
        $this->belongsTo('Customers', [
            'foreignKey' => 'customer_id'
        ]);
        $this->belongsTo('Consumers', [
            'foreignKey' => 'consumer_id'
        ]);
        $this->belongsTo('Users', [
            'foreignKey' => 'created_by'
        ]);
        $this->belongsTo('Millers', [
            'foreignKey' => 'miller_id'
        ]);
        $this->belongsTo('Users', [
            'foreignKey' => 'miller_user_id'
        ]);
        $this->hasMany('ClaimStatus', [
            'foreignKey' => 'claim_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->allowEmptyString('id', null, 'create');

        /*$validator
            ->scalar('claim_number')
            ->maxLength('claim_number', 50)
            ->allowEmptyString('claim_number');

        $validator
            ->scalar('dealer_invoice')
            ->maxLength('dealer_invoice', 50)
            ->allowEmptyString('dealer_invoice');

        $validator
            ->scalar('primco_invoice')
            ->maxLength('primco_invoice', 50)
            ->allowEmptyString('primco_invoice');

        $validator
            ->scalar('type_of_claim')
            ->maxLength('type_of_claim', 50)
            ->allowEmptyString('type_of_claim');

        $validator
            ->integer('quantity_purchased')
            ->allowEmptyString('quantity_purchased');

        $validator
            ->date('notification_date')
            ->allowEmptyDate('notification_date');

        $validator
            ->scalar('photo_attached')
            ->allowEmptyString('photo_attached');

        $validator
            ->scalar('samples_available')
            ->allowEmptyString('samples_available');

        $validator
            ->scalar('detailed_claim_reason')
            ->allowEmptyString('detailed_claim_reason');

        $validator
            ->dateTime('created_date')
            ->allowEmptyDateTime('created_date');

        $validator
            ->scalar('date_installed')
            ->maxLength('date_installed', 100)
            ->allowEmptyString('date_installed');

        $validator
            ->scalar('areas_installed')
            ->maxLength('areas_installed', 100)
            ->allowEmptyString('areas_installed');

        $validator
            ->scalar('installed')
            ->maxLength('installed', 10)
            ->allowEmptyString('installed');

        $validator
            ->scalar('request_for_resolution')
            ->allowEmptyString('request_for_resolution');*/

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['customer_id'], 'Customers'));

        return $rules;
    }
}
