<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\OnlineUserRegistration[]|\Cake\Collection\CollectionInterface $onlineUserRegistration
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Online User Registration'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Credit Applications'), ['controller' => 'CreditApplications', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Credit Application'), ['controller' => 'CreditApplications', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="onlineUserRegistration index large-9 medium-8 columns content">
    <h3><?= __('Online User Registration') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('credit_application_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('user_type') ?></th>
                <th scope="col"><?= $this->Paginator->sort('submission_date') ?></th>
                <th scope="col"><?= $this->Paginator->sort('customer_number') ?></th>
                <th scope="col"><?= $this->Paginator->sort('company_name') ?></th>
                <th scope="col"><?= $this->Paginator->sort('administartor_will_see') ?></th>
                <th scope="col"><?= $this->Paginator->sort('standard_name') ?></th>
                <th scope="col"><?= $this->Paginator->sort('standard_email') ?></th>
                <th scope="col"><?= $this->Paginator->sort('standard_name1') ?></th>
                <th scope="col"><?= $this->Paginator->sort('standard_email1') ?></th>
                <th scope="col"><?= $this->Paginator->sort('standard_name2') ?></th>
                <th scope="col"><?= $this->Paginator->sort('standard_email2') ?></th>
                <th scope="col"><?= $this->Paginator->sort('standard_name3') ?></th>
                <th scope="col"><?= $this->Paginator->sort('standard_email3') ?></th>
                <th scope="col"><?= $this->Paginator->sort('first__last_name') ?></th>
                <th scope="col"><?= $this->Paginator->sort('user_email') ?></th>
                <th scope="col"><?= $this->Paginator->sort('can_see_account_details') ?></th>
                <th scope="col"><?= $this->Paginator->sort('can_see_placed_order') ?></th>
                <th scope="col"><?= $this->Paginator->sort('can_see_invoiced_order') ?></th>
                <th scope="col"><?= $this->Paginator->sort('can_see_credit_memos') ?></th>
                <th scope="col"><?= $this->Paginator->sort('can_see_prices') ?></th>
                <th scope="col"><?= $this->Paginator->sort('can_see_price_list') ?></th>
                <th scope="col"><?= $this->Paginator->sort('print_name') ?></th>
                <th scope="col"><?= $this->Paginator->sort('authorized_signature') ?></th>
                <th scope="col"><?= $this->Paginator->sort('_date') ?></th>
                <th scope="col"><?= $this->Paginator->sort('_title') ?></th>
                <th scope="col"><?= $this->Paginator->sort('anti_spam_consent') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($onlineUserRegistration as $onlineUserRegistration): ?>
            <tr>
                <td><?= $this->Number->format($onlineUserRegistration->id) ?></td>
                <td><?= $onlineUserRegistration->has('credit_application') ? $this->Html->link($onlineUserRegistration->credit_application->id, ['controller' => 'CreditApplications', 'action' => 'view', $onlineUserRegistration->credit_application->id]) : '' ?></td>
                <td><?= h($onlineUserRegistration->user_type) ?></td>
                <td><?= h($onlineUserRegistration->submission_date) ?></td>
                <td><?= h($onlineUserRegistration->customer_number) ?></td>
                <td><?= h($onlineUserRegistration->company_name) ?></td>
                <td><?= h($onlineUserRegistration->administartor_will_see) ?></td>
                <td><?= h($onlineUserRegistration->standard_name) ?></td>
                <td><?= h($onlineUserRegistration->standard_email) ?></td>
                <td><?= h($onlineUserRegistration->standard_name1) ?></td>
                <td><?= h($onlineUserRegistration->standard_email1) ?></td>
                <td><?= h($onlineUserRegistration->standard_name2) ?></td>
                <td><?= h($onlineUserRegistration->standard_email2) ?></td>
                <td><?= h($onlineUserRegistration->standard_name3) ?></td>
                <td><?= h($onlineUserRegistration->standard_email3) ?></td>
                <td><?= h($onlineUserRegistration->first__last_name) ?></td>
                <td><?= h($onlineUserRegistration->user_email) ?></td>
                <td><?= $this->Number->format($onlineUserRegistration->can_see_account_details) ?></td>
                <td><?= $this->Number->format($onlineUserRegistration->can_see_placed_order) ?></td>
                <td><?= $this->Number->format($onlineUserRegistration->can_see_invoiced_order) ?></td>
                <td><?= $this->Number->format($onlineUserRegistration->can_see_credit_memos) ?></td>
                <td><?= $this->Number->format($onlineUserRegistration->can_see_prices) ?></td>
                <td><?= $this->Number->format($onlineUserRegistration->can_see_price_list) ?></td>
                <td><?= h($onlineUserRegistration->print_name) ?></td>
                <td><?= h($onlineUserRegistration->authorized_signature) ?></td>
                <td><?= h($onlineUserRegistration->_date) ?></td>
                <td><?= h($onlineUserRegistration->_title) ?></td>
                <td><?= $this->Number->format($onlineUserRegistration->anti_spam_consent) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $onlineUserRegistration->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $onlineUserRegistration->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $onlineUserRegistration->id], ['confirm' => __('Are you sure you want to delete # {0}?', $onlineUserRegistration->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div>
