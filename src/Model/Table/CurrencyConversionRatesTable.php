<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * CurrencyConversionRates Model
 *
 * @method \App\Model\Entity\CurrencyConversionRate get($primaryKey, $options = [])
 * @method \App\Model\Entity\CurrencyConversionRate newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\CurrencyConversionRate[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\CurrencyConversionRate|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\CurrencyConversionRate saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\CurrencyConversionRate patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\CurrencyConversionRate[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\CurrencyConversionRate findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class CurrencyConversionRatesTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);
        $this->setTable('ccr');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->allowEmptyString('id', null, 'create');

        // $validator
        //     ->numeric('cad')
        //     ->requirePresence('cad', 'create')
        //     ->notEmptyString('cad');

        // $validator
        //     ->numeric('usd_mannington')
        //     ->requirePresence('usd_mannington', 'create')
        //     ->notEmptyString('usd_mannington');

        // $validator
        //     ->numeric('usd_general')
        //     ->requirePresence('usd_general', 'create')
        //     ->notEmptyString('usd_general');

        // $validator
        //     ->numeric('usd_others')
        //     ->requirePresence('usd_others', 'create')
        //     ->notEmptyString('usd_others');

        // $validator
        //     ->numeric('usd_custom_1')
        //     ->requirePresence('usd_custom_1', 'create')
        //     ->notEmptyString('usd_custom_1');

        // $validator
        //     ->numeric('usd_custom_2')
        //     ->requirePresence('usd_custom_2', 'create')
        //     ->notEmptyString('usd_custom_2');

        // $validator
        //     ->numeric('usd_custom_3')
        //     ->requirePresence('usd_custom_3', 'create')
        //     ->notEmptyString('usd_custom_3');
        // $validator
        //     ->numeric('usd_custom_4')
        //     ->requirePresence('usd_custom_4', 'create')
        //     ->notEmptyString('usd_custom_4');

            //  $validator
            //  ->numeric('c_key')
            //  ->requirePresence('c_value', 'create')
            //  ->notEmptyString('c_value');

        return $validator;
    }
}
