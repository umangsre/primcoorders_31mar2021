<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * ToolKitCategory Model
 *
 * @method \App\Model\Entity\ToolKitCategory get($primaryKey, $options = [])
 * @method \App\Model\Entity\ToolKitCategory newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\ToolKitCategory[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\ToolKitCategory|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\ToolKitCategory saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\ToolKitCategory patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\ToolKitCategory[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\ToolKitCategory findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class ToolKitCategoryTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('tool_kit_category');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->hasMany('ToolKits', [
            'className' => 'ToolKits',
            'foreignKey' => 'tool_kit_category_id',
            'propertyName' => 'ToolKits'
        ]);
    }
}
