<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Bill $bill
 */
?>
<style>
    .save-data {
        background: #2c323f;
        border: none;
        padding: 10px 20px;
        margin-top: 15px;
        color: #fff;
    }
</style>
<div class="bill-comment index large-9 medium-8 columns content card card card-default scrollspy">
    <div class="card-content">

        <div class="bill form large-9 medium-8 columns content">
            <?= $this->Form->create($bill, array('type' => 'file')) ?>
            <fieldset>
                <legend><?= __('Add Bill') ?></legend>
                <div class="input text">
                    <label for="billing-image">Billing Date</label>
                    <input type="text" name="billing_date" id="billing_date" required >
                </div>
                <?php
                //                echo $this->Form->control('billing_date', ['empty' => true]);
                echo $this->Form->control('billing_type_of_expense', ['multiple' => 'true', 'class' => 'billing_type_of_expense',
                    'options' => [
                        "Auto_Allowance|Car_Expenses" => "Auto Allowance/Car Expenses",
                        "Airfare" => "Airfare",
                        "Accommodation" => "Accommodation",
                        "Meals_&_Entertainment" => "Meals & Entertainment",
                        "Meal_Allowance" => "Meal Allowance",
                        "Other_Travel_expenses" => "Other Travel expenses",
                        "Meal_Allowance" => "Meal Allowance",
                        "Miscellaneous" => "Miscellaneous (supplies, internet, etc.)"
                    ]
                ]);

                // echo $this->Form->control('billing_type_of_expense');
                //                echo $this->Form->control('billing_place_of_expense');
                echo $this->Form->control('billing_amount', ['onkeydown' => 'getamount()', 'onkeyup' => 'getamount()', 'label' => 'Billing Amount (Before Tax)']);

                echo $this->Form->control('billing_tax', ['class' => 'billing_tax',
                    'options' => [
                        "SASK: 6% PST and 5% GST" => "SASK: 6% PST and 5% GST",
                        "MANITOBA: 7% PST and 5% GST" => "MANITOBA: 7% PST and 5% GST",
                        "BC: 7% PST and 5% GST" => "BC: 7% PST and 5% GST",
                        "EXEMPTED" => "EXEMPTED"
                    ],
                    'onchange' => 'changePercentage()'
                ]);
                echo $this->Form->control('billing_tax_amount', ['onkeydown' => 'getamount()', 'onkeyup' => 'getamount()']);
                //echo $this->Form->control('billing_tax',['class' =>'float-number']);
                echo $this->Form->control('billing_total_amount', ['readonly' => true]);
                echo $this->Form->control('company_credit_card', ['options' => ['Yes' => 'Yes', 'No' => 'No'], 'label' => 'Company Credit Card Used?']);
                // echo $this->Form->control('billing_image');
                //echo $this->Form->input('billing_image', array('type' => 'file', 'multiple'));
                ?>
                <div class="input file">
                    <label for="billing-image">Billing Image</label>
                    <input type="file" name="image" multiple="multiple" id="image" onchange="imagePreview(this, 1)">
                    <input type="hidden" name="files" id="files-1" />
                    <br /><br />
                    <label>Please upload only jpeg/jpg/png files of maximum size 5MB</label>
                </div>
                <div id="preview-images-1"></div>
            </fieldset>
            <div id="imageModal" class="modal">
                <div class="modal">
                    <span class="close" onclick="closeModal()">&times;</span>
                    <div id="imageArea"></div>
                </div>
            </div>
            <?= $this->Form->button(__('Submit')) ?>
            <a href="/sales/bill" class="save-data">Cancel </a>
            <?= $this->Form->end() ?>
        </div>
    </div>
</div>

<script>
    function changePercentage() {
        const billingType = $('#billing-tax').val();
        let placeOfExpense = '';
        let percentage = 0;
        switch (billingType) {
            case 'SASK: 6% PST and 5% GST':
                placeOfExpense = 'SASK';
                percentage = 11;
                break;
            case 'MANITOBA: 7% PST and 5% GST':
                placeOfExpense = 'MANITOBA';
                percentage = 12;
                break;
            case 'BC: 7% PST and 5% GST':
                placeOfExpense = 'BC';
                percentage = 12;
                break;
            case 'EXEMPTED':
                percentage = 0;
                break;
        }
        const amount = parseInt($('#billing-amount').val());
        const taxAmount = (amount * percentage) / 100;
        $('#billing-tax-amount').val(taxAmount);
        $('#billing-total-amount').val(amount + taxAmount);
    }
</script>
