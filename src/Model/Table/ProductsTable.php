<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Products Model
 *
 * @method \App\Model\Entity\Product get($primaryKey, $options = [])
 * @method \App\Model\Entity\Product newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Product[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Product|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Product saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Product patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Product[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Product findOrCreate($search, callable $callback = null, $options = [])
 */
class ProductsTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('products');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('product_name')
            ->maxLength('product_name', 255)
            ->requirePresence('product_name', 'create')
            ->notEmptyString('product_name');

        $validator
            ->scalar('item_name')
            ->maxLength('item_name', 255)
            ->requirePresence('item_name', 'create')
            ->notEmptyString('item_name');

        $validator
            ->scalar('item_number')
            ->maxLength('item_number', 255)
            ->requirePresence('item_number', 'create')
            ->notEmptyString('item_number');

        $validator
            ->scalar('color_name')
            ->maxLength('color_name', 255)
            ->requirePresence('color_name', 'create')
            ->notEmptyString('color_name');

        $validator
            ->numeric('single_item')
            ->requirePresence('single_item', 'create')
            ->notEmptyString('single_item');

        $validator
            ->numeric('bulk_item')
            ->requirePresence('bulk_item', 'create')
            ->notEmptyString('bulk_item');

        $validator
            ->numeric('single_franklin')
            ->requirePresence('single_franklin', 'create')
            ->notEmptyString('single_franklin');

        $validator
            ->numeric('bulk_franklin')
            ->requirePresence('bulk_franklin', 'create')
            ->notEmptyString('bulk_franklin');

        $validator
            ->numeric('per_item')
            ->requirePresence('per_item', 'create')
            ->notEmptyString('per_item');

        $validator
            ->requirePresence('bulk_count', 'create')
            ->notEmptyString('bulk_count');

        $validator
            ->scalar('item_type')
            ->maxLength('item_type', 50)
            ->requirePresence('item_type', 'create')
            ->notEmptyString('item_type');

        $validator
            ->scalar('franklin_type')
            ->maxLength('franklin_type', 50)
            ->requirePresence('franklin_type', 'create')
            ->notEmptyString('franklin_type');

        $validator
            ->dateTime('created_at')
            ->notEmptyDateTime('created_at');

        $validator
            ->dateTime('updated_on')
            ->allowEmptyDateTime('updated_on');

        return $validator;
    }
}
