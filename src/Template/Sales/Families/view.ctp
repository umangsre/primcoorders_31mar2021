<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Family $family
 */
?>
<!-- <nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Family'), ['action' => 'edit', $family->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Family'), ['action' => 'delete', $family->id], ['confirm' => __('Are you sure you want to delete # {0}?', $family->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Families'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Family'), ['action' => 'add']) ?> </li>
    </ul>
</nav> -->
<div class="families view large-9 medium-8 columns content">
    <h3><?= h($family->name) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Family') ?></th>
            <td><?= h($family->family) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Name') ?></th>
            <td><?= h($family->name) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Product Code') ?></th>
            <td><?= h($family->product_code) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Role Cut') ?></th>
            <td><?= h($family->role_cut) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Usd Cad') ?></th>
            <td><?= h($family->usd_cad) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Units') ?></th>
            <td><?= h($family->units) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Mill') ?></th>
            <td><?= h($family->mill) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('VendorID') ?></th>
            <td><?= h($family->vendorID) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($family->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Mill Cost') ?></th>
            <td><?= $this->Number->format($family->mill_cost) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Conversion Rate') ?></th>
            <td><?= $this->Number->format($family->conversion_rate) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Selling Price') ?></th>
            <td><?= $this->Number->format($family->selling_price) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Brokerage Multiplier') ?></th>
            <td><?= $this->Number->format($family->brokerage_multiplier) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Brokerage') ?></th>
            <td><?= $this->Number->format($family->brokerage) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Freight') ?></th>
            <td><?= $this->Number->format($family->freight) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Landed Cost Per Unit') ?></th>
            <td><?= $this->Number->format($family->landed_cost_per_unit) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Load Percentage') ?></th>
            <td><?= $this->Number->format($family->load_percentage) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Margin Lic') ?></th>
            <td><?= $this->Number->format($family->margin_lic) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Sell Price Level1') ?></th>
            <td><?= $this->Number->format($family->sell_price_level1) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Rebate Level1') ?></th>
            <td><?= $this->Number->format($family->rebate_level1) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Gross Margin Level1') ?></th>
            <td><?= $this->Number->format($family->gross_margin_level1) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Sell Price Level2') ?></th>
            <td><?= $this->Number->format($family->sell_price_level2) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Rebate Level2') ?></th>
            <td><?= $this->Number->format($family->rebate_level2) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Gross Margin Level2') ?></th>
            <td><?= $this->Number->format($family->gross_margin_level2) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Sell Price Level3') ?></th>
            <td><?= $this->Number->format($family->sell_price_level3) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Rebate Level3') ?></th>
            <td><?= $this->Number->format($family->rebate_level3) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Gross Margin Level3') ?></th>
            <td><?= $this->Number->format($family->gross_margin_level3) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Created') ?></th>
            <td><?= h($family->created) ?></td>
        </tr>
    </table>
</div>
