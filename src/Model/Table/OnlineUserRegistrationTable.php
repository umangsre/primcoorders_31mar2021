<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * OnlineUserRegistration Model
 *
 * @property \App\Model\Table\CreditApplicationsTable&\Cake\ORM\Association\BelongsTo $CreditApplications
 *
 * @method \App\Model\Entity\OnlineUserRegistration get($primaryKey, $options = [])
 * @method \App\Model\Entity\OnlineUserRegistration newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\OnlineUserRegistration[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\OnlineUserRegistration|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\OnlineUserRegistration saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\OnlineUserRegistration patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\OnlineUserRegistration[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\OnlineUserRegistration findOrCreate($search, callable $callback = null, $options = [])
 */
class OnlineUserRegistrationTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('online_user_registration');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('CreditApplications', [
            'foreignKey' => 'credit_application_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('user_type')
            ->maxLength('user_type', 150)
            ->allowEmptyString('user_type');

        $validator
            ->scalar('submission_date')
            ->maxLength('submission_date', 155)
            ->allowEmptyString('submission_date');

        $validator
            ->scalar('customer_number')
            ->maxLength('customer_number', 155)
            ->allowEmptyString('customer_number');

        $validator
            ->scalar('company_name')
            ->maxLength('company_name', 155)
            ->allowEmptyString('company_name');

        $validator
            ->scalar('administartor_will_see')
            ->maxLength('administartor_will_see', 155)
            ->allowEmptyString('administartor_will_see');

        $validator
            ->scalar('standard_name')
            ->maxLength('standard_name', 155)
            ->allowEmptyString('standard_name');

        $validator
            ->scalar('standard_email')
            ->maxLength('standard_email', 155)
            ->allowEmptyString('standard_email');

        $validator
            ->scalar('standard_name1')
            ->maxLength('standard_name1', 155)
            ->allowEmptyString('standard_name1');

        $validator
            ->scalar('standard_email1')
            ->maxLength('standard_email1', 155)
            ->allowEmptyString('standard_email1');

        $validator
            ->scalar('standard_name2')
            ->maxLength('standard_name2', 155)
            ->allowEmptyString('standard_name2');

        $validator
            ->scalar('standard_email2')
            ->maxLength('standard_email2', 155)
            ->allowEmptyString('standard_email2');

        $validator
            ->scalar('standard_name3')
            ->maxLength('standard_name3', 155)
            ->allowEmptyString('standard_name3');

        $validator
            ->scalar('standard_email3')
            ->maxLength('standard_email3', 155)
            ->allowEmptyString('standard_email3');

        $validator
            ->scalar('first__last_name')
            ->maxLength('first__last_name', 150)
            ->requirePresence('first__last_name', 'create')
            ->notEmptyString('first__last_name');

        $validator
            ->scalar('user_email')
            ->maxLength('user_email', 100)
            ->allowEmptyString('user_email');

        $validator
            ->allowEmptyString('can_see_account_details');

        $validator
            ->allowEmptyString('can_see_placed_order');

        $validator
            ->allowEmptyString('can_see_invoiced_order');

        $validator
            ->allowEmptyString('can_see_credit_memos');

        $validator
            ->allowEmptyString('can_see_prices');

        $validator
            ->allowEmptyString('can_see_price_list');

        $validator
            ->scalar('print_name')
            ->maxLength('print_name', 100)
            ->allowEmptyString('print_name');

        $validator
            ->scalar('authorized_signature')
            ->maxLength('authorized_signature', 100)
            ->allowEmptyString('authorized_signature');

        $validator
            ->scalar('_date')
            ->maxLength('_date', 50)
            ->allowEmptyString('_date');

        $validator
            ->scalar('_title')
            ->maxLength('_title', 100)
            ->allowEmptyString('_title');

        $validator
            ->allowEmptyString('anti_spam_consent');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['credit_application_id'], 'CreditApplications'));

        return $rules;
    }
}
