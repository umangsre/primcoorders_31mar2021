<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Meeting $meeting
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List Meetings'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Salesmen'), ['controller' => 'Salesmen', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Salesman'), ['controller' => 'Salesmen', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="meetings form large-9 medium-8 columns content">
    <?= $this->Form->create($meeting) ?>
    <fieldset>
        <legend><?= __('Add Meeting') ?></legend>
        <?php
            echo $this->Form->control('email');
            echo $this->Form->control('salesman_id', ['options' => $salesmen, 'empty' => true]);
            echo $this->Form->control('agenda');
            echo $this->Form->control('description');
            echo $this->Form->control('date_time');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
