<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\SubEvent $subEvent
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List Sub Events'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Events'), ['controller' => 'Events', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Event'), ['controller' => 'Events', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="subEvents form large-9 medium-8 columns content">
    <?= $this->Form->create($subEvent) ?>
    <fieldset>
        <legend><?= __('Add Sub Event') ?></legend>
        <?php
            echo $this->Form->control('event_id', ['options' => $events]);
            echo $this->Form->control('_event');
            echo $this->Form->control('_time');
            echo $this->Form->control('from_time', ['empty' => true]);
            echo $this->Form->control('to_time', ['empty' => true]);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
