<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Order $order
 */
//print_r($order);
?>

<div class="row">
    <div class="col-12 logo-container text-center p-4">
        <?= $this->Html->image('logo.png'); ?>           
    </div>
    <div class="col-sm-12 col-12 full-width-mobile">
        <div class="steps-right-form">
            <div class="steps_nav text-center pt-3 mb-3 row">
                <div class="steps_item col">
                    <p>1<br>
                        <em>Dealer Section</em>
                    </p>
                </div>
                <div class="steps_item   col">
                    <p>2<br>
                        <em>Sales Section</em>
                    </p>
                </div>
                <div class="steps_item active col">
                    <p>3<br>
                        <em>Order Form</em>
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="orders form large-9 medium-8 columns content">
    <?= $this->Form->create($order,['id' =>'regForm']) ?>
    <div class="row">
        <div class="tab w-100">
            <div class="form-fields">
                <input type="hidden" name="order_id" class="order_id"  value="<?=$order['id']?>">
                <div class="table-responsive">
                    <table id='userTable' class="table table-bordered">
                        <thead>
                            <tr>
                                <!--th>Sr.No.</th-->
                                <th>Product</th>
                                <th>Item Name</th>
                                <th>Item number</th>
                                <th>Description</th>
                                <th>Unit of Measure</th>
								<th class="is_cash">Cash</th>
                                <th>Price</th>
                                <th>Quantity</th>
								<th>Discount</th>
                                <th>Total</th>
                                <th>Enable Franklin</th>
                                <th>Franklin</th>
                                <th>promotions Points/UOM</th>
                                
                            </tr>
                        </thead>
                        <tbody id="myTabledata">
                            <?php
                                $j=0;
								$grand_total = 0;
								$franklin_total =0;
								$points_total =0;
								$discount_total =0;
                                $gross_total =0;
                                while ($j < 11) {
							    //print_r(count($order_item_data));
								 //echo array_key_exists($j, $order_item_data) == false;	
								//if($order_item_data[$j]){
									if($j < count($order_item_data)){
									$quantity = $order_item_data[$j]['quantity']?:'';
									$discount = $order_item_data[$j]['discount']?:0;
									$incentives = $order_item_data[$j]['incentives']?:0;
									$price = $order_item_data[$j]['price']?:'';
									$desc = $order_item_data[$j]['desc']?:'';
									$total = $order_item_data[$j]['total']?:0;
									$promo = $order_item_data[$j]['promo']?:0;
                                    $franklin_enabled = $order_item_data[$j]['franklin_enabled']?:'';
									$name = $order_item_data[$j]['product']['product_name'];
									$product_json = json_encode($order_item_data[$j]['product']);
									$grand_total += $total;
									$franklin_total += $incentives;
									$points_total += $promo;
									$discount_total += $discount;
									$item_names = $order_item_data[$j]['product']['item_name'];
									$product_id = $order_item_data[$j]['product']['id'];
									$item_number = $order_item_data[$j]['product']['item_number'];
									$item_type = $order_item_data[$j]['product']['item_type'];
									$product_name = $order_item_data[$j]['product']['item_number'].$order_item_data[$j]['product']['name'].', '.$order_item_data[$j]['product']['item_name'];
                                    $gross_total=$grand_total-$discount_total ;

                                    if($order['grand_total']){
                                        $grand_total = $order['grand_total'];
                                    }

                                    if($order['franklin_total']){
                                        $franklin_total = $order['franklin_total'];
                                    }

                                    if($order['points_total']){
                                        $points_total = $order['points_total'];
                                    }

                                    if($order['discount_total']){
                                        $discount_total = $order['discount_total'];
                                    }

                                    if($order['gross_total']){
                                        $gross_total = $order['gross_total'];
                                    }
								}else{
									$quantity ='';
									$discount ='';
									$incentives ='';
									$price = '';
									$total = '';
									$promo = '';
									$name ='';
									$item_name = '';
									$item_number = '';
									$item_type='';
									$desc='';
									$product_json='';
									$product_name ='';
									$product_id ='';
                                    $franklin_enabled=0;
								}	
                                ?>
                            <tr class="product_row" data-index="<?php  echo $j; ?>" data-filled="" data-order_id="<?=$order['id']; ?>" >
                                <input type="hidden" name="product_json" class="product_json"  value="<?=htmlspecialchars($product_json)?>">
                                <!--td class="list_item" ><?php  echo $j; ?> </td -->
                                <td class="list_item item_ls" >
                                    <select class="product_name"  name="product_name[]"  >
                                        <option value="">Select Product</option>
                                        <?php foreach($product_records as $item_name):?>
                                        <option value="<?=$item_name['name']?>" <?php if($item_name['name'] ==$name) echo 'selected';?> ><?=$item_name['name']?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </td>
                                <td class="list_item item_ls" >
                                    <select class="item_name"  name="item_name[]"  >
                                        <option>Select Item</option>
                                        <?php foreach($item_names_arr[$product_id]['item_names'] as $k => $item_name):?>
                                        <option value="<?=$item_name?>" <?php if($item_name ==$item_names) echo 'selected';?> ><?=$item_name?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </td>
                                <td class="list_item item_ls" >
                                    <select class="item_number"  name="item_number[]"   >
                                        <option>Select Item Number</option>
                                        <?php foreach($item_names_arr[$product_id]['item_numbers'] as $k => $item_name):?>
                                        <option value="<?=$item_name?>" <?php if($item_name ==$item_number) echo 'selected';?> ><?=$item_name?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </td>
                                <td class="list_item" >
                                    <input class="desc"  name="desc[]"  value="<?=($product_name)?>" />
                                    <div class="suggesstion-products list_view"></div>
                                    <input type="hidden" class="product_id"  name="product_id[]" value="<?=($product_id)?>" />
                                </td>
                                <td class="list_item" >
                                    <input class="item_type"  name="item_type[]"  value="<?=$item_type?>" />
                                </td>
								<td class="list_item is_cash" >
								<input type="checkbox" class="cash-checkbox"  name="cash_enabled[]" value="1" style="display:none;" />
								
                                </td>
                                <td class="list_item" ><input type="text"  min="1" class="price" name="price[]" value="<?=$price?>"  />
                                </td>
                                <td class="list_item"> 
                                <input  type="number"  min="1"  class="quantity"  name="quantity[]" value="<?=$quantity?>"  />
                                </td>
                                <td class="list_item"> 
                                <input  type="number"  min="1"  class="discount"  name="discount[]" value="<?=$discount?>" />
                                </td>
                                <td class="list_item" >
                                    <input class="total"  name="total[]" value="<?=$total?>" />
                                </td>
                                <td class="list_item" >
                                    <input type="checkbox" class="franklin-checkbox"  name="franklin_enabled[]" value="1" <?php if($franklin_enabled == '1') echo 'checked';?> />
                                    <input type="hidden" class="franklin-checkbox-input"  name="franklin_enabled_input[]" value="<?=($franklin_enabled == '1')? '1':'0';?>" />
                                </td>
                                <td class="list_item" ><input class="franklin"   name="incentives[]" value="<?=$incentives?>" /></td>
                                <td class="list_item" ><input  min="0"   type="number"class="promo"  name="promo[]" value="<?=$promo?>" /></td>
                                
                                
                            </tr>
                            <?php
                                $j++;
                                }
                                ?>
                        </tbody>
                    </table>
                     </div>
                    <div class="row">
                        <div class="col-12 col-sm-6">
                            <p>
                                <span>Grand Total is before GST, other taxes, and additional freight charges: $</span>
                                <span id="order-grand-total"><?=$gross_total; ?> 
                                </span>
                                 <input type="hidden" class="input-order-grand-total"  name="grand_total" value="<?=$grand_total-$discount_total ?>" />
                                <input type="hidden" class="input-order-discount-total"  name="discount_total" value="<?=($discount_total)?>" /> 
                            </p>
                            <p>
                                <span>Gross Total: </span>
                                <span id="order-gross-total"><?=$grand_total?></span>
                                <input type="hidden" class="input-order-gross-total"  name="gross_total" value="<?=($grand_total)?>" />
                            </p>    
                            <p>
                                <span>Franklin Total: </span>
                                <span></span>
                                <input  id="order-franklin-total" class="input-order-franklin-total"  name="franklin_total" value="<?=($franklin_total)?>" />
                               
                            </p> 
                            </p>
    						<p>
                                <span>Promotion points Total: </span>
                                <span id="order-points-total"><?=$points_total ?></span>
                                <input type="hidden" class="input-order-points-total"  name="points_total" value="<?=($points_total)?>" />
                            </p>
                            
                        </div>
                        

                        <div class="col-12 col-sm-6">
                            <div id="signature-pad" class="signature-pad">
                                <div class="signature-pad--header">
                                  <div class="description">Signature</div>
                                </div>
                                <div class="signature-pad--body">
                                  <canvas id="drawing_pad" width="500" height="200" style="border:1px solid #ccc"></canvas>
                                </div>
                                <div class="signature-pad--actions">
                                    <div>
                                      <button type="button" class="button clear btn btn-info" data-action="clear">Clear</button>
                                    </div>
                                    
                                </div>
                            
                          </div>
                        </div>  
					</div>


                    <div class="btn btn-default add-morebtn">
                        <?php  
                       

                        echo $this->Html->link(
                          'Back',
                          ['controller' => 'Orders', 'action' => 'addsalesman',base64_encode($id )],
                          ['class' => 'btn btn-success']
                        ); 
                        
                        ?>
                        <?= $this->Form->button(__('Add More'), array( 'class' => 'btn btn-info tr_clone_add' , 'type' => 'button' )  ) ?>

                        <?= $this->Form->button(__('Submit'), array( 'class' => 'btn btn-primary', 'type' => 'button', 'id' => 'submit-form' )  ) ?>
                        
                        <!-- <button type="button" class="btn btn-info tr_clone_add">
                            Add More
                        </button> -->
                    </div>

               
            </div>
        </div>
    </div>  

    
    
    <?= $this->Form->end() ?>
</div>
