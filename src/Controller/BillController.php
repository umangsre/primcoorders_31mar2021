<?php

namespace App\Controller;

use Cake\I18n\Time;
use Cake\ORM\TableRegistry;
use App\Controller\AppController;
use Cake\Mailer\MailerAwareTrait;
use Cake\Mailer\Email;

/**
 * Bill Controller
 *
 * @property \App\Model\Table\BillTable $Bill
 *
 * @method \App\Model\Entity\Bill[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class BillController extends AppController
{
    public $helpers = ['Cewi/Excel.Excel'];
    private $currentUser = '';

    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function initialize()
    {

        parent::initialize();
        $this->loadComponent('Cewi/Excel.Import');
        $this->loadComponent('RequestHandler', [
            'enableBeforeRedirect' => false,
        ]);

        $this->loadComponent('Flash');
        $this->loadModel('Files');
        $this->currentUser = $this->Auth->user();
    }

    /**
     *
     */
    public function index()
    {
        $this->viewBuilder()->setLayout('dashboard');
        $bill = $this->paginate($this->Bill);
        $search = $this->request->getQueryParams();
        $newdate = "";
        $key = array('billing_date', 'billing_type_of_expense', 'billing_amount', 'billing_tax', 'billing_tax_amount', 'billing_total_amount');
        $upload_data = [];

        $query_all = $this->Bill->find('all')
            ->join([
                'table' => 'salesmen',
                'alias' => 'c',
                'type' => 'INNER',
                'conditions' => 'billing_userid  = c.user_id',
            ])->where(['OR' => [['billing_status' => 'A'],['billing_status' => 'AWS'],['billing_status' => 'RA'], ['billing_status' => 'AA']]]);

        if (!empty($search['start'])) {
            $query_all = $query_all->where(["AND" => ['billing_date >=' => $search['start'], 'billing_date <=' => $search['end']]]);
        }
        if (!empty($search['status'])) {
            $query_all = $query_all->where(['billing_status' => $search['status']]);
        }
        if (!empty($search['search'])) {
            $query_all = $query_all->where(['name Like' => '%' . $search['search'] . '%']);
        }
        foreach ($query_all as $family) {
            $keydata = [];

            $user = TableRegistry::getTableLocator()->get('salesmen')->find()->where(["user_id" => $family->billing_userid]);
            //$user = $userinfo;
            $userinfo = "";
            foreach ($user as $info) {
                $userinfo = $info;
            }

            $keydata['Name of Employee'] = $userinfo->name;

            for ($k = 0; $k < count($key); $k++) {
                $keydata[$this->stringreplace($key[$k])] = $family[$key[$k]];
            }

            $billingtax = $family['billing_tax'];
            $gst = 0;
            $pst = 0;
            switch ($billingtax) {
                case 'SASK: 6% PST and 5% GST':
                    $pst = number_format($family['billing_tax_amount'] * (54.55 / 100), 2);
                    $gst = number_format($family['billing_tax_amount'] * (45.45 / 100), 2);
                    break;
                case 'MANITOBA: 7% PST AND 5% GST':
                    $pst = number_format($family['billing_tax_amount'] * (58.34 / 100), 2);
                    $gst = number_format($family['billing_tax_amount'] * (41.66 / 100), 2);
                    break;
                case 'BC: 7% PST and 5% GST':
                    $pst = number_format($family['billing_tax_amount'] * (58.34 / 100), 2);
                    $gst = number_format($family['billing_tax_amount'] * (41.66 / 100), 2);
                    break;
                case 'ALBERTA: 5% GST':
                    $gst = number_format($family['billing_tax_amount'], 2);
                    $pst = 0;
                    break;
                case 'Exempted':
                    $gst = 0;
                    $pst = 0;
                    break;
                case 'default':
                    $gst = 0;
                    $pst = 0;
                    break;
            }
            $keydata['GST'] = $gst;
            $keydata['PST'] = $pst;

            $keydata['Email'] = $userinfo->rep_email;


            $billcomments = TableRegistry::getTableLocator()->get('bill_comment')->find()->where(['billid' => $family->id]);
            $comment = [];
            $i = 1;
            foreach ($billcomments as $coments) {
                $comment[] = $i . " " . $coments->comment;
                $i++;
            }
            $keydata['Rejection Comments'] = implode(", ", $comment);

            $upload_data[] = $keydata;
        }

        if (empty($upload_data)) {
            $upload_data[] = array("Name of Employee" => "", "Type of Expense" => "", "Total Amount" => "", "Tax Amount" => "", "Type of Tax" => "", "Total Amount including tax" => "", "Email" => "", "Rejection Comments" => "");
        }
        $articles = $upload_data;

        $query = $this->Bill->find('all', ['order' => 'Bill.id DESC'])
            // >hydrate(false)
            ->select(['Bill.id', 'Bill.billing_date', 'Bill.billing_type_of_expense', 'Bill.billing_place_of_expense', 'Bill.billing_amount', 'Bill.billing_tax', 'Bill.billing_tax_amount', 'Bill.billing_total_amount', 'Bill.billing_image', 'Bill.billing_status', 'Bill.billing_userid', 'Bill.billing_comment', 'Bill.created', 'Bill.created'])
            ->join([
                'table' => 'salesmen',
                'alias' => 'c',
                'type' => 'LEFT',
                'conditions' => 'billing_userid  = c.user_id',
            ])
            ->contain([
                'Users' => function ($q) {
                    return $q->select(['name']);
                }
            ])
            ->where(['OR' => [['billing_status' => 'A'], ['billing_status' => 'AWS'], ['billing_status' => 'RA'], ['billing_status' => 'AA']]]);

        if (!empty($search['start'])) {
            $query = $query->where(["AND" => ['billing_date >=' => $search['start'], 'billing_date <=' => $search['end']]]);
        }
        if (!empty($search['status'])) {
            $query = $query->where(['billing_status' => $search['status']]);
        }
        if (!empty($search['bid'])) {
            $query = $query->where(['Bill.id' => $search['bid']]);
        }
        if (!empty($search['search'])) {
            $query = $query->where(['c.name Like' => '%' . $search['search'] . '%']);
        }
        $bill = $this->paginate($query);

        $this->set(compact('bill', 'articles'));
    }

    /**
     * @param $newtext
     * @return string|string[]
     */
    function stringreplace($newtext)
    {

        $old = ["billing_type_of_expense", "billing_tax_amount", "billing_amount", "billing_tax", "billing_total_amount", "billing_date", "name", "rep_email"];
        $new = ["Type of Expense", "Tax Amount", "Total Amount", "Type of Tax", "Total Amount including tax", "Date of Expence ", "Name of Employee", "Email"];

        return str_replace($old, $new, $newtext);
    }

    /**
     * View method
     *
     * @param string|null $id Bill id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $this->viewBuilder()->setLayout('dashboard');

        $bill = $this->Bill->get($id, [
            'contain' => []
        ]);
        $billcommnets = TableRegistry::getTableLocator()->get('bill_comment')->find()->where(['and' => ['billid' => $id]]);
        $userinfo = TableRegistry::getTableLocator()->get('users')->find()->where(['and' => ['id' => $bill->billing_userid]]);

        //echo $userinfo[0]->id;
        //exit;
        $this->set(compact('billcommnets', 'bill', 'userinfo'));
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $this->viewBuilder()->setLayout('dashboard');
        $bill = $this->Bill->newEntity();
        if ($this->request->is('post')) {
            $bill = $this->Bill->patchEntity($bill, $this->request->getData());
            if ($this->Bill->save($bill)) {
                $this->Flash->success(__('The bill has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The bill could not be saved. Please, try again.'));
        }
        $this->set(compact('bill'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Bill id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $this->viewBuilder()->setLayout('dashboard');
        $bill = $this->Bill->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {

            $data = $this->request->getData();

            $billcomments = "";
            if ($data['billing_status'] == 'D') {
				
                $data['billing_status'] = 'RA';

                $articlesTable = TableRegistry::getTableLocator()->get('BillComment');
                $article = $articlesTable->newEntity();

                $article->comment = $data['billing_comment'];
                $article->billid = $id;

                if ($articlesTable->save($article)) {
                    // The $article entity contains the id now
                    $id = $article->id;
                }
                $billcomments = $data['billing_comment'];
                unset($data['billing_comment']);
            }

            $bill = $this->Bill->patchEntity($bill, $data);

            if ($this->Bill->save($bill)) {
                $bill = $bill->toArray();
                $mailRecipients = [];
                $emailSubject = 'Billing Update';

                $this->loadModel('Users');
                $billUser = $this->Users->find()
                    ->where(['id' => $bill['billing_userid']])
                    ->first();

                if ($bill['billing_status'] == 'A' || $bill['billing_status'] == 'AWS') {
					
                    if ($billUser->role == 'sdm') {
                        $mailRecipients = array(
                            $this->Auth->user('username'),
                            $billUser->username, //SDM
                        );
                    } elseif ($billUser->role == 'sales') {
                        $this->loadModel('SdmSales');
                        $sdm = $this->SdmSales->find()->where(['salesmen_id' => $bill['billing_userid']])
                            ->contain(['sdm' => function ($q) {
                                return $q->select(['id', 'name', 'username']);
                            }])
                            ->first();
						
						if($sdm!=null){
	                        $mailRecipients = array(
	                            $this->Auth->user('username'), //Admin
	                            $billUser->username, //Sales
	                            $sdm->sdm->usernam //SDM
	                        );	
						}else{
	                        $mailRecipients = array(
	                            $this->Auth->user('username'), //Admin
	                            $billUser->username //Sales
	                        );	
						}
                        
                    }
                    $emailSubject = "Bill Approved by Admin";
                } elseif ($bill['billing_status'] == 'R') {
                    if ($billUser->role == 'sdm') {
                        $mailRecipients = array(
                            $this->Auth->user('username'),
                            $billUser->username //SDM
                        );
                    } elseif ($billUser->role == 'sales') {
                        $this->loadModel('SdmSales');
                        $sdm = $this->SdmSales->find()->where(['salesmen_id' => $bill['billing_userid']])
                            ->contain(['sdm' => function ($q) {
                                return $q->select(['id', 'name', 'username']);
                            }])
                            ->first();
						
						if($sdm!=null){
		                	$mailRecipients = array(
		                    	$this->Auth->user('username'), //Admin
		                    	$sdm->sdm->usernam //SDM
		                 	);	
						 }else{
		                 	$mailRecipients = array(
		                    	$this->Auth->user('username'), //Admin
		                        $billUser->username //Sales
		                     );	
						  }
                    }
                    $emailSubject = "Bill Rejected by Admin";
                }
                $bill['comment'] = $billcomments;

                if ($bill['billing_status'] != 'C') {
                    if ($mailRecipients) {
                        $this->sendMail($emailSubject, $bill, $mailRecipients);
                    }
                }

                $this->Flash->success(__('The bill has been saved.'));
                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The bill could not be saved. Please, try again.'));
        }
        $this->set(compact('bill'));
    }

    /**
     * @param $billSubject
     * @param $bill
     * @param $mailRecipients
     * @return bool
     */
    public function sendMail($billSubject, $bill, $mailRecipients)
    {
        
       $email = new Email();
        $email->setTransport('default');
        $email->viewVars(['bill' => $bill]);
        $email->viewBuilder()->setTemplate('billing');
        $email->setEmailFormat('html');
        $email->setFrom(['support@primco.ca' => 'Primco'])
            ->setTo($mailRecipients)
            ->setBcc('Juneja.ankit@gmail.com')
            ->setSubject('Notification: ' . $billSubject)
            ->send();
        return true;
    }

    /**
     * @param null $id
     * @return \Cake\Http\Response|null
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $bill = $this->Bill->get($id);
        if ($this->Bill->delete($bill)) {
            $this->Flash->success(__('The bill has been deleted.'));
        } else {
            $this->Flash->error(__('The bill could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    /**
     * @return \Cake\Http\Response
     */
    public function createBillImagesZip()
    {
        !$this->request->is('get') && exit;
        $bills = $this->Bill->find('all');
        $data = $_GET;
        $from = $data['from'];
        $to = $data['to'];
        $salesman = $data['salesman'];
        $status = $data['status'];
        if (!empty($from)) {
            $bills = $bills->where(['Bill.billing_date >=' => $from]);
        }
        if (!empty($to)) {
            $bills = $bills->where(['Bill.billing_date <=' => $to]);
        }
        if (!empty($salesman)) {
            $this->loadModel('Salesmen');
            $salesmanInfo = $this->Salesmen->find('all')->where(['Salesmen.name LIKE' => '%' . $salesman . '%'])->first();
            $bills = $bills->where(['Bill.billing_userid' => $salesmanInfo['user_id']]);
        }
        if (!empty($status)) {
            $bills = $bills->where(['Bill.billing_status' => $status]);
        }
        $bills = $bills->all();
        $tempFolder = 'temp-bills' . time() . '/';
        $tempFolderName = WWW_ROOT . $tempFolder;
        if (!file_exists($tempFolderName)) {
            mkdir($tempFolderName);
        }
        foreach ($bills as $bill) {
            $this->loadModel('Salesmen');
            $salesmanInfo = $this->Salesmen->find('all')->where(['user_id' => $bill->billing_userid])->first();
            $salesmanName = $salesmanInfo['name'];
            $salesmanFolder = $tempFolderName . $salesmanName . '/';
            if (!file_exists($salesmanFolder)) {
                mkdir($salesmanFolder);
            }
            $images = $bill->billing_image;
            $imagesArray = explode(',', $images);
            foreach ($imagesArray as $image) {
                $imageName = explode('/uploads/files/', $image)[1];
                if (!empty($image))
                    copy(BASE_URL . $image, $salesmanFolder . $imageName);
            }
        }
        $fileController = new FileController();
        $fileController->createZip($tempFolder);
        $fileController->deleteDirectory($tempFolderName);
        return $this->response;
    }
}
