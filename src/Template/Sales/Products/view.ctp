<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Product $product
 */
?>
<!--nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Product'), ['action' => 'edit', $product->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Product'), ['action' => 'delete', $product->id], ['confirm' => __('Are you sure you want to delete # {0}?', $product->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Products'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Product'), ['action' => 'add']) ?> </li>
    </ul>
</nav -->
<div class="orders index large-9 medium-8 columns content card card card-default scrollspy">
    <div class="card-content">
        <div class="products view large-9 medium-8 columns content">
            <h3><?= h($product->id) ?></h3>
            <table class="vertical-table">
                <tr>
                    <th scope="row"><?= __('Product Name') ?></th>
                    <td><?= h($product->product_name) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Item Name') ?></th>
                    <td><?= h($product->item_name) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Item Number') ?></th>
                    <td><?= h($product->item_number) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Color Name') ?></th>
                    <td><?= h($product->color_name) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Item Type') ?></th>
                    <td><?= h($product->item_type) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Franklin Type') ?></th>
                    <td><?= h($product->franklin_type) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Id') ?></th>
                    <td><?= $this->Number->format($product->id) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Single Item') ?></th>
                    <td><?= $this->Number->format($product->single_item) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Bulk Item') ?></th>
                    <td><?= $this->Number->format($product->bulk_item) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Single Franklin') ?></th>
                    <td><?= $this->Number->format($product->single_franklin) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Bulk Franklin') ?></th>
                    <td><?= $this->Number->format($product->bulk_franklin) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Per Item') ?></th>
                    <td><?= $this->Number->format($product->per_item) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Bulk Count') ?></th>
                    <td><?= $this->Number->format($product->bulk_count) ?></td>
                </tr>
                <!--tr>
                    <th scope="row"><?= __('Created At') ?></th>
                    <td><?= h($product->created_at) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Updated On') ?></th>
                    <td><?= h($product->updated_on) ?></td -->
                </tr>
            </table>
        </div>
    </div>
</div>
