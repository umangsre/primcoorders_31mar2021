<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Bill $bill
 */
?>
<style>
    img.billing_image {
        height: 300px;
        width: 300px;
        margin-left: 10px;
    }
</style>
<div class="bill index large-9 medium-8 columns content card card card-default scrollspy">
    <div class="card-content">
        <div class="bill view large-9 medium-8 columns content">
            <?php
            $username = $useremail = "";
            foreach ($userinfo as $userinfo) {
                $useremail = $userinfo->username;
                $exname = explode("@", $useremail);
                $username = $exname[0];
            }
            ?>

            <h3><?= h($bill->id) ?></h3>
            <table class="vertical-table">
                <tr>
                    <th scope="row"><?= __('Name') ?></th>
                    <td><?= $username ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Email') ?></th>
                    <td><?= $useremail ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Type Of Expense') ?></th>
                    <td><?= h($bill->billing_type_of_expense) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('City Of Expense') ?></th>
                    <td><?= h($bill->billing_place_of_expense) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Tax') ?></th>
                    <td><?= h($bill->billing_tax) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Tax Amount') ?></th>
                    <td><?= h($bill->billing_tax_amount) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Id') ?></th>
                    <td><?= $this->Number->format($bill->id) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __(' Amount') ?></th>
                    <td><?= $this->Number->format($bill->billing_amount) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __(' Total Amount') ?></th>
                    <td><?= $this->Number->format($bill->billing_total_amount) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __(' Date') ?></th>
                    <td><?= h($bill->billing_date) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __(' Status') ?></th>
                    <td><?php if ($bill->billing_status == 'P') {
                            echo 'In Process';
                        } elseif ($bill->billing_status == 'A') {
                            echo 'Approve';
                        } elseif ($bill->billing_status == 'D') {
                            echo 'Decline';
                        } elseif ($bill->billing_status == 'R') {
                            echo 'Reupdate';
                        } elseif ($bill->billing_status == 'C') {
                            echo 'Closed';
                        } ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Company Credit Card Used?') ?></th>
                    <td><?= h($bill->company_credit_card) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Created') ?></th>
                    <td><?= h($bill->created) ?></td>
                </tr>
            </table>
            <div class="row">
                <h4><?= __(' Image') ?></h4>
                <?php
                $explodeimage = explode(",", $bill->billing_image);
                if (!empty($explodeimage)) {
                    foreach ($explodeimage as $key => $value) {
                        echo "<a href='$value' target='_blank' ><img src='$value' class='billing_image'></a>";
                    }
                }
                ?>

            </div>
            <div class="row">
                <h4><?= __(' History') ?></h4>
                <!--<p><?= h($bill->billing_comment) ?></p>-->
                <?php
                foreach ($billcommnets as $billcommnet) {
                    echo '<p style="border: 1px solid #eee;padding: 7px 30px;">' . $billcommnet->comment . ' <small style="float: right;">' . $billcommnet->created . '</small></p>';
                }
                if ($bill->billing_status == 'A') {
                    echo '<p style="border: 1px solid #eee;padding: 7px 30px;">The bill is approved </p>';
                }

                ?>
            </div>
            <div class="row">
                <?php if ($bill->billing_status == 'P') { ?>
                    <a class="modal-trigger update-trigger" href="#modal1" title="Reject" data-id="<?= $bill->id ?>"><i
                            class="material-icons dp48">highlight_off</i></a>
                    <a class="done-trigger" href="#done" title="Approve" data-id="<?= $bill->id ?>"><i
                            class="material-icons dp48">done</i></a>
                    <a class="close-trigger" href="#close" title="Close" data-id="<?= $bill->id ?>"><i
                            class="material-icons dp48">close</i></a>
                <?php } ?>
            </div>
            <!-- Modal Structure -->
            <!-- Rejected bill from  -->
            <div id="modal1" class="modal">
                <div class="modal-content">
                    <?= $this->Form->create($bill, array(
                        'id' => 'mybill'
                    )) ?>
                    <div class="input textarea">
                        <label for="billing-comment" class="">Billing Comment</label>
                        <textarea name="billing_comment" id="billing-comment" rows="5" required></textarea>
                    </div>
                    <input type="hidden" name="billing_status" value="D">

                    <?= $this->Form->button(__('Submit')) ?>
                    <?= $this->Form->end() ?>
                </div>
                <div class="modal-footer">
                    <a href="#!" class="modal-close waves-effect waves-green btn-flat">Close</a>
                </div>
            </div>

            <!--Approve bill form  -->
            <?= $this->Form->create($bill, array(
                'id' => 'mybillaccept'
            )) ?>
            <input type="hidden" name="billing_status" value="A">
            <?= $this->Form->end() ?>

            <?= $this->Form->create($bill, array(
                'id' => 'billclose'
            )) ?>
            <input type="hidden" name="billing_status" value="C">
            <?= $this->Form->end() ?>
        </div>
    </div>
</div>