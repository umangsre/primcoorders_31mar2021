<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\SubEvent $subEvent
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Sub Event'), ['action' => 'edit', $subEvent->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Sub Event'), ['action' => 'delete', $subEvent->id], ['confirm' => __('Are you sure you want to delete # {0}?', $subEvent->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Sub Events'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Sub Event'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Events'), ['controller' => 'Events', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Event'), ['controller' => 'Events', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="subEvents view large-9 medium-8 columns content">
    <h3><?= h($subEvent->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Event') ?></th>
            <td><?= $subEvent->has('event') ? $this->Html->link($subEvent->event->name, ['controller' => 'Events', 'action' => 'view', $subEvent->event->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __(' Event') ?></th>
            <td><?= h($subEvent->_event) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __(' Time') ?></th>
            <td><?= h($subEvent->_time) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($subEvent->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('From Time') ?></th>
            <td><?= h($subEvent->from_time) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('To Time') ?></th>
            <td><?= h($subEvent->to_time) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Created') ?></th>
            <td><?= h($subEvent->created) ?></td>
        </tr>
    </table>
</div>
