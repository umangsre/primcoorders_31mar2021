<?php
namespace App\Controller\Sales;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;
use Cake\Event\Event;
use Cake\Mailer\Email;
use Cake\Routing\Router;
use \PHPQRCode\QRcode;

/**
 * OnlineUserRegistration Controller
 *
 * @property \App\Model\Table\OnlineUserRegistrationTable $OnlineUserRegistration
 *
 * @method \App\Model\Entity\OnlineUserRegistration[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class OnlineUserRegistrationController extends AppController
{
	public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);
        $this->Auth->allow(['addUser', 'viewUser', 'editUser', 'indexUser']);
    }
	public function indexUser()
    {
        $onlineUserRegistration = $this->paginate($this->OnlineUserRegistration);

        $this->set(compact('onlineUserRegistration'));
    }

    /**
     * View method
     *
     * @param string|null $id Online User Registration id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function viewUser($id = null)
    {
        $onlineUserRegistration = $this->OnlineUserRegistration->get($id, [
            'contain' => []
        ]);

        $this->set('onlineUserRegistration', $onlineUserRegistration);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function addUser($id = null)
    {
		
        $id = base64_decode($id);
        $creditApplication =[];
        if(!($id == null)){
            $CreditApplications = TableRegistry::get('CreditApplications');
            $creditApplication = $CreditApplications->get($id, [
                'contain' => []
            ]);

        }

        
        $onlineUserRegistration = $this->OnlineUserRegistration->newEntity();
        if ($this->request->is(['patch', 'post', 'put'])) {
           
        //print_r($this->request->getData()); 
        $data_array = [];
        foreach($this->request->getData() as $key => $val){
            if($key =='administartor_will_see'){
                $data_array[$key] = serialize($val);
             }else{
                 $data_array[$key] = $val;
             }
        }

          $onlineUserRegistration->credit_application_id =$id;
	      $onlineUserRegistration = $this->OnlineUserRegistration->patchEntity($onlineUserRegistration, $data_array);
     
            if ($this->OnlineUserRegistration->save($onlineUserRegistration)) { 
                $this->Flash->success(__('The online user registration has been saved.'));
                //return $this->redirect(['contoller' =>'OnlineUserRegistration' ,'action' => 'addUser',base64_encode($id)]);
                return $this->redirect(['controller' =>'CreditApplications','action' => 'thankyou',base64_encode($id)]);
            }else{
                 
            }
            $this->Flash->error(__('The online user registration could not be saved. Please, try again.'));
        }
        $this->set(compact('onlineUserRegistration','id','creditApplication'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Online User Registration id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function editUser($id = null)
    {
        if ( $id == null ) {
            return $this->redirect( $this->referer() );
        }
        $id = base64_decode($id);
        $onlineUserRegistration = $this->OnlineUserRegistration->find('all', [
			'conditions' => ['credit_application_id' => $id],
            'contain' => []
        ])->first();
        if ($this->request->is(['patch', 'post', 'put'])) {
			//print_r($this->request->getData()); 
       
          
            $data_array = [];
			foreach($this->request->getData() as $key => $val){
				if($key =='administartor_will_see'){
					$data_array[$key] = serialize($val);
				 }else{
					 $data_array[$key] = $val;
				 }
			}
       
            $onlineUserRegistration = $this->OnlineUserRegistration->patchEntity($onlineUserRegistration, $data_array);
            if ($this->OnlineUserRegistration->save($onlineUserRegistration)) {
                $this->Flash->success(__('The online user registration has been saved.'));

                return $this->redirect(['controller' =>'CreditApplications','action' => 'thankyou',base64_encode($id)]);
            }
            $this->Flash->error(__('The online user registration could not be saved. Please, try again.'));
        }
        $this->set(compact('onlineUserRegistration','id'));
    }
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $onlineUserRegistration = $this->paginate($this->OnlineUserRegistration);

        $this->set(compact('onlineUserRegistration'));
    }

    /**
     * View method
     *
     * @param string|null $id Online User Registration id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $onlineUserRegistration = $this->OnlineUserRegistration->get($id, [
            'contain' => []
        ]);

        $this->set('onlineUserRegistration', $onlineUserRegistration);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $onlineUserRegistration = $this->OnlineUserRegistration->newEntity();
        if ($this->request->is('post')) {
            $onlineUserRegistration = $this->OnlineUserRegistration->patchEntity($onlineUserRegistration, $this->request->getData());
            if ($this->OnlineUserRegistration->save($onlineUserRegistration)) {
                $this->Flash->success(__('The online user registration has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The online user registration could not be saved. Please, try again.'));
        }
        $this->set(compact('onlineUserRegistration'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Online User Registration id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
		$this->viewBuilder()->setLayout('dashboard');
		 if ( $id == null ) {
            return $this->redirect( $this->referer() );
        }
        $id = base64_decode($id);
        $onlineUserRegistration = $this->OnlineUserRegistration->find('all', [
			'conditions' => ['credit_application_id' => $id],
            'contain' => []
        ])->first();
        if ($this->request->is(['patch', 'post', 'put'])) {
			$data_array = [];
			foreach($this->request->getData() as $key => $val){
				if($key =='administartor_will_see'){
					$data_array[$key] = serialize($val);
				 }else{
					 $data_array[$key] = $val;
				 }
			}
            $onlineUserRegistration = $this->OnlineUserRegistration->patchEntity($onlineUserRegistration, $data_array);
            if ($this->OnlineUserRegistration->save($onlineUserRegistration)) {
                $this->Flash->success(__('The online user registration has been saved.'));

                return $this->redirect(['controller' =>'CreditApplications','action' => 'index']);
            }
            $this->Flash->error(__('The online user registration could not be saved. Please, try again.'));
        }
        $this->set(compact('onlineUserRegistration'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Online User Registration id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $onlineUserRegistration = $this->OnlineUserRegistration->get($id);
        if ($this->OnlineUserRegistration->delete($onlineUserRegistration)) {
            $this->Flash->success(__('The online user registration has been deleted.'));
        } else {
            $this->Flash->error(__('The online user registration could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
