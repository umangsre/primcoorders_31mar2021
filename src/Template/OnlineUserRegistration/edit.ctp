<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\OnlineUserRegistration $onlineUserRegistration
 */
 $options = array(
                    '1' => 'Yes',
                    '0' => 'No'
                  );
$options_ = array( 
	                'Account Details, Including Credit Details',
					'Invoices(Open/Closed/Credit Memos)', 
					'Pricelists', 
					'Quotes', 
					'Purchases', 
					'Prices'
				);
$array_name =unserialize ($onlineUserRegistration->administartor_will_see) ;
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <!--li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $onlineUserRegistration->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $onlineUserRegistration->id)]
            )
        ?></li --->
        <li><?= $this->Html->link(__('List Credit Applications'), ['controller' => 'CreditApplications','action' => 'index']) ?></li>
    </ul>
</nav>
<div class="onlineUserRegistration form large-9 medium-8 columns content">
   <?= $this->Form->create($onlineUserRegistration,['id' =>'user_form']) ?>
    <div class="row">
        <h2 class="_heading"><?= __('Primco Online User Registration') ?></h2>
	</div>
	<div class="row">
		<div class="col-sm-4 col-12 mt-3">
		
		<?php
            echo $this->Form->control('order_id',['value' =>$creditApplication['id'], 'id' =>'order_id', 'type' =>'hidden' ]); 
			?>	
        <?php
            echo $this->Form->control('submission_date',['class' => 'form-control','id' =>'submission_date', 'label' =>'Submission Date:' ]); 
			?>
		</div>
		<div class="col-sm-4 col-12 mt-3">	
            <?php echo $this->Form->control('customer_number',['class' => 'form-control', 'label' =>'Customer Number:' ]); ?>
		</div>
		<div class="col-sm-4 col-12 mt-3">	
            <?php echo $this->Form->control('company_name',['class' => 'form-control', 'label' =>'Company Name:' ]); ?>
		</div>
	</div>
	
	<div class="row">
		<div class="col-sm-12 col-12 mt-3">	
			<?php 
			  echo $this->Form->control('administartor_will_see', ['multiple' => 'checkbox', 'options' => $options_, 'value' =>$array_name, 'type' =>'select',   'class' => 'pd_10', 'label' => 'Administrator Will See:']); ?>
		</div>
	</div>
	
	
	<div class="row">	
		<div class="col-sm-12 col-12 mt-3">
			<h5>Administrator:</h5>
		</div>
		<div class="col-sm-6 col-12 mt-3">
			<?php echo $this->Form->control('first__last_name',['class' => 'form-control','required' => 'required', 'label' =>'First & Last Name*' ]); ?>
		</div>
		<div class="col-sm-6 col-12 mt-3">	
            <?php echo $this->Form->control('user_email',[ 'type' => 'email', 'class' => 'form-control','required' => 'required' ,'label' =>'Email*']); ?>
		</div>
	</div>
	<div class="row">
		<div class="col-sm-12 col-12 mt-3">
			<h5>Standard User:</h5>
		</div>
		
		<div class="col-sm-6 col-12 mt-3">
        <?php
            echo $this->Form->control('standard_name',['class' => 'form-control', 'label' =>'Name(First & Last):' ]); 
			?>
		</div>
		<div class="col-sm-6 col-12 mt-3">	
            <?php echo $this->Form->control('standard_email',['class' => 'form-control','type' => 'email', 'label' =>'User Email:' ]); ?>
		</div>
		
		<div class="col-sm-6 col-12 mt-3">
        <?php
            echo $this->Form->control('standard_name1',['class' => 'form-control', 'label' =>'Name(First & Last):' ]); 
			?>
		</div>
		<div class="col-sm-6 col-12 mt-3">	
            <?php echo $this->Form->control('standard_email1',['class' => 'form-control','type' => 'email', 'label' =>'User Email:' ]); ?>
		</div>
		
		<div class="col-sm-6 col-12 mt-3">
        <?php
            echo $this->Form->control('standard_name2',['class' => 'form-control', 'label' =>'Name(First & Last):' ]); 
			?>
		</div>
		<div class="col-sm-6 col-12 mt-3">	
            <?php echo $this->Form->control('standard_email2',['class' => 'form-control','type' => 'email', 'label' =>'User Email:' ]); ?>
		</div>
		
		<div class="col-sm-6 col-12 mt-3">
        <?php
            echo $this->Form->control('standard_name3',['class' => 'form-control', 'label' =>'Name(First & Last):' ]); 
			?>
		</div>
		<div class="col-sm-6 col-12 mt-3">	
            <?php echo $this->Form->control('standard_email3',['class' => 'form-control','type' => 'email', 'label' =>'User Email:' ]); ?>
		</div>
		
	</div>	
	<div class="row">
		<div class="col-sm-12 col-12 mt-3">
			<h5>Standard User Options:</h5>
		</div>
		<div class="col-sm-6 col-12 mt-3">	
			<?php echo $this->Form->control('can_see_account_details',['class' => 'form-control','options' => $options,'default' => '0']); ?>
		</div>
		<div class="col-sm-6 col-12 mt-3">	
			<?php echo $this->Form->control('can_see_placed_order',['class' => 'form-control','options' => $options,'default' => '0']); ?>
		</div>
		<div class="col-sm-6 col-12 mt-3">    
			<?php echo $this->Form->control('can_see_invoiced_order',['class' => 'form-control','options' => $options,'default' => '0']); ?>
		</div>
		<div class="col-sm-6 col-12 mt-3">	
			<?php echo $this->Form->control('can_see_credit_memos',['class' => 'form-control','options' => $options,'default' => '0']); ?>
		</div>
		<div class="col-sm-6 col-12 mt-3">	
			<?php echo $this->Form->control('can_see_prices',['class' => 'form-control','options' => $options,'default' => '0']); ?>
		</div>
		<div class="col-sm-6 col-12 mt-3">	
			<?php echo $this->Form->control('can_see_price_list',['class' => 'form-control','default' => '0','options' => $options]); ?>
	  </div>
	  <div class="col-sm-6 col-12 mt-3"></div>

	</div>
	<div class="row">
		 <div class="col-sm-3 col-12 mt-3">
	      <?php   echo $this->Form->control('_date',['class' => 'form-control','id' =>'dated']); ?>
	    </div>
	    <div class="col-sm-3 col-12 mt-3">
            <?php echo $this->Form->control('_title',['class' => 'form-control' ]);  ?>
        </div>
	     
        <div class="col-sm-3 col-12 mt-3">
            <?php echo $this->Form->control('print_name',['class' => 'form-control']); ?>
        </div> 
        
        <div class="col-sm-3 col-12 mt-3">    
           <?php  echo $this->Form->control('authorized_signature',['class' => 'form-control', 'type' =>'hidden' ]); ?>

           <!---div id="authorized_signature-pad" class="signature-pad">
				<div class="authorized_signature-pad--header">
				  <div class="description">Signature</div>
				</div>
				<div class="authorized_signature-pad--body">
				  <canvas id="drawing_pad" width="240" height="150" style="border:1px solid #ccc"></canvas>
				</div>
				<div class="authorized_signature-pad--actions">
					<div>
					  <button type="button" class="button clear btn btn-info" data-action="clear">Clear</button>
					</div>
					
				</div>
			
		  </div -->
         </div>
     </div>   
		
		<?= $this->Form->button(__('Save'),['class' => 'btn btn-primary','id' =>'user_submit']) ?>
	</div>
    <?= $this->Form->end() ?>
</div>
