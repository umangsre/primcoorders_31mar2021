<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Bill[]|\Cake\Collection\CollectionInterface $bill
 */
?>
<!--<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Bill'), ['action' => 'add']) ?></li>
    </ul>
</nav>-->

<div class="bill index large-9 medium-8 columns content card card card-default scrollspy">
    <div class="card-content">
        <!--<h3><?= __('Bill') ?></h3>-->
        <table cellpadding="0" cellspacing="0">
            <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('billing_date') ?></th>
                <th scope="col"><?= $this->Paginator->sort('billing_type_of_expense') ?></th>
                <th scope="col"><?= $this->Paginator->sort('billing_place_of_expense') ?></th>
                <th scope="col"><?= $this->Paginator->sort('billing_total_amount') ?></th>
                <th scope="col"><?= $this->Paginator->sort('billing_status') ?></th>
                <th scope="col">Comments</th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>

                <!-- <th scope="col"><?= __('Expense Amount') ?></th>
                <th scope="col"><?= __('Date') ?></th>
                <th scope="col"><?= __('Type of Expense') ?></th>
                <th scope="col"><?= __('Place of Expense') ?></th>
                <th scope="col"><?= __('Tax') ?></th>
                <th scope="col"><?= __('Total Amount') ?></th>
                <th scope="col"><?= __('Status') ?></th>
                <th scope="col"><?= __('Action') ?></th>-->
            </tr>
            </thead>
            <tbody>
            <?php foreach ($bill as $bill): ?>
                <tr>
                    <td><?= $this->Number->format($bill->id) ?></td>
                    <td><?= h($bill->billing_date) ?></td>
                    <td><?php $typeofexpense = h($bill->billing_type_of_expense);
                        $typeofexpense = str_replace("_", " ", $typeofexpense);
                        $typeofexpense = str_replace("|", "/", $typeofexpense);
                        echo $typeofexpense;


                        ?></td>
                    <td><?= h($bill->billing_place_of_expense) ?></td>

                    <td><?= h($bill->billing_total_amount) ?></td>
                    <td><?php
                        if ($bill->billing_status == 'P') {
                            echo 'Pending with SDM';
                        } elseif ($bill->billing_status == 'S') {
                            echo 'Saved';
                        } elseif ($bill->billing_status == 'A' || $bill->billing_status == 'AWS') {
                            echo 'Pending with Admin';
						}elseif ($bill->billing_status == 'RA' && !$isSDMAvailable){
							echo 'Rejected by Admin';
                        } elseif ($bill->billing_status == 'D') {
                            echo 'Rejected by SDM';
                        } elseif ($bill->billing_status == 'R') {
                            echo 'Rejected by SDM';
                        } elseif ($bill->billing_status == 'C') {
                            echo 'Closed';
                        } elseif ($bill->billing_status == 'AA') {
                            echo 'Approved by Admin';
                        } elseif ($bill->billing_status == 'RA') {
                            echo 'Pending with SDM';
                        }
                        ?></td>
                    <!--<td><?= h($bill->created) ?></td>-->
                    <td>
                        <?php
                        if (($bill->billing_status == 'D' || $bill->billing_status == 'R') && isset($bill->bill_comment[0])) {
                            echo $bill->bill_comment[0]->comment;
                        } else {
                            echo "";
                        }
                        ?>
                    </td>
                    <td class="actions">

                        <?= $this->Html->link('<i class="material-icons dp48">view_list</i>', ['action' => 'view', $bill->id], ['escape' => false, 'title' => __('View')]) ?>
                        <?php
                        if ($bill->billing_status == 'S' || $bill->billing_status == 'R') {
                            //echo $this->Html->link(__('Edit'), ['action' => 'edit', $bill->id]);
                            echo $this->Html->link('<i class="material-icons dp48">edit</i>', ['action' => 'edit', $bill->id], ['escape' => false, 'title' => __('Edit')]);
                        }
                        ?>
                        <?php
                        echo $this->Form->postLink('<i class="material-icons dp48">delete</i>', ['action' => 'delete', $bill->id], ['escape' => false, 'title' => __('Delete'), 'confirm' => __('Are you sure you want to delete # {0}?', $bill->id)]);
                        ?>
                    </td>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
        <div class="paginator">
            <ul class="pagination">
                <?= $this->Paginator->first('<< ' . __('first')) ?>
                <?= $this->Paginator->prev('< ' . __('previous')) ?>
                <?= $this->Paginator->numbers() ?>
                <?= $this->Paginator->next(__('next') . ' >') ?>
                <?= $this->Paginator->last(__('last') . ' >>') ?>
            </ul>
            <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
        </div>
    </div>
</div>