<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\User $user
 */
$roles = ['select' => 'Select', 'admin' => 'Admin', 'admin-claims' => 'Claims Admin', 'miller' => 'Miller', 'sales' => 'Sales', 'dealer' => 'Dealer', 'sdm' => 'SDM'];
?>
<div class="orders index large-9 medium-8 columns content card card card-default scrollspy">
    <div class="card-content">
        <div class="users form large-9 medium-8 columns content">
            <?= $this->Form->create($user) ?>
            <fieldset>
                <legend><?= __('Edit User') ?></legend>
                <?php
                echo $this->Form->control('name', ['required']);
                echo $this->Form->control('username', ['label' => 'Email / Username', 'type' => 'email', 'required']);
                echo $this->Form->control('password', ['value' => '']);
                echo $this->Form->control('role', ['required', 'options' => $roles, 'onchange' => 'changeUserType(this)']);
                ?>
                <div id="sales" style="display: <?php echo $user->role == 'sales' ? 'block' : 'none' ?>;">
                    <?php
                    $salesman = isset($user->salesmen[0]) ? $user->salesmen[0] : [];
                    $sdmSales = isset($user->sdm_sales[0]) ? $user->sdm_sales[0] : [];
                    echo $this->Form->control('sales_phone', ['value' => isset($salesman['phone']) ? $salesman['phone'] : '']);
                    echo $this->Form->control('sales_province', ['value' => isset($salesman['province']) ? $salesman['province'] : '']);
                    echo $this->Form->control('sales_rep', ['value' => isset($salesman['sales_rep']) ? $salesman['sales_rep'] : '']);
                    echo $this->Form->control('sd_rep', ['value' => isset($salesman['sd_rep']) ? $salesman['sd_rep'] : '']);
                    echo $this->Form->control('spec', ['value' => isset($salesman['spec']) ? $salesman['spec'] : '']);
                    echo $this->Form->control('sdm_id', ['options' => $sdmList, 'value' => isset($sdmSales['sdm_id']) ? $sdmSales['sdm_id'] : '']);
                    ?>
                </div>
                <div id="dealer" style="display: <?php echo $user->role == 'dealer' ? 'block' : 'none' ?>;">
                    <?php
                    $dealer = isset($user->customers[0]) ? $user->customers[0] : [];
                    echo $this->Form->control('dealer_customerid', ['value' => isset($dealer['customerid']) ? $dealer['customerid'] : '']);
                    echo $this->Form->control('dealer_phone', ['value' => isset($dealer['phone']) ? $dealer['phone'] : '']);
                    echo $this->Form->control('dealer_contact_name', ['value' => isset($dealer['contact_name']) ? $dealer['contact_name'] : '']);
                    echo $this->Form->control('dealer_address', ['value' => isset($dealer['address']) ? $dealer['address'] : '']);
                    echo $this->Form->control('dealer_city', ['value' => isset($dealer['city']) ? $dealer['city'] : '']);
                    echo $this->Form->control('dealer_province', ['value' => isset($dealer['province']) ? $dealer['province'] : '']);
                    echo $this->Form->control('dealer_postal_code', ['value' => isset($dealer['postal_code']) ? $dealer['postal_code'] : '']);
                    echo $this->Form->control('dealer_sin', ['value' => isset($dealer['sin']) ? $dealer['sin'] : '']);
                    ?>
                </div>
                <div id="miller" style="display: <?php echo $user->role == 'miller' ? 'block' : 'none' ?>;">
                    <?php
                    $miller = isset($user->millers[0]) ? $user->millers[0] : [];
                    echo $this->Form->control('mill_contact_name', ['value' => isset($miller['contact_name']) ? $miller['contact_name'] : '']);
                    echo $this->Form->control('mill_phone', ['value' => isset($miller['phone']) ? $miller['phone'] : '']);
                    echo $this->Form->control('mill_address', ['value' => isset($miller['address']) ? $miller['address'] : '']);
                    echo $this->Form->control('mill_city', ['value' => isset($miller['city']) ? $miller['city'] : '']);
                    echo $this->Form->control('mill_postal_code', ['value' => isset($miller['postal_code']) ? $miller['postal_code'] : '']);
                    ?>
                </div>
            </fieldset>
            <?php
            echo $this->Form->control('previous_user_details', ['value' => $user, 'type' => 'hidden']);
            ?>
            <?= $this->Form->button(__('Submit'), ['class' => 'btn waves-effect waves-light border-round gradient-45deg-purple-deep-orange col s12 mt-4']) ?>
            <?= $this->Form->end() ?>
        </div>
    </div>
</div>
<script>
    function changeUserType(event) {
        const $userType = event.value;
        const $salesDiv = $('#sales');
        const $dealerDiv = $('#dealer');
        const $millerDiv = $('#miller');
        if ($userType === 'sales') {
            $salesDiv.show();
            $dealerDiv.hide();
            $millerDiv.hide();
        } else if ($userType === 'dealer') {
            $salesDiv.hide();
            $dealerDiv.show();
            $millerDiv.hide();
        } else if ($userType === 'miller') {
            $salesDiv.hide();
            $dealerDiv.hide();
            $millerDiv.show();
        } else {
            $salesDiv.hide();
            $dealerDiv.hide();
            $millerDiv.hide();
        }
    }
</script>
