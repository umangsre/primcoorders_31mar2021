<?php
namespace App\Controller\Sales;

use App\Controller\AppController;

/**
 * Families Controller
 *
 * @property \App\Model\Table\FamiliesTable $Families
 *
 * @method \App\Model\Entity\Family[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class FamiliesController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $family = $this->Families->newEntity();
	//print_r($this->Auth->user());
	//	exit;
        $user_id = $this->Auth->user('id');
        $this->viewBuilder()->setLayout('sales_dashboard');
        $query = $this->Families->find();
       // if($this->request->is('post')){
         // $search = $this->request->getData('search');
         // $query->where(['OR' =>['name LIKE' => '%'.$search.'%','product_code LIKE' => '%'.$search.'%']]);
       // }
	   $search = $this->request->getQueryParams();

		if(!empty($search['search'])){
			$search = $search['search'];
			$query->where(['OR' =>['name LIKE' => '%'.$search.'%','product_code LIKE' => '%'.$search.'%','family LIKE' => '%'.$search.'%','mill LIKE' => '%'.$search.'%']]);
		}
        $families = $this->paginate($query);

        $this->set(compact('families','family','search'));
    }

    /**
     * View method
     *
     * @param string|null $id Family id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $user_id = $this->Auth->user('id');
        $this->viewBuilder()->setLayout('sales_dashboard');
        $family = $this->Families->get($id, [
            'contain' => []
        ]);

        $this->set('family', $family);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $user_id = $this->Auth->user('id');
        $this->viewBuilder()->setLayout('sales_dashboard');
        $family = $this->Families->newEntity();
        if ($this->request->is('post')) {
            $family = $this->Families->patchEntity($family, $this->request->getData());
            if ($this->Families->save($family)) {
                $this->Flash->success(__('The family has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The family could not be saved. Please, try again.'));
        }
        $this->set(compact('family'));
    }

    public function marginCalculator()
    {
        $user_id = $this->Auth->user('id');
       // $this->viewBuilder()->setLayout('sales_dashboard');
        $query = $this->Families->find();
        if ($this->request->is('post')) {
            $query->where(['id' => $this->request->getData('id')])->first();
        }
        $content = json_encode($query);
        $this->response = $this->response->withStringBody($content);
        $this->response = $this->response->withType('json');
        return $this->response;
    }


     public function findproductbyname()
    {
       $this->autoRender = false;
       $query = $this->Families->find();
        if ($this->request->is('post')) {
            if($id =$this->request->getData('id')){
            $query->where(['id' => $id])->first();
            }else{
                $search = '%'.$this->request->getData('search').'%';
                $query->where(['OR' =>['name LIKE' => '%'.$search.'%','family LIKE' => '%'.$search.'%','product_code LIKE' => '%'.$search.'%']])->all();
            }

        }
        $content = json_encode($query);
        $this->response = $this->response->withStringBody($content);
        $this->response = $this->response->withType('json');
        return $this->response;
    }



    /**
     * Edit method
     *
     * @param string|null $id Family id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $user_id = $this->Auth->user('id');
        $this->viewBuilder()->setLayout('sales_dashboard');
        $family = $this->Families->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $family = $this->Families->patchEntity($family, $this->request->getData());
            if ($this->Families->save($family)) {
                $this->Flash->success(__('The family has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The family could not be saved. Please, try again.'));
        }
        $this->set(compact('family'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Family id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $user_id = $this->Auth->user('id');
        $this->request->allowMethod(['post', 'delete']);
        $family = $this->Families->get($id);
        if ($this->Families->delete($family)) {
            $this->Flash->success(__('The family has been deleted.'));
        } else {
            $this->Flash->error(__('The family could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    public function viewCalculator($id = null){
        //$this->viewBuilder()->setLayout('dashboard');
        $this->set(compact('id'));
    }
}
