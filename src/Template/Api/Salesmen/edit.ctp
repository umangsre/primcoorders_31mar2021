<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Salesman $salesman
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $salesman->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $salesman->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Salesmen'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Orders'), ['controller' => 'Orders', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Order'), ['controller' => 'Orders', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="salesmen form large-9 medium-8 columns content">
    <?= $this->Form->create($salesman) ?>
    <fieldset>
        <legend><?= __('Edit Salesman') ?></legend>
        <?php
            echo $this->Form->control('name');
            echo $this->Form->control('sales_rep');
            echo $this->Form->control('sd_rep');
            echo $this->Form->control('rep_email');
            echo $this->Form->control('spec');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
