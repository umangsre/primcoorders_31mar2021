<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Customer $customer
 */
?>

<div class="orders index large-9 medium-8 columns content card card card-default scrollspy">
    <div class="card-content">
        <div class="customers view large-9 medium-8 columns content">
            <h3><?= h($customer->name) ?></h3>
            <table class="vertical-table">
                <tr>
                    <th scope="row"><?= __('Name') ?></th>
                    <td><?= h($customer->name) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Contact Name') ?></th>
                    <td><?= h($customer->contact_name) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Address') ?></th>
                    <td><?= h($customer->address) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('City') ?></th>
                    <td><?= h($customer->city) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Province') ?></th>
                    <td><?= h($customer->province) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Postal Code') ?></th>
                    <td><?= h($customer->postal_code) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Sin') ?></th>
                    <td><?= h($customer->sin) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Id') ?></th>
                    <td><?= $this->Number->format($customer->id) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Customerid') ?></th>
                    <td><?= $this->Number->format($customer->customerid) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Created') ?></th>
                    <td><?= h($customer->created) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Updated') ?></th>
                    <td><?= h($customer->updated) ?></td>
                </tr>
            </table>
        </div>
    </div>
</div>
