<?php
namespace App\Test\TestCase\Controller;

use App\Controller\Sales/BillController;
use Cake\TestSuite\IntegrationTestTrait;
use Cake\TestSuite\TestCase;

/**
 * App\Controller\Sales/BillController Test Case
 *
 * @uses \App\Controller\Sales/BillController
 */
class Sales/BillControllerTest extends TestCase
{
    use IntegrationTestTrait;

    /**
     * Test initial setup
     *
     * @return void
     */
    public function testInitialization()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
