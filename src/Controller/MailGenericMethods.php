<?php


namespace App\Controller;

use Cake\Mailer\Email;

class MailGenericMethods extends AppController
{
    public function initialize()
    {
        parent::initialize();
    }

    /*
     * sendMail() - $mail_data array contains parameter keys -:
     * to - array of target email strings
     * from - source email string
     * cc - array of cc email strings (optional)
     * bcc - array of bcc email strings (optional)
     * subject - subject string
     * body - array of body variables
     * template - template name string
     */
    public function sendMail($mail_data)
    {
        $email = new Email();
        $email->setTransport('default')
            ->viewVars($mail_data['body'])
            ->viewBuilder()->setTemplate($mail_data['template'])
            ->setEmailFormat('html')
            ->setFrom($mail_data['from'])
            ->setTo($mail_data['to'])
            ->setSubject($mail_data['subject']);
        if ((array_key_exists('cc', $mail_data)) && !empty($mail_data['cc']))
            $email->setCc($mail_data['cc']);
        if ((array_key_exists('bcc', $mail_data)) && !empty($mail_data['bcc']))
            $email->setBcc($mail_data['bcc']);
        $email->send();
        return true;
    }
}
