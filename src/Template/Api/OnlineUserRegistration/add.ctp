<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\OnlineUserRegistration $onlineUserRegistration
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List Online User Registration'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Credit Applications'), ['controller' => 'CreditApplications', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Credit Application'), ['controller' => 'CreditApplications', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="onlineUserRegistration form large-9 medium-8 columns content">
    <?= $this->Form->create($onlineUserRegistration) ?>
    <fieldset>
        <legend><?= __('Add Online User Registration') ?></legend>
        <?php
            echo $this->Form->control('credit_application_id', ['options' => $creditApplications, 'empty' => true]);
            echo $this->Form->control('user_type');
            echo $this->Form->control('submission_date');
            echo $this->Form->control('customer_number');
            echo $this->Form->control('company_name');
            echo $this->Form->control('administartor_will_see');
            echo $this->Form->control('standard_name');
            echo $this->Form->control('standard_email');
            echo $this->Form->control('standard_name1');
            echo $this->Form->control('standard_email1');
            echo $this->Form->control('standard_name2');
            echo $this->Form->control('standard_email2');
            echo $this->Form->control('standard_name3');
            echo $this->Form->control('standard_email3');
            echo $this->Form->control('first__last_name');
            echo $this->Form->control('user_email');
            echo $this->Form->control('can_see_account_details');
            echo $this->Form->control('can_see_placed_order');
            echo $this->Form->control('can_see_invoiced_order');
            echo $this->Form->control('can_see_credit_memos');
            echo $this->Form->control('can_see_prices');
            echo $this->Form->control('can_see_price_list');
            echo $this->Form->control('print_name');
            echo $this->Form->control('authorized_signature');
            echo $this->Form->control('_date');
            echo $this->Form->control('_title');
            echo $this->Form->control('anti_spam_consent');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
