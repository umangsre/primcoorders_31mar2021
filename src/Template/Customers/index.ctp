<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Customer[]|\Cake\Collection\CollectionInterface $customers
 */
?>

<div class="customers index large-9 medium-8 columns content">
    <div class="orders index large-9 medium-8 columns content card card card-default scrollspy">
    
        <div class="card-content">
            <?= $this->Form->create('search') ?>
            <fieldset>
                <?php
                    echo $this->Form->input('search',['label' =>false]);
                    echo $this->Form->button(__('Search'));
                ?>
            </fieldset>
           
            <?= $this->Form->end() ?>
            
            <table cellpadding="0" cellspacing="0">
                <thead>
                    <tr>
                        <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                        <th scope="col"><?= $this->Paginator->sort('customerid') ?></th>
                        <th scope="col"><?= $this->Paginator->sort('name') ?></th>
                        <th scope="col"><?= $this->Paginator->sort('address') ?></th>
                        <th scope="col"><?= $this->Paginator->sort('city') ?></th>
                        <th scope="col"><?= $this->Paginator->sort('province') ?></th>
                       <!--  <th scope="col"><?= $this->Paginator->sort('postal_code') ?></th>
                        <th scope="col"><?= $this->Paginator->sort('sin') ?></th>
                        <th scope="col"><?= $this->Paginator->sort('created') ?></th>
                        <th scope="col"><?= $this->Paginator->sort('updated') ?></th> -->
                        <th scope="col" class="actions"><?= __('Actions') ?></th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($customers as $customer): ?>
                    <tr>
                        <td><?= $this->Number->format($customer->id) ?></td>
                        <td><?= $this->Number->format($customer->customerid) ?></td>
                        <td><?= h($customer->name) ?></td>
                        <td><?= h($customer->address) ?></td>
                        <td><?= h($customer->city) ?></td>
                        <td><?= h($customer->province) ?></td>
                        <!-- <td><?= h($customer->postal_code) ?></td>
                        <td><?= h($customer->sin) ?></td>
                        <td><?= h($customer->created) ?></td>
                        <td><?= h($customer->updated) ?></td> -->
                        <td class="actions">
                            <?= $this->Html->link(__('View'), ['action' => 'view', $customer->id]) ?>
                            <?= $this->Html->link(__('Edit'), ['action' => 'edit', $customer->id]) ?>
                            <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $customer->id], ['confirm' => __('Are you sure you want to delete # {0}?', $customer->id)]) ?>
                        </td>
                    </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
            <div class="paginator">
                <ul class="pagination">
                    <?= $this->Paginator->first('<< ' . __('first')) ?>
                    <?= $this->Paginator->prev('< ' . __('previous')) ?>
                    <?= $this->Paginator->numbers() ?>
                    <?= $this->Paginator->next(__('next') . ' >') ?>
                    <?= $this->Paginator->last(__('last') . ' >>') ?>
                </ul>
                <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
            </div>
        </div>
    </div>
</div>
