<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\User $user
 */
?>
<div class="orders index large-9 medium-8 columns content card card card-default scrollspy">
    <div class="card-content">
        <div class="users form large-9 medium-8 columns content">
            <?= $this->Form->create($user) ?>
            <fieldset>
                <legend><?= __('Update Password') ?></legend>
                <?php if($password_change_status=="old") { ?>
                <div class="input-field col s12">
                <?= $this->Form->control('current_password', ['label' => 'Current Password', 'type' =>'password']); ?>
                </div>
                <?php } else if($password_change_status=="new") { ?>
                <div class="input-field col s12">
                <?= $this->Form->control('new_password', ['label' => 'Enter New Password', 'type' =>'password']); ?>
                </div>
                <div class="input-field col s12">
                <?= $this->Form->control('confirm_password', ['label' => 'Confirm New Password', 'type' =>'password']); ?>
                </div>
                <?php } ?>
            </fieldset>
            <?= $this->Form->button(__('Submit')) ?>
            <?= $this->Form->end() ?>
        </div>
    </div>
</div>
