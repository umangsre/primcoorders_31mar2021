<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Consumers Model
 *
 * @property \App\Model\Table\ClaimsTable&\Cake\ORM\Association\HasMany $Claims
 *
 * @method \App\Model\Entity\Consumer get($primaryKey, $options = [])
 * @method \App\Model\Entity\Consumer newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Consumer[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Consumer|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Consumer saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Consumer patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Consumer[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Consumer findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class ConsumersTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('consumers');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->hasMany('Claims', [
            'foreignKey' => 'consumer_id'
        ]);
    }
}
