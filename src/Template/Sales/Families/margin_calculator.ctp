<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Family $family
 */
use Cake\Routing\Router;
$link  =Router::url([ 'action' => 'findproductbyname'])
?>
<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Order $order
 */
?>

        <div class="row">
            <div class="col-12 logo-container text-center p-4">
                <?= $this->Html->image('logo.png'); ?>           
         </div>
       
<div class="container mt-2">
    <div class="large-9 medium-8 columns content card card card-default scrollspy">
        <div class=" form large-9 medium-8 columns content">
            <div class="row">
                  <h2><?= __('Margin Calculator') ?></h2>
                <div class="tab w-100">
                    <div class="form-fields">
                       
                        <?= $this->Form->create($family) ?>
                      
                           
                            <div class='row mt-2'>
                                <div class="col m6 s12">
                                    <?php
                                 echo $this->Form->control('name', ['class' =>'search_name' , 'label'=>false,
                                 'placeholder' =>'Search Product Name or Product Code or Product Families','data-link' =>$link]); ?>
                                  <div id="suggesstion-pricing" class="list_wraper shadow-sm" style="display:none;">    
                                  </div>
                                </div>
                                 <div class="col m6 s12">
                                    <?= $this->Form->button(__('Reset'), array( 'class' => 'btn btn-success pricing_reset', 'type' => 'button' )  ) ?>
                                 </div>
                            </div> 
                        </div>
                        <br>
                            <table id="pricing-matrix-table" class="table table-bordered">
                             <thead>
                            <tr>
                                <!--th>Sr.No.</th-->
                                <th>Slab Type</th>
                                <th>Sell Price</th>
                                <th>Rebate</th>
                                <th>Gross margin(In Percentage)</th>
                                <th></th>
                                
                            </tr>
                        </thead> 
                        <tbody id="pricing-matrix">   
                             <tr class="product_row">

                                <td class="price-ls"><?php  echo $this->Form->control('slevel',['value' =>'Slab 1','id' =>'slab_1','label' => false, 'readonly' => true]); ?></td>
                                <td class="price-ls"> <?php echo $this->Form->control('sell_price_level1',['label' => false,'readonly' => true]); ?>
                                </td>
                                <td class="price-ls"><?php echo $this->Form->control('rebate_level1',['label' => false,'readonly' => true]); ?></td>
                                <td class="price-ls"><?php  echo $this->Form->control('gross_margin_level1',['label' => false,'readonly' => true]); ?></td>
                                <td> </td>
                             </tr> 

                             <tr class="product_row">

                                <td class="price-ls"><?php  echo $this->Form->control('slevel',['value' =>'Slab 2','id' =>'slab_2','label' => false,'readonly' => true]); ?></td>
                                <td class="price-ls"> <?php echo $this->Form->control('sell_price_level2',['label' => false,'readonly' => true]); ?>
                                </td>
                                <td class="price-ls"><?php echo $this->Form->control('rebate_level2',['label' => false,'readonly' => true]); ?></td>
                                <td class="price-ls"><?php  echo $this->Form->control('gross_margin_level2',['label' => false,'readonly' => true]); ?></td>
                                 <td> </td>
                             </tr>  
                             <tr class="product_row">

                                <td class="col"><?php  echo $this->Form->control('slevel',['value' =>'Slab 3','id' =>'slab_3','label' => false,'readonly' => true]); ?></td>
                                <td class="col"> <?php echo $this->Form->control('sell_price_level3',['label' => false]); ?>
                                </td>
                                <td class="col"><?php echo $this->Form->control('rebate_level3',['label' => false,'readonly' => true]); ?></td>
                                <td class="col"><?php  echo $this->Form->control('gross_margin_level3',['label' => false,'readonly' => true]); ?></td>
                                 <td> </td>
                             </tr> 

                             <tr class="product_row">

                                <td class="price-ls"><?php  echo $this->Form->control('slevel',['class' =>'slab_select','label' => false,
                                    'options' =>['0' =>'Select Slab','1' =>'slab 1','2' =>'slab 2', '3' =>'slab 3']
                                    ] ); ?></td>
                                <td class="price-ls"> <?php echo $this->Form->control('sell_price',[ 'type' =>'number', 'min' =>'0', 'step' =>'0.01','class' =>'sell_price', 'label' => false]); ?>
                                </td>
                                <td class="price-ls"><?php echo $this->Form->control('rebate',['class' =>'rebate','label' => false]); ?></td>
                                <td class="price-ls"><?php  echo $this->Form->control('gross_margin',['class' =>'gross_margin','label' => false]); ?></td>
                                 <td><?= $this->Form->button(__('+'), array( 'class' => 'btn btn-info pricing_clone_add', 'type' => 'button' )  ) ?></td>
                             </tr>    
                            </tbody>
                        </table>

                            
                        <?= $this->Form->end() ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<style type="text/css">
    #pricing-matrix-table{
        table-layout: fixed;
    }
</style>