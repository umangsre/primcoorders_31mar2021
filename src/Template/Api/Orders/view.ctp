<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Order $order
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Order'), ['action' => 'edit', $order->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Order'), ['action' => 'delete', $order->id], ['confirm' => __('Are you sure you want to delete # {0}?', $order->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Orders'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Order'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Customers'), ['controller' => 'Customers', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Customer'), ['controller' => 'Customers', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Salesmen'), ['controller' => 'Salesmen', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Salesman'), ['controller' => 'Salesmen', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Order Items'), ['controller' => 'OrderItems', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Order Item'), ['controller' => 'OrderItems', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="orders view large-9 medium-8 columns content">
    <h3><?= h($order->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Customer') ?></th>
            <td><?= $order->has('customer') ? $this->Html->link($order->customer->name, ['controller' => 'Customers', 'action' => 'view', $order->customer->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Salesman') ?></th>
            <td><?= $order->has('salesman') ? $this->Html->link($order->salesman->name, ['controller' => 'Salesmen', 'action' => 'view', $order->salesman->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Po Number') ?></th>
            <td><?= h($order->po_number) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Ship To') ?></th>
            <td><?= h($order->ship_to) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Ship Via') ?></th>
            <td><?= h($order->ship_via) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Additional Comments') ?></th>
            <td><?= h($order->additional_comments) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Events') ?></th>
            <td><?= h($order->events) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Sale Order Number') ?></th>
            <td><?= h($order->sale_order_number) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Incentive Reciept') ?></th>
            <td><?= h($order->incentive_reciept) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Purchaser Name') ?></th>
            <td><?= h($order->purchaser_name) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Purchaser Sign') ?></th>
            <td><?= h($order->purchaser_sign) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($order->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Grand Total') ?></th>
            <td><?= $this->Number->format($order->grand_total) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Status') ?></th>
            <td><?= $this->Number->format($order->status) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Sale Date') ?></th>
            <td><?= h($order->sale_date) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Ship Date') ?></th>
            <td><?= h($order->ship_date) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Created') ?></th>
            <td><?= h($order->created) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Updated') ?></th>
            <td><?= h($order->updated) ?></td>
        </tr>
    </table>
    <div class="row">
        <h4><?= __('Signature') ?></h4>
        <?= $this->Text->autoParagraph(h($order->signature)); ?>
    </div>
    <div class="related">
        <h4><?= __('Related Order Items') ?></h4>
        <?php if (!empty($order->order_items)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Order Id') ?></th>
                <th scope="col"><?= __('Product Id') ?></th>
                <th scope="col"><?= __('Price') ?></th>
                <th scope="col"><?= __('Quantity') ?></th>
                <th scope="col"><?= __('Incentives') ?></th>
                <th scope="col"><?= __('Total') ?></th>
                <th scope="col"><?= __('Promo') ?></th>
                <th scope="col"><?= __('Discount') ?></th>
                <th scope="col"><?= __('Franklin Enabled') ?></th>
                <th scope="col"><?= __('Created') ?></th>
                <th scope="col"><?= __('Modified') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($order->order_items as $orderItems): ?>
            <tr>
                <td><?= h($orderItems->id) ?></td>
                <td><?= h($orderItems->order_id) ?></td>
                <td><?= h($orderItems->product_id) ?></td>
                <td><?= h($orderItems->price) ?></td>
                <td><?= h($orderItems->quantity) ?></td>
                <td><?= h($orderItems->incentives) ?></td>
                <td><?= h($orderItems->total) ?></td>
                <td><?= h($orderItems->promo) ?></td>
                <td><?= h($orderItems->discount) ?></td>
                <td><?= h($orderItems->franklin_enabled) ?></td>
                <td><?= h($orderItems->created) ?></td>
                <td><?= h($orderItems->modified) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'OrderItems', 'action' => 'view', $orderItems->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'OrderItems', 'action' => 'edit', $orderItems->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'OrderItems', 'action' => 'delete', $orderItems->id], ['confirm' => __('Are you sure you want to delete # {0}?', $orderItems->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>
