<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Family[]|\Cake\Collection\CollectionInterface $families
 */

use Cake\Routing\Router;

$link = Router::url(['action' => 'findproductbyname']);
$product_link = Router::url(['action' => 'marginCalculator']);


?>
<style>
    .message.success {
        text-align: center;
        font-size: 15px;
        background: #eee;
        padding: 10px 0;
        margin-bottom: 6px;
    }

    .modal {
        max-height: 80%;
    }

    th.db_persentage, th.db_persentage2 {
        width: 1%;
    }

    .modal-m {
        height: 800px;
    }
</style>
<div class="orders index large-9 medium-8 columns content card card card-default scrollspy">
    <div class="card-content">
        <?= $this->Flash->render() ?>
        <div class="families index large-9 medium-8 columns content">
            <?= $this->Form->create('search', ['type' => 'get']) ?>
            <fieldset>
                <div class="input text"><input type="text" name="search" id="search" value="<?= @$_GET['search'] ?>">
                </div>
                <?php
                echo $this->Form->button(__('Search'));
                ?>
            </fieldset>

           
             <fieldset>
                <div class="input text"><input type="text" name="search_f" id="search_f" value="<?= @$_GET['search_f'] ?>">
                </div>
                <?php
                echo $this->Form->button(__('Search Family'));
                ?>
            </fieldset> 


      <?= $this->Form->end() ?>

            <!-- Modal Trigger -->
            <div class="import export right-align">
                <a class="waves-effect waves-light btn modal-trigger" href="#modal1">Import Data</a>
                <?= $this->Html->link(__('Excel'), ['action' => 'index', '_ext' => 'xlsx', '?' => array('search' => @$_GET['search'])], array(
                    'id' => 'myId',
                    'class' => 'waves-effect waves-light btn'
                )); ?>
            </div>

            <!-- Modal Structure -->
            <div id="modal1" class="modal">
                <div class="modal-content">
                    <h4>Import Data</h4>
                    <div class="content">


                        <div class="upload-frm">
                            <?php echo $this->Form->create($uploadData, ['type' => 'file']); ?>
                            <?php echo $this->Form->input('file', ['type' => 'file', 'class' => 'form-control']); ?>
                            <?php echo $this->Form->button(__('Upload File'), ['type' => 'submit', 'class' => 'form-controlbtn btn-default']); ?>
                            <?php echo $this->Form->end(); ?>


                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <a href="#!" class="modal-close waves-effect waves-green btn-flat">close</a>
                </div>
            </div>


            <table class="responsive-table">
                <thead>
                <tr>
                    
                    <th scope="col"><?= $this->Paginator->sort('family') ?></th>
                    <th scope="col"><?= $this->Paginator->sort('name') ?></th>
                    <th scope="col"><?= $this->Paginator->sort('mill') ?></th>
                    <th scope="col"
                        title="<?= __('Product Code') ?>"><?= $this->Paginator->sort('product_code', 'Code') ?></th>
                    
                    <th scope="col" title="<?= __('Mill Cost') ?>"><?= $this->Paginator->sort('mill_cost', 'MC') ?></th>
                    <th scope="col"
                        title="<?= __('USD/CAD Currency') ?>"><?= $this->Paginator->sort('usd_cad', 'CY') ?></th>
                    
                    <th scope="col"
                        title="<?= __('New Mill Cost') ?>"><?= $this->Paginator->sort('new_mill_cost', 'NMC') ?></th>
                    <th scope="col" class="db_persentage"
                        title="<?= __('Brokerage Multiplier') ?>"><?= $this->Paginator->sort('brokerage_multiplier', 'D&B (%)') ?></th>
                    <th scope="col" class="db_persentage"
                        title="<?= __('Brokerage') ?>"><?= $this->Paginator->sort('BR', 'D&B ($)') ?></th>
                    <th scope="col" title="<?= __('Units') ?>"><?= $this->Paginator->sort('units') ?></th>
                    <th scope="col" class="db_persentage2"
                        title="<?= __('Freight') ?>"><?= $this->Paginator->sort('freight', 'Freight ($)') ?></th>
                    <th scope="col"
                        title="<?= __('Landed Cost Per Unit') ?>"><?= $this->Paginator->sort('landed_cost_per_unit', 'LCPU') ?></th>
                    <th scope="col" class="db_persentage2"
                        title="<?= __('Load Percentage') ?>"><?= $this->Paginator->sort('load_percentage', 'Load (%)') ?></th>
                    <th scope="col" title="<?= __('Margin lIC') ?>"><?= $this->Paginator->sort('MLIC') ?></th>
                 
                    <th scope="col" class="actions"><?= __('Actions') ?></th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($families as $family): ?>
                    <tr>
                      
                        <td><?= h($family->family) ?></td>
                        <td><?= h($family->name) ?></td>
                        <td><?= h($family->mill) ?></td>
                        <td><?= h($family->product_code) ?></td>
                        <td><?= $this->Number->format($family->mill_cost) ?></td>
                        <td><?php
                            $usd = (h($family->usd_cad));
                            $t = explode('_',$usd);
                            $name = '';
                            foreach($t as $key => $value){
                                if($key > 0){
                                    $name .= '-'.substr($value, 0,1);
                                } else {
                                    $name = $value;
                                }
                            }
                           echo strtoupper($name);
                            ?></td>
                        <td><?= $this->Number->format($family->new_mill_cost) ?></td>
                        <td><?= ($this->Number->format($family->brokerage_multiplier) * 100) . "%" ?></td>
                        <td><?= $this->Number->format($family->brokerage) ?></td>
                        <td><?= h($family->units) ?></td>
                        <td><?= $this->Number->format($family->freight) ?></td>
                        <td><?= $this->Number->format($family->landed_cost_per_unit) ?></td>
                        <td><?= ($this->Number->format($family->load_percentage) * 100) . "%" ?></td>
                        <td><?= $this->Number->format($family->margin_lic) ?></td>
                        
                        <td class="actions">
                            <?= $this->Html->link('<i class="material-icons dp48">view_list</i>', ['action' => 'view', $family->id], ['escape' => false, 'title' => __('View')]) ?>

                            <?= $this->Html->link('<i class="material-icons dp48">edit</i>', ['action' => 'edit', $family->id], ['escape' => false, 'title' => __('Edit')]) ?>
                            <?= $this->Form->postLink('<i class="material-icons dp48">delete</i>', ['action' => 'delete', $family->id], ['escape' => false, 'title' => __('Delete'), 'confirm' => __('Are you sure you want to delete # {0}?', $family->id)]) ?>
                            <?= $this->Html->link('<i class="material-icons dp48">queue_play_next</i>', ['action' => 'view-calculator', $family->id], ['escape' => false, 'title' => __('View')]) ?>

                        </td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
            <div class="paginator">
                <ul class="pagination">
                    <?= $this->Paginator->first('<< ' . __('first')) ?>
                    <?= $this->Paginator->prev('< ' . __('previous')) ?>
                    <?= $this->Paginator->numbers() ?>
                    <?= $this->Paginator->next(__('next') . ' >') ?>
                    <?= $this->Paginator->last(__('last') . ' >>') ?>
                </ul>
                <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
            </div>
        </div>
    </div>
</div>
<!-- Modal Structure -->
<div id="modal1" class="modal modal-m" style="height: 800px;">
    <div class="modal-content" style="height: 90%;    overflow: auto;">
        <h3>Margin Calculator</h3>
        <div class="container mt-2">
            <div class="fgdf">
                <div class="gfg">
                    <div class="row">
                        <div class="tab w-100">
                            <?= $this->Form->create($family) ?>
                            <div class="form-fields">


                                <div class='row mt-2'>
                                    <div class="col m3 s12 position-relative">
                                        <?php
                                        echo $this->Form->control('search_name', ['class' => 'search_name', 'label' => false,
                                            'placeholder' => 'Search Product Name or Product Code or Product Families', 'data-link' => $link]);
                                        ?>
                                        <div id="suggesstion-pricing" class="list_wraper" style="display:none;">
                                        </div>
                                    </div>
                                    <div class="col m3 s12 position-relative">
                                        <?php
                                        echo $this->Form->control('defaut_dropdown', ['class' => 'defaut_dropdown', 'id' => 'defaut_dropdown', 'label' => false,
                                            'options' => [
                                                'defaut' => 'Defaut',
                                                'manually' => 'Dollar'
                                            ]
                                        ]);

                                        ?>
                                    </div>
                                    <div class="col m2 s12 position-relative">
                                        <?php
                                        echo $this->Form->control('defaut_value', ['class' => 'defaut_value float-number', 'id' => 'defaut_value', 'label' => false, 'readonly' => true, 'onkeyup' => 'getdefaut()']);
                                        ?>
                                    </div>

                                    <div class="col m4 s12">
                                        <?= $this->Form->button(__('Reset'), array('class' => 'btn btn-success pricing_reset', 'type' => 'button')) ?>
                                    </div>
                                </div>

                            </div>
                            <br>
                            <table id="pricing-matrix-table" class="table table-bordered">
                                <thead>
                                <tr>
                                    <th>Slab Type</th>
                                    <th>Sell Price</th>
                                    <th>Rebate</th>
                                    <th>Gross margin(In Percentage)</th>
                                    <th></th>

                                </tr>
                                </thead>
                                <tbody id="pricing-matrix">
                                <tr class="product_row">

                                    <td class="price-ls"><?php echo $this->Form->control('slevel', ['value' => 'Slab 1', 'id' => 'slab_1', 'label' => false, 'readonly' => true]); ?></td>
                                    <td class="price-ls"> <?php echo $this->Form->control('sell_price_level1', ['label' => false, 'readonly' => true, 'class' => 'sell_price']); ?>
                                    </td>
                                    <td class="price-ls"><?php echo $this->Form->control('rebate_level1', ['class' => 'rebate', 'label' => false, 'readonly' => true]); ?></td>
                                    <td class="price-ls"><?php echo $this->Form->control('gross_margin_level1', ['class' => 'gross_margin', 'label' => false, 'readonly' => true]); ?></td>
                                    <td></td>
                                </tr>

                                <tr class="product_row">

                                    <td class="price-ls"><?php echo $this->Form->control('slevel', ['value' => 'Slab 2', 'id' => 'slab_2', 'label' => false, 'readonly' => true]); ?></td>
                                    <td class="price-ls"> <?php echo $this->Form->control('sell_price_level2', ['label' => false, 'readonly' => true, 'class' => 'sell_price']); ?>
                                    </td>
                                    <td class="price-ls"><?php echo $this->Form->control('rebate_level2', ['class' => 'rebate', 'label' => false, 'readonly' => true]); ?></td>
                                    <td class="price-ls"><?php echo $this->Form->control('gross_margin_level2', ['class' => 'gross_margin', 'label' => false, 'readonly' => true]); ?></td>
                                    <td></td>
                                </tr>
                                <tr class="product_row">

                                    <td class="price-ls"><?php echo $this->Form->control('slevel', ['value' => 'Slab 3', 'id' => 'slab_3', 'label' => false, 'readonly' => true]); ?></td>
                                    <td class="price-ls"> <?php echo $this->Form->control('sell_price_level3', ['label' => false, 'class' => 'sell_price']); ?>
                                    </td>
                                    <td class="price-ls"><?php echo $this->Form->control('rebate_level3', ['class' => 'rebate', 'label' => false, 'readonly' => true]); ?></td>
                                    <td class="price-ls"><?php echo $this->Form->control('gross_margin_level3', ['class' => 'gross_margin', 'label' => false, 'readonly' => true]); ?>

                                    </td>
                                    <td>
                                    </td>
                                </tr>

                                <tr class="product_row">

                                    <td class="price-ls"><?php echo $this->Form->control('slevel', ['class' => 'slab_select', 'label' => false, 'value' => 'Slab 4', 'readonly' => true]);
                                        ?>

                                    </td>
                                    <td class="price-ls">
                                        <?php echo $this->Form->control('sell_price', ['type' => 'number', 'min' => '0', 'step' => '0.01', 'class' => 'sell_price', 'label' => false]); ?>
                                    </td>
                                    <td class="price-ls">
                                        <?php echo $this->Form->control('rebate', ['class' => 'rebate', 'label' => false, 'value' => '0']); ?>

                                    </td>
                                    <td class="price-ls"><?php echo $this->Form->control('gross_margin', ['class' => 'gross_margin', 'label' => false]); ?></td>
                                    <td></td>
                                </tr>
                                </tbody>
                            </table>


                            <?= $this->Form->end() ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <?= $this->Form->button(__('+'), array('class' => 'btn btn-info pricing_clone_add', 'type' => 'button')) ?>
        <a href="#!" class="modal-close waves-effect waves-light pink accent-2 white-text btn">Close</a>
    </div>
</div>
<script>
    setInterval(function () {
        $(".success").hide();
    }, 10000);
</script>
