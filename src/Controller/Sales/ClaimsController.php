<?php

namespace App\Controller\Sales;

use App\Controller\AppController;
use App\Controller\ClaimsGenericMethods;
use App\Controller\ConsumerGenericMethods;
use App\Controller\DealerGenericMethods;
use App\Controller\MillerGenericMethods;
use Cake\Event\Event;
use Cake\I18n\Time;
use Cake\Mailer\Email;

/**
 * Claims Controller
 *
 * @property \App\Model\Table\ClaimsTable $Claims
 *
 * @method \App\Model\Entity\Claim[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class ClaimsController extends AppController
{
    private $currentUser = '';

    public function initialize()
    {
        parent::initialize();
        $this->loadComponent('RequestHandler');
        $this->currentUser = $this->Auth->user();
        $this->viewBuilder()->setLayout('sales_dashboard');
    }

    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);
        $this->Auth->allow(['add']);
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $claimMethods = new ClaimsGenericMethods();
        $searchData = $this->request->getData();
        $claims = ($this->currentUser['role'] == 'admin' && $this->request->is('put')) ?
            $this->paginate($claimMethods->getAllClaims(true, $searchData)) :
            $this->paginate($claimMethods->getAllClaims());
        ($this->currentUser['role'] == 'admin' && $this->request->is('put')) &&
        $this->set(compact('searchData'));
        $claimsArray = [];
        $currentUser = $this->currentUser;
        foreach ($claims as $claim) {
            if ($claimMethods->isUserAllowedToViewClaim($claim, $currentUser)) {
                if ($currentUser['role'] === 'dealer') {
                    $claimStatuses = $claim->claim_status;
                    if (count($claimStatuses) > 0) {
                        $lastStatus = $claimStatuses[count($claimStatuses) - 1];
                        if ($lastStatus->status !== 'approved by primco' ||
                            $lastStatus->status !== 'declined by primco') {
                            $lastStatus->status = 'Pending';
                        }
                    }
                }
                $claimsArray[] = $claim;
            }
        }
        $claims = $claimsArray;
        $this->set(compact('claims'));
        $this->set(compact('currentUser'));
    }

    /**
     * View method
     *
     * @param string|null $id Claim id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $claimMethods = new ClaimsGenericMethods();
        $millerMethods = new MillerGenericMethods();
        $claim = $claimMethods->getClaim($id);
        $currentUserInfo = $this->currentUser;
        if ($claimMethods->isUserAllowedToViewClaim($claim, $currentUserInfo)) {
            $millers = [];
            if ($currentUserInfo['role'] === 'admin') {
                $millers = $millerMethods->getAllMillers()->all();
            }
            $claim['role'] = $currentUserInfo['role'];
            $categoryData = $claimMethods->getAllCategoryLabourCost()->all();
            $generalData = $claimMethods->getAllGeneralLabourCost()->all();
            $this->set('claim', $claim);
            $this->set(compact('categoryData'));
            $this->set(compact('generalData'));
            $this->set(compact('millers'));
            if ($this->request->is('put')) {
                $this->modifyClaim($claim, $this->request->getData());
            }
        } else {
            return $this->redirect(['action' => 'index']);
        }
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $claimMethods = new ClaimsGenericMethods();
        $dealerMethods = new DealerGenericMethods();
        $consumerMethods = new ConsumerGenericMethods();
        $currentUserInfo = $this->currentUser;
        $category = json_encode($claimMethods->getAllCategoryLabourCost()->all());
        $this->set(compact('category'));
        $general = json_encode($claimMethods->getAllGeneralLabourCost()->all());
        $this->set(compact('general'));
        $claim = $this->Claims->newEntity();
        $this->set(compact('claim'));
        $dealerDetails = $currentUserInfo['role'] == 'dealer' ? $dealerMethods->getDealerDetails($currentUserInfo['id']) : [];
        $this->set(compact('dealerDetails'));
        if ($this->request->is('post')) {
            $data = $this->request->getData();
            $dealerData = $data;
            if (empty($data['customer_id']) && array_key_exists('customer_name', $data) && !empty($data['customer_name'])) {
                if (empty($dealerData['customer_email'])) {
                    $this->Flash->error(__('Please enter dealer information.'));
                    return;
                }
                $password = $dealerMethods->createUniquePassword();
                $addDealerToUsers = $dealerMethods->addDealerToUsers($dealerData, $password);
                $data['customerid'] = $addDealerToUsers['id'];
                $addDealer = $dealerMethods->addDealer($dealerData, $addDealerToUsers['id']);
                $data['customer_id'] = $addDealer['id'];
                $this->Flash->success(__('The dealer added successfully with password ' . $password));
            } else {
                $updateDealer = $dealerMethods->updateDealer($data['customer_id'], $data);
            }
            $consumerDetails = [
                'retailer_name' => $data['retailer_name'],
                'contact_name' => '',
                'email' => '',
                'phone' => $data['retailer_phone'],
                'address' => $data['retailer_address'],
                'city' => $data['retailer_city'],
                'postal_code' => $data['retailer_postal_code'],
                'created' => new Time()
            ];
            $addConsumer = $consumerMethods->addConsumer($consumerDetails);
            $consumerId = $addConsumer['id'];
            if (array_key_exists('claims', $data)) {
                $claim['claim_json'] = json_encode($data['claims']);
            }
            if ($data['settlement_method'] == 'replacement') {
                $data['total_settlement_amount'] = $data['total_labour_cost_claimed'];
            }
            $claim['created_by'] = $currentUserInfo['id'];
            $claim = $this->Claims->patchEntity($claim, $data);
            if ($this->Claims->save($claim)) {
                $claimStatusDetails = [
                    'claim_id' => $claim['id'],
                    'status_by' => $currentUserInfo['id'],
                    'status' => $data['clicked'] == 'save' ? 'saved' : 'send to primco',
                    'created_at' => new Time()
                ];
                $addStatus = $claimMethods->addClaimStatus($claimStatusDetails);
                $claimCreatedByRole = $currentUserInfo['role'];
                $mail = '';
                if ($claimCreatedByRole === 'sales' || 'retailer' || 'customer') {
                    // send to admin mail
                }
                $this->Flash->success(__('The claim has been saved.'));
                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The claim could not be saved. Please, try again.'));
        }
    }

    /**
     * Edit method
     *
     * @param $claim
     * @param $data
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     */
    public function modifyClaim($claim, $data)
    {
        $claimMethods = new ClaimsGenericMethods();
        $millerMethods = new MillerGenericMethods();
        $updateClaimInfo = $data;
        $millerData = $data;
        if (empty($data['miller_id']) && array_key_exists('miller_name', $data) && !empty($data['miller_name'])) {
            if (empty($millerData['miller_email'])) {
                $this->Flash->error(__('Please enter miller information.'));
                return;
            }
            $password = $millerMethods->createUniquePassword();
            $addMillerToUsers = $millerMethods->addMillerToUsers($millerData, $password);
            $updateClaimInfo['miller_user_id'] = $addMillerToUsers['id'];
            $addMiller = $millerMethods->addMiller($millerData, $addMillerToUsers['id']);
            $updateClaimInfo['miller_id'] = $addMiller['id'];
            $this->Flash->success(__('The miller added successfully with password ' . $password));
        } else {
            $updateClaimInfo['miller_user_id'] = $millerData['miller_user_id'];
            $updateClaimInfo['miller_id'] = $millerData['miller_id'];
        }
        if (array_key_exists('primco_invoice_number', $data)) {
            $update['primco_invoice_number'] = $data['primco_invoice_number'];
        }
        if (array_key_exists('miller_invoice_number', $data)) {
            $updateClaimInfo['miller_invoice_number'] = $data['miller_invoice_number'];
        }
        $updateClaim = $claimMethods->updateClaim($claim, $updateClaimInfo);
        if ($updateClaim) {
            $this->Flash->success(__('The claim has been updated.'));
        }
        $claimStatusDetails = $data;
        $status = $claimStatusDetails['status'];
        $claimStatusDetails['status_by'] = $this->currentUser['id'];
        $claimStatusDetails['created_at'] = new Time();
        $addClaimStatus = $claimMethods->addClaimStatus($claimStatusDetails);

        if ($addClaimStatus) {
            $statusCreatedByRole = $this->currentUser['role'];
            $mail = '';
            if ($statusCreatedByRole === 'admin') {
                if ($status === 'send to mill') {
                    $claimStatusCount = $claimMethods->checkStatusCount($claim->id, 'send to mill');
                    if ($claimStatusCount === 0) {
                        // send mail to miller for new claim
                    } else {
                        // send mail to miller for new comment on claim
                    }
                } else {
                    // send mail to claim created by id, customer, retailer
                }
            } else if ($statusCreatedByRole === 'miller') {
                // send mail to admin
            } else if ($statusCreatedByRole === 'dealer') {
                // send mail to admin
            }
            $this->Flash->success(__('The claim has been ' . $status . '.'));
            return $this->redirect(['action' => 'index']);
        }
        $this->Flash->error(__('The claim could not be ' . $status . '. Please, try again.'));
    }

    public function sendMail($image_name, $link, $id)
    {
        $toEmail = [
            's.nesbitt@primco.ca',
            't.yelland@primco.ca',
            'd.grona@primco.ca',
            'm.malhotra@primco.ca',
        ];
        $email = new Email();
        $email->setTransport('default');
        $email->viewVars(['image_name' => $image_name, 'link' => $link]);
        $email->viewBuilder()->setTemplate('creditapplication');
        $email->setEmailFormat('html');
        $email->setFrom(['support@primco.ca' => 'Primco'])
            ->setTo($toEmail)
            ->setBcc('Juneja.ankit@gmail.com')
            ->setSubject('New Customer #' . $id)
            ->send();
        return true;
    }

    /**
     * Delete method
     *
     * @param string|null $id Claim id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $claim = $this->Claims->get($id);
        if ($this->Claims->delete($claim)) {
            $this->Flash->success(__('The claim has been deleted.'));
        } else {
            $this->Flash->error(__('The claim could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
