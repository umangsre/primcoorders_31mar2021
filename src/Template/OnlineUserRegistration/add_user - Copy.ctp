<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\OnlineUserRegistration $onlineUserRegistration
 */
$options = array(
                    '1' => 'Yes',
                    '0' => 'No'
                  );
$options_user_type = array(
                    '1' => 'Adminstrator',
                    '2' => 'Standard User'
                  );
?>
<div class="row">
    <div class="col-12 logo-container text-center p-4">
        <?= $this->Html->image('logo.png'); ?>           
    </div>
</div>
<div class="onlineUserRegistration form large-9 medium-8 columns content">
    <?= $this->Form->create($onlineUserRegistration) ?>
    <div class="row">
        <legend><?= __('Add Online User Registration') ?></legend>
	</div>	
	<div class="row">	
	
		<div class="col-sm-6 col-12 mt-3">
        <?php
            echo $this->Form->control('first__last_name',['class' => 'form-control','value' =>'','required' => 'required', 'label' =>'First & last Name' ]); 
			?>
		</div>
		<div class="col-sm-6 col-12 mt-3">	
            <?php echo $this->Form->control('email',['class' => 'form-control','value' =>'','required' => 'required' ]); ?>
		</div>
		<div class="col-sm-6 col-12 mt-3">	
            <?php echo $this->Form->control('user_type',['class' => 'form-control','options' => $options_user_type,'default' => '2','empty' => true ]); ?>
		</div>
		<div class="col-sm-6 col-12 mt-3"></div>
	</div>
	<div class="user_options_main">
		
		<div class="user_options_list">
			<div class="row">

				<div class="col-sm-6 col-12 mt-3">	
		            <?php echo $this->Form->control('can_see_account_details',['class' => 'form-control','options' => $options,'default' => '0']); ?>
				</div>
				<div class="col-sm-6 col-12 mt-3">	
		            <?php echo $this->Form->control('can_see_placed_order',['class' => 'form-control','options' => $options,'default' => '0']); ?>
		        </div>
				<div class="col-sm-6 col-12 mt-3">    
					<?php echo $this->Form->control('can_see_invoiced_order',['class' => 'form-control','options' => $options,'default' => '0']); ?>
				</div>
				<div class="col-sm-6 col-12 mt-3">	
		            <?php echo $this->Form->control('can_see_credit_memos',['class' => 'form-control','options' => $options,'default' => '0']); ?>
				</div>
				<div class="col-sm-6 col-12 mt-3">	
		            <?php echo $this->Form->control('can_see_prices',['class' => 'form-control','options' => $options,'default' => '0']); ?>
				</div>
				<div class="col-sm-6 col-12 mt-3">	
		            <?php echo $this->Form->control('can_see_price_list',['class' => 'form-control','default' => '0','options' => $options]); ?>
		      </div>
			  <div class="col-sm-6 col-12 mt-3"></div>
		
    		</div>
    	</div>
    </div>
    <div class="row">
			<button type="button" class="btn btn-info user_options_add">+</button>
	</div>
	<div class="row">
    <?= $this->Form->button(__('Submit'),['class' => 'btn btn-primary']) ?>
	</div>
    <?= $this->Form->end() ?>
</div>
