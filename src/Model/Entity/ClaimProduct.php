<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * ClaimProduct Entity
 *
 * @property int $id
 * @property int|null $claim_id
 * @property int|null $product_id
 * @property string|null $product_number
 * @property int|null $claim_quantity
 * @property string|null $is_installed
 * @property string|null $installed_area
 * @property \Cake\I18n\FrozenDate|null $installed_date
 * @property \Cake\I18n\FrozenDate|null $created
 *
// * @property \App\Model\Entity\Claim $claim
 * @property \App\Model\Entity\Product $product
 */
class ClaimProduct extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'claim_id' => true,
        'product_id' => true,
        'product_number' => true,
        'claim_quantity' => true,
        'is_installed' => true,
        'installed_area' => true,
        'installed_date' => true,
        'created' => true,
//        'claim' => true,
        'product' => true
    ];
}
