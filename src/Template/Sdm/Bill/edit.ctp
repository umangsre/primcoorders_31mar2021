<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Bill $bill
 */
?>
<!--<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
    __('Delete'),
    ['action' => 'delete', $bill->id],
    ['confirm' => __('Are you sure you want to delete # {0}?', $bill->id)]
)
?></li>
        <li><?= $this->Html->link(__('List Bill'), ['action' => 'index']) ?></li>
    </ul>
</nav>-->
<style>
    img.billing_image {
        height: 150px;
        width: 150px;
        margin-left: 10px;
    }

    .save-data {
        background: #2c323f;
        border: none;
        padding: 9px 20px;
        margin-top: 15px;
        color: #fff;
    }
</style>
<div class="bill-comment index large-9 medium-8 columns content card card card-default scrollspy">
    <div class="card-content">
        <div class="bill form large-9 medium-8 columns content">
            <?= $this->Form->create($bill) ?>
            <fieldset>
                <legend><?= __('Edit Bill') ?></legend>
                <div class="input text">
                    <label for="billing-image">Billing Date</label>
                    <?php

                    $date = date_create($bill->billing_date);
                    ?>
                    <input type="text" name="billing_date" id="billing_date" required
                           value="<?= date_format($date, "Y-m-d") ?>">
                </div>
                <?php
                //                echo $this->Form->control('billing_date', ['empty' => true]);
                // echo $this->Form->control('billing_type_of_expense');
                echo $this->Form->control('billing_type_of_expense', ['class' => 'billing_type_of_expense',
                    'options' => [
                        "Auto_Allowance|Car_Expenses" => "Auto Allowance/Car Expenses",
                        "Airfare" => "Airfare",
                        "Accommodation" => "Accommodation",
                        "Meals_&_Entertainment" => "Meals & Entertainment",
                        "Meal_Allowance" => "Meal Allowance",
                        "Other_Travel_expenses" => "Other Travel expenses",
                        "Meal_Allowance" => "Meal Allowance",
                        "Miscellaneous" => "Miscellaneous (supplies, internet, etc.)"
                    ]
                ]);

                echo $this->Form->control('billing_place_of_expense');
                echo $this->Form->control('billing_amount', ['onkeydown' => 'getamount()', 'onkeyup' => 'getamount()', 'label' => 'Billing Amount (Before Tax)']);
                //  echo $this->Form->control('billing_tax',['class' =>'float-number']);
                echo $this->Form->control('billing_tax', ['class' => 'billing_tax',
                    'options' => [
                        "GST" => "GST",
                        "PST" => "PST"
                    ]
                ]);
                echo $this->Form->control('billing_tax_amount', ['onkeydown' => 'getamount()', 'onkeyup' => 'getamount()']);
                echo $this->Form->control('billing_total_amount', ['readonly' => true]);
                echo $this->Form->control('company_credit_card', ['options' => ['Yes' => 'Yes', 'No' => 'No'], 'label' => 'Company Credit Card Used?', 'value' => $bill->company_credit_card]);
                // echo $this->Form->control('billing_image');

                //print_r($bill['billing_image']);
                ?>
                <div class="input file">
                    <label for="billing-image">Billing Image</label>
                    <input type="file" name="image" multiple="multiple" id="image" onchange="imagePreview(this)">
                    <input type="hidden" name="files" id="files" value="<?= $bill->billing_image ?>"/>
                    <br/><br/>
                </div>
                <div id="preview-images"></div>
                <input type="hidden" name="billing_status" value="A">
                <input type="hidden" name="billing_userid" value="<?= $user_id ?>">
            </fieldset>
            <?= $this->Form->button(__('Submit')) ?>
            <a href="<?php echo \Cake\Routing\Router::url(['action' => 'index']) ?>" class="save-data">Cancel </a>
            <?= $this->Form->end() ?>
        </div>
    </div>
</div>
