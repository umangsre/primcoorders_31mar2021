<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Family $family
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List Families'), ['action' => 'index']) ?></li>
    </ul>
</nav>
<div class="families form large-9 medium-8 columns content">
    <?= $this->Form->create($family) ?>
    <fieldset>
        <legend><?= __('Add Family') ?></legend>
        <?php
            echo $this->Form->control('family');
            echo $this->Form->control('name');
            echo $this->Form->control('product_code');
            echo $this->Form->control('role_cut');
            echo $this->Form->control('mill_cost');
            echo $this->Form->control('usd_cad');
            echo $this->Form->control('conversion_rate');
            echo $this->Form->control('selling_price');
            echo $this->Form->control('brokerage_multiplier');
            echo $this->Form->control('brokerage');
            echo $this->Form->control('units');
            echo $this->Form->control('freight');
            echo $this->Form->control('landed_cost_per_unit');
            echo $this->Form->control('load_percentage');
            echo $this->Form->control('margin_lic');
            echo $this->Form->control('sell_price_level1');
            echo $this->Form->control('rebate_level1');
            echo $this->Form->control('gross_margin_level1');
            echo $this->Form->control('sell_price_level2');
            echo $this->Form->control('rebate_level2');
            echo $this->Form->control('gross_margin_level2');
            echo $this->Form->control('sell_price_level3');
            echo $this->Form->control('rebate_level3');
            echo $this->Form->control('gross_margin_level3');
            echo $this->Form->control('mill');
            echo $this->Form->control('vendorID');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
