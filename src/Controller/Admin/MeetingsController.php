<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Mailer\MailerAwareTrait;
use Cake\Mailer\Email;

/**
 * Meetings Controller
 *
 * @property \App\Model\Table\MeetingsTable $Meetings
 *
 * @method \App\Model\Entity\Meeting[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class MeetingsController extends AppController
{
     public $paginate = [
        'limit' => 25,
        'order' => [
            'Meetings.id' => 'desc'
        ]
    ];
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $this->viewBuilder()->setLayout('dashboard');
        // $this->paginate = [
        //     'contain' => ['Salesmen']
        // ];
        $query = $this->Meetings->find('all',['contain' => ['Salesmen']]);
        if($this->request->is('post')){
         $search = '%'.$this->request->getData('search').'%';
         $query->where(['OR' =>['Salesmen.name LIKE' => '%'.$search.'%','Meetings.email LIKE' => '%'.$search.'%']]);
       }
        $meetings = $this->paginate($query);

        $this->set(compact('meetings'));
    }

    /**
     * View method
     *
     * @param string|null $id Meeting id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $this->viewBuilder()->setLayout('dashboard');
        $meeting = $this->Meetings->get($id, [
            'contain' => ['Salesmen']
        ]);

        $this->set('meeting', $meeting);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $this->viewBuilder()->setLayout('dashboard');
        $meeting = $this->Meetings->newEntity();
        if ($this->request->is('post')) {
            $meeting = $this->Meetings->patchEntity($meeting, $this->request->getData());
            if ($this->Meetings->save($meeting)) {
                if($salesman_id=$meeting->salesman_id){
                    $saleman = $this->Meetings->Salesmen->get($salesman_id, [
                        'contain' => []
                    ]);

                    $this->sendMail($meeting, $saleman);
                }
                $this->Flash->success(__('The meeting has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The meeting could not be saved. Please, try again.'));
        }
        $salesmen = $this->Meetings->Salesmen->find('list', ['limit' => 200]);
        $this->set(compact('meeting', 'salesmen'));
    }

    public function addcustomer()
    {
        $this->viewBuilder()->setLayout('dashboard');
        $meeting = $this->Meetings->newEntity();
        if ($this->request->is('post')) {
            $meeting = $this->Meetings->patchEntity($meeting, $this->request->getData());
            if ($this->Meetings->save($meeting)) {
                if($salesman_id=$meeting->salesman_id){
                    $saleman = $this->Meetings->Salesmen->get($salesman_id, [
                        'contain' => []
                    ]);
                    
                    $this->sendMail($meeting, $saleman);
                }
                $this->Flash->success(__('The meeting has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The meeting could not be saved. Please, try again.'));
        }
        $salesmen = $this->Meetings->Salesmen->find('list', ['limit' => 200]);
        $this->set(compact('meeting', 'salesmen'));
    }


    public function sendMail($meeting, $salesman){
        
        $toEmail = [
            'm.malhotra@primco.ca',
            $meeting['email'],
            $salesman['rep_email'],
        ];
        $email = new Email();
        $email->setTransport('default');
        $email->viewVars(['meeting' => $meeting ]);
        $email->viewBuilder()->setTemplate('meetings');
        $email->setEmailFormat('html');
        $email->setFrom(['support@primco.ca' => 'Primco'])
        ->setTo($toEmail)
        ->setBcc('Juneja.ankit@gmail.com')
        ->setSubject('Notification: New Meeting')
        ->send();   
      return true;
    }

    /**
     * Edit method
     *
     * @param string|null $id Meeting id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $this->viewBuilder()->setLayout('dashboard');
        $meeting = $this->Meetings->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $meeting = $this->Meetings->patchEntity($meeting, $this->request->getData());
            if ($this->Meetings->save($meeting)) {
                $this->Flash->success(__('The meeting has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
             //dd($meeting->getErrors());
            $this->Flash->error(__('The meeting could not be saved. Please, try again.'));
        }
        $salesmen = $this->Meetings->Salesmen->find('list', ['limit' => 200]);
        $this->set(compact('meeting', 'salesmen'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Meeting id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->viewBuilder()->setLayout('dashboard');
        $this->request->allowMethod(['post', 'delete']);
        $meeting = $this->Meetings->get($id);
        if ($this->Meetings->delete($meeting)) {
            $this->Flash->success(__('The meeting has been deleted.'));
        } else {
            $this->Flash->error(__('The meeting could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
