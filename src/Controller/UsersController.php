<?php

namespace App\Controller;

use App\Controller\AppController;
use Cake\Event\Event;
use Cake\Auth\DefaultPasswordHasher;
use Cake\I18n\Time;

/**
 * Users Controller
 *
 * @property \App\Model\Table\UsersTable $Users
 *
 * @method \App\Model\Entity\User[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class UsersController extends AppController
{

    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);
        $this->Auth->allow(['login', 'forgot', 'reset', 'logout']);
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $this->viewBuilder()->setLayout('dashboard');
        $users = $this->paginate($this->Users);

        $this->set(compact('users'));
    }

    /**
     * View method
     *
     * @param string|null $id User id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $this->viewBuilder()->setLayout('dashboard');
        $user = $this->Users->get($id, [
            'contain' => ['Salesmen', 'Millers', 'Customers']
        ]);

        $this->set('user', $user);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $this->viewBuilder()->setLayout('dashboard');
        $user = $this->Users->newEntity();
        $sdmList = $this->Users
            ->find('list', [
                'keyField' => 'id',
                'valueField' => 'name'
            ])
            ->where(['role' => 'sdm']);
        if ($sdmList) {
            $sdmList = $sdmList->toArray();
        }
        if ($this->request->is('post')) {
            $data = $this->request->getData();
            if ($data['role'] === 'select') {
                $this->Flash->error(__('Select a valid role'));
                return;
            }
            $user = $this->Users->patchEntity($user, $data);
            $userCreated = false;
            if ($this->Users->save($user)) {
                $userType = $data['role'];
                if ($userType == 'sales') {
                    $this->loadModel('Salesmen');
                    $salesman = $this->Salesmen->newEntity();
                    $salesmanInfo = [
                        'user_id' => $user['id'],
                        'name' => $data['name'],
                        'sales_rep' => $data['sales_rep'],
                        'sd_rep' => $data['sd_rep'],
                        'rep_email' => $data['username'],
                        'spec' => $data['spec'],
                        'phone' => $data['sales_phone'],
                        'province' => $data['sales_province'],
                        'created' => new Time()
                    ];
                    $salesman = $this->Salesmen->patchEntity($salesman, $salesmanInfo);
                    if ($this->Salesmen->save($salesman)) {
                        $userCreated = true;
                    }

                    $this->loadModel('SdmSales');
                    $sdmSales = $this->SdmSales->newEntity();
                    $sdmInfo = [
                        'sdm_id' => $data['sdm_id'],
                        'salesmen_id' => $user['id'],
                        'created_at' => new Time()
                    ];
                    $sdmSales = $this->SdmSales->patchEntity($sdmSales, $sdmInfo);
                    if ($this->SdmSales->save($sdmSales)) {
                        $userCreated = true;
                    }
                } else if ($userType == 'dealer') {
                    $this->loadModel('Customers');
                    $dealer = $this->Customers->newEntity();
                    $dealerInfo = [
                        'user_id' => $user['id'],
                        'customerid' => $data['dealer_customerid'],
                        'name' => $data['name'],
                        'contact_name' => $data['dealer_contact_name'],
                        'phone' => $data['dealer_phone'],
                        'address' => $data['dealer_address'],
                        'city' => $data['dealer_city'],
                        'province' => $data['dealer_province'],
                        'postal_code' => $data['dealer_postal_code'],
                        'sin' => $data['dealer_sin'],
                        'created_by' => $this->Auth->user('id'),
                        'created' => new Time()
                    ];
                    $dealer = $this->Customers->patchEntity($dealer, $dealerInfo);
                    if ($this->Customers->save($dealer)) {
                        $userCreated = true;
                    }
                } else if ($userType == 'miller') {
                    $this->loadModel('Millers');
                    $miller = $this->Millers->newEntity();
                    $millerInfo = [
                        'user_id' => $user['id'],
                        'miller_name' => $data['name'],
                        'contact_name' => $data['mill_contact_name'],
                        'email' => $data['mill_contact_name'],
                        'phone' => $data['mill_phone'],
                        'address' => $data['mill_address'],
                        'city' => $data['mill_city'],
                        'postal_code' => $data['mill_postal_code'],
                        'created' => new Time()
                    ];
                    $miller = $this->Millers->patchEntity($miller, $millerInfo);
                    if ($this->Millers->save($miller)) {
                        $userCreated = true;
                    }
                } else if ($userType == 'sdm') {
                    $userCreated = true;
                } else if ($userType == 'admin' || $userType == 'admin-claims') {
                    $userCreated = true;
                }
                if ($userCreated) {
                    $this->Flash->success(__('The user has been saved.'));
                    return $this->redirect(['action' => 'index']);
                }
            }
            $this->Flash->error(__('The user could not be saved. Please, try again.'));
        }
        $this->set(compact('user', 'sdmList'));
    }

    /**
     * Edit method
     *
     * @param string|null $id User id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $this->viewBuilder()->setLayout('dashboard');
        $user = $this->Users->get($id, [
            'contain' => ['Salesmen', 'Millers', 'Customers', 'SdmSales']
        ]);
        $sdmList = $this->Users
            ->find('list', [
                'keyField' => 'id',
                'valueField' => 'name'
            ])
            ->where(['role' => 'sdm']);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $data = $this->request->getData();
            if (!$data['password']) {
                unset($data['password']);
            }
            $oldRole = $user->role;
            $user = $this->Users->patchEntity($user, $data);
            if ($this->Users->save($user)) {
                $userType = $user->role;
                $userUpdated = false;
                if ($oldRole == $userType) {
                    if ($userType == 'sales') {
                        $this->loadModel('Salesmen');
                        $salesman = $this->Salesmen->get($user->salesmen[0]['id']);
                        $salesmanInfo = [
                            'name' => $data['name'],
                            'sales_rep' => $data['sales_rep'],
                            'sd_rep' => $data['sd_rep'],
                            'rep_email' => $data['username'],
                            'spec' => $data['spec'],
                            'phone' => $data['sales_phone'],
                            'province' => $data['sales_province'],
                        ];
                        $salesman = $this->Salesmen->patchEntity($salesman, $salesmanInfo);
                        if ($this->Salesmen->save($salesman)) {
                            $userUpdated = true;
                        }

                        $this->loadModel('SdmSales');
                        $sdmSales = $this->SdmSales->find()->where(['salesmen_id' => $user['id']])->first();
                        $sdmInfo = [
                            'sdm_id' => $data['sdm_id'],
                            'salesmen_id' => $user['id']
                        ];
                        $sdmSales = $this->SdmSales->patchEntity($sdmSales, $sdmInfo);
                        if ($this->SdmSales->save($sdmSales)) {
                            $userCreated = true;
                        }
                    } else if ($userType == 'dealer') {
                        $this->loadModel('Customers');
                        $dealer = $this->Customers->get($user->customers[0]['id']);
                        $dealerInfo = [
                            'user_id' => $user['id'],
                            'customerid' => $data['dealer_customerid'],
                            'name' => $data['name'],
                            'contact_name' => $data['dealer_contact_name'],
                            'phone' => $data['dealer_phone'],
                            'address' => $data['dealer_address'],
                            'city' => $data['dealer_city'],
                            'province' => $data['dealer_province'],
                            'postal_code' => $data['dealer_postal_code'],
                            'sin' => $data['dealer_sin'],
                            'created_by' => $this->Auth->user('id'),
                            'created' => new Time()
                        ];
                        $dealer = $this->Customers->patchEntity($dealer, $dealerInfo);
                        if ($this->Customers->save($dealer)) {
                            $userUpdated = true;
                        }
                    } else if ($userType == 'miller') {
                        $this->loadModel('Millers');
                        $miller = $this->Millers->get($user->millers[0]['id']);
                        $millerInfo = [
                            'user_id' => $user['id'],
                            'miller_name' => $data['name'],
                            'contact_name' => $data['mill_contact_name'],
                            'email' => $data['mill_contact_name'],
                            'phone' => $data['mill_phone'],
                            'address' => $data['mill_address'],
                            'city' => $data['mill_city'],
                            'postal_code' => $data['mill_postal_code'],
                            'created' => new Time()
                        ];
                        $miller = $this->Millers->patchEntity($miller, $millerInfo);
                        if ($this->Millers->save($miller)) {
                            $userUpdated = true;
                        }
                    } else if ($userType == 'sdm') {
                        $userUpdated = true;
                    }
                } else {
                    $user = $user->toArray();
                    $userId = $user['id'];
                    $this->loadModel('Salesmen');
                    $salesman = $this->Salesmen->find()->where(['user_id' => $userId])->first();
                    if ($salesman) {
                        $this->Salesmen->delete($salesman);
                    }

                    $this->loadModel('SdmSales');
                    $sdmSales = $this->SdmSales->find()->where(['salesmen_id' => $userId])->first();
                    if ($sdmSales) {
                        $this->SdmSales->delete($sdmSales);
                    }

                    $this->loadModel('Customers');
                    $customer = $this->Customers->find()->where(['user_id' => $userId])->first();
                    if ($customer) {
                        $this->Customers->delete($customer);
                    }

                    $this->loadModel('Millers');
                    $miller = $this->Millers->find()->where(['user_id' => $userId])->first();
                    if ($miller) {
                        $this->Millers->delete($miller);
                    }

                    if ($userType == 'sales') {
                        $salesman = $this->Salesmen->newEntity();
                        $salesmanInfo = [
                            'user_id' => $user['id'],
                            'name' => $data['name'],
                            'sales_rep' => $data['sales_rep'],
                            'sd_rep' => $data['sd_rep'],
                            'rep_email' => $data['username'],
                            'spec' => $data['spec'],
                            'phone' => $data['sales_phone'],
                            'province' => $data['sales_province'],
                            'created' => new Time()
                        ];
                        $salesman = $this->Salesmen->patchEntity($salesman, $salesmanInfo);
                        if ($this->Salesmen->save($salesman)) {
                            $userUpdated = true;
                        }

                        $sdmSales = $this->SdmSales->newEntity();
                        $sdmInfo = [
                            'sdm_id' => $data['sdm_id'],
                            'salesmen_id' => $user['id'],
                            'created_at' => new Time()
                        ];
                        $sdmSales = $this->SdmSales->patchEntity($sdmSales, $sdmInfo);
                        if ($this->SdmSales->save($sdmSales)) {
                            $userUpdated = true;
                        }
                    } else if ($userType == 'dealer') {

                        $dealer = $this->Customers->newEntity();
                        $dealerInfo = [
                            'user_id' => $user['id'],
                            'customerid' => $data['dealer_customerid'],
                            'name' => $data['name'],
                            'contact_name' => $data['dealer_contact_name'],
                            'phone' => $data['dealer_phone'],
                            'address' => $data['dealer_address'],
                            'city' => $data['dealer_city'],
                            'province' => $data['dealer_province'],
                            'postal_code' => $data['dealer_postal_code'],
                            'sin' => $data['dealer_sin'],
                            'created_by' => $this->Auth->user('id'),
                            'created' => new Time()
                        ];
                        $dealer = $this->Customers->patchEntity($dealer, $dealerInfo);
                        if ($this->Customers->save($dealer)) {
                            $userUpdated = true;
                        }
                    } else if ($userType == 'miller') {
                        $miller = $this->Millers->newEntity();
                        $millerInfo = [
                            'user_id' => $user['id'],
                            'miller_name' => $data['name'],
                            'contact_name' => $data['mill_contact_name'],
                            'email' => $data['mill_contact_name'],
                            'phone' => $data['mill_phone'],
                            'address' => $data['mill_address'],
                            'city' => $data['mill_city'],
                            'postal_code' => $data['mill_postal_code'],
                            'created' => new Time()
                        ];
                        $miller = $this->Millers->patchEntity($miller, $millerInfo);
                        if ($this->Millers->save($miller)) {
                            $userUpdated = true;
                        }
                    } else if ($userType == 'sdm') {
                        $userUpdated = true;
                    } else if ($userType == 'admin' || $userType == 'admin-claims') {
                        $userUpdated = true;
                    }
                }
                if ($userUpdated) {
                    $this->Flash->success(__('The user has been saved.'));
                    return $this->redirect(['action' => 'index']);
                }
            }
            $this->Flash->error(__('The user could not be saved. Please, try again.'));
        }
        $this->set(compact('user', 'sdmList'));
    }

    /**
     * Delete method
     *
     * @param string|null $id User id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->viewBuilder()->setLayout('dashboard');
        $this->request->allowMethod(['post', 'delete']);
        $user = $this->Users->get($id);
        if ($this->Users->delete($user)) {
            $this->Flash->success(__('The user has been deleted.'));
        } else {
            $this->Flash->error(__('The user could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    public function change($password_change_status = "old")
    {
        $passwordHasher = new DefaultPasswordHasher();
        $id = $this->Auth->user('id');
        $user = $this->Users->get($id, [
            'contain' => []
        ]);
        if ($user['role'] == 'sales') {
            $this->viewBuilder()->setLayout('sales_dashboard');
        } else if ($user['role'] == 'admin') {
            $this->viewBuilder()->setLayout('dashboard');
        }
        //password validations
        if ($this->request->is(['patch', 'post', 'put'])) {
            $post_data = $this->request->getData();
            if (isset($post_data['current_password'])) {
                $old_password = $user['password'];
                $current_password = $post_data['current_password'];
                if ($passwordHasher->check($current_password, $old_password)) {
                    return $this->redirect(['action' => 'change', "new"]);
                } else
                    $this->Flash->error(__('The current password you have entered is incorrect. Please, try again.'));
            } else if ($post_data['new_password']) {
                $new_password = $post_data['new_password'];
                $confirm_password = $post_data['confirm_password'];
                if ($new_password == $confirm_password) {
                    $user = $this->Users->patchEntity($user, ['password' => $new_password]);
                    if ($this->Users->save($user)) {
                        $this->Flash->success(__('The password has has been changed.'));
                        return $this->redirect(['action' => 'change']);
                    } else
                        $this->Flash->error(__('The password could not be changed. Please, try again.'));
                } else
                    $this->Flash->error(__('The new passwords do not match. Please, try again.'));
            }
        }
        $this->set(compact('user', 'password_change_status'));
    }

    public function login()
    {
        // $this->layout = 'login';
        $this->viewBuilder()->setLayout('login');

        if ($this->request->is('post')) {
            $user = $this->Auth->identify();
            if ($user) {
                $this->Auth->setUser($user);

                if ($user['role'] == 'sales') {
                    return $this->redirect(['controller' => 'ToolKits', 'action' => 'index', 'prefix' => 'sales']);
                } elseif ($user['role'] == 'sdm') {
                    return $this->redirect(['controller' => 'Bill', 'action' => 'index', 'prefix' => 'sdm']);
                } else {
                    return $this->redirect(['controller' => 'Claims', 'action' => 'index']);
                }
                //return $this->redirect($this->Auth->redirectUrl());
            }
            $this->Flash->error(__('Invalid username or password, try again'));
        }
    }

    public function forgot()
    {
        $this->viewBuilder()->setLayout('login');

        if ($this->request->is('post')) {
            if (!empty($this->request->getData())) {
                $email = $this->request->getData('email');
                $user = $this->Users->findByEmail($email)->first();

                /*if (!empty($user)) {
                    $password = sha1(Text::uuid());

                    $password_token = Security::hash($password, 'sha256', true);

                    $hashval = sha1($user->username . rand(O, 100));

                    $user->password_reset_token = $password_token;
                    $user->hashval = $hashval;

                    $reset_token_link = Router::url(['controller' => 'Users', 'action' => 'reset'], TRUE) . '/' . $password_token . '#' . $hashval;

                    $emailData = [$user->email, $reset_token_link];
                    $this->getMailer('SendEmail')->send('forgotPasswordEmail', [$emailData]);

                    $this->Users->save($user);
                    $this->Flash->success('Please click on password reset link, sent in your email address to reset password.');
                } else
                    $this->Flash->error('Sorry! Email address is not available here.');*/
            } else
                $this->Flash->error(__('Invalid email address, try again'));
        }
    }

    public function reset($token = null)
    {
        $this->viewBuilder()->setLayout('login');

        if (!empty($token)) {

            $user = $this->Users->findByPasswordResetToken($token)->first();

            if ($user) {

                if (!empty($this->request->getData())) {
                    /*$user = $this->Users->patchEntity($user, [
                        'password' => $this->request->getData('new_password'),
                        'new_password' => $this->request->getData('new_password'),
                        'confirm_password' => $this->request->getData('confirm_password')
                    ], ['validate' => 'password']
                    );

                    $hashvalnew = sha1($user->username . rand(O, 100));
                    $user->password_reset_token = $hashvalnew;

                    if ($this->Users->save($user)) {
                        $this->Flash->success('Your password has been changed successfully');
                        $this->redirect(['controller' => 'Users', 'action' => 'login']);
                    } else {
                        $this->Flash->error('Error changing password. Please try again!');
                    }*/
                }
            } else {
                $this->Flash->error('Sorry your password token has been expired.');
            }

            $this->set(compact('user'));
            $this->set('_serialize', ['user']);

        } else {
            $this->Flash->error('Error loading password reset.');
        }
    }

    public function logout()
    {
        return $this->redirect($this->Auth->logout());
    }

    public function createUniquePassword()
    {
        $passwordString = '1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZabcefghijklmnopqrstuvwxyz';
        return substr(str_shuffle($passwordString), 0, rand(8, 20));
    }

}
