<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * ClaimStatus Model
 *
 * @property &\Cake\ORM\Association\BelongsTo $Claims
 *
 * @method \App\Model\Entity\ClaimStatus get($primaryKey, $options = [])
 * @method \App\Model\Entity\ClaimStatus newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\ClaimStatus[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\ClaimStatus|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\ClaimStatus saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\ClaimStatus patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\ClaimStatus[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\ClaimStatus findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class ClaimStatusTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('claim_status');
        $this->setDisplayField('claim_id');
        $this->setPrimaryKey('id');

        $this->belongsTo('Users', [
            'foreignKey' => 'status_by'
        ]);

        $this->belongsTo('Claims', [
            'foreignKey' => 'claim_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        return $validator;
    }
}
