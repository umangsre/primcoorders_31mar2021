<?php
namespace App\Controller\Api;

use App\Controller\AppController;
use Cake\Mailer\MailerAwareTrait;
use Cake\Mailer\Email;
/**
 * Meetings Controller
 *
 * @property \App\Model\Table\MeetingsTable $Meetings
 *
 * @method \App\Model\Entity\Meeting[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class MeetingsController extends AppController
{
    use MailerAwareTrait;
    public function initialize()
    {
        parent::initialize();
        $this->loadComponent('RequestHandler');
        $this->Auth->allow(['addmeeting','editmeeting','getmeeting']);
    }
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Salesmen']
        ];
        $meetings = $this->paginate($this->Meetings);

        $this->set(compact('meetings'));
    }

    /**
     * View method
     *
     * @param string|null $id Meeting id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function getmeeting()
    {
        $this->request->allowMethod(['post', 'put']);
        $id =$this->request->getData('id');
        $meeting = $this->Meetings->get($id, [
            'contain' => ['Salesmen']
        ]);
       $this->set([
                    'status' => 1,
                    'meeting' => $meeting,
                    '_serialize' => ['status', 'meeting']
                ]);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function addmeeting()
    {
        $this->request->allowMethod(['post', 'put']);
        $meeting = $this->Meetings->newEntity();

        if ($this->request->is('post')) {
           
            $salesman_id = $this->request->getData('salesman_id');
            $salesman_arr = explode(': ',$salesman_id);
            $salesman_id =$salesman_arr[0];
            $salesman =$this->request->getData();
            $salesman['salesman_id'] =$salesman_id;
           // $saleman_data = $this->Meetings->Salesmen->get($salesman_id);
           // $salesman['email'] = $saleman_data['rep_email'];
            $meeting = $this->Meetings->patchEntity($meeting,  $salesman);

            if ($this->Meetings->save($meeting)) {
                 $saleman = $this->Meetings->Salesmen->get($salesman_id, [
                    'contain' => []
                ]);

                $this->sendMail($meeting, $saleman);
                
                 $this->set([
                    'status' => 1,
                    'meeting' => $meeting,
                    '_serialize' => ['status', 'meeting']
                ]);
               // $this->getMailer('Meetings')->send('meetingBySalesRep', [$meeting]);
            }else{
               // print_r($meeting->errors());
                 $this->set([
                    'status' => 0,
                    'meeting' => $meeting,
                    '_serialize' => ['status', 'meeting']
                ]);
            }
            
        }
        
        // $salesmen = $this->Meetings->Salesmen->find('list', ['limit' => 200]);
        // $this->set(compact('meeting', 'salesmen'));
    }

    public function sendMail($meeting, $salesman){
        
        $toEmail = [
            'm.malhotra@primco.ca',
            $meeting['email'],
            $salesman['rep_email']
        ];


        $email = new Email();
        $email->setTransport('default');
        $email->viewVars(['meeting' => $meeting ]);
        $email->viewBuilder()->setTemplate('meetings');
        $email->setEmailFormat('html');

        $email->setFrom(['support@primco.ca' => 'Primco'])
        ->setTo($toEmail)
        ->setBcc('Juneja.ankit@gmail.com')
        ->setSubject('Notification: New Meeting')
        ->send();
        
        
      return true;
    }

    /**
     * Edit method
     *
     * @param string|null $id Meeting id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function editmeeting()
    {
        $this->request->allowMethod(['post', 'put']);
        $id =$this->request->getData('id');
        $meeting = $this->Meetings->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $meeting = $this->Meetings->patchEntity($meeting, $this->request->getData());
            if ($this->Meetings->save($meeting)) {
               $this->set([
                    'status' => 1,
                    'meeting' => $meeting,
                    '_serialize' => ['status', 'meeting']
                ]);
            }else{
                $this->set([
                    'status' =>0,
                    'meeting' => $meeting,
                    '_serialize' => ['status', 'meeting']
                ]);
            }
           
        }
        // $salesmen = $this->Meetings->Salesmen->find('list', ['limit' => 200]);
        // $this->set(compact('meeting', 'salesmen'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Meeting id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $meeting = $this->Meetings->get($id);
        if ($this->Meetings->delete($meeting)) {
            $this->Flash->success(__('The meeting has been deleted.'));
        } else {
            $this->Flash->error(__('The meeting could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
