<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;
use Cake\Event\Event;
/**
 * Orders Controller
 *
 * @property \App\Model\Table\OrdersTable $Orders
 *
 * @method \App\Model\Entity\Order[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class OrdersController extends AppController
{
    public $uses = [ 'Order', 'Customer' ];
    public function initialize()
    {
        parent::initialize();
        $this->loadComponent('RequestHandler');
    }

    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);
        $this->Auth->allow(['add', 'addsalesman', 'addorderitem', 'thankyou', 'getcustomers', 'getcustomersdata', 'getsalesmen', 'getsalesmendata', 'getitemname', 'getitemnumber', 'getproduct', 'thankyou']);
    }

    public function pdfView($schedule_id = null){
         
        $this->viewBuilder()->layout('ajax');
        $this->set('title', 'My Great Title');
        $this->set('file_name', '2016-06' . '_June_CLM.pdf');
        $this->response->type('pdf');
 
    }


    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $this->viewBuilder()->setLayout('dashboard');
        $orders = $this->paginate($this->Orders);

        $this->set(compact('orders'));
    }

    /**
     * View method
     *
     * @param string|null $id Order id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $this->viewBuilder()->setLayout('dashboard');
        $order = $this->Orders->get($id, [
            'contain' => []
        ]);

        $this->set('order', $order);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $order = $this->Orders->newEntity();
        if ($this->request->is('post')) {
            
            /*Update date*/
            $this->request->data['ship_date'] = $this->request->data['ship_date']['year'].'-'.$this->request->data['ship_date']['month'].'-'.$this->request->data['ship_date']['day'].' 00:00:00';
            $order = $this->Orders->patchEntity($order, $this->request->getData());

            try {
                $orderdata = $this->Orders->saveOrFail($order);
                //$this->Flash->success(__('The order has been saved.'));

                return $this->redirect(['action' => 'addsalesman', base64_encode($orderdata->id) ]);
            } catch (\Cake\ORM\Exception\PersistenceFailedException $e) {
                 $this->Flash->error(__('The order could not be saved. Please, try again.'));
            }
            
        }
        $this->set(compact('order'));
    }

    public function addsalesman( $id = null )
    {
        if ($id == null ) {
            return $this->redirect(['action' => 'add']);
        }
        $id = base64_decode($id);
        $order = $this->Orders->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $this->request->data['sale_date'] = $this->request->data['sale_date']['year'].'-'.$this->request->data['sale_date']['month'].'-'.$this->request->data['sale_date']['day'].' 00:00:00';
            $order = $this->Orders->patchEntity($order, $this->request->getData());
            
            try {
                $this->Orders->saveOrFail($order);
                //$this->Flash->success(__('The order has been saved.'));

                return $this->redirect(['action' => 'addorderitem', base64_encode($id) ]);
            } catch (\Cake\ORM\Exception\PersistenceFailedException $e) {
                $this->Flash->error(__('The order could not be saved. Please, try again.'));
            }

            
        }
        $this->set(compact('order'));

    }

    public function addorderitem( $id = null )
    {
        if ($id == null ) {
            return $this->redirect(['action' => 'add']);
        }
        $id = base64_decode($id);
        $order = $this->Orders->get($id, [
            'contain' => []
        ]);
        
        $product = TableRegistry::get('Products');

        $all_products = $product->find('all')
        ->select( ['product_name'] )
        ->distinct('product_name');

        if ($this->request->is(['patch', 'post', 'put'])) {
            
            $records = [];
            $order_item_table = TableRegistry::getTableLocator()->get('OrderItems');
            foreach ( $this->request->getData()['product_id'] as $key => $value ) {

                if ( $this->request->getData()['quantity'][$key] <= 0 || $this->request->getData()['product_name'][$key] == "" || $this->request->getData()['item_name'][$key] == "" || $this->request->getData()['product_id'][$key] == "" || $this->request->getData()['total'][$key] <= 0 ) {
                    continue;
                }

                $order_item = $order_item_table->newEntity();
                $order_item->order_id = $id;
                $order_item->product_id = $this->request->getData()['product_id'][$key];
                $order_item->price = $this->request->getData()['price'][$key];
                $order_item->quantity = $this->request->getData()['quantity'][$key];
                $order_item->incentives = $this->request->getData()['incentives'][$key];
                $order_item->total = $this->request->getData()['total'][$key];
                $order_item->promo = $this->request->getData()['promo'][$key];
                
                try {
                    $order_item_table->save($order_item);
                    $records['suc'][] = $this->request->getData()['product_id'][$key];
                } catch (\Cake\ORM\Exception\PersistenceFailedException $e) {
                    $records['fail'][] = $this->request->getData()['desc'][$key].$e->getMessage();
                }
                
            }
            if ( !empty($records) ) {
                $this->Flash->success(__(' Order Saved. Successful Products:'.implode(', ', @$records['suc'] ).' - Failed: '.implode(', ', @$records['fail'] )  ) );
                return $this->redirect(['action' => 'thankyou', base64_encode($id) ]);
            } else {
                $this->Flash->error(__('The order could not be saved. Please, try again.'));
            }
            
            
        }
        $product_records = [];
        if (!empty($all_products->toList())) {
            
            foreach ($all_products->toList() as $key => $value) {
                $product_records[] = [
                    'id' => $value->id,
                    'name' => $value->product_name,
                ];
            }
        }
        
        $this->set(compact('order', 'product_records'));

    }


    public function thankyou( $id = null )
    {
        // $this->autoRender = false;
        if ( $id == null ) {
            return $this->redirect( $this->referer() );
        }

        $id = base64_decode($id);
        $order = $this->Orders->get($id, [
            'contain' => []
        ]);
        

        $order_item = TableRegistry::get('OrderItems');
        $salesmen = TableRegistry::get('Salesmen');
        $customers = TableRegistry::get('Customers');

        $pdf_data = [];
        // print_r($order->toArray());
        if ( !empty( $order->toArray() ) ) {
            //foreach ($order->toArray() as $order_key => $order_value) {
            
            $pdf_data['order'] = [
                'grand_total' => $order->toArray()['grand_total'],
                'po_number' => $order->toArray()['po_number'],
                'ship_to' => $order->toArray()['ship_to'],
                'ship_date' => $order->toArray()['ship_date'],
                'ship_via' => $order->toArray()['ship_via'],
                'additional_comments' => $order->toArray()['additional_comments'],
                'events' => $order->toArray()['events'],
                'sale_order_number' => $order->toArray()['sale_order_number'],
                'incentive_reciept' => $order->toArray()['incentive_reciept'],
                'sale_date' => $order->toArray()['sale_date'],
                'purchaser_name' => $order->toArray()['purchaser_name'],
                
            ];

            /*Salesmen*/
            $salesmen_data = $salesmen->find( 'all' )
            ->where( [ 'id' => $order->toArray()['salesman_id'] ] );
            
            if( !empty( $salesmen_data->toList() ) ) {
                foreach ($salesmen_data->toList() as $sm_key => $sm_value) {
                    $pdf_data['salesman'] = [
                        'name' => $sm_value['name'],
                        'sales_rep' => $sm_value['sales_rep'],
                        'sd_rep' => $sm_value['sd_rep'],
                        'rep_email' => $sm_value['rep_email'],
                        'spec' => $sm_value['spec']
                    ];
                }
                
            }
            
            $customer_data = $customers->find( 'all' )
            ->where( [ 'id' => $order->toArray()['customer_id'] ] );
            
            if( !empty( $customer_data->toList() ) ) {
                foreach ($customer_data->toList() as $c_key => $c_value) {
                    $pdf_data['customer'] = [
                        'name' => $c_value['name'],
                        'address' => $c_value['address'],
                        'city' => $c_value['city'],
                        'province' => $c_value['province'],
                        'postal_code' => $c_value['postal_code'],
                        'sin' => $c_value['sin'],
                    ];
                }
                
            }

            $order_item_data = $order_item->find( 'all' )
            ->where( [ 'order_id' => $order->toArray()['id'] ] );
            
            if( !empty( $order_item_data->toList() ) ) {
                foreach ($order_item_data->toList() as $oi_key => $oi_value) {
                    $pdf_data['order_item'][] = [
                        'price' => $oi_value['price'],
                        'quantity' => $oi_value['quantity'],
                        'incentives' => $oi_value['incentives'],
                        'total' => $oi_value['total'],
                        
                    ];
                }
                
            }

            echo "<pre>";
            print_r($pdf_data);
            echo "</pre>";
            
        }

        
        $this->set(compact('pdf_data'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Order id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $order = $this->Orders->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $order = $this->Orders->patchEntity($order, $this->request->getData());
            if ($this->Orders->save($order)) {
                $this->Flash->success(__('The order has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The order could not be saved. Please, try again.'));
        }
        $this->set(compact('order'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Order id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $order = $this->Orders->get($id);
        if ($this->Orders->delete($order)) {
            $this->Flash->success(__('The order has been deleted.'));
        } else {
            $this->Flash->error(__('The order could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }



    public function getcustomers()
    {
        $this->autoRender = false;
        $customers = TableRegistry::get('Customers');
        $customer_data = $customers->find()
        ->where( [ "name LIKE" => "%".$_POST['name']."%" ] );
        
        $json_records = array();
        foreach ($customer_data->toList() as $key => $value) {
            $json_records[] = [
                'id' => $value->id,
                'customerid' => $value->customerid,
                'name' => $value->name,
                'address' => $value->address,
                'city' => $value->city,
                'province' => $value->province,
                'postal_code' => $value->postal_code,
                'sin' => $value->sin,

            ];
        }
        
        $content = json_encode($json_records);
        $this->response = $this->response->withStringBody($content);
        $this->response = $this->response->withType('json');
        return $this->response;
    }

    public function getcustomersdata()
    {
        $this->autoRender = false;
        $customers = TableRegistry::get('Customers');
        $customer_data = $customers->find('all')
        ->where( [ "id" => $_POST['customer_id'] ] );
        
        $json_records = array();
        foreach ($customer_data->toList() as $key => $value) {
            $json_records[] = [
                'id' => $value->id,
                'customerid' => $value->customerid,
                'name' => $value->name,
                'address' => $value->address,
                'city' => $value->city,
                'province' => $value->province,
                'postal_code' => $value->postal_code,
                'sin' => $value->sin,

            ];
        }
        
        $content = json_encode($json_records);
        $this->response = $this->response->withStringBody($content);
        $this->response = $this->response->withType('json');
        return $this->response;
    }

    public function getsalesmen()
    {
        $this->autoRender = false;
        $salesmen = TableRegistry::get('Salesmen');
        $salesmen_data = $salesmen->find('all')
        ->where( [ "name LIKE" => "%".$_POST['name']."%" ] );
        
        $json_records = array();
        foreach ($salesmen_data->toList() as $key => $value) {
            $json_records[] = [
                'id' => $value->id,
                'name' => $value->name,
            ];
        }
        
        $content = json_encode($json_records);
        $this->response = $this->response->withStringBody($content);
        $this->response = $this->response->withType('json');
        return $this->response;
    }

    public function getsalesmendata()
    {
        $this->autoRender = false;
        $salesmen = TableRegistry::get('Salesmen');
        $salesmen_data = $salesmen->find('all')
        ->where( [ "id" => $_POST['salesman_id'] ] );
        
        $json_records = array();
        foreach ($salesmen_data->toList() as $key => $value) {
            $json_records[] = [
                'id' => $value->id,
                'name' => $value->name,
                'email' => $value->rep_email,
            ];
        }
        
        $content = json_encode($json_records);
        $this->response = $this->response->withStringBody($content);
        $this->response = $this->response->withType('json');
        return $this->response;
    }

    public function getitemname()
    {
        $this->autoRender = false;
        $products = TableRegistry::get('Products');
        $salesmen_data = $products->find('all')
        ->distinct( 'item_name' )
        ->select( ['item_name'] )
        ->where( [ "product_name" => $_POST['product_name'] ] );
        
        $json_records = array();
        foreach ($salesmen_data->toList() as $key => $value) {
            $json_records[] = [
                'id' => $value->id,
                'item_name' => $value->item_name,
            ];
        }
        
        $content = json_encode($json_records);
        $this->response = $this->response->withStringBody($content);
        $this->response = $this->response->withType('json');
        return $this->response;
    }

    public function getitemnumber()
    {
        $this->autoRender = false;
        $products = TableRegistry::get('Products');
        $products_data = $products->find('all')
        ->select( [ 'id', 'item_number'] )
        ->where( [ "product_name" => $_POST['product_name'], 'item_name' => $_POST['item_name'] ] );
        
        $json_records = array();
        foreach ($products_data->toList() as $key => $value) {
            $json_records[] = [
                'id' => $value->id,
                'item_number' => $value->item_number,
            ];
        }
        
        $content = json_encode($json_records);
        $this->response = $this->response->withStringBody($content);
        $this->response = $this->response->withType('json');
        return $this->response;
    }

    public function getproduct()
    {
        $this->autoRender = false;
        $product = TableRegistry::get('Products');
        $product_data = $product->find('all')
        ->where( [ "id" => $_POST['product_id'] ] );
        
        $json_records = array();
        foreach ($product_data->toList() as $key => $value) {
            $json_records[] = [
                'id' => $value->id,
                'product_name' => $value->product_name,
                'item_name' => $value->item_name,
                'item_number' => $value->item_number,
                'item_type' => $value->item_type,
                'color_name' => $value->color_name,
                'single_franklin' => $value->single_franklin,
                'bulk_franklin' => $value->bulk_franklin,
                'per_item' => $value->per_item,
                'single_item' => $value->single_item,
                'bulk_item' => $value->bulk_item,
                'franklin_type' => $value->franklin_type,
                'bulk_count' => $value->bulk_count,

            ];
        }
        
        $content = json_encode($json_records);
        $this->response = $this->response->withStringBody($content);
        $this->response = $this->response->withType('json');
        return $this->response;
    }
}
