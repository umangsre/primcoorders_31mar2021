<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\CreditApplication $creditApplication
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Credit Application'), ['action' => 'edit', $creditApplication->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Credit Application'), ['action' => 'delete', $creditApplication->id], ['confirm' => __('Are you sure you want to delete # {0}?', $creditApplication->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Credit Applications'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Credit Application'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Contacts List'), ['controller' => 'ContactsList', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Contacts List'), ['controller' => 'ContactsList', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Online User Registration'), ['controller' => 'OnlineUserRegistration', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Online User Registration'), ['controller' => 'OnlineUserRegistration', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="creditApplications view large-9 medium-8 columns content">
    <h3><?= h($creditApplication->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Company Legal Name') ?></th>
            <td><?= h($creditApplication->company_legal_name) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Company Trade Name') ?></th>
            <td><?= h($creditApplication->company_trade_name) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Street Address') ?></th>
            <td><?= h($creditApplication->street_address) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Phone') ?></th>
            <td><?= h($creditApplication->phone) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Fax') ?></th>
            <td><?= h($creditApplication->fax) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('City') ?></th>
            <td><?= h($creditApplication->city) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Province') ?></th>
            <td><?= h($creditApplication->province) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Postal Code') ?></th>
            <td><?= h($creditApplication->postal_code) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Email Address') ?></th>
            <td><?= h($creditApplication->email_address) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Owner Name') ?></th>
            <td><?= h($creditApplication->owner_name) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Owner Home Address') ?></th>
            <td><?= h($creditApplication->owner_home_address) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Owner Home Phone') ?></th>
            <td><?= h($creditApplication->owner_home_phone) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Owner Cell Phone') ?></th>
            <td><?= h($creditApplication->owner_cell_phone) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Manager Name') ?></th>
            <td><?= h($creditApplication->manager_name) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Manager Phone') ?></th>
            <td><?= h($creditApplication->manager_phone) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Manager Finacial Address') ?></th>
            <td><?= h($creditApplication->manager_finacial_address) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Manager Bank Account') ?></th>
            <td><?= h($creditApplication->manager_bank_account) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Manager City') ?></th>
            <td><?= h($creditApplication->manager_city) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Manager Province') ?></th>
            <td><?= h($creditApplication->manager_province) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Manager Postal Code') ?></th>
            <td><?= h($creditApplication->manager_postal_code) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Manager Phone 2') ?></th>
            <td><?= h($creditApplication->manager_phone_2) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Manager Gst') ?></th>
            <td><?= h($creditApplication->manager_gst) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Province Sales Tax') ?></th>
            <td><?= h($creditApplication->province_sales_tax) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Amount Credit Required') ?></th>
            <td><?= h($creditApplication->amount_credit_required) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Yearly Sales Volume') ?></th>
            <td><?= h($creditApplication->yearly_sales_volume) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Fiscal Year End') ?></th>
            <td><?= h($creditApplication->fiscal_year_end) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Group Name') ?></th>
            <td><?= h($creditApplication->group_name) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __(' Date') ?></th>
            <td><?= h($creditApplication->_date) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Authorized Signature') ?></th>
            <td><?= h($creditApplication->authorized_signature) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Print Name') ?></th>
            <td><?= h($creditApplication->print_name) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __(' Title') ?></th>
            <td><?= h($creditApplication->_title) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($creditApplication->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Is Group') ?></th>
            <td><?= $this->Number->format($creditApplication->is_group) ?></td>
        </tr>
    </table>
    <div class="related">
        <h4><?= __('Related Contacts List') ?></h4>
        <?php if (!empty($creditApplication->contacts_list)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Credit Application Id') ?></th>
                <th scope="col"><?= __('Statement Name') ?></th>
                <th scope="col"><?= __('Statement Email') ?></th>
                <th scope="col"><?= __('Invoice Name') ?></th>
                <th scope="col"><?= __('Invoice Email') ?></th>
                <th scope="col"><?= __('Price Pages Name') ?></th>
                <th scope="col"><?= __('Price Pages Email') ?></th>
                <th scope="col"><?= __('Order Confirmation Name') ?></th>
                <th scope="col"><?= __('Order Confirmation Email') ?></th>
                <th scope="col"><?= __('Bank Order Name') ?></th>
                <th scope="col"><?= __('Bank Order Email') ?></th>
                <th scope="col"><?= __('Purchasing Agents Name') ?></th>
                <th scope="col"><?= __('Purchasing Agents Email') ?></th>
                <th scope="col"><?= __(' Date') ?></th>
                <th scope="col"><?= __('Authorized Signature') ?></th>
                <th scope="col"><?= __('Print Name') ?></th>
                <th scope="col"><?= __(' Title') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($creditApplication->contacts_list as $contactsList): ?>
            <tr>
                <td><?= h($contactsList->id) ?></td>
                <td><?= h($contactsList->credit_application_id) ?></td>
                <td><?= h($contactsList->statement_name) ?></td>
                <td><?= h($contactsList->statement_email) ?></td>
                <td><?= h($contactsList->invoice_name) ?></td>
                <td><?= h($contactsList->invoice_email) ?></td>
                <td><?= h($contactsList->price_pages_name) ?></td>
                <td><?= h($contactsList->price_pages_email) ?></td>
                <td><?= h($contactsList->order_confirmation_name) ?></td>
                <td><?= h($contactsList->order_confirmation_email) ?></td>
                <td><?= h($contactsList->bank_order_name) ?></td>
                <td><?= h($contactsList->bank_order_email) ?></td>
                <td><?= h($contactsList->purchasing_agents_name) ?></td>
                <td><?= h($contactsList->purchasing_agents_email) ?></td>
                <td><?= h($contactsList->_date) ?></td>
                <td><?= h($contactsList->authorized_signature) ?></td>
                <td><?= h($contactsList->print_name) ?></td>
                <td><?= h($contactsList->_title) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'ContactsList', 'action' => 'view', $contactsList->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'ContactsList', 'action' => 'edit', $contactsList->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'ContactsList', 'action' => 'delete', $contactsList->id], ['confirm' => __('Are you sure you want to delete # {0}?', $contactsList->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Online User Registration') ?></h4>
        <?php if (!empty($creditApplication->online_user_registration)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Credit Application Id') ?></th>
                <th scope="col"><?= __('User Type') ?></th>
                <th scope="col"><?= __('Submission Date') ?></th>
                <th scope="col"><?= __('Customer Number') ?></th>
                <th scope="col"><?= __('Company Name') ?></th>
                <th scope="col"><?= __('Administartor Will See') ?></th>
                <th scope="col"><?= __('Standard Name') ?></th>
                <th scope="col"><?= __('Standard Email') ?></th>
                <th scope="col"><?= __('Standard Name1') ?></th>
                <th scope="col"><?= __('Standard Email1') ?></th>
                <th scope="col"><?= __('Standard Name2') ?></th>
                <th scope="col"><?= __('Standard Email2') ?></th>
                <th scope="col"><?= __('Standard Name3') ?></th>
                <th scope="col"><?= __('Standard Email3') ?></th>
                <th scope="col"><?= __('First  Last Name') ?></th>
                <th scope="col"><?= __('User Email') ?></th>
                <th scope="col"><?= __('Can See Account Details') ?></th>
                <th scope="col"><?= __('Can See Placed Order') ?></th>
                <th scope="col"><?= __('Can See Invoiced Order') ?></th>
                <th scope="col"><?= __('Can See Credit Memos') ?></th>
                <th scope="col"><?= __('Can See Prices') ?></th>
                <th scope="col"><?= __('Can See Price List') ?></th>
                <th scope="col"><?= __('Print Name') ?></th>
                <th scope="col"><?= __('Authorized Signature') ?></th>
                <th scope="col"><?= __(' Date') ?></th>
                <th scope="col"><?= __(' Title') ?></th>
                <th scope="col"><?= __('Anti Spam Consent') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($creditApplication->online_user_registration as $onlineUserRegistration): ?>
            <tr>
                <td><?= h($onlineUserRegistration->id) ?></td>
                <td><?= h($onlineUserRegistration->credit_application_id) ?></td>
                <td><?= h($onlineUserRegistration->user_type) ?></td>
                <td><?= h($onlineUserRegistration->submission_date) ?></td>
                <td><?= h($onlineUserRegistration->customer_number) ?></td>
                <td><?= h($onlineUserRegistration->company_name) ?></td>
                <td><?= h($onlineUserRegistration->administartor_will_see) ?></td>
                <td><?= h($onlineUserRegistration->standard_name) ?></td>
                <td><?= h($onlineUserRegistration->standard_email) ?></td>
                <td><?= h($onlineUserRegistration->standard_name1) ?></td>
                <td><?= h($onlineUserRegistration->standard_email1) ?></td>
                <td><?= h($onlineUserRegistration->standard_name2) ?></td>
                <td><?= h($onlineUserRegistration->standard_email2) ?></td>
                <td><?= h($onlineUserRegistration->standard_name3) ?></td>
                <td><?= h($onlineUserRegistration->standard_email3) ?></td>
                <td><?= h($onlineUserRegistration->first__last_name) ?></td>
                <td><?= h($onlineUserRegistration->user_email) ?></td>
                <td><?= h($onlineUserRegistration->can_see_account_details) ?></td>
                <td><?= h($onlineUserRegistration->can_see_placed_order) ?></td>
                <td><?= h($onlineUserRegistration->can_see_invoiced_order) ?></td>
                <td><?= h($onlineUserRegistration->can_see_credit_memos) ?></td>
                <td><?= h($onlineUserRegistration->can_see_prices) ?></td>
                <td><?= h($onlineUserRegistration->can_see_price_list) ?></td>
                <td><?= h($onlineUserRegistration->print_name) ?></td>
                <td><?= h($onlineUserRegistration->authorized_signature) ?></td>
                <td><?= h($onlineUserRegistration->_date) ?></td>
                <td><?= h($onlineUserRegistration->_title) ?></td>
                <td><?= h($onlineUserRegistration->anti_spam_consent) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'OnlineUserRegistration', 'action' => 'view', $onlineUserRegistration->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'OnlineUserRegistration', 'action' => 'edit', $onlineUserRegistration->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'OnlineUserRegistration', 'action' => 'delete', $onlineUserRegistration->id], ['confirm' => __('Are you sure you want to delete # {0}?', $onlineUserRegistration->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>
