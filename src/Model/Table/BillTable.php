<?php

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Bill Model
 *
 * @method \App\Model\Entity\Bill get($primaryKey, $options = [])
 * @method \App\Model\Entity\Bill newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Bill[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Bill|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Bill saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Bill patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Bill[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Bill findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class BillTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('bill');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Users', [
            'foreignKey' => 'billing_userid'
        ]);

        $this->hasMany('BillComment')
            ->setForeignKey('billid');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->date('billing_date')
            ->allowEmptyDate('billing_date');

        $validator
            ->scalar('billing_type_of_expense')
            ->maxLength('billing_type_of_expense', 255)
            ->allowEmptyString('billing_type_of_expense');

        $validator
            ->scalar('billing_place_of_expense')
            ->maxLength('billing_place_of_expense', 255)
            ->allowEmptyString('billing_place_of_expense');

        $validator
            ->numeric('billing_amount')
            ->allowEmptyString('billing_amount');

        $validator
            ->scalar('billing_tax')
            ->maxLength('billing_tax', 255)
            ->allowEmptyString('billing_tax');

        $validator
            ->numeric('billing_tax_amount')
            ->allowEmptyString('billing_tax_amount');

        $validator
            ->numeric('tip_amount')
            ->allowEmptyString('tip_amount');

        $validator
            ->numeric('billing_total_amount')
            ->allowEmptyString('billing_total_amount');

        $validator
            ->scalar('billing_image')
            ->allowEmptyFile('billing_image');

        $validator
            ->scalar('billing_status')
            ->notEmptyString('billing_status');

        $validator
            ->integer('billing_userid')
            ->requirePresence('billing_userid', 'create')
            ->notEmptyString('billing_userid');

        $validator
            ->scalar('billing_comment')
            ->allowEmptyString('billing_comment');

        return $validator;
    }
}
