<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Bill[]|\Cake\Collection\CollectionInterface $bill
 */
?>
<!--<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Bill'), ['action' => 'add']) ?></li>
    </ul>
</nav>-->
<link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

<style>
    .input.text {
        width: 20%;
    }
</style>

<div class="bill index large-9 medium-8 columns content card card card-default scrollspy">
    <div class="card-content">
        <?= $this->Flash->render() ?>
        <div class="families index large-9 medium-8 columns content">
            <?= $this->Form->create('search', ['type' => 'get']) ?>
            <fieldset>
                <div class="input text"><input type="text" name="start" id="start" value="<?= @$_GET['start'] ?>"
                                               autocomplete="off" ; placeholder="Start Date"></div>
                <div class="input text"><input type="text" name="end" id="end" value="<?= @$_GET['end'] ?>"
                                               autocomplete="off" placeholder="End Date"></div>
                <div class="input text"><input type="text" name="search" id="search" value="<?= @$_GET['search'] ?>"
                                               autocomplete="off" placeholder="Enter User"></div>

                <div class="input text">
                    <select name="status" id="status">
                        <option value="">Select Option</option>
                        <option value="A" <?= ((@$_GET['status'] == 'A') ? 'Selected' : '') ?>>Approve</option>
                        <option value="P" <?= ((@$_GET['status'] == 'P') ? 'Selected' : '') ?>>Pending</option>
                        <option value="R" <?= ((@$_GET['status'] == 'R') ? 'Selected' : '') ?>>Open</option>

                    </select>

                </div>

                <?php

                echo $this->Form->button(__('Search'));
                ?>
            </fieldset>

            <?= $this->Form->end() ?>
            <br/>
            <div class="import export right-align">
                <?= $this->Html->link(__('Download All Images'), ['action' => 'create-bill-images-zip', '?' => array(
                    'from' => @$_GET['start'],
                    'to' => @$_GET['end'],
                    'salesman' => @$_GET['search'],
                    'status' => @$_GET['status'])],
                    array(
                        'id' => 'myImages',
                        'class' => 'waves-effect waves-light btn'
                    )); ?>
                <?= $this->Html->link(__('Excel'), ['action' => 'index', '_ext' => 'xlsx', '?' => array(
                    'start' => @$_GET['start'],
                    'end' => @$_GET['end'],
                    'search' => @$_GET['search'],
                    'status' => @$_GET['status'])],
                    array(
                        'id' => 'myId',
                        'class' => 'waves-effect waves-light btn'
                    )); ?>
            </div>
            <!--<h3><?= __('Bill') ?></h3>-->
            <table cellpadding="0" cellspacing="0">
                <thead>
                <tr>
                    <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                    <th scope="col"><?= $this->Paginator->sort('Users.name', 'Bill Owner') ?></th>
                    <th scope="col"><?= $this->Paginator->sort('billing_amount', 'Amount ( Without Tax )') ?></th>
                    <th scope="col"><?= $this->Paginator->sort('billing_date') ?></th>
                    <th scope="col"><?= $this->Paginator->sort('billing_type_of_expense') ?></th>
                    <th scope="col"><?= $this->Paginator->sort('billing_place_of_expense') ?></th>

                    <th scope="col"><?= $this->Paginator->sort('billing_tax') ?></th>
                    <th scope="col"><?= $this->Paginator->sort('billing_tax_amount') ?></th>
                    <th scope="col"><?= $this->Paginator->sort('billing_total_amount') ?></th>
                    <th scope="col"><?= $this->Paginator->sort('billing_status') ?></th>


                    <!--<th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Expense Amount') ?></th>
                <th scope="col"><?= __('Date') ?></th>
                <th scope="col"><?= __('Type of Expense') ?></th>
                <th scope="col"><?= __('Place of Expense') ?></th>
                <th scope="col"><?= __('Tax') ?></th>
                <th scope="col"><?= __('Total Amount') ?></th>
                <th scope="col"><?= __('Status') ?></th>-->

                    <th scope="col" class="actions"><?= __('Actions') ?></th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($bill as $bill): ?>
                    <tr>
                        <td><?= $this->Number->format($bill->id) ?></td>
                        <td>
                            <?php
                            if ($bill->user) {
                                echo $bill->user->name;
                            }
                            ?>
                        </td>
                        <td><?= $this->Number->format($bill->billing_amount) ?></td>
                        <td><?= h($bill->billing_date) ?></td>
                        <td><?php $typeofexpense = h($bill->billing_type_of_expense);
                            $typeofexpense = str_replace("_", " ", $typeofexpense);
                            $typeofexpense = str_replace("|", "/", $typeofexpense);
                            echo $typeofexpense;


                            ?></td>
                        <td><?= h($bill->billing_place_of_expense) ?></td>

                        <td><?= h($bill->billing_tax) ?></td>
                        <td><?= h($bill->billing_tax_amount) ?></td>
                        <td><?= $this->Number->format($bill->billing_total_amount) ?></td>
                        <td><?php if ($bill->billing_status == 'P') {
                                echo 'Pending';
                            } elseif ($bill->billing_status == 'A') {
                                echo 'Approved by SDM';
                            } elseif ($bill->billing_status == 'S') {
                                echo 'Save';
                            } elseif ($bill->billing_status == 'R') {
                                echo 'Reject by SDM';
                            } elseif ($bill->billing_status == 'C') {
                                echo 'Closed';
                            } elseif ($bill->billing_status == 'AA') {
                                echo 'Approved by Admin';
                            } elseif ($bill->billing_status == 'RA') {
                                echo 'Reject by Admin';
                            }elseif ($bill->billing_status == 'AWS') {
                                echo 'Submitted for approval';
                            } ?></td>
                        <!--<td><?= h($bill->created) ?></td>-->
                        <td class="actions">

                            <?= $this->Html->link('<i class="material-icons dp48">view_list</i>', ['action' => 'view', $bill->id], ['escape' => false, 'title' => __('View')]) ?>
                            <?php /*  $this->Html->link(__('Edit'), ['action' => 'edit', $bill->id]) */ ?>


                            <?= $this->Form->postLink('<i class="material-icons dp48">delete</i>', ['action' => 'delete', $bill->id], ['escape' => false, 'title' => __('Delete'), 'confirm' => __('Are you sure you want to delete # {0}?', $bill->id)]) ?>


                            <a class="modal-trigger update-trigger" href="#modal1" data-toggle="tooltip"
                               title="Reject Bill" data-id="<?= $bill->id ?>"><i
                                    class="material-icons dp48">highlight_off</i></a>
                            <a class="done-trigger" href="#done" data-toggle="tooltip" title="Approve Bill"
                               data-id="<?= $bill->id ?>"><i
                                    class="material-icons dp48">done</i></a>

                            <?php if ($bill->billing_status == 'P') { ?>
                                <!--<a class="modal-trigger update-trigger" href="#modal1" data-toggle="tooltip"
                                   title="Reject Bill" data-id="<? /*= $bill->id */ ?>"><i
                                        class="material-icons dp48">highlight_off</i></a>
                                <a class="done-trigger" href="#done" data-toggle="tooltip" title="Approve Bill"
                                   data-id="<? /*= $bill->id */ ?>"><i
                                        class="material-icons dp48">done</i></a>-->
                            <?php } else if ($bill->billing_status != 'C') {
                                ?>
                                <a class="close-trigger" href="#close" data-toggle="tooltip" title="Close Bill"
                                   data-id="<?= $bill->id ?>"><i
                                        class="material-icons dp48">close</i></a>
                                <?php
                            } ?>
                        </td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
            <div class="paginator">
                <ul class="pagination">
                    <?= $this->Paginator->first('<< ' . __('first')) ?>
                    <?= $this->Paginator->prev('< ' . __('previous')) ?>
                    <?= $this->Paginator->numbers() ?>
                    <?= $this->Paginator->next(__('next') . ' >') ?>
                    <?= $this->Paginator->last(__('last') . ' >>') ?>
                </ul>
                <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
            </div>
        </div>
    </div>

    <!-- Modal Structure -->
    <!-- Rejected bill from  -->
    <div id="modal1" class="modal">
        <div class="modal-content">
            <?= $this->Form->create($bill, array(
                'id' => 'mybill'
            )) ?>
            <div class="input textarea">
                <label for="billing-comment" class="">Billing Comment</label>
                <textarea name="billing_comment" id="billing-comment" rows="5" required></textarea>
            </div>
            <input type="hidden" name="billing_status" value="D">

            <?= $this->Form->button(__('Submit')) ?>
            <?= $this->Form->end() ?>
        </div>
        <div class="modal-footer">
            <a href="#!" class="modal-close waves-effect waves-green btn-flat">Close</a>
        </div>
    </div>

    <!--Approve bill form  -->
    <?= $this->Form->create($bill, array(
        //'url' => array('action' => 'edit'),
        'id' => 'mybillaccept'
    )) ?>
    <input type="hidden" name="billing_status" value="AA">
    <?= $this->Form->end() ?>

    <?= $this->Form->create($bill, array(
        //'url' => array('action' => 'edit1'),
        'id' => 'billclose'
    )) ?>
    <input type="hidden" name="billing_status" value="C">
    <?= $this->Form->end() ?>
