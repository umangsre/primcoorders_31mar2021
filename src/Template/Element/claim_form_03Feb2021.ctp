<?php   
// Set some content to print
//'authorized_signature_'+ order_id +'.jpg'
//$file_name = 'authorized_signature_'. $creditApplication->id .'.jpg';
//$_signature = BASE_URL.'webroot/img/Signs/'.$file_name;
$logo = BASE_URL.'webroot/img/primco-logo.png';

?>

<style type="text/css">
	p,td,h3{ font-size: 11px; }
	p{
		line-height: 12px;
		padding: 0px;
		margin: 0px !important;
	}
	h3{ text-align: center; }	
	.text-center{
		text-align: center;
	}
</style>
<table>
	<!-- Page 1 Header -->
	<tr>
		<td>
			<table>
				<tr>
					<td>
						<h1 style="font-size:25px; text-align: right;">RETAILER CLAIM FOR
						</h1>	
					</td>
					<td style="text-align: right;">
						<img width="250" src="<?= $logo?>">
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td>
			<h3 class="text-center mb-2">RETAILER INFORMATION <br></h3>
		</td>
	</tr>
	<!-- Page 1 Section 1 -->
	<tr>
		<td>
			<table style="border:1;" cellpadding="5">
				<tr>
					<td colspan="2" >Retailer Name 
						<strong><?= $claim->has('customer')? h($claim->customer->name):'' ?> </strong>
					</td>
					<td colspan="2">Consumer Name
						<strong><?= $claim->has('consumer') ?h($claim->consumer->consumer_name):'' ?></strong>
					</td>
				</tr>
				<tr>
					<td colspan="2">Contact Name 
						<strong> <?= $claim->has('consumer') ?h($claim->consumer->consumer_name):'' ?></strong>
					</td>
					<td colspan="2"> Consumer address
					<strong> <?= $claim->has('consumer') ?h($claim->consumer->consumer_address):'' ?></strong>
				    </td>
				</tr>
				<tr>    
					<td colspan="2">Consumer phone #
						<strong> <?= $claim->has('consumer') ?h($claim->consumer->consumer_phone):'' ?></strong>
					</td>
					<td colspan="2">City <strong><?= $claim->has('consumer') ?h($claim->consumer->city):'' ?></strong>
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<!-- Page 1 Section 2 -->
	<tr>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td>
			<h3 class="text-center mb-2">CLAIM INFORMATION <br></h3>
		</td>
	</tr>
	<tr>
		<td>
			<table style="boder:1;" cellpadding="5">
				<tr>
					<td colspan="2">Primco Invoice#  <strong><?= h($claim->primco_invoice) ?></strong></td>
					<td colspan="2">Qty purchased <strong><?= h($claim->quantity_purchased) ?></strong></td>
				</tr>
				<tr>
					<td colspan="2">Date Installed <strong>
						<?= h($claim->date_installed) ?></strong>
					</td>
					<td colspan="2">Areas installed <strong>
						<?= $claim->areas_installed ?></strong>
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<!-- Page 1 Section 3 -->
	<tr>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td>
			<h3 class="text-center mb-2">PRODUCT INFORMATION<br></h3>
		</td>
	</tr>
	<tr>
		<td>
			<table border="1" cellpadding="5">
				<tr>
					<td>Product number </td>
					<td>Quantity affected</td>
				</tr>
				 <?php foreach ($claim->claim_products as $claimProducts): ?>
				<tr>
					<td><strong><?= $claimProducts->product_number ?></strong></td>
					<td><strong><?= $claimProducts->claim_quantity ?></strong></td>
				</tr>
				 <?php endforeach; ?>
				<tr>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
				</tr>
				<tr>
				<td>&nbsp;</td>
					<td>&nbsp;</td>	
				</tr>
				<tr>
					<td colspan="2"> 
						Detailed Reason for Claim: <strong>
						<?= h($claim->detailed_claim_reason) ?></strong>
					</td>
				</tr>
				<tr>	
					<td colspan="2">
						Installed (YES or NO) : <strong>
						<?= h($claim->installed) ?></strong>
					</td>
				</tr>
				<tr>	
					<td colspan="2">
						Request for resolution : <strong>
						<?= h($claim->request_for_resolution) ?></strong>
					</td>
				</tr>
				<tr>
					<td colspan="2"> 
						ATTACH PICTURE (yes or no): <strong>
						<?= h($claim->photo_attached) ?></strong>
					</td>
				</tr>
				<tr>
					<td colspan="2"> 
						SAMPLES: <strong>
						<?= h($claim->samples_available) ?></strong>
					</td>
				</tr>
				<tr>
					<td colspan="2">
						<h3>IMPORTANT NOTES
						</h3>
					</td>
				</tr>
				<tr>
					<td colspan="4">
						<p>Unless a claim is approved or accepted by the responsible mill, you the retailer cannot hold back funds or deduct funds owed to Primco for outstanding invoices.
						</p>
						<p>If the retailer chooses to remove and replace the claimed material without an approved claim or a claim accepted by the responsible mill, this will be at the cost of the retailer and the claim may not be honored by the mill.
						</p>
						<p>What you will need to provide after claims form is complete:
					<br>2-3 full uninstalled samples from the order
					<br>Full room pictures of the area(s) where the issue took place
					<br>Pictures of the issue
					<br>Copy(s) of invoice(s) from Primco specific to the job and your invoice to the customer
						</p>
						<p>
							Upon return and inspection of uninstalled material, freight cost will be reimbursed to account via credit note if and when material is deemed defective.
						</p>
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<!-- Page 1 Section 4 -->
	<tr>
		<td>&nbsp;</td>
	</tr>
</table>
