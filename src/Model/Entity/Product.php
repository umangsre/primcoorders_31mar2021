<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Product Entity
 *
 * @property int $id
 * @property string $product_name
 * @property string $item_name
 * @property string $item_number
 * @property string $color_name
 * @property float $single_item
 * @property float $bulk_item
 * @property float $single_franklin
 * @property float $bulk_franklin
 * @property float $per_item
 * @property int $bulk_count
 * @property string $item_type
 * @property string $franklin_type
 * @property \Cake\I18n\FrozenTime $created_at
 * @property \Cake\I18n\FrozenTime|null $updated_on
 */
class Product extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'product_name' => true,
        'item_name' => true,
        'item_number' => true,
        'color_name' => true,
        'single_item' => true,
        'bulk_item' => true,
        'single_franklin' => true,
        'bulk_franklin' => true,
        'per_item' => true,
        'bulk_count' => true,
        'item_type' => true,
        'franklin_type' => true,
        'created_at' => true,
        'updated_on' => true
    ];
}
