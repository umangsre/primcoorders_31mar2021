<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Claim[]|\Cake\Collection\CollectionInterface $claims
 */
?>
<!--nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Claim'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Customers'), ['controller' => 'Customers', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Customer'), ['controller' => 'Customers', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Consumers'), ['controller' => 'Consumers', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Consumer'), ['controller' => 'Consumers', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Claim Products'), ['controller' => 'ClaimProducts', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Claim Product'), ['controller' => 'ClaimProducts', 'action' => 'add']) ?></li>
    </ul>
</nav  -->
<style>
    .input.text {
        width: 20%;
    }
    .message.success {
        margin-top: 1rem;
        background: #0d4219;
        text-align: center;
        color: #fff;
        -webkit-transition: -webkit-box-shadow .25s;
        -moz-transition: box-shadow .25s;
        -o-transition: box-shadow .25s;
        transition: -webkit-box-shadow .25s;
        transition: box-shadow .25s;
        transition: box-shadow .25s, -webkit-box-shadow .25s;
        border-radius: 2px;
    }
</style>
<div class="orders index large-9 medium-8 columns content card card card-default scrollspy">
    <div class="card-content">
        <div class="claims index large-9 medium-8 columns content">
            <?php if ($loggedInUserInfo['role'] == 'admin') { ?>
            <?= $this->Form->create('search', ['type' => 'put']) ?>
            <fieldset>
                <div class="input text"><input type="text" name="start" id="start" value="<?= @$searchData['start'] ?>"
                                               autocomplete="off" placeholder="Start Date"></div>
                <div class="input text"><input type="text" name="end" id="end" value="<?= @$searchData['end'] ?>"
                                               autocomplete="off" placeholder="End Date"></div>
                <div class="input text"><input type="text" name="name" id="name" value="<?= @$searchData['name'] ?>"
                                               autocomplete="off" placeholder="Enter Dealer"></div>
                <!--<div class="input text">
                    <select name="status" id="status">
                        <option value="">Select Option</option>
                        <option value="A" <?/*= ((@$searchData['status'] == 'A') ? 'Selected' : '') */?>>Approve</option>
                        <option value="P" <?/*= ((@$searchData['status'] == 'P') ? 'Selected' : '') */?>>Pending</option>
                        <option value="R" <?/*= ((@$searchData['status'] == 'R') ? 'Selected' : '') */?>>Open</option>
                    </select>
                </div>-->
                <?php
                echo $this->Form->button(__('Search'));
                ?>
            </fieldset>
            <?= $this->Form->end() ?>
            <?php } ?>
            <!--<div class="import export right-align">
                <a class="waves-effect waves-light btn" href="/bill/create-bill-images-zip" target="_blank">Download All
                    Images</a>
                <?/*= $this->Html->link(__('Excel'), ['action' => 'index', '_ext' => 'xlsx', '?' => array(
                    'start' => @$searchData['start'],
                    'end' => @$searchData['end'],
                    'search' => @$searchData['search'],
                    'status' => @$searchData['status'])],
                    array(
                        'id' => 'myId',
                        'class' => 'waves-effect waves-light btn'
                    )); */?>
            </div>-->
            <table cellpadding="0" cellspacing="0">
                <thead>
                <tr>
                    <th scope="col"><?= $this->Paginator->sort('id/Claim No.') ?></th>
                    <?php if($currentUser['role'] != 'dealer'){?>
                    <th scope="col"><?= $this->Paginator->sort('customer_name') ?></th>
                    <?php } ?>
                    <?php if($currentUser['role'] != 'admin'){?>
                    <th scope="col"><?= $this->Paginator->sort('consumer_name') ?></th>
                    <?php } ?>
                    <!-- <th scope="col"><?= $this->Paginator->sort('total_claim_products') ?></th> -->
                    <th scope="col"><?= $this->Paginator->sort('status') ?></th>
                    <th scope="col"><?= $this->Paginator->sort('created_date') ?></th -->
                    <th scope="col" class="actions"><?= __('Actions') ?></th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($claims as $claim): ?>
                    <tr>
                        <td><?= $this->Number->format($claim->id) ?></td>
                        <?php if($currentUser['role'] != 'dealer'){?>
                        <td><?= h($claim->customer->name) ?></td>
                        <?php } ?>
                        <?php if($currentUser['role'] != 'admin'){?>
                        <td><?= h($claim->consumer->consumer_name) ?></td>
                        <?php } ?>
                        <!-- <td><?= is_array($claim->claim_json) ? h(count($claim->claim_json)) : 0 ?></td> -->
                        <?php
                        $claimStatuses = $claim->claim_status;
                        $totalStatuses = count($claimStatuses);
                        //debug($claimStatuses);
                        $status = 'Pending';
                        if ($totalStatuses > 0) {
                            $status = $claimStatuses[$totalStatuses - 1]['status'];
                            if($currentUser['role'] == 'dealer' && ($status == 'Declined by Mill' || $status == 'Approved by Mill' || $status = 'Send to Primco')){
                                $status = 'Pending on Admin';
                            }
                            if($currentUser['role'] == 'admin' && $status == 'send to mill'){
                                $status = 'Sent To Mill';
                            }
                            if($currentUser['role'] == 'admin' && $status == 'send to primco'){
                                $status = 'Pending on Admin';
                            }
                        }
                        ?>
                        <td><?= h(ucfirst($status)) ?></td>
                        <td><?= h($claim->created_date) ?></td>
                        <td class="actions">
                            <?= $this->Html->link(__('View'), ['action' => 'view', $claim->id]) ?>
                            <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $claim->id], ['confirm' => __('Are you sure you want to delete # {0}?', $claim->id)]) ?>
                            <?= $this->Html->link(__('Email'), ['action' => 'send_emails', $claim->id]) ?> 
                            
                        </td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
            <div class="paginator">
                <ul class="pagination">
                    <?= $this->Paginator->first('<< ' . __('first')) ?>
                    <?= $this->Paginator->prev('< ' . __('previous')) ?>
                    <?= $this->Paginator->numbers() ?>
                    <?= $this->Paginator->next(__('next') . ' >') ?>
                    <?= $this->Paginator->last(__('last') . ' >>') ?>
                </ul>
                <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
            </div>
        </div>
    </div>
</div>
