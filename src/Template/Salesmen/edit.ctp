<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Salesman $salesman
 */
?>
<div class="orders index large-9 medium-8 columns content card card card-default scrollspy">
    <div class="card-content">
        <div class="salesmen form large-9 medium-8 columns content">
            <?= $this->Form->create($salesman) ?>
            <fieldset>
                <legend><?= __('Edit Salesman') ?></legend>
                <?php
                    echo $this->Form->control('name');
                    echo $this->Form->control('rep_email');
                    echo $this->Form->control('phone');
                    echo $this->Form->control('province');
                    echo $this->Form->control('sales_rep');
                    echo $this->Form->control('sd_rep');
                    echo $this->Form->control('spec');
                ?>
            </fieldset>
            <?= $this->Form->button(__('Submit'), ['class' => 'btn waves-effect waves-light border-round gradient-45deg-purple-deep-orange col s12 mt-4']) ?>
            <?= $this->Form->end() ?>
        </div>
    </div>
</div>