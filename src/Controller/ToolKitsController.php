<?php

namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;
use Cake\Mailer\MailerAwareTrait;
use Cake\Mailer\Email;

/**
 * Bill Controller
 *
 *
 * @method \App\Model\Entity\Bill[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class ToolKitsController extends AppController {
	
	public function initialize() {
		parent::initialize();
	    $this->loadComponent('ToolKitHandler');
    }
		
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $this->viewBuilder()->setLayout('dashboard');
        $user_id = $this->Auth->user('id');
		$user_name = $this->Auth->user('name');
		$user_email = $this->Auth->user('username');
        $user_role = $this->Auth->user('role');
        
		if($this->getRequest()->is('post')){
			$result = $this->ToolKitHandler->processPostRequest($this->getRequest(), $user_email);
			return $this->redirect(['action' => 'index']);
			
		}else{
			
			$result = $this->ToolKitHandler->handleGetRequest($user_id);
			$toolkitsCategories = $result['toolkitsCategories'];
			$salesRep = $result['salesRep'];
	        $this->set(compact('toolkitsCategories', 'salesRep', 'user_name'));
		}
		
    }
	
}