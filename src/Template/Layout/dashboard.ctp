<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 */

$cakeDescription = 'CakePHP: the rapid development php framework';
use Cake\Routing\Router;
?>
<!DOCTYPE html>
<html>
<head>
    <?= $this->Html->charset() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>
        Primco
    </title>
    <?= $this->Html->meta('icon') ?>

    <?= $this->Html->css('theme/vendors.min.css') ?>
    <?= $this->Html->css('theme/materialize.css') ?>
    <?= $this->Html->css('theme/style.css') ?>
    <?= $this->Html->css('datatables.css') ?>
    <?= $this->Html->css('jquery.datetimepicker.min.css') ?>
    <?= $this->Html->css('https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/css/all.min.css') ?>
    <?= $this->Html->css('theme/dashboard.css') ?>
    <?= $this->Html->css('theme/custom.css') ?>



    <?= $this->fetch('meta') ?>
    <?= $this->fetch('css') ?>
    <?= $this->fetch('script') ?>

    <script src="https://cdn.jsdelivr.net/npm/signature_pad@2.3.2/dist/signature_pad.min.js"></script>
    <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <script type="text/javascript">
        var csrfCustomerToken = <?= json_encode($this->request->getParam('_csrfToken')) ?>;
        var customer_ajax_url = '<?= $this->Url->build([ 'controller' => 'Orders', 'action' => 'getcustomers' ]) ?>';
        var customer_data_ajax_url = '<?= $this->Url->build([ 'controller' => 'Orders', 'action' => 'getcustomersdata' ]) ?>';
        var salesmen_ajax_url = '<?= $this->Url->build([ 'controller' => 'Orders', 'action' => 'getsalesmen' ]) ?>';
        var salesmen_data_ajax_url = '<?= $this->Url->build([ 'controller' => 'Orders', 'action' => 'getsalesmendata' ]) ?>';
        var item_name_ajax_url = '<?= $this->Url->build([ 'controller' => 'Orders', 'action' => 'getitemname' ]) ?>';
        var item_number_ajax_url = '<?= $this->Url->build([ 'controller' => 'Orders', 'action' => 'getitemnumber' ]) ?>';
        var product_ajax_url = '<?= $this->Url->build([ 'controller' => 'Orders', 'action' => 'getproduct' ]) ?>';
        var all_customers_ajax_url = '<?= $this->Url->build([ 'controller' => 'Orders', 'action' => 'getallcustomers' ]) ?>';
        var all_salesmen_ajax_url = '<?= $this->Url->build([ 'controller' => 'Orders', 'action' => 'getallsalesmen' ]) ?>';
        var all_products_ajax_url = '<?= $this->Url->build([ 'controller' => 'Orders', 'action' => 'getallproducts' ]) ?>';
        var all_conversion_rate_ajax_url = '<?= Router::url([ 'controller' => 'CurrencyConversionRates', 'action' => 'getConverionRate', 'prefix' => false ]) ?>';
        var sign_ajax_url = '<?= $this->Url->build([ 'controller' => 'Orders', 'action' => 'savesignature' ]) ?>';
        var BASE_URL = ' <?= BASE_URL ?>';
    </script>
</head>
<body class="vertical-layout page-header-light vertical-menu-collapsible vertical-dark-menu 2-columns  " data-open="click" data-menu="vertical-dark-menu" data-col="2-columns">

    <?= $this->element('header'); ?>
    <?= $this->element('nav'); ?>

    <div id="main">
        <div class="row">
            <div id="breadcrumbs-wrapper" data-image="<?= $this->Url->image('breadcrumb-bg.jpg') ?>" class="breadcrumbs-bg-image" style="background-image: url(<?= $this->Url->image('breadcrumb-bg.jpg') ?>);">

                  <!-- Search for small screen-->
                  <div class="container">
                    <div class="row">
                      <div class="col s12 m6 l6">
                        <h5 class="breadcrumbs-title mt-0 mb-0"><?=(($this->request->params['controller'] == 'Bill')?"Expense Allowance":$this->request->params['controller']) ?></h5>
                      </div>
                      <div class="col s12 m6 l6 right-align-md">
                          <h5 class="breadcrumbs-title mt-0 mb-0"><?= $loggedInUserInfo['name']; ?></h5>
                        <!--ol class="breadcrumbs mb-0">
                          <li class="breadcrumb-item"><a href="index.html">Home</a>
                          </li>
                          <li class="breadcrumb-item"><a href="#">User</a>
                          </li>
                          <li class="breadcrumb-item active">User Profile Page
                          </li>
                        </ol-->
                      </div>
                    </div>
                  </div>
                </div>

        </div>
        <div class="row">
            <div class="col s12">
                <div class="container">
                    <?= $this->Flash->render() ?>
                    <?= $this->fetch('content') ?>
                </div>
            </div>
        </div>
    </div>

    <?= $this->element('footer'); ?>
    <?= $this->Html->script('jquery.min.js') ?>
    <?= $this->Html->script('bootstrap.min.js') ?>
    <?= $this->Html->script('theme/vendors.min.js') ?>
    <?= $this->Html->script('theme/plugins.js') ?>
    <?= $this->Html->script('datatables.min.js') ?>
    <?= $this->Html->script('jquery-ui.min.js') ?>
    <?= $this->Html->script('jquery.datetimepicker.full.min.js') ?>
    <?= $this->Html->script('theme/custom-script.js') ?>
    <?= $this->Html->script('form.js?token='.date('YmdHis')) ?>
    <?= $this->Html->script('claims.js') ?>
    <?= $this->Html->script('jquery.zoom.min.js') ?>
    <?= $this->Html->script('files.js') ?>

    <script>
        $( function() {
            var dateFormat = "yy-mm-dd",
                from = $( "#start" )
                    .datepicker({
                        defaultDate: "+1w",
                        dateFormat: 'yy-mm-dd',
                        numberOfMonths: 1,
                    })
                    .on( "change", function() {
                        to.datepicker( "option", "minDate", getDate( this ) );
                    }),
                to = $( "#end" ).datepicker({
                    defaultDate: "+1w",
                    dateFormat: 'yy-mm-dd',
                    numberOfMonths: 1
                })
                    .on( "change", function() {
                        from.datepicker( "option", "maxDate", getDate( this ) );
                    });

            function getDate( element ) {
                var date;
                try {
                    date = $.datepicker.parseDate( dateFormat, element.value );
                } catch( error ) {
                    date = null;
                }

                return date;
            }
            $('#date-installed').datepicker({
                dateFormat: 'yy-mm-dd',
                maxDate: 0
            });
            $('.billing_image')
            .wrap('<span style="display:inline-block"></span>')
            .css('display', 'block')
            .parent()
            .zoom({
                magnify: 2.5,
            });
        } );
    </script>
</body>
</html>
