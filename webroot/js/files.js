const filesArray = [];

$(document).ready(function () {
    $("#accordion").accordion();
    if (window.location.pathname.includes('/sales/bill/edit')) {
        setFilesArrayDefaultValue($('#files').val());
    }
});

function imagePreview(input, index) {

    if (input.files && input.files.length > 0) {
        console.log(input.files[0]);
        for (let i = 0; i < input.files.length; i++) {
            let fileType = input.files[i].type;
            if (fileType.indexOf('image') > -1) {
                fileType = 'image';
            }
            if (fileType.indexOf('pdf') > -1) {
                fileType = 'pdf';
            }
            uploadFile(input.files[i], fileType, `${filesArray.length}-${i}`, index);
            const reader = new FileReader();
            reader.onload = function (event) {
                let elem = 'preview-images';
                if (index) {
                    elem += '-' + index;
                }
                $('#' + elem).append(buildImageHtml(i, event.target.result, ''));
            };
            reader.readAsDataURL(input.files[i]);
        }
        $(input).val('');
    }
}

function imagePreviewNew(input, index) {
    if (input.files && input.files.length > 0) {
        console.log(input.files[0]);
        var file = input.files[0];
        //for (let i = 0; i < input.files.length; i++) {
        let fileType = input.files[0].type;
        if (fileType.indexOf('image') > -1) {
            fileType = 'image';
        }
        if (fileType.indexOf('pdf') > -1) {
            fileType = 'pdf';
        }
        //uploadFile(input.files[0], fileType, `${filesArray.length}-${0}`, index);
        const reader = new FileReader();
        reader.onload = function (event) {
            let elem = 'preview-images';
            if (index) {
                elem += '-' + index;
            }
            $('#' + elem).html(buildImageHtml(0, event.target.result, ''));
        };
        reader.readAsDataURL(input.files[0]);
        //}
        //$(input).val('');
    }
}

function buildImageHtml(i, src, filePath) {
    let html = `<div class="col-2" id="image-area-${filesArray.length}-${i}" style="display: inline-block">`;
    html += `<img src="${src}" data-id="uploaded-img-${filesArray.length}-${i}" style="width: 200px; margin-right: 10px;" />`;
    html += `<br />`;
    html += `<a data-id="uploaded-btn-${filesArray.length}-${i}" onclick="deleteFile('${filePath}', 'image', '${filesArray.length}-${i}')">Delete</a>`;
    html += '</div>';
    return html;
}

function uploadFile(file, type, fileIndex, index) {
    const formData = new FormData();
    formData.append('file', file);
    $.ajax({
        url: '/file/add-file',
        headers: {
            'X-CSRF-Token': csrfCustomerToken
        },
        type: 'post',
        crossDomain: true,
        processData: false,
        contentType: false,
        data: formData,
        success: function (data) {
            data = JSON.parse(data);
            const elem = $(`img[data-id=uploaded-img-${fileIndex}]`);
            const btn = $(`a[data-id=uploaded-btn-${fileIndex}]`);
            if (data.status === 'success') {
                if (type === 'image') {
                    elem[0].src = `${data.filePath}`;
                }
                if (type === 'pdf') {
                    elem[0].src = '/img/pdf-icon.png';
                    elem.attr('style', 'width: 50px;');
                    elem.attr('onclick', `openPdf('${data.filePath}')`);
                }
                btn.attr('onclick', `deleteFile('${data.filePath}', '${type}', '${fileIndex}', '${index}')`);
            } else {
                elem.remove();
            }
            if (!filesArray[index]) {
                filesArray[index] = [];
            }
            filesArray[index].push(`${data.filePath}`);
            $('#files-' + index).val(filesArray[index].toString());
        },
        error: function (data) {
            console.log(data);
        }
    });
}

function openPdf(filePath) {
    window.open(filePath, '_blank');
}

function deleteFile(filePath, type, fileIndex, index) {
    $.ajax({
        url: '/file/delete-file',
        headers: {
            'X-CSRF-Token': csrfCustomerToken
        },
        type: 'post',
        crossDomain: true,
        data: {
            filePath
        },
        success: function (data) {
            $(`#image-area-${fileIndex}`).remove();
            const arrayIndex = filesArray[index].indexOf(`${filePath}`);
            if (arrayIndex > -1) {
                filesArray[index].splice(arrayIndex, 1);
                $(`#files-${index}`).val(filesArray[index].toString());
            }
        },
        error: function (data) {
            console.log(data);
        }
    });
}

function setFilesArrayDefaultValue(value) {
    value = value.split(',');
    for (let i = 0; i < value.length; i++) {
        filesArray.push(value[i]);
        $('#preview-images').append(buildImageHtml(i, value[i], value[i]));
    }
}
