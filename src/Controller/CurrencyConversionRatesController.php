<?php
namespace App\Controller;
use Cake\ORM\TableRegistry;
use App\Controller\AppController;

/**
 * CurrencyConversionRates Controller
 *
 * @property \App\Model\Table\CurrencyConversionRatesTable $CurrencyConversionRates
 *
 * @method \App\Model\Entity\CurrencyConversionRate[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class CurrencyConversionRatesController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $currencyConversionRates = $this->paginate($this->CurrencyConversionRates);

        $this->set(compact('currencyConversionRates'));
    }

    /**
     * View method
     *
     * @param string|null $id Currency Conversion Rate id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $currencyConversionRate = $this->CurrencyConversionRates->get($id, [
            'contain' => []
        ]);

        $this->set('currencyConversionRate', $currencyConversionRate);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function addnew()
    {
        $this->viewBuilder()->setLayout('dashboard');
        //$currencyConversionRate = $this->CurrencyConversionRates->newEntity();
        if ($this->request->is('post')) {
            $data = $this->request->getData();
            $data['ckey'] = str_replace(" ","_",$data['ckey']);
            
            $currencyConversionRate = $this->CurrencyConversionRates->newEntity($data);
            //debug($currencyConversionRate);exit;
            if ($this->CurrencyConversionRates->save($currencyConversionRate)) {
                $this->Flash->success(__('The currency conversion rate has been saved.'));

                return $this->redirect(['action' => 'edit']);
            }
            $this->Flash->error(__('The currency conversion rate could not be saved. Please, try again.'));
        }
        $this->set(compact('currencyConversionRate'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Currency Conversion Rate id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $this->viewBuilder()->setLayout('dashboard');
        
        $currencyConversionRate =  $this->CurrencyConversionRates->find('all')->all();
        
        $oldprice = [];
        foreach($currencyConversionRate as $key => $rate){
            $oldprice[$rate->ckey] = $rate->cvalue;
        }

		//echo $usd_mannington;
		//exit;
        if ($this->request->is(['patch', 'post', 'put'])) {
            $data = $this->request->getData();
            
            unset($data['clicked']);
           
            //$currencyConversionRate = $this->CurrencyConversionRates->patchEntity($currencyConversionRate,$data);
			$arraydata = [];
			//for($i=0; $i<count($data); $i++){
				$i = 0;
				foreach($data as $key=>$value){
					if($value != $oldprice[$key]){
						$arraydata[$key] = $value;
					}
					$i++;
				}
                
				$i = 1;
				foreach($arraydata as $mkey=>$mvalue){
                    
					$response = TableRegistry::getTableLocator()->get('families')->find()->where(['and' =>['usd_cad' => $mkey]]);
				    
                    foreach($response as $res){
                        
						$new_cost = $res->mill_cost;
						$new_mill_cost = $new_cost / $mvalue; 
						
						$brokerage = $res->brokerage_multiplier * $new_mill_cost; 
						$ad_fund = $new_mill_cost * $res->ad_fund_percentage;
						$landed_cost_per_unit = $new_mill_cost + $brokerage + $res->freight + $ad_fund;
						$load = $landed_cost_per_unit * $res->load_percentage;
						$margin_lic = $landed_cost_per_unit + $load;
						
                        if($res->sell_price_level1){
						    $gross_margin_level1 = ((($res->sell_price_level1 - $margin_lic)+$res->rebate_level1)/$res->sell_price_level1) * 100;
                        }
						if($res->sell_price_level2){
                            $gross_margin_level2 = ((($res->sell_price_level2 - $margin_lic)+$res->rebate_level2)/$res->sell_price_level2) * 100;
                        }
                        if($res->sell_price_level3){
                            $gross_margin_level3 = ((($res->sell_price_level3 - $margin_lic)+$res->rebate_level3)/$res->sell_price_level3) * 100;
                        }
                        //exit;
						
						$Families = TableRegistry::getTableLocator()->get('families');
						$family = $Families->get($res->id); 

											
						$family->new_mill_cost = $new_mill_cost;
						$family->conversion_rate = $mvalue;
						$family->brokerage = $brokerage;
						$family->ad_fund = $ad_fund;
						$family->landed_cost_per_unit = $landed_cost_per_unit;
						$family->_load = $load;
						$family->margin_lic = $margin_lic;
						$family->gross_margin_level1 = $gross_margin_level1;
						$family->gross_margin_level2 = $gross_margin_level2;
						$family->gross_margin_level3 = $gross_margin_level3;
						$Families->save($family);


					$i++;
						
					}
				
				}
				//print_r($arraydata);
			//exit;
            //$this->CurrencyConversionRates->save($currencyConversionRate)
          
                
            foreach($data as $key=>$value){
                $conversion = $this->CurrencyConversionRates->find('all')->where(['ckey'=> $key])->first();
                $conversion->cvalue = $value;
                $this->CurrencyConversionRates->patchEntity($conversion, array('cvalue'=>$value));
                $this->CurrencyConversionRates->save($conversion);
            }

            $this->Flash->success(__('The currency conversion rate has been saved.'));

               return $this->redirect(['controller' => 'Families', 'action' => 'index']);
            //$this->Flash->error(__('The currency conversion rate could not be saved. Please, try again.'));
        }
        $this->set(compact('currencyConversionRate'));
    }

    //getConverionRate

    public function getConverionRate()
    {
        $this->autoRender = false;
        /*$currencyConversionRate = $this->CurrencyConversionRates->get(1, [
            'contain' => []
        ]);*/
        $rates = [];
        $currencyConversionRate =  $this->CurrencyConversionRates->find('all')->all();
        foreach($currencyConversionRate as $key => $rate){
            $rates[$rate->ckey] = $rate->cvalue;
        }


        $content = json_encode($rates);
        $this->response = $this->response->withStringBody($content);
        $this->response = $this->response->withType('json');
        return $this->response;

        //$this->set('currencyConversionRate', $currencyConversionRate);
    }


    /**
     * Delete method
     *
     * @param string|null $id Currency Conversion Rate id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $currencyConversionRate = $this->CurrencyConversionRates->get($id);
        if ($this->CurrencyConversionRates->delete($currencyConversionRate)) {
            $this->Flash->success(__('The currency conversion rate has been deleted.'));
        } else {
            $this->Flash->error(__('The currency conversion rate could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
