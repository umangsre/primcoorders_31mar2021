<?php


namespace App\Controller;


use Cake\I18n\Time;

class ClaimsGenericMethods extends AppController
{
    public function initialize()
    {
        parent::initialize();
        $this->loadModel('Claims');
        $this->loadModel('ClaimStatus');
        $this->loadModel('PrimcoCategoryLabourCost');
        $this->loadModel('PrimcoGeneralLabourCost');
    }

    public function getAllClaims($search = false, $searchData = '') {
        $query = $this->Claims->find('all', ['contain' => ['Customers', 'Consumers', 'Millers', 'ClaimStatus', 'Users'], 'order' => ['Claims.id' => 'DESC']]);
        if ($search) {
            if (!empty($searchData['start'])) {
                $startDate = new Time($searchData['start']);
                $query = $query->where(['Claims.created_date >=' => $startDate]);
            }
            if (!empty($searchData['end'])) {
                $endDate = new Time($searchData['end']);
                $query = $query->where(['Claims.created_date <=' => $endDate]);
            }
            if (!empty($searchData['name'])) {
                $name = $searchData['name'];
                $query = $query->where(['Customers.name LIKE' => '%' . $name . '%']);
            }
        }
        if ($this->Auth->user('role') == 'dealer') {
            $query = $query->where(['Claims.created_by', $this->Auth->user('id')]);
        }
        if ($this->Auth->user('role') == 'miller') {
            $query = $query->where(['Claims.miller_user_id', $this->Auth->user('id')]);
        }
        return $query;
    }

    public function getClaim($claimId) {
        $claim = $this->Claims->get($claimId, ['contain' => ['Customers', 'Consumers', 'Millers', 'ClaimStatus', 'Users']]);
        $claimStatuses = json_decode(json_encode($claim->claim_status), true);
        $claimStatuses = array_reverse($claimStatuses);
        $claim->claim_status = $claimStatuses;
        for ($i = 0; $i < count($claim->claim_status); $i++) {
            $claimStatus = $claim->claim_status[$i];
            $this->loadModel('Users');
            $userInfo = $this->Users->get($claimStatus['status_by']);
            $claimStatus['username'] = $userInfo['username'];
            $claimStatus['role'] = $userInfo['role'];
            $claim->claim_status[$i] = $claimStatus;
        }
        return $claim;
    }

    public function getAllCategoryLabourCost() {
        return $this->PrimcoCategoryLabourCost->find('all');
    }

    public function getAllGeneralLabourCost() {
        return $this->PrimcoGeneralLabourCost->find('all');
    }

    public function isUserAllowedToViewClaim($claim, $currentUser) {
        return $currentUser['role'] === 'admin' ||
            $claim->created_by == $currentUser['id'] ||
            $claim->miller_user_id == $currentUser['id'] ||
            $claim->miller_id == $currentUser['id'];
    }

    public function isUserAllowedToUpdateClaim($claim, $currentUser) {
        return $currentUser['role'] === 'admin' ||
            $claim->created_by == $currentUser['id'] ||
            $claim->miller_user_id == $currentUser['id'] ||
            $claim->miller_id == $currentUser['id'];
    }

    public function updateClaim($claim, $details) {
        $updateClaim = $this->Claims->patchEntity($claim, $details);
        if ($this->Claims->save($updateClaim)) {
            return $updateClaim;
        }
    }

    public function addClaimStatus($statusDetails) {
        $claimStatus = $this->ClaimStatus->newEntity();
        $claimStatus = $this->ClaimStatus->patchEntity($claimStatus, $statusDetails);
        if ($this->ClaimStatus->save($claimStatus)) {
            return $claimStatus;
        }
    }

    public function getAllClaimStatuses($claimId) {
        return $this->ClaimStatus->find('all')->where(['ClaimStatus.claim_id' => $claimId]);
    }

    public function checkStatusCount($claimId, $claimStatus) {
        return $this->getAllClaimStatuses($claimId)
            ->where(['ClaimStatus.status' => $claimStatus])
            ->count();
    }
}
