<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Order $order
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List Orders'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Customers'), ['controller' => 'Customers', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Customer'), ['controller' => 'Customers', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Salesmen'), ['controller' => 'Salesmen', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Salesman'), ['controller' => 'Salesmen', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Order Items'), ['controller' => 'OrderItems', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Order Item'), ['controller' => 'OrderItems', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="orders form large-9 medium-8 columns content">
    <?= $this->Form->create($order) ?>
    <fieldset>
        <legend><?= __('Add Order') ?></legend>
        <?php
            echo $this->Form->control('customer_id', ['options' => $customers, 'empty' => true]);
            echo $this->Form->control('salesman_id', ['options' => $salesmen, 'empty' => true]);
            echo $this->Form->control('grand_total');
            echo $this->Form->control('po_number');
            echo $this->Form->control('ship_to');
            echo $this->Form->control('ship_via');
            echo $this->Form->control('additional_comments');
            echo $this->Form->control('events');
            echo $this->Form->control('sale_order_number');
            echo $this->Form->control('incentive_reciept');
            echo $this->Form->control('sale_date', ['empty' => true]);
            echo $this->Form->control('ship_date', ['empty' => true]);
            echo $this->Form->control('purchaser_name');
            echo $this->Form->control('status');
            echo $this->Form->control('purchaser_sign');
            echo $this->Form->control('signature');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
