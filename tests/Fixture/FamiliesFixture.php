<?php
namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * FamiliesFixture
 */
class FamiliesFixture extends TestFixture
{
    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'id' => ['type' => 'biginteger', 'length' => 20, 'unsigned' => true, 'null' => false, 'default' => null, 'comment' => '', 'autoIncrement' => true, 'precision' => null],
        'family' => ['type' => 'string', 'length' => 250, 'null' => false, 'default' => null, 'collate' => 'latin1_swedish_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'name' => ['type' => 'string', 'length' => 250, 'null' => false, 'default' => null, 'collate' => 'latin1_swedish_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'product_code' => ['type' => 'string', 'length' => 150, 'null' => true, 'default' => null, 'collate' => 'latin1_swedish_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'role_cut' => ['type' => 'string', 'length' => 100, 'null' => true, 'default' => null, 'collate' => 'latin1_swedish_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'mill_cost' => ['type' => 'float', 'length' => 20, 'precision' => 4, 'unsigned' => true, 'null' => true, 'default' => null, 'comment' => ''],
        'usd_cad' => ['type' => 'string', 'length' => 100, 'null' => false, 'default' => null, 'collate' => 'latin1_swedish_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'conversion_rate' => ['type' => 'float', 'length' => 20, 'precision' => 4, 'unsigned' => false, 'null' => true, 'default' => '1.0000', 'comment' => ''],
        'selling_price' => ['type' => 'float', 'length' => 20, 'precision' => 4, 'unsigned' => true, 'null' => true, 'default' => null, 'comment' => ''],
        'brokerage_multiplier' => ['type' => 'float', 'length' => 20, 'precision' => 4, 'unsigned' => false, 'null' => true, 'default' => '0.0000', 'comment' => ''],
        'brokerage' => ['type' => 'float', 'length' => 20, 'precision' => 4, 'unsigned' => true, 'null' => false, 'default' => '0.0000', 'comment' => ''],
        'units' => ['type' => 'string', 'length' => 100, 'null' => false, 'default' => null, 'collate' => 'latin1_swedish_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'freight' => ['type' => 'float', 'length' => 20, 'precision' => 4, 'unsigned' => true, 'null' => false, 'default' => null, 'comment' => ''],
        'ad_fund_percentage' => ['type' => 'float', 'length' => 20, 'precision' => 4, 'unsigned' => true, 'null' => true, 'default' => '0.0000', 'comment' => ''],
        'ad_fund' => ['type' => 'float', 'length' => 20, 'precision' => 4, 'unsigned' => true, 'null' => true, 'default' => '0.0000', 'comment' => ''],
        'landed_cost_per_unit' => ['type' => 'float', 'length' => 20, 'precision' => 4, 'unsigned' => true, 'null' => false, 'default' => null, 'comment' => ''],
        'load_percentage' => ['type' => 'float', 'length' => 20, 'precision' => 4, 'unsigned' => true, 'null' => false, 'default' => '1.0000', 'comment' => ''],
        '_load' => ['type' => 'float', 'length' => 20, 'precision' => 4, 'unsigned' => true, 'null' => true, 'default' => null, 'comment' => ''],
        'margin_lic' => ['type' => 'float', 'length' => 20, 'precision' => 4, 'unsigned' => true, 'null' => false, 'default' => null, 'comment' => ''],
        'sell_price_level1' => ['type' => 'float', 'length' => 20, 'precision' => 4, 'unsigned' => true, 'null' => true, 'default' => null, 'comment' => ''],
        'rebate_level1' => ['type' => 'float', 'length' => 20, 'precision' => 4, 'unsigned' => true, 'null' => true, 'default' => '0.0000', 'comment' => ''],
        'gross_margin_level1' => ['type' => 'float', 'length' => 20, 'precision' => 4, 'unsigned' => true, 'null' => true, 'default' => null, 'comment' => ''],
        'sell_price_level2' => ['type' => 'float', 'length' => 20, 'precision' => 4, 'unsigned' => true, 'null' => true, 'default' => null, 'comment' => ''],
        'rebate_level2' => ['type' => 'float', 'length' => 20, 'precision' => 4, 'unsigned' => true, 'null' => true, 'default' => '0.0000', 'comment' => ''],
        'gross_margin_level2' => ['type' => 'float', 'length' => 20, 'precision' => 4, 'unsigned' => true, 'null' => true, 'default' => null, 'comment' => ''],
        'sell_price_level3' => ['type' => 'float', 'length' => 20, 'precision' => 4, 'unsigned' => true, 'null' => true, 'default' => null, 'comment' => ''],
        'rebate_level3' => ['type' => 'float', 'length' => 20, 'precision' => 4, 'unsigned' => true, 'null' => true, 'default' => '0.0000', 'comment' => ''],
        'gross_margin_level3' => ['type' => 'float', 'length' => 20, 'precision' => 4, 'unsigned' => true, 'null' => true, 'default' => null, 'comment' => ''],
        'mill' => ['type' => 'string', 'length' => 255, 'null' => false, 'default' => null, 'collate' => 'latin1_swedish_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'vendorID' => ['type' => 'string', 'length' => 50, 'null' => true, 'default' => null, 'collate' => 'latin1_swedish_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'sqft_ctn' => ['type' => 'float', 'length' => 20, 'precision' => 4, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => ''],
        'wgt_lbs_ctn' => ['type' => 'float', 'length' => 20, 'precision' => 4, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => ''],
        'ctn_pallet' => ['type' => 'float', 'length' => 20, 'precision' => 4, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => ''],
        'sqft_pallet' => ['type' => 'float', 'length' => 20, 'precision' => 4, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => ''],
        'pallet_container_20' => ['type' => 'float', 'length' => 20, 'precision' => 4, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => ''],
        'sqft_container_20' => ['type' => 'float', 'length' => 20, 'precision' => 4, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => ''],
        'sqft_roll' => ['type' => 'float', 'length' => 20, 'precision' => 4, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => ''],
        'sqyd_roll' => ['type' => 'float', 'length' => 20, 'precision' => 4, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => ''],
        'created' => ['type' => 'datetime', 'length' => null, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['id'], 'length' => []],
        ],
        '_options' => [
            'engine' => 'InnoDB',
            'collation' => 'latin1_swedish_ci'
        ],
    ];
    // @codingStandardsIgnoreEnd
    /**
     * Init method
     *
     * @return void
     */
    public function init()
    {
        $this->records = [
            [
                'id' => 1,
                'family' => 'Lorem ipsum dolor sit amet',
                'name' => 'Lorem ipsum dolor sit amet',
                'product_code' => 'Lorem ipsum dolor sit amet',
                'role_cut' => 'Lorem ipsum dolor sit amet',
                'mill_cost' => 1,
                'usd_cad' => 'Lorem ipsum dolor sit amet',
                'conversion_rate' => 1,
                'selling_price' => 1,
                'brokerage_multiplier' => 1,
                'brokerage' => 1,
                'units' => 'Lorem ipsum dolor sit amet',
                'freight' => 1,
                'ad_fund_percentage' => 1,
                'ad_fund' => 1,
                'landed_cost_per_unit' => 1,
                'load_percentage' => 1,
                '_load' => 1,
                'margin_lic' => 1,
                'sell_price_level1' => 1,
                'rebate_level1' => 1,
                'gross_margin_level1' => 1,
                'sell_price_level2' => 1,
                'rebate_level2' => 1,
                'gross_margin_level2' => 1,
                'sell_price_level3' => 1,
                'rebate_level3' => 1,
                'gross_margin_level3' => 1,
                'mill' => 'Lorem ipsum dolor sit amet',
                'vendorID' => 'Lorem ipsum dolor sit amet',
                'sqft_ctn' => 1,
                'wgt_lbs_ctn' => 1,
                'ctn_pallet' => 1,
                'sqft_pallet' => 1,
                'pallet_container_20' => 1,
                'sqft_container_20' => 1,
                'sqft_roll' => 1,
                'sqyd_roll' => 1,
                'created' => '2020-02-25 07:01:02'
            ],
        ];
        parent::init();
    }
}
