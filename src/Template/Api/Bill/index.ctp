<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Bill[]|\Cake\Collection\CollectionInterface $bill
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Bill'), ['action' => 'add']) ?></li>
    </ul>
</nav>
<div class="bill index large-9 medium-8 columns content">
    <h3><?= __('Bill') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('billing_date') ?></th>
                <th scope="col"><?= $this->Paginator->sort('billing_type_of_expense') ?></th>
                <th scope="col"><?= $this->Paginator->sort('billing_place_of_expense') ?></th>
                <th scope="col"><?= $this->Paginator->sort('billing_amount') ?></th>
                <th scope="col"><?= $this->Paginator->sort('billing_tax') ?></th>
                <th scope="col"><?= $this->Paginator->sort('billing_total_amount') ?></th>
                <th scope="col"><?= $this->Paginator->sort('billing_status') ?></th>
                <th scope="col"><?= $this->Paginator->sort('billing_userid') ?></th>
                <th scope="col"><?= $this->Paginator->sort('created') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($bill as $bill): ?>
            <tr>
                <td><?= $this->Number->format($bill->id) ?></td>
                <td><?= h($bill->billing_date) ?></td>
                <td><?= h($bill->billing_type_of_expense) ?></td>
                <td><?= h($bill->billing_place_of_expense) ?></td>
                <td><?= $this->Number->format($bill->billing_amount) ?></td>
                <td><?= h($bill->billing_tax) ?></td>
                <td><?= $this->Number->format($bill->billing_total_amount) ?></td>
                <td><?= h($bill->billing_status) ?></td>
                <td><?= $this->Number->format($bill->billing_userid) ?></td>
                <td><?= h($bill->created) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $bill->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $bill->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $bill->id], ['confirm' => __('Are you sure you want to delete # {0}?', $bill->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div>
