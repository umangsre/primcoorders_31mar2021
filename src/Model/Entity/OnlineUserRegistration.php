<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * OnlineUserRegistration Entity
 *
 * @property int $id
 * @property int|null $credit_application_id
 * @property string|null $user_type
 * @property string|null $submission_date
 * @property string|null $customer_number
 * @property string|null $company_name
 * @property string|null $administartor_will_see
 * @property string|null $standard_name
 * @property string|null $standard_email
 * @property string|null $standard_name1
 * @property string|null $standard_email1
 * @property string|null $standard_name2
 * @property string|null $standard_email2
 * @property string|null $standard_name3
 * @property string|null $standard_email3
 * @property string $first__last_name
 * @property string|null $user_email
 * @property int|null $can_see_account_details
 * @property int|null $can_see_placed_order
 * @property int|null $can_see_invoiced_order
 * @property int|null $can_see_credit_memos
 * @property int|null $can_see_prices
 * @property int|null $can_see_price_list
 * @property string|null $print_name
 * @property string|null $authorized_signature
 * @property string|null $_date
 * @property string|null $_title
 * @property int|null $anti_spam_consent
 *
 * @property \App\Model\Entity\CreditApplication $credit_application
 */
class OnlineUserRegistration extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'credit_application_id' => true,
        'user_type' => true,
        'submission_date' => true,
        'customer_number' => true,
        'company_name' => true,
        'administartor_will_see' => true,
        'standard_name' => true,
        'standard_email' => true,
        'standard_name1' => true,
        'standard_email1' => true,
        'standard_name2' => true,
        'standard_email2' => true,
        'standard_name3' => true,
        'standard_email3' => true,
        'first__last_name' => true,
        'user_email' => true,
        'can_see_account_details' => true,
        'can_see_placed_order' => true,
        'can_see_invoiced_order' => true,
        'can_see_credit_memos' => true,
        'can_see_prices' => true,
        'can_see_price_list' => true,
        'print_name' => true,
        'authorized_signature' => true,
        '_date' => true,
        '_title' => true,
        'anti_spam_consent' => true,
        'credit_application' => true
    ];
}
